package com.smargav.northernsky.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.RelativeLayout;

import com.smargav.northernsky.R;

public class CustomRelativeLayout extends RelativeLayout implements Checkable {
    private boolean checked = true;

    private CheckBox checkBox;
    private static final int[] CHECKED_STATE_SET = {android.R.attr.state_checked};

    public CustomRelativeLayout(Context context) {
        super(context);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
    }

    public CustomRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
    }

    public CustomRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked())
            mergeDrawableStates(drawableState, CHECKED_STATE_SET);
        return drawableState;
    }

    @Override
    public boolean isChecked() {
        return checked;
    }

    @Override
    public void setChecked(boolean _checked) {
        checked = _checked;
        if (findViewById(R.id.checkbox) != null) {
            ((CheckBox) findViewById(R.id.checkbox)).setChecked(checked);
        }
        refreshDrawableState();

    }

    @Override
    public void toggle() {
        setChecked(!checked);
    }

}