package com.smargav.northernsky.gcm;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.smargav.api.logger.AppLogger;
import com.smargav.northernsky.services.GCMService;

public class GCMReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Explicitly specify that GcmIntentService will handle the intent.
        //GCMService.class.getName()
        String className = GCMService.class.getName();
        if(className == null){
            AppLogger.e(getClass(), "GCM Service class name is NULL." + context.getPackageName());
            return;
        }
        ComponentName comp = new ComponentName(context.getPackageName(),
                className);
        Log.i("Received", "Action Received");
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }


}
