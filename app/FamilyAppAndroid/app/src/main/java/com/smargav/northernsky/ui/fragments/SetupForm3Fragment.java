package com.smargav.northernsky.ui.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.AdBanner;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.ui.activities.GlobalData;
import com.smargav.northernsky.ui.activities.SetupActivity;
import com.smargav.northernsky.ui.adapter.BannerFragmentAdapter;
import com.smargav.northernsky.utils.Utils;
import com.smargav.northernsky.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 26/04/15.
 */
public class SetupForm3Fragment extends Fragment {

    private static final long SCROLL_DELAY = 2500;
    private ViewPager mPager;
    private BannerFragmentAdapter adapter;
    private List<AdBanner> bannersList = new ArrayList<AdBanner>();

    private Handler handler = new Handler();
    protected boolean scrollReverse;

    int scrollCount = 0;
    private Runnable bannerScroller = new Runnable() {

        @Override
        public void run() {

            scrollOnce();
            handler.postDelayed(this, SCROLL_DELAY);
        }
    };

    ViewGroup rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_setup_3, container, false);

        setupForm(rootView);
        return rootView;
    }

    private void setupForm(ViewGroup rootView) {

        mPager = (ViewPager) rootView.findViewById(R.id.pager);

        bannersList = getStaticBanners();
        adapter = new BannerFragmentAdapter(bannersList,
                getFragmentManager());
        mPager.setAdapter(adapter);


        rootView.findViewById(R.id.setup_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SetupActivity) getActivity()).setupComplete();
            }
        });


        new DownloadContactsTask().execute();
    }


    private List<AdBanner> getStaticBanners() {
        List<AdBanner> list = new ArrayList<>();

        AdBanner banner = new AdBanner(
                "Search your Neighbours"
                , "Click on 'Search' icon to find your block mates");
        list.add(banner);

        banner = new AdBanner(
                "Online Service Booking"
                , "Click on Services to book applicable services online");
        list.add(banner);
        banner = new AdBanner(
                "Vendor Listing"
                , "Click on Vendor Listing to view near by stores");
        list.add(banner);
        banner = new AdBanner(
                "Events"
                , "Click on Events to view upcoming Events with in the community");
        list.add(banner);

        return list;
    }


    @Override
    public void onResume() {
        super.onResume();
        handler.postDelayed(bannerScroller, SCROLL_DELAY);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(bannerScroller);
    }

    //int currentPage;
    public void scrollOnce() {

        int currentPage = mPager.getCurrentItem();

        if (currentPage == bannersList.size() - 1) {
            currentPage = 0;
            mPager.setCurrentItem(currentPage, false);
        } else {
            ++currentPage;
            mPager.setCurrentItem(currentPage, true);
        }

    }


    private class DownloadContactsTask extends AsyncTask<String, String, Integer> {

        private String message = "Unknown error occured. Please try again";

        public DownloadContactsTask() {
        }

        @Override
        protected Integer doInBackground(String... strings) {
            Context ctx = getActivity();
            try {
                Person user = PreferencesUtil.get(getActivity(), GlobalData.USER_INFO, Person.class);

                long pageNo = PreferencesUtil.getLong(ctx, GlobalData.PAGE_NO, 1);

                List<Person> persons = new ArrayList<>();
                publishProgress("Updating Contacts. Please bear with us.");
                int count = 50;
                while (pageNo < 100) {
                    persons = MasterAPIs.downloadAllContacts(user.getId(), (int) pageNo, count);
                    PersonDao.saveData(ctx, persons);
                    //message = "Could not save member details.  Please clear Data and try again";
                    //return 0;
                    pageNo++;
                    PreferencesUtil.putLong(ctx, GlobalData.PAGE_NO, pageNo);
                    if (persons == null || persons.size() != count) {
                        break;
                    }
                }


                PreferencesUtil.remove(ctx, GlobalData.PAGE_NO);
                GlobalData.banners = MasterAPIs.getBanners();

                try {
                    MasterDBManager.getInstance().getCategoryDao().saveAll(MasterAPIs.getCategories());
                } catch (Exception e) {
                }
                return 1;
            } catch (Exception e) {
                e.printStackTrace();
            }
            PreferencesUtil.remove(ctx, GlobalData.PAGE_NO);
            return 0;
        }

        public void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == 0) {
                Utils.showPrompt(getActivity(), "Error", message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == AlertDialog.BUTTON_POSITIVE) {
                            new DownloadContactsTask().execute();
                        } else {
                            getActivity().finish();
                        }
                    }
                }, new int[]{R.string.alert_ok, R.string.alert_cancel});
                return;
            }

            rootView.findViewById(R.id.setup_next).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.download_bar).setVisibility(View.GONE);

            ((TextView) rootView.findViewById(R.id.setup_text)).setText("Setup Complete.");
            PreferencesUtil.putString(getActivity(), GlobalData.APP_INITIALIZED, "true");
        }
    }


}
