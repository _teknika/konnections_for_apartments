package com.smargav.northernsky.model;

public class AppUpdate {

    private String packageName;
    private String versionNumber;
    private String date;
    private int versionCode;
    private int frequency = 30;
    private boolean isCritical;
    private boolean isUpdatePending;

    public boolean isUpdatePending() {
        return isUpdatePending;
    }

    public void setUpdatePending(boolean isUpdatePending) {
        this.isUpdatePending = isUpdatePending;
    }

    public boolean isCritical() {
        return isCritical;
    }

    public void setCritical(boolean isCritical) {
        this.isCritical = isCritical;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
