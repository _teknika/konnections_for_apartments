package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.SearchView;
import android.widget.TextView;

import com.smargav.api.logger.AppLogger;
import com.smargav.northernsky.R;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.ui.activities.BulkMessagingActivity;
import com.smargav.northernsky.ui.activities.GlobalData;
import com.smargav.northernsky.ui.activities.PersonDetailsActivity;
import com.smargav.northernsky.ui.adapter.PersonDisplayAdapter;

import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 26/04/15.
 */
public class PersonsListFragment extends Fragment implements ExpandableListView.OnChildClickListener {

    private ContentResolver resolver;
    private PersonDao personDao;
    private View stickyHeader;

    private ExpandableListView list;
    private PersonDisplayAdapter adapter;
    private SearchView search;
    private List<Person> persons;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_person_list, container, false);
        resolver = getActivity().getContentResolver();
        personDao = MasterDBManager.getInstance().getPersonDao();
        loadData(rootView);
        setupSearchBar(rootView);
        setHasOptionsMenu(true);
        return rootView;
    }

    private void setupSearchBar(ViewGroup rootView) {
        search = (SearchView) rootView.findViewById(R.id.search_bar);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        search.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        search.setIconifiedByDefault(false);
        search.setQueryHint("Search Head of Family");
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                adapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                AppLogger.i(getClass(), "Search: " + s);
                adapter.getFilter().filter(s);
                return false;
            }
        });
        search.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.getFilter().filter("");
                return false;
            }
        });


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.frag_list_grid_menu, menu);
        inflater.inflate(R.menu.bulk_sms_item, menu);

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_frag_search) {
            toggleSearchView();
            return true;
        }
        if (item.getItemId() == R.id.menu_bulk_sms) {
            GlobalData.searchPersonList = persons;
            getActivity().startActivity(new Intent(getActivity(), BulkMessagingActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void toggleSearchView() {
        if (search.isShown()) {
            search.setVisibility(View.GONE);
        } else {
            search.setVisibility(View.VISIBLE);
            search.requestFocus();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(search, InputMethodManager.SHOW_FORCED);

        }
    }

    private void loadData(ViewGroup rootView) {
        stickyHeader = rootView.findViewById(R.id.sticky_header);
        list = (ExpandableListView) rootView.findViewById(R.id.list);
        //list.setFastScrollEnabled(true);

//        PersonDao dao = MasterDBManager.getInstance().getPersonDao();
//        List<Person> persons = GlobalData.fullPersonsList;
//        if (persons == null) {
//            persons = dao.getAllHeadsList();
//            GlobalData.fullPersonsList = persons;
//        }

        persons = GlobalData.searchPersonList;

        adapter = new PersonDisplayAdapter(getActivity(), persons) {
            public Filter getFilter() {
                return new AllPersonFilter(GlobalData.searchPersonList);
            }
        };

        list.setAdapter(adapter);
        list.setOnChildClickListener(this);
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    stickyHeader.setVisibility(View.GONE);
                    return;
                }

                View view = list.getChildAt(0);
                if (view != null && view.getId() == R.id.group_header_layout) {
                    //This is header.
                    changeStickyHeaderText((String) view.getTag());
                    ((TextView) stickyHeader.findViewById(R.id.item_group_moreinfo)).setText(((TextView) view.findViewById(R.id.item_group_moreinfo)).getText());
                } else if (view != null && view.getId() == R.id.item_person_layout) {
                    String text = ((TextView) view.findViewById(R.id.person_name)).getText().toString();
                    changeStickyHeaderText(StringUtils.substring(text, 0, 1));
                }
            }
        });

        expandAll();
    }

    private void changeStickyHeaderText(String tag) {
        ((TextView) stickyHeader.findViewById(R.id.item_group_name)).setText(tag);
        ((TextView) stickyHeader.findViewById(R.id.item_group_moreinfo)).setText(persons.size() + " Contacts");
        stickyHeader.setVisibility(View.VISIBLE);
    }

    public void expandAll() {
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            list.expandGroup(i);
        }

        list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });
    }

    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l) {

        Person person = (Person) view.getTag();
        try {
            personDao.refreshPersonInfo(person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        GlobalData.selectedPerson = person;
        startActivity(new Intent(getActivity(), PersonDetailsActivity.class));
        return false;
    }


    private class AllPersonFilter extends Filter {

        private List<Person> masterPersons;

        private int totalCount = 0;

        public AllPersonFilter(List<Person> masterPersons) {
            this.masterPersons = masterPersons;
            this.totalCount = masterPersons.size();

        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            if (charSequence == null || charSequence.length() < 3) {
                results.values = masterPersons;
                results.count = masterPersons.size();
                return results;
            }

            //do filtering
            List<Person> list = new ArrayList<>();
            String searchText = charSequence.toString();
            for (Person p : masterPersons) {
                if (StringUtils.containsIgnoreCase(p.getFirstName(), searchText) || StringUtils.containsIgnoreCase(p.getLastName(), searchText) || StringUtils.contains(p.getMobileNumber(), searchText) || StringUtils.containsIgnoreCase(p.getBusinessCategory(), searchText) || StringUtils.containsIgnoreCase(p.getBusinessOccupation(), searchText)) {
                    list.add(p);
                }
            }

            if (list.isEmpty()) {
                results.values = masterPersons;
                results.count = masterPersons.size();
            } else {
                results.count = list.size();
                results.values = list;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            List<Person> persons = (List<Person>) filterResults.values;
            //if (!persons.isEmpty())
            adapter.notifyDataSetChanged(persons);
            adapter.notifyDataSetInvalidated();
            expandAll();

        }

    }
}
