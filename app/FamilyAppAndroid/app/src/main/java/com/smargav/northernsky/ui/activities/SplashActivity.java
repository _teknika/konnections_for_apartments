package com.smargav.northernsky.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.northernsky.R;


public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

//        clockwise();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadNextScreen();
            }
        }, 2500);
        // GCMRegister.registerToGCM(this);
    }

    public void clockwise(){
        ImageView image = (ImageView)findViewById(R.id.splash);
        TextView message = (TextView)findViewById(R.id.splash_message);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.clockwise);
        image.startAnimation(animation);
//        YoYo.with(Techniques.RotateInUpLeft).duration(2000).playOn(image);
//        YoYo.with(Techniques.Wave).duration(2000).playOn(message);
    }
    private void loadNextScreen() {


        if (!PreferencesUtil.contains(this, GlobalData.PHONE_NUMBER_KEY) || !PreferencesUtil.contains(this, GlobalData.APP_INITIALIZED)) {
            startActivity(new Intent(SplashActivity.this, SetupActivity.class));
        } else {
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        }

        finish();
    }


}
