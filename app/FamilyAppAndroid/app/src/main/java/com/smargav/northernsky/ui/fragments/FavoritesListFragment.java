package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;

import com.smargav.northernsky.R;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.ui.activities.BulkMessagingActivity;
import com.smargav.northernsky.ui.activities.GlobalData;
import com.smargav.northernsky.ui.activities.PersonDetailsActivity;
import com.smargav.northernsky.ui.adapter.PersonAutoCompleteAdapter;
import com.smargav.northernsky.ui.adapter.PersonDisplayAdapter;
import com.smargav.northernsky.utils.Utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 26/04/15.
 */
public class FavoritesListFragment extends Fragment implements ExpandableListView.OnChildClickListener {

    private ContentResolver resolver;
    private PersonDao personDao;

    private ExpandableListView list;
    private PersonDisplayAdapter adapter;
    private ViewGroup rootView;
    private List<Person> persons = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_myfavorites, container, false);
        resolver = getActivity().getContentResolver();
        personDao = MasterDBManager.getInstance().getPersonDao();
        loadData(rootView);
        setupAddNewBox();
        setHasOptionsMenu(true);
        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.bulk_sms_item, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_bulk_sms) {
            GlobalData.searchPersonList = persons;
            getActivity().startActivity(new Intent(getActivity(), BulkMessagingActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupAddNewBox() {
        final AutoCompleteTextView addPersonToGrpView = (AutoCompleteTextView) rootView.findViewById(R.id.favorite_add_person);
        PersonAutoCompleteAdapter adapter = new PersonAutoCompleteAdapter(GlobalData.fullPersonsList, getActivity());
        addPersonToGrpView.setAdapter(adapter);

        addPersonToGrpView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Person person = (Person) view.getTag();
                addPersonToFavorite(person);
                addPersonToGrpView.setText("");
            }
        });
    }

    private void addPersonToFavorite(Person person) {
        if (persons.contains(person)) {
            return;
        }
        person.setFavorite(true);
        Utils.toggleFavoriteStatus(person);
        persons.add(person);
        adapter.notifyDataSetChanged(persons);
        expandAll();
    }

    public void onResume() {
        super.onResume();
        try {
            PersonDao dao = MasterDBManager.getInstance().getPersonDao();
            List<Person> list = dao.getAllFavorites();
            persons.clear();
            persons.addAll(list);
            adapter.notifyDataSetChanged(persons);
            expandAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loadData(ViewGroup rootView) {
        list = (ExpandableListView) rootView.findViewById(R.id.list);
        //list.setFastScrollEnabled(true);

        PersonDao dao = MasterDBManager.getInstance().getPersonDao();
        persons = dao.getAllFavorites();

        adapter = new PersonDisplayAdapter(getActivity(), persons);
        list.setAdapter(adapter);

        list.setOnChildClickListener(this);


        expandAll();
    }

    public void expandAll() {
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            list.expandGroup(i);
        }

        list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });
    }

    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l) {

        Person person = (Person) view.getTag();
        try {
            personDao.refreshPersonInfo(person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        GlobalData.selectedPerson = person;
        startActivity(new Intent(getActivity(), PersonDetailsActivity.class));
        return false;
    }
}
