package com.smargav.northernsky.ui.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.smargav.northernsky.R;
import com.smargav.northernsky.db.GroupDao;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.model.Group;
import com.smargav.northernsky.utils.Utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 18/05/15.
 */
public class GroupsActivity extends BaseActivity {

    private List<Group> groups = new ArrayList<Group>();
    private GroupsAdapter adapter;
    private GroupDao groupDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        getSupportActionBar().setTitle("My Groups");

    }

    @Override
    protected void onResume() {
        super.onResume();
        setupForm();
    }


    private void setupForm() {
        try {
            groupDao = MasterDBManager.getInstance().getGroupDao();
            groups = groupDao.queryForAll();
            setupGroups(groups);
            if (groups.isEmpty()) {
                findViewById(R.id.group_list).setVisibility(View.INVISIBLE);
                findViewById(R.id.empty_groups_section).setVisibility(View.VISIBLE);
                findViewById(R.id.button_add_group).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addNewGroupDlg();
                    }
                });
            } else {
                findViewById(R.id.group_list).setVisibility(View.VISIBLE);
                findViewById(R.id.empty_groups_section).setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setupGroups(List<Group> groups) {
        try {
            ListView view = (ListView) findViewById(R.id.group_list);
            adapter = new GroupsAdapter(this, groups);
            view.setAdapter(adapter);
            view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    GlobalData.selectedGroup = (Group) view.getTag();
                    startActivity(new Intent(GroupsActivity.this, GroupDetailActivity.class));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_group, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_add_group:
                addNewGroupDlg();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void addNewGroupDlg() {
        final EditText grpName = new EditText(this);
        grpName.setHint("Enter Group name");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("New Group");

        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String groupName = grpName.getText().toString();
                createNewGroup(groupName);
            }
        }).setNegativeButton("Cancel", null);
        builder.setView(grpName);
        builder.create().show();
    }

    private void createNewGroup(String groupName) {
        try {
            if (StringUtils.isBlank(groupName)) {
                Utils.showPrompt(this, "Error", "Group name cannot be empty", true);
                return;
            }

            GroupDao dao = MasterDBManager.getInstance().getGroupDao();
            List<Group> list = dao.queryForEq("name", StringUtils.deleteWhitespace(groupName.toLowerCase()));
            if (!list.isEmpty()) {
                Utils.showPrompt(this, "Error", "A group with similar name already exists.", true);
                return;
            }

            Group group = new Group(groupName);
            dao.create(group);

            groups.add(group);
            adapter.notifyDataSetChanged();

            findViewById(R.id.group_list).setVisibility(View.VISIBLE);
            findViewById(R.id.empty_groups_section).setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GroupsAdapter extends BaseAdapter {

        private List<Group> list;
        private Context context;

        public GroupsAdapter(Context ctx, List<Group> groupList) {
            this.list = groupList;
            this.context = ctx;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Group grp = (Group) getItem(i);
            if (view == null) {
                view = View.inflate(context, R.layout.item_group, null);
            }

            ((TextView) view.findViewById(R.id.group_name)).setText(StringUtils.capitalize(grp.getLabel()));

            ((TextView) view.findViewById(R.id.group_count)).setText(groupDao.getCountOfPersonIn(grp) + " people");
            view.setTag(grp);
            return view;
        }
    }
}
