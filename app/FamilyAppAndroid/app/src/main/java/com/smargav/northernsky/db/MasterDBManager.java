package com.smargav.northernsky.db;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.smargav.northernsky.model.BusinessCategory;
import com.smargav.northernsky.model.City;
import com.smargav.northernsky.model.Contact;
import com.smargav.northernsky.model.Group;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.model.PersonGrpMap;
import com.smargav.northernsky.model.State;

import java.sql.SQLException;

public class MasterDBManager {
    static private MasterDBManager instance;
    public static String dbName = "konnections.db";

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new MasterDBManager(ctx);
        }
    }

    static public MasterDBManager getInstance() {
        return instance;
    }

    private DatabaseHelper helper;
    private Class[] dbClasses = {Person.class, Contact.class, Group.class, PersonGrpMap.class, City.class, State.class, BusinessCategory.class};

    private MasterDBManager(Context ctx) {
        helper = new DatabaseHelper(ctx, dbClasses, dbName);
    }


    public PersonDao getPersonDao() {
        try {
            return (PersonDao) createDao(Person.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ContactDao getContactDao() throws Exception {
        return (ContactDao) createDao(Contact.class);
    }


    public CategoryDao getCategoryDao() throws Exception {
        return (CategoryDao) createDao(BusinessCategory.class);
    }


    public GroupDao getGroupDao() throws Exception {
        return (GroupDao) createDao(Group.class);
    }

    public PersonGrpMapDao getPersonGrpMapDao() throws Exception {
        return (PersonGrpMapDao) createDao(PersonGrpMap.class);
    }

    public Dao<City, Integer> getCityDao() throws SQLException {
        return DaoManager.createDao(helper.getConnectionSource(), City.class);
    }

    public Dao<State, Integer> getStateDao() throws SQLException {
        return DaoManager.createDao(helper.getConnectionSource(), State.class);
    }

    private <D> Dao<D, ?> createDao(Class<D> clazz) throws SQLException {
        return DaoManager.createDao(helper.getConnectionSource(), clazz);
    }



}