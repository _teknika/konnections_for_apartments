package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;

import com.smargav.northernsky.R;
import com.smargav.northernsky.db.GroupDao;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.model.Group;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.ui.activities.GlobalData;
import com.smargav.northernsky.ui.adapter.GroupPersonDisplayAdapter;
import com.smargav.northernsky.ui.adapter.PersonAutoCompleteAdapter;
import com.smargav.northernsky.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class GroupDetailFragment extends Fragment {

    private View rootView;
    private Group group;
    private GroupPersonDisplayAdapter adapter;
    private ExpandableListView list;
    private List<Person> personsInGroupList;

    public GroupDetailFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_group_detail, container, false);
        group = GlobalData.selectedGroup;
        setupList();
        setupAddNewBox();
        setHasOptionsMenu(true);
        return rootView;
    }

    private void setupAddNewBox() {
        final AutoCompleteTextView addPersonToGrpView = (AutoCompleteTextView) rootView.findViewById(R.id.group_add_person);
        PersonAutoCompleteAdapter adapter = new PersonAutoCompleteAdapter(GlobalData.fullPersonsList, getActivity());
        addPersonToGrpView.setAdapter(adapter);

        addPersonToGrpView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Person person = (Person) view.getTag();

                addPersonToGroup(person);
                addPersonToGrpView.setText("");
            }
        });
    }

    private void addPersonToGroup(Person person) {
        try {
            if (person == null || personsInGroupList.contains(person)) {
                return;
            }

            GroupDao grpDao = MasterDBManager.getInstance().getGroupDao();
            List<Group> selectedGrps = new ArrayList<Group>();
            selectedGrps.add(group);
            grpDao.addPersonToGroups(person, selectedGrps);

            personsInGroupList.add(person);
            adapter.notifyDataSetChanged(personsInGroupList);
            expandAll();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupList() {
        try {
            list = (ExpandableListView) rootView.findViewById(R.id.list);
            personsInGroupList = MasterDBManager.getInstance().getGroupDao().getPersonsIn(group);
            adapter = new GroupPersonDisplayAdapter(getActivity(), personsInGroupList, group);
            list.setAdapter(adapter);
            expandAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void expandAll() {
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            list.expandGroup(i);
        }

        list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.frag_group_menu, menu);
//        MenuItem item = menu.findItem(R.id.menu_favorite);
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_frag_group_delete:
                Utils.showYesNoPrompt(getActivity(), "Confirm", "Are you sure you want to delete Group - " + GlobalData.selectedGroup.getLabel(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == DialogInterface.BUTTON_POSITIVE) {
                            if (deleteGroup(GlobalData.selectedGroup)) {
                                getActivity().finish();
                            }
                        }
                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean deleteGroup(Group selectedGroup) {
        try {
            GroupDao grpDao = MasterDBManager.getInstance().getGroupDao();
            return grpDao.deleteGrp(selectedGroup);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


}
