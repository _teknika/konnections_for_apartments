package com.smargav.northernsky.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.ui.activities.DirectoryActivity;
import com.smargav.northernsky.ui.activities.GroupsActivity;
import com.smargav.northernsky.ui.activities.MyFavoritesActivity;
import com.smargav.northernsky.ui.activities.SearchActivity;
import com.smargav.northernsky.ui.activities.WebPageDisplayActivity;
import com.smargav.northernsky.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 27/04/15.
 */
public class NavDrawerFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ViewGroup rootView;
    private DrawerLayout drawerLayout;
    private int selectedIndex;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.nav_drawer, container, false);

        setupPersonDetails();
        setupList();

        return rootView;
    }

    private void setupPersonDetails() {

    }

    public void setDrawerLayout(DrawerLayout drawer) {
        this.drawerLayout = drawer;
    }

    private void setupList() {

        ListView drawerListView = (ListView) rootView.findViewById(R.id.drawer_items);

        drawerListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        List<NavBarItem> list = getNavBarItemsList();
        selectedIndex = findSelectedIndex(list);

        NavBarAdapter adapter = new NavBarAdapter(list, getActivity());
        drawerListView.setAdapter(adapter);

        drawerListView.setOnItemClickListener(this);
        drawerListView.setItemChecked(selectedIndex, true);
    }

    private int findSelectedIndex(List<NavBarItem> list) {
        for (int i = 0; i < list.size(); i++) {
            NavBarItem item = list.get(i);
            if (getActivity().getClass().equals(item.className)) {
                return i;
            }
        }
        return 0;
    }


    private List<NavBarItem> getNavBarItemsList() {
        List<NavBarItem> list = new ArrayList<NavBarItem>();
        list.add(new NavBarItem("Home", R.mipmap.ic_launcher, DirectoryActivity.class));
        list.add(new NavBarItem("My Favorites", R.drawable.ic_favorite, MyFavoritesActivity.class));
        list.add(new NavBarItem("Groups", R.drawable.ic_committee, GroupsActivity.class));
        list.add(new NavBarItem("Search", R.drawable.ic_nav_search, SearchActivity.class));
        //list.add(new NavBarItem("SMS services", R.drawable.ic_sms_service));
        list.add(new NavBarItem("Events", R.drawable.ic_events, WebPageDisplayActivity.class));
        list.add(new NavBarItem("Advertisements", R.drawable.ic_articles, WebPageDisplayActivity.class));
        list.add(new NavBarItem("Committee Members", R.drawable.ic_aboutus, WebPageDisplayActivity.class));
        list.add(new NavBarItem("About App", R.drawable.smargav_logo, WebPageDisplayActivity.class));
        /*
         * list.add(new NavBarItem("Invite Friends",
		 * R.drawable.nav_invite_friends)); list.add(new NavBarItem("Feedback",
		 * R.drawable.nav_feedback));
		 */
        return list;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        NavBarItem item = (NavBarItem) view.getTag();

        if (i == selectedIndex) {
            drawerLayout.closeDrawers();
            return;
        }

        switch (item.icon) {
            case R.mipmap.ic_launcher:
            case R.drawable.ic_committee:
            case R.drawable.ic_favorite:
            case R.drawable.ic_nav_search:
                launchScreen(item.className);
                break;
            case R.drawable.ic_aboutus:
            case R.drawable.smargav_logo:
            case R.drawable.ic_articles:
            case R.drawable.ic_events:
                launchWebPage(item.className, item.icon);
                break;
        }

        drawerLayout.closeDrawers();

    }

    private void launchWebPage(Class className, int icon) {
        String url = MasterAPIs.EVENTS_URL;
        String title = "Events";
        if (icon == R.drawable.ic_articles) {
            url = MasterAPIs.AD_URL;
            title = "Advertisements";
        } else if (icon == R.drawable.ic_events) {
            url = MasterAPIs.EVENTS_URL;
            title = "Events";
        } else if (icon == R.drawable.ic_aboutus) {
            url = MasterAPIs.MEMBERS_URL;
            title = "Committee Members";
        } else if (icon == R.drawable.smargav_logo) {
            url = MasterAPIs.ABOUT_APP;
            title = "About App";
        }

        Intent intent = new Intent(getActivity(), className);
        intent.putExtra("url", url);
        intent.putExtra("title", title);
        getActivity().startActivity(intent);

    }


    private void launchScreen(Class clazz) {
        getActivity().startActivity(new Intent(getActivity(), clazz));
        if (!(getActivity() instanceof DirectoryActivity)) {
            getActivity().finish();
        }
    }


    private class NavBarAdapter extends BaseAdapter {

        private List<NavBarItem> NavBarItemsList = new ArrayList<NavBarItem>();
        private Activity ctx;

        public NavBarAdapter(List<NavBarItem> NavBarItemsList, Activity ctx) {
            this.NavBarItemsList = NavBarItemsList;
            this.ctx = ctx;
        }

        @Override
        public View getView(int position, View view, ViewGroup group) {

            NavBarItem e = (NavBarItem) getItem(position);

            if (e.icon != -1) {
                View layout = ctx.getLayoutInflater().inflate(
                        R.layout.nav_bar_item, null);
                TextView v = ((TextView) layout.findViewById(R.id.navBarText));
                ImageView i = ((ImageView) layout.findViewById(R.id.navBarIcon));


                i.setBackgroundResource(e.icon);

                v.setText(e.name);
                layout.setTag(e);
                return layout;
            } else {
                LinearLayout layout = (LinearLayout) ctx.getLayoutInflater()
                        .inflate(R.layout.nav_bar_header_item, null);
                ((TextView) layout.findViewById(R.id.navBarText)).setText(e.name);
                return layout;
            }
        }

        @Override
        public int getCount() {
            return NavBarItemsList.size();
        }

        @Override
        public Object getItem(int position) {
            return NavBarItemsList.get(position);
        }

        @Override
        public long getItemId(int position) {

            return position;
        }
    }


    private class NavBarItem {

        public int icon;
        public String name;
        public Class className;

        public NavBarItem() {
        }

        public NavBarItem(String name, int icon) {
            this.icon = icon;
            this.name = name;
        }

        public NavBarItem(String name, int icon, Class c) {
            this.icon = icon;
            this.name = name;
            className = c;
        }
    }
}
