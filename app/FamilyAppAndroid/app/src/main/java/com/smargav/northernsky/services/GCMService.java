package com.smargav.northernsky.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.smargav.api.logger.AppLogger;
import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.northernsky.R;
import com.smargav.northernsky.gcm.GCMReceiver;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.ui.activities.ArticlesActivity;
import com.smargav.northernsky.ui.activities.WebPageDisplayActivity;
import com.smargav.northernsky.utils.AppConstants;

public class GCMService extends IntentService {
    private Context context;
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public GCMService() {
        super("GCMService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.context = getBaseContext();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        AppLogger.d(getClass(), "Received GCM Message");
        String messageType = gcm.getMessageType(intent);
        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                processRequest(extras.getString("message"));
            }
//            Log.i("GCM Received", "Received: " + extras.getString("message"));
//        }
            // Release the wake lock provided by the WakefulBroadcastReceiver.
            GCMReceiver.completeWakefulIntent(intent);
        }

    }
        public void processRequest (String json){
            NMessage msg = new Gson().fromJson(json, NMessage.class);


            Intent intent = new Intent();
//        if ("ad".equals(msg.type)) {
//            intent.setClass(context, WebPageDisplayActivity.class);
//            intent.putExtra("url", MasterAPIs.AD_URL);
//            intent.putExtra("title", "Advertisements");
//        }
            if ("event".equals(msg.type)) {
                intent.setClass(context, WebPageDisplayActivity.class);
                intent.putExtra("url", MasterAPIs.EVENTS_URL);
                intent.putExtra("title", "Events");
            } else {
                intent.setClass(context, ArticlesActivity.class);
            }
            intent.putExtra("type", msg.type);
            // NotificationUtils.createNotification(context, msg.title, msg.message, intent, 0, R.drawable.ic_notification_icon, false);
            AppLogger.i(getClass(), "Notification fired - " + json);


            int icon = R.drawable.northernlogo;

            int mNotificationId = 001;

            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            context,
                            0,
                            intent,
                            PendingIntent.FLAG_CANCEL_CURRENT
                    );

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    context);
            Notification notification = mBuilder.setSmallIcon(icon).setTicker(msg.title).setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(msg.title)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(msg.message))
                    .setContentIntent(resultPendingIntent)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.northernlogo))
                    .setContentText(msg.message).build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(mNotificationId, notification);
            PreferencesUtil.putString(this, AppConstants.LAST_NOTIFICATION_TIME, msg.transactionTime);

        }


        class NMessage {
            public String title;
            public String message;
            //ad, event, general
            public String type;
            public String transactionTime;
        }

    }



