package com.smargav.northernsky.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.smargav.api.utils.ToastUtils;
import com.smargav.northernsky.R;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Amu on 30/05/15.
 */
public class WebPageDisplayActivity extends BaseActivity {
    private static final FrameLayout.LayoutParams ZOOM_PARAMS = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM);
    private Handler handler = new Handler();
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad);
        loadAdView();
        showProgress();
    }

    private void showProgress() {
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage("Loading...");
        dialog.show();
    }


    private void loadAdView() {


        String title = getIntent().getStringExtra("title");
        final String loadUrl = getIntent().getStringExtra("url");

        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final WebView webView = (WebView) findViewById(R.id.webView);
        //final String loadUrl = MasterAPIs.EVENTS_URL;
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setDatabaseEnabled(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setDomStorageEnabled(true);
//        WebSettings settings = webView.getSettings();
//
//        settings.setDomStorageEnabled(true);
//        FrameLayout mContentView = (FrameLayout) getWindow().
//                getDecorView().findViewById(android.R.id.content);
//        final View zoom = webView.getZoomControls();
//        mContentView.addView(zoom, ZOOM_PARAMS);
//        zoom.setVisibility(View.GONE);

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // Otherwise allow the OS to handle it
                if (url.startsWith("tel:")) {
                    Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(tel);
                    return true;
                } else if (url.startsWith("mailto:")) {
                    String body = "Enter your Question, Enquiry or Feedback below:\n\n";
                    Intent mail = new Intent(Intent.ACTION_SEND);
                    mail.setType("application/octet-stream");
                    mail.putExtra(Intent.EXTRA_EMAIL, new String[]{"contact@smargav.com"});
                    mail.putExtra(Intent.EXTRA_SUBJECT, "Mail from Sindhi Sadan User");
                    mail.putExtra(Intent.EXTRA_TEXT, body);
                    startActivity(mail);
                    return true;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

//            @Override
//            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//
//                handler.proceed();
//            }
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            public void onPageFinished(WebView view, String url) {
                if (StringUtils.equals(url, loadUrl) && dialog.isShowing()) {
                    dialog.dismiss();
                }

                view.clearCache(true);
                super.onPageFinished(view, url);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                ToastUtils.showCenteredToast(WebPageDisplayActivity.this, "Could not load page. Please try again");
                finish();
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {

                super.onReceivedHttpError(view, request, errorResponse);
            }


        });

        webView.loadUrl("about:blank");
        webView.loadUrl(loadUrl);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
