package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smargav.northernsky.R;
import com.smargav.northernsky.ui.activities.SetupActivity;

/**
 * Created by amu on 26/04/15.
 */
public class SetupForm0Fragment extends Fragment {


    private Handler handler = new Handler();
    private Runnable runnable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_setup_0, container, false);

        setupForm(rootView);
        return rootView;
    }

    private void setupForm(ViewGroup rootView) {
        // setExitTransition(TransitionU);

        rootView.findViewById(R.id.setup_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SetupActivity) getActivity()).goToStep1();
            }
        });


//        final TwoWayGridView textScroller = (TwoWayGridView) rootView.findViewById(R.id.setup_scroll);
//
//        final String[] texts = {"A fast and new way of connecting with your community", "Bulk SMS lets you send reminders and notifications instantly"};
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.item_text_scroll, texts);
//        textScroller.setAdapter(adapter);
//
//        runnable = new Runnable() {
//            @Override
//            public void run() {
//                int moveTo = textScroller.getSelectedItemPosition();
//                if (moveTo >= texts.length) {
//                    moveTo = 0;
//                } else {
//                    moveTo++;
//                }
//                textScroller.smoothScrollToPosition(moveTo);
//                handler.postDelayed(runnable, 1000);
//            }
//        };
//        handler.postDelayed(runnable, 1000);

    }

    public void onDestroy() {
        super.onDestroy();

        handler.removeCallbacks(runnable);

    }


}
