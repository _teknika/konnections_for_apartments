package com.smargav.northernsky.db;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.smargav.northernsky.model.Contact;
import com.smargav.northernsky.model.Person;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by amu on 26/04/15.
 */
public class ContactDao extends BaseDaoImpl<Contact, Integer> {

    public ContactDao(ConnectionSource connSource, Class<Contact> dataClass) throws SQLException {
        super(connSource, dataClass);
    }

    public List<Contact> getContactsFor(Person person) throws SQLException {
        return queryForEq("person_id", person);
    }
}
