package com.smargav.northernsky.model;

/**
 * Created by smargav on 10/5/18.
 */

public class BasicInfo {

    private String userId;

    private int parentId;

    private int orgId;

    private int ishead;

    private String relationwithhead;

    private int role;

    private String firstname;

    private String lastname;

    private String mobilenumber;

    private String altnumber;

    private int gender;

    private String dateofmarriage;

    private String emailId;

    private String altMail;

    private String blockName;

    private String dob;

    private PrivacyMask privacyMask;



    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getIshead() {
        return ishead;
    }

    public void setIshead(int ishead) {
        this.ishead = ishead;
    }

    public String getRelationwithhead() {
        return relationwithhead;
    }

    public void setRelationwithhead(String relationwithhead) {
        this.relationwithhead = relationwithhead;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getAltnumber() {
        return altnumber;
    }

    public void setAltnumber(String altnumber) {
        this.altnumber = altnumber;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getDateofmarriage() {
        return dateofmarriage;
    }

    public void setDateofmarriage(String dateofmarriage) {
        this.dateofmarriage = dateofmarriage;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAltMail() {
        return altMail;
    }

    public void setAltMail(String altMail) {
        this.altMail = altMail;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public PrivacyMask getPrivacyMask() {
        return privacyMask;
    }

    public void setPrivacyMask(PrivacyMask privacyMask) {
        this.privacyMask = privacyMask;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
