package com.smargav.northernsky.model;

/**
 * Created by smargav on 9/5/18.
 */

public class PrivacyMask {

    private boolean email;

    private boolean  mobileNumber;

    private boolean flat;

    public boolean isEmail() {
        return email;
    }

    public void setEmail(boolean email) {
        this.email = email;
    }

    public boolean isMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(boolean mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public boolean isFlat() {
        return flat;
    }

    public void setFlat(boolean flat) {
        this.flat = flat;
    }
}
