package com.smargav.northernsky.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.smargav.northernsky.R;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.utils.Utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PersonAutoCompleteAdapter extends BaseAdapter implements Filterable {

        private Context context;
        private List<Person> persons;
        private List<Person> masterPersons;

        public PersonAutoCompleteAdapter(List<Person> persons, Context ctx) {
            this.context = ctx;
            this.persons = persons;
            this.masterPersons = persons;

        }

        @Override
        public int getCount() {
            return persons.size();
        }

        @Override
        public Object getItem(int i) {
            return persons.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Person person = (Person) getItem(i);

            if (view == null) {
                view = View.inflate(context, R.layout.item_person_small, null);
            }

            ImageView photo = (ImageView) view.findViewById(R.id.person_photo);

            TextView name = (TextView) view.findViewById(R.id.person_name);
            name.setText(Utils.getFormattedName(person));

            Utils.fillPersonPhoto(photo, person);
            view.setTag(person);

            return view;
        }

        @Override
        public Filter getFilter() {

            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    FilterResults results = new FilterResults();
                    if (charSequence == null || charSequence.length() < 3) {
                        results.values = masterPersons;
                        results.count = masterPersons.size();
                    } else {
                        List<Person> list = new ArrayList<>();
                        String searchText = charSequence.toString();
                        for (Person p : masterPersons) {
                            if (StringUtils.containsIgnoreCase(p.getFirstName(), searchText)|| StringUtils.containsIgnoreCase(p.getLastName(), searchText) || StringUtils.contains(p.getMobileNumber(), searchText)) {
                                list.add(p);
                            }
                        }

                        results.count = list.size();
                        results.values = list;
                    }
                    return results;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    if (charSequence == null || charSequence.length() < 3) {
                        return;
                    }

                    persons = (List<Person>) filterResults.values;
                    notifyDataSetChanged();

                }
            };
        }
    }