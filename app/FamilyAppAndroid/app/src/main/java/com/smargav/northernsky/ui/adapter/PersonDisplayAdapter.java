package com.smargav.northernsky.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.smargav.northernsky.R;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by amu on 29/04/15.
 */
public class PersonDisplayAdapter extends BaseExpandableListAdapter implements Filterable {

    protected Context context;
    protected List<Person> items;
    protected List<String> categories = new ArrayList<String>();
    protected Map<String, List<Person>> map = new LinkedHashMap<String, List<Person>>();

    protected int categoriesCount = 0;

    public PersonDisplayAdapter(Context ctx, List<Person> items) {
        this.items = items;
        this.context = ctx;
        categorizeItems();
    }

    public void categorizeItems() {
        map.clear();
        for (int i = 0; i < items.size(); i++) {
            Person p = items.get(i);
            if (p.getParentId() <=0) {
                String name = p.getFirstName();

//            String ch = name.substring(0, 1);
                name = name.toUpperCase(Locale.US);
                if (!map.containsKey(name)) {
                    List<Person> list = new ArrayList<Person>();
                    list.add(p);
                    map.put(name, list);
                } else {
                    map.get(name).add(p);
                }
            }
        }

        categoriesCount = map.keySet().size();
        categories = new ArrayList(map.keySet());
        Collections.sort(categories);
    }

    @Override
    public int getGroupCount() {
        return categoriesCount;
    }

    @Override
    public int getChildrenCount(int i) {
        return map.get(categories.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return categories.get(i);
    }

    @Override
    public Object getChild(int i, int i2) {
        return map.get(categories.get(i)).get(i2);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i2) {
        return i2;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup viewGroup) {

        String groupName = (String) getGroup(groupPosition);
        if (view == null) {
            view = View.inflate(context, R.layout.item_group_header, null);
        }

        //First group so show total count.
        if (groupPosition == 0) {
            ((TextView) view.findViewById(R.id.item_group_moreinfo)).setText("" + items.size() + " Contacts");
        } else {
            ((TextView) view.findViewById(R.id.item_group_moreinfo)).setText("");
        }

        ((TextView) view.findViewById(R.id.item_group_name)).setText(groupName);
        view.setTag(groupName);

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup viewGroup) {
        Person person = (Person) getChild(groupPosition, childPosition);



        if (view == null) {
            view = View.inflate(context, R.layout.item_person_list, null);
        }

        QuickContactBadge photo = (QuickContactBadge) view.findViewById(R.id.person_photo);
        Button photoButton = (Button) view.findViewById(R.id.person_photo_button);
        TextView name = (TextView) view.findViewById(R.id.person_name);

//        if (StringUtils.isNotBlank(person.getThumbnailUri()) && StringUtils.isNumeric(person.getContactId())) {
//            Uri uri = ContactsContract.Contacts.getLookupUri(Long.parseLong(person.getContactId()), person.getLookupKey());
//            photo.assignContactUri(uri);
//            photo.setImageURI(Uri.parse(person.getThumbnailUri()));
//            photo.setVisibility(View.VISIBLE);
//            photoButton.setVisibility(View.INVISIBLE);
//        } else {
        photo.setVisibility(View.INVISIBLE);
        photoButton.setVisibility(View.VISIBLE);
        char firstChar = person.getFirstName().toUpperCase().charAt(0);
        photoButton.setText("" + firstChar);
        //photo.setImageResource(R.drawable.contact);
//        photoButton.setBackgroundColor(Utils.getColorResource(context, firstChar));
        //}




        if (person.getParentId() <= 0){
            isLastChild =false;
        }
        if (isLastChild) {
            view.findViewById(R.id.item_child_divider).setVisibility(View.INVISIBLE);
            view.findViewById(R.id.item_person_layout).setVisibility(View.INVISIBLE);
        } else {
            name.setText(Utils.getFormattedName(person));
            view.findViewById(R.id.item_child_divider).setVisibility(View.VISIBLE);


        }
        view.setTag(person);

        return view;

    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }


    public void notifyDataSetChanged() {
        categorizeItems();
        super.notifyDataSetChanged();
    }

    public void notifyDataSetChanged(List<Person> list) {
        items = list;
        categorizeItems();
        super.notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return null;
    }
}
