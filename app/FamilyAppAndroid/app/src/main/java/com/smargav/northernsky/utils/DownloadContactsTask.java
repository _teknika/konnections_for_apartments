package com.smargav.northernsky.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.api.utils.ThreadUtils;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.ui.activities.DirectoryActivity;
import com.smargav.northernsky.ui.activities.GlobalData;

import java.util.ArrayList;
import java.util.List;

public class DownloadContactsTask extends ProgressAsyncTask<String, Integer> {

    private String message = "Unknown error occured. Please try again";

    public DownloadContactsTask(Context ctx) {
        super(ctx);
    }

    @Override
    protected Integer doInBackground(String... strings) {
        try {

            MasterDBManager.getInstance().getPersonDao().deleteAll();
            Person user = PreferencesUtil.get(ctx, GlobalData.USER_INFO, Person.class);

//            List<State> states = MasterAPIs.getStates();
//            AppConstants.saveStates(ctx, states);
//            List<City> cities = MasterAPIs.getCities();
//            AppConstants.saveCities(ctx, cities);

            long pageNo = PreferencesUtil.getLong(ctx, GlobalData.PAGE_NO, 1);

            List<Person> persons = new ArrayList<>();
            publishProgress("Syncing Contacts. It may take 5-10 minutes. Please bear with us.");

            int count = 50;

            while (pageNo < 100) {
                persons = MasterAPIs.downloadAllContacts(user.getId(), (int) pageNo, count);
//                AppConstants.savePersonData(persons);
                PersonDao.saveData(ctx, persons);
                //message = "Could not save member details.  Please clear Data and try again";
                //return 0;
                pageNo++;
                PreferencesUtil.putLong(ctx, GlobalData.PAGE_NO, pageNo);
                if (persons == null || persons.size() != count) {
                    break;
                }
                ThreadUtils.sleep(2);
            }

            PreferencesUtil.remove(ctx, GlobalData.PAGE_NO);
            GlobalData.fullPersonsList = persons;
            return SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();

        }
        PreferencesUtil.remove(ctx, GlobalData.PAGE_NO);

        return FAILED;
    }

    public void onPostExecute(Integer result) {
        super.onPostExecute(result);

        if (result == FAILED) {
            Utils.showPrompt((Activity) ctx, "Error", message, true);
            return;
        }
//            GlobalData.fullPersonsList = null;
        Intent intent = new Intent(ctx, DirectoryActivity.class);
        ctx.startActivity(intent);
        ((Activity) ctx).finish();


    }

}

