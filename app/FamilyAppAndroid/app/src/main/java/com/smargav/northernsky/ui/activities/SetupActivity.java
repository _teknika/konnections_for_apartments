package com.smargav.northernsky.ui.activities;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.northernsky.ui.fragments.SetupForm0Fragment;
import com.smargav.northernsky.ui.fragments.SetupForm1Fragment;
import com.smargav.northernsky.ui.fragments.SetupForm2Fragment;
import com.smargav.northernsky.ui.fragments.SetupForm3Fragment;
import com.smargav.northernsky.R;

/**
 * Created by amu on 18/05/15.
 */
public class SetupActivity extends AppCompatActivity {

    private static final int STEP_0 = 0;
    private static final int STEP_1 = 1;
    private static final int STEP_2 = 2;
    private static final int STEP_3 = 3;
    private static final String KEY = "stepKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        int step = (int) PreferencesUtil.getLong(this, KEY, -1);

        switch (step) {
            case STEP_1:
                goToStep1();
                break;
            case STEP_2:
                goToStep2();
                break;
            case STEP_3:
                goToStep3();
                break;
            case STEP_0:
            default:
                gotoStep0();
        }


    }

    private void gotoStep0() {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        ft.replace(R.id.frame, new SetupForm0Fragment()).commit();
        PreferencesUtil.putLong(this, KEY, STEP_0);
    }

    public void goToStep1() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        ft.replace(R.id.frame, new SetupForm1Fragment()).commit();
        PreferencesUtil.putLong(this, KEY, STEP_1);
    }

    public void goToStep2() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frame, new SetupForm2Fragment()).commit();
        PreferencesUtil.putLong(this, KEY, STEP_2);
    }

    public void goToStep3() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        //ft.setTransition(R.anim.slide_in);
        ft.replace(R.id.frame, new SetupForm3Fragment()).commit();
        PreferencesUtil.putLong(this, KEY, STEP_3);
    }

    public void setupComplete() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}
