package com.smargav.northernsky.net;

/**
 * Created by amu on 28/05/15.
 */
public class NetRequest {

    private Object request;

    public Object getRequest() {
        return request;
    }

    public void setRequest(Object request) {
        this.request = request;
    }
}
