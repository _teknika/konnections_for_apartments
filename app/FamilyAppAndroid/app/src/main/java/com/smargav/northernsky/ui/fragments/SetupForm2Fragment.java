package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.ui.activities.GlobalData;
import com.smargav.northernsky.ui.activities.SetupActivity;
import com.smargav.northernsky.utils.Utils;
import com.smargav.northernsky.R;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by amu on 26/04/15.
 */
public class SetupForm2Fragment extends Fragment {

    private EditText otp;
    String number;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_setup_2, container, false);

        setupForm(rootView);
        return rootView;
    }

    private void setupForm(ViewGroup rootView) {

        otp = (EditText) rootView.findViewById(R.id.setup_otp);

        otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null && otp.getError() != null) {
                    otp.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        rootView.findViewById(R.id.setup_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 number = otp.getText().toString();
                if (StringUtils.isBlank(number)) {
                    otp.setError("OTP field is mandatory");
                    return;
                }
//                    resendOTP();
                new SetupAsyncTask(getActivity()).execute(number);


            }
        });

        rootView.findViewById(R.id.resend_otp_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendOTP();
            }
        });

        rootView.findViewById(R.id.change_number_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeNumber();
            }


        });
    }

    private void changeNumber() {
        ((SetupActivity) getActivity()).goToStep1();
    }

    private void resendOTP() {
        new ProgressAsyncTask<String, Integer>(getActivity()) {

            @Override
            protected Integer doInBackground(String... strings) {
                try {
                    String number = PreferencesUtil.getString(getActivity(), GlobalData.PHONE_NUMBER_KEY, "");
//                    String regKey = GCMRegister.getRegId(ctx);
//                    if (regKey == null) {
//                        GCMRegister.registerToGCM(ctx);
//                        regKey = GCMRegister.getRegId(ctx);
//                    }
//                    MasterAPIs.generateOTP(number);

                    String otp = MasterAPIs.generateOTP(number);

                    if (otp == null) {
                        return FAILED;
                    }

                    PreferencesUtil.putString(getActivity(), GlobalData.OTP, otp);

                    return SUCCESS;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return FAILED;
            }

            public void onPostExecute(Integer result) {
                super.onPostExecute(result);
                if (result == FAILED) {
                    Utils.showPrompt(getActivity(), "Error", "Could not resend OTP. Please try again.", true);
                    return;

                }

                Utils.showPrompt(getActivity(), "Success", "OTP has been resent to the registered Phone number.", true);
//                new SetupAsyncTask(getActivity()).execute(number);
            }

        }.execute();


    }


    private class SetupAsyncTask extends ProgressAsyncTask<String, Integer> {

        private String number;
        private String message = "Unknown error occured. Please try again";

        public SetupAsyncTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... strings) {

            try {
                number = strings[0];
                if (StringUtils.equals(number, PreferencesUtil.getString(getActivity(), GlobalData.OTP, null))) {
                    return SUCCESS;
                }else {
                    message = "Invalid OTP. Please enter correct OTP";
                    return FAILED;
                }

//                return SUCCESS;

            } catch (Exception e) {
                e.printStackTrace();
            }

            return FAILED;
        }

        public void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == FAILED) {
                Utils.showPrompt(getActivity(), "Error", message, true);
                return;
            }

            ((SetupActivity) getActivity()).goToStep3();
        }
    }

}
