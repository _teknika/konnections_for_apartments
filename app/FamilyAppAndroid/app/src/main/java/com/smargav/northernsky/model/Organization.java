package com.smargav.northernsky.model;

/**
 * Created by amu on 01/05/15.
 */
public class Organization {

    private int id;

    private String name;

    private String description;


    public Organization(){

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
