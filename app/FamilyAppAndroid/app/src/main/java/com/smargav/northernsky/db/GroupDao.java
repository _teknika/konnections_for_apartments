package com.smargav.northernsky.db;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.smargav.northernsky.model.Group;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.model.PersonGrpMap;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 26/04/15.
 */
public class GroupDao extends BaseDaoImpl<Group, Integer> {

    public GroupDao(ConnectionSource connSource, Class<Group> dataClass) throws SQLException {
        super(connSource, dataClass);
    }


    public List<Group> getAllGroupsFor(Person person) throws Exception {
        PersonGrpMapDao mapDao = MasterDBManager.getInstance().getPersonGrpMapDao();
        QueryBuilder<PersonGrpMap, String> mapQB = mapDao.queryBuilder();
        mapQB.selectColumns("group_id");
        mapQB.where().eq("person_id", person.getId());

        // build our outer query for Group objects
        QueryBuilder<Group, Integer> groupQb = queryBuilder();
        // where the id matches in the group-id from the inner query
        groupQb.where().in("id", mapQB);
        PreparedQuery<Group> prepQuery = groupQb.prepare();

        return query(prepQuery);
        //return new ArrayList<Group>();
    }

    public void addPersonToGroups(Person person, List<Group> selectedGrps) throws Exception {
        PersonGrpMapDao mapDao = MasterDBManager.getInstance().getPersonGrpMapDao();
        for (Group grp : selectedGrps) {
            PersonGrpMap map = new PersonGrpMap(grp, person);
            mapDao.createOrUpdate(map);
        }
    }

    public void removePersonFromGroups(Person person, List<Group> groups) throws Exception {
        PersonGrpMapDao mapDao = MasterDBManager.getInstance().getPersonGrpMapDao();
        for (Group grp : groups) {
            PersonGrpMap map = new PersonGrpMap(grp, person);
            mapDao.deleteById(map.getId());
        }
    }

    public int getCountOfPersonIn(Group group) {
        try {

            PersonGrpMapDao mapDao = MasterDBManager.getInstance().getPersonGrpMapDao();
            PersonDao personDao = MasterDBManager.getInstance().getPersonDao();
            QueryBuilder<PersonGrpMap, String> mapQB = mapDao.queryBuilder();
            mapQB.selectColumns("person_id");
            mapQB.where().eq("group_id", group.getId());
            // build our outer query for Group objects
            QueryBuilder<Person, Integer> personQb = personDao.queryBuilder();
            // where the id matches in the group-id from the inner query
            personQb.where().in("id", mapQB);
            PreparedQuery<Person> prepQuery = personQb.prepare();
            return personDao.query(prepQuery).size();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    public List<Person> getPersonsIn(Group group) {
        try {

            PersonGrpMapDao mapDao = MasterDBManager.getInstance().getPersonGrpMapDao();
            PersonDao personDao = MasterDBManager.getInstance().getPersonDao();
            QueryBuilder<PersonGrpMap, String> mapQB = mapDao.queryBuilder();
            mapQB.selectColumns("person_id");
            mapQB.where().eq("group_id", group.getId());
            // build our outer query for Group objects
            QueryBuilder<Person, Integer> personQb = personDao.queryBuilder();
            // where the id matches in the group-id from the inner query
            personQb.where().in("id", mapQB);
            PreparedQuery<Person> prepQuery = personQb.prepare();
            return personDao.query(prepQuery);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<Person>();
    }

    public boolean deleteGrp(Group grp) {
        try {
            PersonGrpMapDao mapDao = MasterDBManager.getInstance().getPersonGrpMapDao();
            DeleteBuilder<PersonGrpMap, String> delBuilder = mapDao.deleteBuilder();

            Where<PersonGrpMap, String> query = delBuilder.where();
            query.eq("group_id", grp);

            delBuilder.delete();
            delete(grp);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

}
