package com.smargav.northernsky.ui.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smargav.northernsky.R;
import com.smargav.northernsky.model.AdBanner;

public class BannerFragment extends Fragment {

    private static final String BANNER_KEY = "banner";
    private AdBanner banner;

    public AdBanner getBanner() {
        return banner;
    }

    public void setBanner(AdBanner banner) {
        this.banner = banner;
    }

    public BannerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View bannerView = inflater.inflate(R.layout.fragment_banner, null);

        TextView mainText = (TextView) bannerView
                .findViewById(R.id.bannerMainText);
        TextView subText = (TextView) bannerView
                .findViewById(R.id.bannerSubText);
        if (banner != null) {
            mainText.setText(Html.fromHtml(banner.getDescription()));
            subText.setText(Html.fromHtml(banner.getSubText()));

        } else if (getArguments() != null
                && getArguments().containsKey(BANNER_KEY)) {
            banner = (AdBanner) getArguments().getSerializable(BANNER_KEY);
            mainText.setText(banner.getDescription());
            subText.setText(banner.getSubText());
        } else {
            mainText.setText("Welcome to Sindhi Sadan");
            subText.setText("A new and faster way to connect with your Community.");
        }
        return bannerView;
    }

}
