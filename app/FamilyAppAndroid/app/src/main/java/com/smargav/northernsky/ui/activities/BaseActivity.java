package com.smargav.northernsky.ui.activities;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.WindowManager;

import com.smargav.northernsky.R;
import com.smargav.northernsky.ui.fragments.NavDrawerFragment;

/**
 * Created by amu on 31/05/15.
 */
public class BaseActivity extends AppCompatActivity {

    public Toolbar toolbar;

    public DrawerLayout mDrawerLayout;
    public ActionBarDrawerToggle mDrawerToggle;


    public void setContentView(int layoutId) {
        super.setContentView(layoutId);
        setupToolbar();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        overridePendingTransition(R.anim.right_in,R.anim.left_out);


    }

    private void setupToolbar() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ActivityHelper.initialize(this);
    }


    private void setNavDrawer(int id, int fid) {

        if (toolbar == null) {
            return;
        }


        mDrawerLayout = (DrawerLayout) findViewById(id);

        if (mDrawerLayout == null) {
            return;
        }

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        Fragment navDrawer = getFragmentManager().findFragmentById(fid);
        if (navDrawer != null) {
            NavDrawerFragment navDrawerFragment = (NavDrawerFragment) navDrawer;
            navDrawerFragment.setDrawerLayout(mDrawerLayout);
        }
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.base_menu, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
//        int id = item.getItemId();
//
//        switch (id) {
//            case R.id.menu_events:
//                loadEvents();
//                break;
//            case R.id.menu_ads:
//                loadAds();
//                break;
//        }
        return super.onOptionsItemSelected(item);
    }

//    private void loadEvents() {
//        Intent intent = new Intent(this, AdDisplayActivity.class);
//        intent.setData(Uri.parse(MasterAPIs.EVENTS_URL));
//        intent.putExtra("title", "Events");
//        startActivity(intent);
//    }
//
//    private void loadAds() {
//        Intent intent = new Intent(this, AdDisplayActivity.class);
//        intent.setData(Uri.parse(MasterAPIs.AD_URL));
//        intent.putExtra("title", "Advertisements");
//        startActivity(intent);
//    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mDrawerToggle != null)
            mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawerToggle != null)
            mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerToggle != null && mDrawerLayout.isDrawerOpen(Gravity.START | Gravity.LEFT)) {
            mDrawerLayout.closeDrawers();
            return;
        }
        super.onBackPressed();
    }


    public void setTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }


}
