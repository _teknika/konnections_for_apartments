package com.smargav.northernsky.db;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.smargav.northernsky.model.BusinessCategory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 26/04/15.
 */
public class CategoryDao extends BaseDaoImpl<BusinessCategory, String> {

    public CategoryDao(ConnectionSource connSource, Class<BusinessCategory> dataClass) throws SQLException {
        super(connSource, dataClass);
    }

    public void saveAll(List<BusinessCategory> categories) throws Exception {
        for (BusinessCategory c : categories) {
            createOrUpdate(c);
        }
    }


    public List<String> getAllCategories() {
        List<String> categories = new ArrayList<>();
        try {
            for (BusinessCategory c : queryForAll()) {
                categories.add(c.getName());
            }
            return categories;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }
}
