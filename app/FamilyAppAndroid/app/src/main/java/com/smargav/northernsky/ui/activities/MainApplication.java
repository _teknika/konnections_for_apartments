package com.smargav.northernsky.ui.activities;

import android.app.Application;
import android.content.Context;

import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.api.utils.ThreadUtils;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.net.MasterAPIs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 25/04/15.
 */
public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MasterDBManager.init(this);
       // new Downloader().start();
    }


    private class Downloader extends Thread {
        @Override
        public void run() {
            try {
                Context ctx = MainApplication.this;
                Person user = PreferencesUtil.get(ctx, GlobalData.USER_INFO, Person.class);
                if (user == null) {
                    return;
                }
                int pageNo = 1;
                List<Person> persons = new ArrayList<>();
                int count = 50;
                while (pageNo < 100) {
                    persons = MasterAPIs.downloadAllContacts(user.getId(), (int) pageNo, count);
                    PersonDao.saveData(ctx, persons);
                    pageNo++;

                    if (persons == null || persons.size() < count) {
                        break;
                    }
                    ThreadUtils.sleep(10);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
