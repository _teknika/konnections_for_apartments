package com.smargav.northernsky.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.api.utils.ThreadUtils;
import com.smargav.northernsky.R;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.ui.fragments.PersonsGridFragment;
import com.smargav.northernsky.ui.fragments.PersonsListFragment;
import com.smargav.northernsky.utils.DownloadContactsTask;
import com.smargav.northernsky.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class DirectoryActivity extends BaseActivity {

    public static final String ACTION_CONTACT_DETAILS = "contactDetails";

    public static boolean isGridDisplay = false;

    private PersonsListFragment listFragment;
    private PersonsGridFragment gridFragment;
    private MenuItem menuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory_listing);
        setTitle("Member Directory");
//        if (ACTION_CONTACT_DETAILS.equals(getIntent().getAction())) {
////            new PopulateContactsTask(this).execute();
//            new DataLoaderTask(this).execute();
//
//        }

        new DataLoaderTask(this).execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
//        this.menuItem = menu.findItem(R.id.menu_toggle_display);
//
//        if (isGridDisplay) {
//            menu.findItem(R.id.menu_toggle_display).setIcon(R.drawable.ic_list_regular);
//        } else {
//            menu.findItem(R.id.menu_toggle_display).setIcon(R.drawable.ic_grid_regular);
//        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
//            case R.id.menu_toggle_display:
//                loadFragment(!isGridDisplay, item);
//                break;
            case R.id.menu_refresh:
                new DownloadContactsTask(this).execute();

        }

        return super.onOptionsItemSelected(item);
    }

    private void loadFragment(boolean isGrid, MenuItem item) {

        isGridDisplay = isGrid;
        if (isGrid) {
            if (gridFragment == null) {
                gridFragment = new PersonsGridFragment();
            }
            getFragmentManager().beginTransaction().replace(R.id.frame, gridFragment).commit();
            if (item != null)
                item.setIcon(R.drawable.ic_list_regular);
        } else {

            if (listFragment == null) {
                listFragment = new PersonsListFragment();
            }
            getFragmentManager().beginTransaction().replace(R.id.frame, listFragment).commit();

            if (item != null)
                item.setIcon(R.drawable.ic_grid_regular);
        }

    }


    private class DataLoaderTask extends ProgressAsyncTask<String, Integer> {

        private List<Person> persons = new ArrayList<>();

        public DataLoaderTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                PersonDao dao = MasterDBManager.getInstance().getPersonDao();
//                persons = dao.
                persons = GlobalData.fullPersonsList;
                if (persons == null || persons.isEmpty()) {
                    persons = dao.getAllHeadsList();

                    GlobalData.fullPersonsList = persons;
                }

                GlobalData.searchPersonList = persons;
                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }

            GlobalData.searchPersonList = persons;
            return FAILED;
        }

        public void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == FAILED) {
                return;
            }
            loadFragment(isGridDisplay, menuItem);


        }


    }

    public class PopulateContactsTask extends ProgressAsyncTask<String, Integer> {

        private String message = "Unknown error occured. Please try again";

        public PopulateContactsTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... strings) {
            try {

                MasterDBManager.getInstance().getPersonDao().deleteAll();
                Person user = PreferencesUtil.get(ctx, GlobalData.USER_INFO, Person.class);

//            List<State> states = MasterAPIs.getStates();
//            AppConstants.saveStates(ctx, states);
//            List<City> cities = MasterAPIs.getCities();
//            AppConstants.saveCities(ctx, cities);

                long pageNo = PreferencesUtil.getLong(ctx, GlobalData.PAGE_NO, 1);

                List<Person> persons = new ArrayList<>();
                publishProgress("Syncing Contacts. It may take 5-10 minutes. Please bear with us.");
                int count = 50;
                while (pageNo < 100) {
                    persons = MasterAPIs.downloadAllContacts(user.getId(), (int) pageNo, count);
                    PersonDao.saveData(ctx, persons);
                    //message = "Could not save member details.  Please clear Data and try again";
                    //return 0;
                    pageNo++;
                    PreferencesUtil.putLong(ctx, GlobalData.PAGE_NO, pageNo);
                    if (persons == null || persons.size() < count) {
                        break;
                    }
                    ThreadUtils.sleep(2);
                }

                PreferencesUtil.remove(ctx, GlobalData.PAGE_NO);
                GlobalData.fullPersonsList = persons;
                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();

            }
            PreferencesUtil.remove(ctx, GlobalData.PAGE_NO);

            return FAILED;
        }

        public void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == FAILED) {
                Utils.showPrompt((Activity) ctx, "Error", message, true);
                return;
            }

//        Intent intent = new Intent(ctx, DirectoryActivity.class);
//        ctx.startActivity(intent);
//        ((Activity) ctx).finish();


        }

    }

}
