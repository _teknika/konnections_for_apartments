package com.smargav.northernsky.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.smargav.northernsky.db.GroupDao;

import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;

/**
 * Created by amu on 13/05/15.
 */
@DatabaseTable(daoClass = GroupDao.class)
public class Group {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String name;

    @DatabaseField
    private long createdTime;

    @DatabaseField
    private String label;

    public Group() {

    }

    public Group(String label) {
        this.label = label;
        this.name = StringUtils.deleteWhitespace(label.toLowerCase());
        this.createdTime = Calendar.getInstance().getTimeInMillis();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
