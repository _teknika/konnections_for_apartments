package com.smargav.northernsky.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.smargav.northernsky.db.ContactDao;

@DatabaseTable(daoClass = ContactDao.class)
public class Contact {

    public enum ContactType {
        HOME("Home"), OFFICE("Office"), NATIVE("Native"), OTHERS("Others");
        private String label;

        ContactType() {

        }

        ContactType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    @DatabaseField(id = true)
    private int id;

    @DatabaseField(foreign = true)
    private Person person;

    @DatabaseField
    private ContactType type;

    @DatabaseField
    private String door;

    @DatabaseField
    private String cross;

    @DatabaseField
    private String street;

    @DatabaseField
    private String area;

    @DatabaseField
    private String landmark;

    @DatabaseField
    private String city;

    @DatabaseField
    private String country;

    @DatabaseField
    private String state;

    @DatabaseField
    private String pincode;

    @DatabaseField
    private float longitude;

    @DatabaseField
    private float latitude;

    @DatabaseField
    private String phone;

    @DatabaseField
    private String email;

    @DatabaseField
    private String name;

    public Contact() {
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    public String getDoor() {
        return door;
    }

    public void setDoor(String door) {
        this.door = door;
    }

    public String getCross() {
        return cross;
    }

    public void setCross(String cross) {
        this.cross = cross;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
