package com.smargav.northernsky.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.northernsky.R;
import com.smargav.northernsky.model.BasicInfo;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.model.PrivacyMask;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.utils.Utils;

import java.io.IOException;

/**
 * Created by smargav on 6/11/17.
 */

public class EditProfile extends BaseActivity implements View.OnClickListener {

    TextView flatNumber,relationWithHead,anniversay;
    TextView mobileNumber,emailId;
    EditText name,lastName,blockName;
    Button saveBTNID;
    CheckBox mobileNumCheckbox,emailCheckbox,flatCheckbox;
    Person person;
    String email,mobile,flat;;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        setTitle("EditProfile");
        person = PreferencesUtil.get(this, GlobalData.USER_INFO,Person.class);
        bindViews();
        initViews();
    }

    public void bindViews(){
        mobileNumber = (TextView) findViewById(R.id.editProfile_mobileNumETID);
        emailId = (TextView)findViewById(R.id.editProfile_emailIdETID);
        saveBTNID = (Button)findViewById(R.id.editProfile_saveBtn);
        name = (EditText) findViewById(R.id.editProfile_nameIVD);
        lastName = (EditText)findViewById(R.id.editProfile_lastNameTVID);
        blockName = (EditText)findViewById(R.id.editProfile_blockNameTVID);
        flatNumber = (TextView)findViewById(R.id.editProfile_flatNumTVID);
        relationWithHead = (TextView)findViewById(R.id.editProfile_relationVID);
        anniversay = (TextView)findViewById(R.id.editProfile_anniversaryTVID);
        mobileNumCheckbox = (CheckBox)findViewById(R.id.editprofile_mobileCheck);
        emailCheckbox = (CheckBox)findViewById(R.id.editprofile_emailCheck);
        flatCheckbox = (CheckBox)findViewById(R.id.editprofile_flatnumCheck);
    }

    public void initViews(){
        prepopulateUserData();
        saveBTNID.setOnClickListener(this);

    }
    public void prepopulateUserData(){

        if (person!=null){
            name.setText(person.getFirstName());
            lastName.setText(person.getLastName());
//            emailId.setText(person.getEmailId());
            blockName.setText(person.getBlockName());
//            flatNumber.setText(person.getFlatNo());
            relationWithHead.setText(person.getRelationWithHead());
            anniversay.setText(person.getDateOfMarriage());

//            String privacy = person.getPrivacy_mask();

            PrivacyMask privacyMask = new PrivacyMask();
                privacyMask = person.getPrivacy_mask();
            if(privacyMask != null) {

//                    JSONObject json = new JSONObject(privacy);
//                    email = json.optString("email");
//                    mobile = json.optString("mobileNumber");
//                    flat = json.optString("flat");


                if (privacyMask.isEmail()== true) {
                    privacyMask.setEmail(true);
                } else {
                    privacyMask.setEmail(false);
                }
                if (privacyMask.isMobileNumber() == true) {
                    privacyMask.setMobileNumber(true);
                } else {
                    privacyMask.setMobileNumber(false);
                }
                if (privacyMask.isFlat() == true) {
                    privacyMask.setFlat(true);
                } else {
                    privacyMask.setFlat(false);
                }

//                person.setPrivacyMaskObj(privacyMask);
            }
            if (privacyMask!=null) {
                if (privacyMask.isMobileNumber() == true) {
                    mobileNumCheckbox.setChecked(true);
                }
                    mobileNumber.setText(person.getMobileNumber());

                if (privacyMask.isEmail() == true) {
                    emailCheckbox.setChecked(true);
                }
                    emailId.setText(person.getEmailId());

                if (privacyMask.isFlat() == true) {
                    flatCheckbox.setChecked(true);
                }
                    flatNumber.setText(person.getFlatNo());

            }
        }
    }
    public BasicInfo updateUserData(){

        BasicInfo basicInfo = new BasicInfo();

        basicInfo.setFirstname(name.getText().toString());
        basicInfo.setLastname(lastName.getText().toString());
        basicInfo.setBlockName(blockName.getText().toString());

        basicInfo.setRelationwithhead(relationWithHead.getText().toString());
        basicInfo.setDateofmarriage(anniversay.getText().toString());
        PrivacyMask privacyMask = new PrivacyMask();

        if (person!=null) {
            basicInfo.setUserId(person.getId());
            basicInfo.setParentId(person.getParentId());
            basicInfo.setOrgId(person.getOrgId());
            basicInfo.setIshead(person.getIsHead());
            basicInfo.setEmailId(person.getEmailId());
            basicInfo.setMobilenumber(person.getMobileNumber());
        }
        if (mobileNumCheckbox.isChecked()){
            privacyMask.setMobileNumber(true);
        }else {
            privacyMask.setMobileNumber(false);
        }

        if (emailCheckbox.isChecked()){
            privacyMask.setEmail(true);
        }else {
            privacyMask.setEmail(false);
        }

        if (flatCheckbox.isChecked()){
            privacyMask.setFlat(true);
        }else {
            privacyMask.setFlat(false);
        }
        basicInfo.setPrivacyMask(privacyMask);

        return basicInfo;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (i){
            case R.id.editProfile_saveBtn:
                new EditProfileAsyncTask(this).execute();
        }
    }

    private class EditProfileAsyncTask extends ProgressAsyncTask<String,Integer>{

        public EditProfileAsyncTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... strings) {

            BasicInfo basicInfo = updateUserData();
            try {
                String resp = MasterAPIs.editProfile(basicInfo);
                return SUCCESS;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return FAILED;
        }

        @Override
        public void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == FAILED) {
                Utils.showPrompt((Activity) ctx, "Error", "Error occured while editing profile", true);
                return;
            }else ;
            startActivity(new Intent(EditProfile.this,HomeActivity.class));
                finish();
            }
        }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}



