package com.smargav.northernsky.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.smargav.northernsky.R;
import com.smargav.northernsky.db.GroupDao;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.model.Group;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.utils.Utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 29/04/15.
 */
public class GroupPersonDisplayAdapter extends PersonDisplayAdapter implements View.OnClickListener {


    private Group group;

    public GroupPersonDisplayAdapter(Context ctx, List<Person> items, Group group) {
        super(ctx, items);
        this.group = group;

    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup viewGroup) {
        Person person = (Person) getChild(groupPosition, childPosition);

        if (view == null) {
            view = View.inflate(context, R.layout.item_person_list, null);
        }

        QuickContactBadge photo = (QuickContactBadge) view.findViewById(R.id.person_photo);
        Button photoButton = (Button) view.findViewById(R.id.person_photo_button);
        TextView name = (TextView) view.findViewById(R.id.person_name);

//        if (StringUtils.isNotBlank(person.getThumbnailUri()) && StringUtils.isNumeric(person.getContactId())) {
//            Uri uri = ContactsContract.Contacts.getLookupUri(Long.parseLong(person.getContactId()), person.getLookupKey());
//            photo.assignContactUri(uri);
//            photo.setImageURI(Uri.parse(person.getThumbnailUri()));
//            photo.setVisibility(View.VISIBLE);
//            photoButton.setVisibility(View.INVISIBLE);
//        } else {
        photo.setVisibility(View.INVISIBLE);
        photoButton.setVisibility(View.VISIBLE);
        photoButton.setText(StringUtils.substring(person.getFirstName(), 0, 1));
        //photo.setImageResource(R.drawable.contact);
        char firstChar = person.getFirstName().toUpperCase().charAt(0);
        photoButton.setText("" + firstChar);
        //photo.setImageResource(R.drawable.contact);
        photoButton.setBackgroundColor(Utils.getColorResource(context, firstChar));

        //}


        name.setText(Utils.getFormattedName(person));


        if (isLastChild) {
            view.findViewById(R.id.item_child_divider).setVisibility(View.INVISIBLE);
            view.findViewById(R.id.item_child_divider).getLayoutParams().height = 5;
        } else {
            view.findViewById(R.id.item_child_divider).setVisibility(View.VISIBLE);
        }

        if (group != null) {
            ImageView deleteIcon = (ImageView) view.findViewById(R.id.person_remove_from_gp);
            deleteIcon.setVisibility(View.VISIBLE);
            deleteIcon.setTag(person);
            deleteIcon.setOnClickListener(this);
        }

        view.setTag(person);
        return view;

    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        if (group != null) {
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View view) {
        try {
            Person person = (Person) view.getTag();
            GroupDao dao = MasterDBManager.getInstance().getGroupDao();
            List<Group> list = new ArrayList<>();
            list.add(group);
            dao.removePersonFromGroups(person, list);
            items.remove(person);
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
