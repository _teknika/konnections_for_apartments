package com.smargav.northernsky.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 25/04/15.
 */
public enum BloodGroup {
    A_POSITIVE("A+"), A_NEGATIVE("A-"), A1_POSITIVE("A1+"), B_POSITIVE("B+"), B_NEGATIVE("B-"), A1B_POSITIVE("A1B+"), A1B_NEGATIVE("A1B-"), A2B_POSITIVE("A2B+"),AB_POSITIVE("AB+"), AB_NEGATIVE("AB-"), O_POSITIVE("O+"), O_NEGATIVE("O-"),;

    private String name;

    private BloodGroup(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static List<String> getValues() {
        List<String> list = new ArrayList<>();
        for (BloodGroup value : values()) {
            list.add(value.getName());
        }
        return list;
    }
}
