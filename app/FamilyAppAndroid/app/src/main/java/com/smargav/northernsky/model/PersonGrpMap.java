package com.smargav.northernsky.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.smargav.northernsky.db.PersonGrpMapDao;

/**
 * Created by amu on 13/05/15.
 */
@DatabaseTable(daoClass = PersonGrpMapDao.class)
public class PersonGrpMap {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField(foreign = true, columnName = "group_id")
    private Group group;

    @DatabaseField(foreign = true, columnName = "person_id")
    private Person person;

    public PersonGrpMap() {

    }

    public PersonGrpMap(Group gp, Person p) {
        group = gp;
        person = p;
        id = gp.getId() + "_" + p.getId();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}

