package com.smargav.northernsky.ui.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.WindowManager;

import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.ui.fragments.PersonsListFragment;
import com.smargav.northernsky.ui.fragments.SearchFormFragment;
import com.smargav.northernsky.R;

import java.util.List;

public class SearchActivity extends BaseActivity {

    private SearchFormFragment searchFormFragment;
    private PersonsListFragment personListFrag;
    private Fragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        loadSearchFragment();

    }


    public void onBackPressed() {
        if (fragment == searchFormFragment) {
            finish();
            super.onBackPressed();
        } else {
            loadSearchFragment();
        }

    }

    public void loadResults(List<Person> persons) {
        GlobalData.searchPersonList = persons;
        loadResultsFragment();
    }

    private void loadSearchFragment() {
        if (searchFormFragment == null) {
            searchFormFragment = new SearchFormFragment();
        }

        fragment = searchFormFragment;
        getSupportActionBar().setTitle("Search People");
        getFragmentManager().beginTransaction().replace(R.id.frame, searchFormFragment).commit();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        mDrawerToggle.setDrawerIndicatorEnabled(true);
//
//        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                loadSearchFragment();
//            }
//        });
    }

    private void loadResultsFragment() {
        if (personListFrag == null) {
            personListFrag = new PersonsListFragment();
        }

        fragment = personListFrag;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getSupportActionBar().setTitle("Found " + GlobalData.searchPersonList.size() + " People");
        getFragmentManager().beginTransaction().replace(R.id.frame, personListFrag).commit();
//        mDrawerToggle.setDrawerIndicatorEnabled(false);
    }


    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_search, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        //noinspection SimplifiableIfStatement
//        switch (id) {
//            case android.R.id.home:
//                if (fragment instanceof PersonsListFragment) {
//                    loadSearchFragment();
//                    return true;
//                }
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
