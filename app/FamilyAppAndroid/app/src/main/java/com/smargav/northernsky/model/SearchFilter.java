package com.smargav.northernsky.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 22/05/15.
 */
public class SearchFilter {

    private String firstName;
    private String lastName;
    private List<Integer> gender = new ArrayList<>();
    private long ageFrom = Integer.MIN_VALUE;
    private long ageTo = Integer.MIN_VALUE;
    private List<String> bloodGroups = new ArrayList<>();
    private List<String> businessCategories = new ArrayList<>();
    private String businessOccupation;
    private String address;
    private String flatNum;
    private boolean isAndSearch;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public long getAgeFrom() {
        return ageFrom;
    }

    public void setAgeFrom(long ageFrom) {
        this.ageFrom = ageFrom;
    }

    public long getAgeTo() {
        return ageTo;
    }

    public void setAgeTo(long ageTo) {
        this.ageTo = ageTo;
    }

    public List<String> getBloodGroups() {
        return bloodGroups;
    }

    public void setBloodGroups(List<String> bloodGroups) {
        this.bloodGroups = bloodGroups;
    }

    public List<String> getBusinessCategories() {
        return businessCategories;
    }

    public void setBusinessCategories(List<String> businessCategories) {
        this.businessCategories = businessCategories;
    }

    public String getBusinessOccupation() {
        return businessOccupation;
    }

    public void setBusinessOccupation(String businessOccupation) {
        this.businessOccupation = businessOccupation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isAndSearch() {
        return isAndSearch;
    }

    public void setIsAndSearch(boolean isAndSearch) {
        this.isAndSearch = isAndSearch;
    }


    public List<Integer> getGender() {
        return gender;
    }

    public void setGender(List<Integer> gender) {
        this.gender = gender;
    }

    public String getFlatNum() {
        return flatNum;
    }

    public void setFlatNum(String flatNum) {
        this.flatNum = flatNum;
    }
}
