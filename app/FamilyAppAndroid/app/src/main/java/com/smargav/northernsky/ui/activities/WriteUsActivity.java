package com.smargav.northernsky.ui.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.api.utils.ToastUtils;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.utils.Utils;
import com.smargav.northernsky.R;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by amu on 27/03/16.
 */
public class WriteUsActivity extends BaseActivity {

    private TextView from;
    private EditText title;
    private EditText message;

    private Person user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_writeus);
        user = PreferencesUtil.get(this, GlobalData.USER_INFO, Person.class);
        from = (TextView) findViewById(R.id.write_from);
        from.setText("From: " + user.getFirstName() + " " + user.getLastName());

        title = (EditText) findViewById(R.id.write_title);
        message = (EditText) findViewById(R.id.write_message);
    }

    public void sendMessage(View view) {
        String t = title.getText().toString();
        String m = message.getText().toString();

        if (StringUtils.isBlank(t) || StringUtils.isBlank(m)) {
            ToastUtils.showCenteredToast(this, "Please enter title and message");
            return;
        }

        new SendMessageTask(this).execute(t, m, ("Name: " + user.getFirstName() + " " + user.getLastName() + " <<>> ID: " + user.getId()));

    }

    private class SendMessageTask extends ProgressAsyncTask<String, Integer> {

        public SendMessageTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... strings) {
            try {
                MasterAPIs.sendMessage(strings);
                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return FAILED;
        }

        @Override
        public void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == SUCCESS) {
                Utils.showPrompt(WriteUsActivity.this, "Success", "Thank you for writing us. We will get back to you soon", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                }, new int[]{R.string.alert_ok});

                return;

            } else {
                Utils.showPrompt(WriteUsActivity.this, "Error", "Error occurred while sending message. Please try again. ", true);
            }
            super.onPostExecute(result);
        }
    }
}
