package com.smargav.northernsky.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.j256.ormlite.stmt.UpdateBuilder;
import com.smargav.northernsky.R;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.Contact;
import com.smargav.northernsky.model.Person;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.sql.SQLException;

/**
 * Created by amu on 27/04/15.
 */
public class Utils {
    public static void fetchContactFromPhone(ContentResolver resolver, Person person) {
        Cursor cursor = null;
        try {
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(person.getMobileNumber()));
            cursor = resolver.query(uri, new String[]{ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.PHOTO_THUMBNAIL_URI, ContactsContract.PhoneLookup.LOOKUP_KEY}, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToNext();
                person.setContactId(cursor.getString(0));
                person.setThumbnailUri(cursor.getString(1));
                person.setLookupKey(cursor.getString(2));
            }
        } catch (Exception e) {
            // e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }

    public static String getFormattedName(Person person) {

//        if (StringUtils.isBlank(mName)) {
//            mName = "";
//        } else {
//            mName = " " + mName;
//        }

            String lName = person.getLastName();
            if (StringUtils.isBlank(lName)) {
                lName = "";
            } else {
                lName = " " + StringUtils.trim(lName);
            }

            return String.format("%s%s", StringUtils.trim(person.getFirstName()), lName);

    }

    public static String getFormattedDate(long time) {
        return DateFormatUtils.format(time, "dd-MMM-yyyy");
    }

    public static void fillPersonPhoto(ImageView photo, Person person) {
//        if (StringUtils.isNotBlank(person.getThumbnailUri()) && StringUtils.isNumeric(person.getContactId())) {
//            Uri uri = ContactsContract.Contacts.getLookupUri(Long.parseLong(person.getContactId()), person.getLookupKey());
//            photo.setImageURI(Uri.parse(person.getThumbnailUri()));
//        } else {
        photo.setImageResource(R.drawable.contact);
        //}
    }


    public static int getColorResource(Context ctx, char alphabet) {
        int[] array = ctx.getResources().getIntArray(R.array.photo_colors);
        return array[alphabet % (array.length)];
    }

    public static void fetchLargePhotoUri(ContentResolver resolver, String contactId) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contactId));
        Cursor cursor = resolver.query(uri, new String[]{ContactsContract.PhoneLookup.PHOTO_URI}, null, null, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
    }

    public static String getFormattedAddress(Contact contact) {
        String l = contact.getLandmark();
        String[] addr = {
                contact.getDoor(),
                contact.getCross(),
                contact.getStreet(),
                contact.getArea(),
                (StringUtils.isNotBlank(l) ? "Landmark: " : "") + l,
                contact.getCity(),
                contact.getState(),
                contact.getPincode()};

        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < addr.length; i++) {
            String line = addr[i];
            if (StringUtils.isNotBlank(line) && StringUtils.length(line) > 1) {
                buffer.append(line);
                if (i != addr.length - 1 && buffer.length() > 1) {
                    buffer.append(", ");
                }
            }
        }

        String finalStr = StringUtils.trim(buffer.toString());
        if (StringUtils.endsWith(finalStr, ",")) {
            finalStr = StringUtils.substring(finalStr, 0, finalStr.length() - 1);
        }

        if (StringUtils.isBlank(finalStr)) {
            return "";
        }

        return finalStr;
    }

    public static void showPrompt(Activity ctx, String title, String message, boolean b) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View customTitleView = ctx.getLayoutInflater().inflate(R.layout.dialog_title, null);
        builder.setCancelable(false);
        ((TextView) customTitleView.findViewById(R.id.dialog_title)).setText(title);
        builder.setCustomTitle(customTitleView).setMessage(message).setPositiveButton("OK", null);
        final Dialog dialog = builder.create();
        customTitleView.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public static void showPrompt(final Activity ctx, String title, String message,
                                  DialogInterface.OnClickListener listener, int[] buttons) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View customTitleView = ctx.getLayoutInflater().inflate(R.layout.dialog_title, null);
        TextView titleText = ((TextView) customTitleView.findViewById(R.id.dialog_title));
        titleText.setText(title);
        builder.setCustomTitle(customTitleView).setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(buttons[0], listener);
        if (buttons.length == 2) {
            builder.setNegativeButton(buttons[1], listener);
        }
        if (buttons.length == 3) {
            builder.setNeutralButton(buttons[2], listener);
        }

        final Dialog dialog = builder.create();
        customTitleView.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showYesNoPrompt(final Activity ctx, String title, String message,
                                       DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View customTitleView = ctx.getLayoutInflater().inflate(R.layout.dialog_title, null);
        ((TextView) customTitleView.findViewById(R.id.dialog_title)).setText(title);
        builder.setCancelable(false);
        builder.setCustomTitle(customTitleView).setMessage(message).setPositiveButton("Yes", listener)
                .setNegativeButton("No", listener);
        final Dialog dialog = builder.create();
        customTitleView.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static boolean toggleFavoriteStatus(Person person) {
        PersonDao personDao = MasterDBManager.getInstance().getPersonDao();
        UpdateBuilder<Person, Integer> updater = personDao.updateBuilder();
        try {
            updater.updateColumnValue("favorite", person.isFavorite());
            updater.where().eq("id", person.getId());
            if (personDao.update(updater.prepare()) == 1)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

//    public static int sortByDob(Person person, Person t1) {
//        try {
//            String d1 = person.getDob();
//            d1 = StringUtils.replace(d1, "-", "/");
//            String d2 = t1.getDob();
//            d2 = StringUtils.replace(d2, "-", "/");
//            LocalDate ld = DateTimeFormat.forPattern("dd/MM/yyyy").parseLocalDate(d1);
//            LocalDate ld2 = DateTimeFormat.forPattern("dd/MM/yyyy").parseLocalDate(d2);
//            return ld.compareTo(ld2);
//        } catch (Exception e) {
//            //e.printStackTrace();
//        }
//        return 0;
//    }
}
