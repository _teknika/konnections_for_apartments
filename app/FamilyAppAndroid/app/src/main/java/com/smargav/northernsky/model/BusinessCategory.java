package com.smargav.northernsky.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.smargav.northernsky.db.CategoryDao;

/**
 * Created by amu on 25/04/15.
 */

@DatabaseTable(daoClass = CategoryDao.class)
public class BusinessCategory {

    @DatabaseField(id = true)
    private String name;

    public BusinessCategory() {

    }

    public BusinessCategory(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BusinessCategory that = (BusinessCategory) o;

        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
