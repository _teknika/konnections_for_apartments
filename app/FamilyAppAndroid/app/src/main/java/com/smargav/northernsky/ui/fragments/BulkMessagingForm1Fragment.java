package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.smargav.northernsky.R;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.ui.activities.BulkMessagingActivity;
import com.smargav.northernsky.ui.activities.GlobalData;
import com.smargav.northernsky.utils.CustomRelativeLayout;
import com.smargav.northernsky.utils.Utils;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by amu on 04/06/15.
 */
public class BulkMessagingForm1Fragment extends Fragment {

    private ListView list;
    private SelectedPersonDisplayAdapter adapter;
    private TextView totalSelected;
    private List<Person> persons;
    private CheckBox toggleAllSelection;
    private ViewGroup rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_bulk_1_selection, container, false);
        setupList();
        setHasOptionsMenu(true);
        return rootView;
    }

    private void setupList() {
        list = (ListView) rootView.findViewById(R.id.list);
        persons = GlobalData.searchPersonList;

        for (Person p : persons) {
            p.setSelected(true);
        }

        totalSelected = (TextView) rootView.findViewById(R.id.bulk_total_selected);
        totalSelected.setText(persons.size() + " Selected");

        toggleAllSelection = (CheckBox) rootView.findViewById(R.id.messaging_toggle_selection);

        toggleAllSelection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                for (Person p : persons) {
                    p.setSelected(b);
                }
                adapter.notifyDataSetChanged();
                updateCount();
            }
        });

        adapter = new SelectedPersonDisplayAdapter(getActivity(), persons);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CustomRelativeLayout rel = ((CustomRelativeLayout) view);
                rel.toggle();
                Person p = (Person) rel.getTag();
                p.setSelected(rel.isChecked());
                updateCount();
            }
        });
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.bulk_sms_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        ((BulkMessagingActivity) getActivity()).goToReview(id);
        return super.onOptionsItemSelected(item);
    }


    private void updateCount() {

        int count = 0;
        for (Person p : persons) {
            count += (p.isSelected() ? 1 : 0);
        }
        totalSelected.setText(count + " Selected");

    }


    public class SelectedPersonDisplayAdapter extends BaseAdapter {

        private List<Person> items;
        private Context context;

        public SelectedPersonDisplayAdapter(Context ctx, List<Person> items) {
            this.items = items;
            this.context = ctx;
        }


        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int i) {
            return items.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Person person = (Person) getItem(i);

            if (view == null) {
                view = View.inflate(context, R.layout.item_person_w_checkbox, null);
            }

            Button photo = (Button) view.findViewById(R.id.person_photo);
            TextView name = (TextView) view.findViewById(R.id.person_name);
            TextView email = (TextView) view.findViewById(R.id.person_email);
            TextView phone = (TextView) view.findViewById(R.id.person_mobile);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);

            if (StringUtils.isNotBlank(person.getThumbnailUri())) {
                photo.setBackground(Drawable.createFromPath(Uri.parse(person.getThumbnailUri()).getPath()));
            } else {
                char firstChar = person.getFirstName().toUpperCase().charAt(0);
                photo.setText("" + firstChar);
                photo.setBackgroundColor(Utils.getColorResource(context, firstChar));
            }

            name.setText(Utils.getFormattedName(person));
            phone.setText(person.getMobileNumber());
            email.setText(person.getEmailId());

            if (StringUtils.isBlank(person.getMobileNumber())) {
                phone.setVisibility(View.GONE);
            }

            if (StringUtils.isBlank(person.getEmailId())) {
                email.setVisibility(View.GONE);
            }

            //checkBox.setChecked(person.isSelected());
            ((CustomRelativeLayout) view).setChecked(person.isSelected());

            view.setTag(person);
            return view;
        }
    }

}
