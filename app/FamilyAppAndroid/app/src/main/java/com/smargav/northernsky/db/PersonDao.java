package com.smargav.northernsky.db;

import android.content.Context;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.smargav.api.logger.AppLogger;
import com.smargav.northernsky.model.Contact;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.model.SearchFilter;
import com.smargav.northernsky.utils.AppConstants;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by amu on 26/04/15.
 */
public class PersonDao extends BaseDaoImpl<Person, Integer> {

    //private String[] minColumnSet = {"id", "firstName", "lastName", "thumbnailUri", "contactId", "lookupKey", "mobileNumber", "email", "businessCategory", "businessOccupation"};
    private String[] minColumnSet = {"id", "firstName", "lastName", "mobileNumber", "emailId", "businessCategory", "businessOccupation", "relationWithHead","flatNo","dateOfMarriage","altnumber","altMail","blockName","customAttr"};

    public PersonDao(ConnectionSource connSource, Class<Person> dataClass) throws SQLException {
        super(connSource, dataClass);
    }

    public static boolean saveData(Context ctx, List<Person> persons) throws Exception {


        ContactDao cDao = MasterDBManager.getInstance().getContactDao();
        PersonDao dao = MasterDBManager.getInstance().getPersonDao();

        Iterator<Person> it = persons.iterator();

        while (it.hasNext()) {
            try {
                Person p = it.next();

//                processBirthDate(p);
                dao.createOrUpdate(p);
                AppConstants.savePersonData(p);
                if (p.getContacts() != null) {

                    for (Contact c : p.getContacts()) {
                        c.setPerson(p);
                        CreateOrUpdateStatus status = cDao.createOrUpdate(c);
                        if (!status.isCreated() && !status.isUpdated()) {
                            return false;
                        }

//                        if (c.getType() == Contact.ContactType.OFFICE) {
//                            if (StringUtils.isNotBlank(c.getName())) {
//                                p.setBizName(c.getName());
//                            }
                            if (StringUtils.isNotBlank(c.getEmail())) {
                                p.setEmailId(c.getEmail());
                            }
//                        }

                    }
                }
                dao.createOrUpdate(p);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;

    }

//    private static void processBirthDate(Person p) {
//        try {
//            String dob = p.getDob();
//            p.setBirthDate(DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(dob).getMillis());
//        } catch (Exception e) {
//        }
//    }

    public List<Person> getAllHeadsList() {

        List<Person> list = new ArrayList<Person>();

        try {
            QueryBuilder<Person, Integer> builder = queryBuilder();
            builder.where().eq("parentId", "-1");

            builder.selectColumns(minColumnSet);
            list = builder.query();
            Collections.sort(list, new PersonSorter());
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }


    public void refreshPersonInfo(Person person) throws SQLException {
        refresh(person);
        person.setMembers(getFamilyMembers(person));
    }


    public List<Person> getFamilyMembers(Person person) {
        List<Person> list = new ArrayList<Person>();
        try {
            QueryBuilder<Person, Integer> builder = queryBuilder();
            builder.where().eq("parentId", person.getId());
            builder.selectColumns(minColumnSet);
            list = builder.query();
            Collections.sort(list, new PersonSorter());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Person> getAllFavorites() {
        List<Person> list = new ArrayList<Person>();
        try {
            QueryBuilder<Person, Integer> builder = queryBuilder();
            builder.where().eq("favorite", true);
            builder.selectColumns(minColumnSet);
            list = builder.query();
            Collections.sort(list, new PersonSorter());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public List<Person> getFilteredList(SearchFilter filter) {
        List<Person> list = new ArrayList<Person>();
        try {

            String clause = "";

            if (filter.isAndSearch()) {
                clause = getORQuery(filter, "AND");
            } else {
                clause = getORQuery(filter, "OR");
            }

            String query = "SELECT `id`,`firstName`,`lastName`,`flatNo` FROM `person` WHERE (" + clause + ")";
            AppLogger.i(getClass(), "Manual - " + query);

            GenericRawResults<Person> results = queryRaw(query, new RawRowMapper<Person>() {
                public Person mapRow(String[] columnNames,
                                     String[] str) {
                    Person person = new Person();
                    person.setId(str[0]);
                    person.setFirstName(str[1]);
                    person.setLastName(str[2]);
                    person.setFlatNo(str[3]);
                    return person;
                }
            });


            Iterator<Person> it = results.iterator();
            List<Person> persons = new ArrayList<>();
            while (it.hasNext()) {
//                String[] str = it.next();
//                Person person = new Person();
//                person.setId(Integer.parseInt(str[0]));
//                person.setFirstName(str[1]);
//                person.setMiddleName(str[2]);
//                person.setLastName(str[3]);
//                person.setMobileNumber(str[4]);
                persons.add(it.next());
            }

            Collections.sort(persons, new PersonSorter());
            if (!persons.isEmpty()) {
                return persons;
            }
//            QueryBuilder<Person, Integer> builder = queryBuilder();
//            Where<Person, Integer> where = builder.where();
//            if (filter.isAndSearch()) {
//                populateAndFields(where, filter);
//            } else {
//                populateOrFields(where, filter);
//            }
//            builder.selectColumns("id", "firstName", "middleName", "lastName", "mobileNumber");
//            AppLogger.i(getClass(), "Query - " + builder.prepare().getStatement());
//            list = builder.query();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

//    private void populateAndFields(Where<Person, Integer> where, SearchFilter filter) throws SQLException {
//
//    }
//
//    private void populateOrFields(Where<Person, Integer> where, SearchFilter filter) throws SQLException {
//
//
//        Where<Person, Integer> or = where.or(5);
//        filter = preProcessFilter(filter);
//
//        Date from = DateUtils.addYears(new Date(), ((int) -filter.getAgeFrom()));
//        Date to = DateUtils.addYears(new Date(), ((int) -filter.getAgeTo()));
//
//        if (StringUtils.isNotBlank(filter.getFirstName())) {
//            or.like("firstName", "%" + filter.getFirstName() + "%");
//        }
//
//
//        if (StringUtils.isNotBlank(filter.getLastName())) {
//            or.like("lastName", "%" + filter.getLastName() + "%");
//        }
//
//
//        if (!filter.getBloodGroups().isEmpty()) {
//            or.in("bloodGroup", filter.getBloodGroups());
//        }
//
//        if (!filter.getBusinessCategories().isEmpty()) {
//            //filter.setBusinessCategories(BusinessCategory.getValues());
//            or.in("businessCategory", filter.getBusinessCategories());
//        }
//
//
//        if (filter.getAgeFrom() != Integer.MIN_VALUE) {
//            to = DateUtils.addYears(new Date(), -100);
//        } else if (filter.getAgeTo() != Integer.MIN_VALUE) {
//            from = DateUtils.addYears(new Date(), 0);
//        }
//
//        or.between("dob", to.getTime(), from.getTime());
//
//        if (StringUtils.isNotBlank(filter.getBusinessOccupation())) {
//            or.like("businessOccupation", "%" + filter.getBusinessOccupation() + "%");
//        }
//
//
////        where.or(
////                where.like("firstName", filter.getFirstName()),
////                where.like("lastName", filter.getLastName()),
////                where.in("bloodGroup", filter.getBloodGroups()),
////                where.like("businessOccupation", filter.getBusinessOccupation()),
////                where.in("businessCategory", filter.getBusinessCategories()),
////                where.between("dob", filter.getAgeFrom(), filter.getAgeTo()),
////                where.in("gender", gender)
////        );
//
//    }

    private String getORQuery(SearchFilter filter, String type) {
        StringBuffer buffer = new StringBuffer();

        if (StringUtils.isNotBlank(filter.getFirstName())) {
            buffer.append("`firstName` LIKE '%" + filter.getFirstName() + "%'");
        }

        if (StringUtils.isNotBlank(filter.getFlatNum())) {
            buffer.append("`flatNo` LIKE '%" + filter.getFlatNum() + "%'");
        }
        if (!filter.getGender().isEmpty()) {
            String str = "`gender` IN (";
            Iterator<Integer> it = filter.getGender().iterator();
            while (it.hasNext()) {
                int gender = it.next();
                str = str + " '" + gender + "'";
                if (it.hasNext())
                    str = str + ", ";

            }

            str = str + " )";

            buffer.append(" " + type + " ");
            buffer.append(str);
        }


        if (StringUtils.isNotBlank(filter.getLastName())) {
            buffer.append(" " + type + " ");
            buffer.append("`lastName` LIKE '%" + filter.getLastName() + "%'");
        }

        if (!filter.getBloodGroups().isEmpty()) {

            String str = "`bloodGroup` IN (";
            Iterator<String> it = filter.getBloodGroups().iterator();
            while (it.hasNext()) {
                String bg = it.next();
                str = str + " '" + bg + "'";
                if (it.hasNext())
                    str = str + ", ";
            }

            str = str + " )";
            buffer.append(" " + type + " ");
            buffer.append(str);
        }


        if (!filter.getBusinessCategories().isEmpty()) {
            String str = "`businessCategory` IN (";
            Iterator<String> it = filter.getBusinessCategories().iterator();
            while (it.hasNext()) {
                String biz = it.next();
                str = str + " '" + biz + "'";
                if (it.hasNext())
                    str = str + ", ";
            }

            str = str + " )";
            buffer.append(" " + type + " ");
            buffer.append(str);
        }


        Date from = DateUtils.addYears(new Date(), ((int) -filter.getAgeFrom()));
        Date to = DateUtils.addYears(new Date(), ((int) -filter.getAgeTo()));


        if (filter.getAgeFrom() != Integer.MIN_VALUE && filter.getAgeTo() != Integer.MIN_VALUE) {
            buffer.append(" " + type + " ");
            buffer.append("`birthDate` BETWEEN " + to.getTime() + " AND " + from.getTime());
        } else if (filter.getAgeFrom() != Integer.MIN_VALUE) {
            to = DateUtils.addYears(new Date(), -100);
            buffer.append(" " + type + " ");
            buffer.append("`birthDate` BETWEEN " + to.getTime() + " AND " + from.getTime());
        } else if (filter.getAgeTo() != Integer.MIN_VALUE) {
            from = DateUtils.addYears(new Date(), 0);
            buffer.append(" " + type + " ");
            buffer.append("`birthDate` BETWEEN " + to.getTime() + " AND " + from.getTime());
        }


        if (StringUtils.isNotBlank(filter.getBusinessOccupation())) {
            buffer.append(" " + type + " ");
            buffer.append("`businessOccupation` LIKE '%" + filter.getBusinessOccupation() + "%'");
        }

        //OR and AND is inserted in the front if firstName is not available.
        // So checking it and removing the "type"
        String query = buffer.toString();
        if (query.startsWith(" " + type)) {
            query = StringUtils.removeStart(query, " " + type);
        }

        if (query.endsWith(type + " ")) {
            query = StringUtils.removeEnd(query, " " + type);
        }

        return query;
    }

    public void deleteAll() throws Exception {
        delete(deleteBuilder().prepare());
    }

    public List<Person> queryBizCategory(String param) throws Exception {
        return queryForEq("businessCategory", param);
    }

    private class PersonSorter implements Comparator<Person> {

        @Override
        public int compare(Person person, Person person2) {
            String fName1 = "" + person.getFirstName();
            String fName2 = "" + person2.getFirstName();
            return fName1.compareToIgnoreCase(fName2);
        }
    }

}
