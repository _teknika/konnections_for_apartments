package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.smargav.api.logger.AppLogger;
import com.smargav.northernsky.R;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.ui.activities.GlobalData;
import com.smargav.northernsky.ui.activities.PersonDetailsActivity;
import com.smargav.northernsky.ui.adapter.PersonGridAdapter;

import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 26/04/15.
 */
public class PersonsGridFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ContentResolver resolver;
    private PersonDao personDao;
    private View stickyHeader;

    private ListView gridView;
    private PersonGridAdapter adapter;
    private SearchView search;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_person_grid, container, false);
        resolver = getActivity().getContentResolver();
        personDao = MasterDBManager.getInstance().getPersonDao();
        loadData(rootView);
        setupSearchBar(rootView);
        return rootView;
    }

    private void setupSearchBar(ViewGroup rootView) {
        search = (SearchView) rootView.findViewById(R.id.search_bar);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        search.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        search.setIconifiedByDefault(false);
        search.setQueryHint("Search Head of Family");
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                adapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                AppLogger.i(getClass(), "Search: " + s);
                adapter.getFilter().filter(s);
                return false;
            }
        });
        search.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.getFilter().filter("");
                return false;
            }
        });


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.frag_list_grid_menu, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_frag_search) {
            toggleSearchView();
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggleSearchView() {
        if (search.isShown()) {
            search.setVisibility(View.GONE);
        } else {
            search.setVisibility(View.VISIBLE);
        }
    }

    private void loadData(ViewGroup rootView) {
        stickyHeader = rootView.findViewById(R.id.sticky_header);
        gridView = (ListView) rootView.findViewById(R.id.list);

//        PersonDao dao = MasterDBManager.getInstance().getPersonDao();
//        List<Person> persons = GlobalData.fullPersonsList;
//        if (persons == null) {
//            persons = dao.getAllHeadsList();
//            GlobalData.fullPersonsList = persons;
//        }

        List<Person> persons = GlobalData.searchPersonList;

        adapter = new PersonGridAdapter(getActivity(), persons) {
            public Filter getFilter() {
                return new AllPersonFilter(GlobalData.fullPersonsList);
            }
        };

        gridView.setOnItemClickListener(this);
        gridView.setAdapter(adapter);
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    changeStickyHeaderText("A");
                    return;
                }

                View view = gridView.getChildAt(0);
                if (view != null && view.getId() == R.id.item_person_layout) {
                    String text = ((TextView) view.findViewById(R.id.person_name)).getText().toString();
                    changeStickyHeaderText(StringUtils.substring(text, 0, 1));
                }
            }
        });
    }


    private void changeStickyHeaderText(String tag) {
        ((TextView) stickyHeader.findViewById(R.id.item_group_name)).setText(tag);
        stickyHeader.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Person person = (Person) view.getTag();
        try {
            personDao.refreshPersonInfo(person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        GlobalData.selectedPerson = person;
        startActivity(new Intent(getActivity(), PersonDetailsActivity.class));

    }


    private class AllPersonFilter extends Filter {

        private List<Person> masterPersons;

        private int totalCount = 0;

        public AllPersonFilter(List<Person> masterPersons) {
            this.masterPersons = masterPersons;
            this.totalCount = masterPersons.size();

        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            if (charSequence == null || charSequence.length() < 3) {
                results.values = masterPersons;
                results.count = masterPersons.size();
                return results;
            }

            //do filtering
            List<Person> list = new ArrayList<>();
            String searchText = charSequence.toString();
            for (Person p : masterPersons) {
                if (StringUtils.containsIgnoreCase(p.getFirstName(), searchText)|| StringUtils.containsIgnoreCase(p.getLastName(), searchText) || StringUtils.contains(p.getMobileNumber(), searchText)) {
                    list.add(p);
                }
            }

            if (list.isEmpty()) {
                results.values = masterPersons;
                results.count = masterPersons.size();
            } else {
                results.count = list.size();
                results.values = list;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            List<Person> persons = (List<Person>) filterResults.values;
            //if (!persons.isEmpty())
            adapter.notifyDataSetChanged(persons);
            adapter.notifyDataSetInvalidated();

        }

    }
}
