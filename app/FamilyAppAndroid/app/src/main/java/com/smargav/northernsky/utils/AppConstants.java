package com.smargav.northernsky.utils;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.smargav.api.utils.GsonUtil;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.City;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.model.State;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Amit S on 09/01/16.
 */
public class AppConstants {
    public static final String REGISTRATION_ID = "regId";
    public static final String APP_UPDATE_RESULT = "appUpdateResult";
    public static final String BANNERS_LIST = "bannersList";
    public static final String LAST_NOTIFICATION_TIME = "lastNotificationTime";

    public static void saveCities(Context ctx, List<City> cities) {
        try {
            Dao<City, Integer> dao = MasterDBManager.getInstance().getCityDao();
            for (City c : cities) {
                dao.createOrUpdate(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveStates(Context ctx, List<State> states) {
        try {
            Dao<State, Integer> dao = MasterDBManager.getInstance().getStateDao();
            for (State c : states) {
                dao.createOrUpdate(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean savePersonData(Person person) throws SQLException {
        PersonDao personDao = MasterDBManager.getInstance().getPersonDao();
        person.setCustomAttr(null);
        person.setCustomAttr(GsonUtil.gson.toJson(person));

        Dao.CreateOrUpdateStatus status = personDao.createOrUpdate(person);

        return status.isCreated() || status.isUpdated();
    }
}
