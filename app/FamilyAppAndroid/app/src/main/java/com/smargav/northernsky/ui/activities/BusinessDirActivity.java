package com.smargav.northernsky.ui.activities;

import android.content.Context;
import android.os.Bundle;

import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.utils.ToastUtils;
import com.smargav.northernsky.R;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.ui.fragments.BusinessDirListFragment;
import com.smargav.northernsky.ui.fragments.CategoriesListFragment;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class BusinessDirActivity extends BaseActivity {

    private BusinessDirListFragment listFragment;
    private CategoriesListFragment categoriesListFragment;
    private boolean isListShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory_listing);


    }

    public void searchUsers(String category) {
        new DataLoaderTask(this).execute(category);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadCategoriesFragment();

    }

    private void loadCategoriesFragment() {
        setTitle("Business Categories");
        if (categoriesListFragment == null) {
            categoriesListFragment = new CategoriesListFragment();
        }
        getFragmentManager().beginTransaction().replace(R.id.frame, categoriesListFragment).commit();
        isListShown = false;
    }

    private void loadResultFragment() {
        setTitle("Search Results");
        if (listFragment == null) {
            listFragment = new BusinessDirListFragment();
        }
        getFragmentManager().beginTransaction().replace(R.id.frame, listFragment).commit();
        isListShown = true;
    }


    @Override
    public void onBackPressed() {
        if (isListShown) {
            loadCategoriesFragment();
        } else {
            super.onBackPressed();
        }

    }

    private class DataLoaderTask extends ProgressAsyncTask<String, Integer> {

        private List<Person> persons = new ArrayList<>();

        public DataLoaderTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                PersonDao dao = MasterDBManager.getInstance().getPersonDao();
                if (StringUtils.endsWithIgnoreCase("Show All", params[0])) {
                    GlobalData.searchPersonList = dao.getAllHeadsList();
                } else {
                    GlobalData.searchPersonList = dao.queryBizCategory(params[0]);
                }

                if (GlobalData.searchPersonList == null || GlobalData.searchPersonList.isEmpty()) {
                    return NO_RESULT;
                }
                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }

            GlobalData.searchPersonList = persons;
            return FAILED;
        }

        public void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == FAILED || result == NO_RESULT) {
                ToastUtils.showCenteredToast(ctx, "No user found under this category.");
                return;
            }

            loadResultFragment();


        }


    }


}
