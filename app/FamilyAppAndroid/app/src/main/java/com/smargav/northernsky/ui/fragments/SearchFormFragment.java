package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.widgets.MultiSelectionSpinner;
import com.smargav.api.widgets.RangeSeekBar;
import com.smargav.northernsky.R;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.model.SearchFilter;
import com.smargav.northernsky.ui.activities.SearchActivity;
import com.smargav.northernsky.utils.Utils;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class SearchFormFragment extends Fragment implements View.OnClickListener {

    private View rootView;
    private EditText searchFirstName;
    private RangeSeekBar ageRangeSeeker;
    private MultiSelectionSpinner businessCategorySpinner;
    private EditText businessOccupationText;
    private MultiSelectionSpinner bloodGroupSpinner;
    private EditText addressText;

    private final String SEARCH_FILTER = "searchFilter";

    private SearchFilter filter = new SearchFilter();
    private EditText searchLastName;
    private MultiSelectionSpinner searchGender;

    public SearchFormFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_search, container, false);

        setupSearchForm();
        setHasOptionsMenu(true);
        return rootView;
    }

    private View get(int id) {
        return rootView.findViewById(id);
    }

    private void setupSearchForm() {
        searchFirstName = (EditText) get(R.id.search_first_name);
        searchLastName = (EditText) get(R.id.search_last_name);
//        searchGender = (MultiSelectionSpinner) get(R.id.search_gender);
//
//        ageRangeSeeker = (RangeSeekBar) get(R.id.search_age_range);
//        bloodGroupSpinner = (MultiSelectionSpinner) get(R.id.search_blood_grp);
//        bloodGroupSpinner.setItems(BloodGroup.getValues());
//
//        businessCategorySpinner = (MultiSelectionSpinner) get(R.id.search_business_category);
//        try {
//            businessCategorySpinner.setItems(MasterDBManager.getInstance().getCategoryDao().getAllCategories());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        businessOccupationText = (EditText) get(R.id.search_business_occupation);
        addressText = (EditText) get(R.id.search_address);

        get(R.id.search_all_button).setOnClickListener(this);
        prePopulate();
    }

    private void prePopulate() {

        searchFirstName.setText(filter.getFirstName());
        searchLastName.setText(filter.getLastName());
//        List<String> list = new ArrayList<>();
//        if (filter.getGender().contains(Person.FEMALE)) {
//            list.add("Female");
//        }
//
//        if (filter.getGender().contains(Person.MALE)) {
//            list.add("Male");
//        }

//        searchGender.setSelection(list);

//        if (filter.getAgeFrom() != Integer.MIN_VALUE)
//            ageRangeSeeker.setSelectedMinValue(filter.getAgeFrom());
//        if (filter.getAgeTo() != Integer.MIN_VALUE)
//            ageRangeSeeker.setSelectedMaxValue(filter.getAgeTo());


//        bloodGroupSpinner.setSelection(filter.getBloodGroups());
//        businessCategorySpinner.setSelection(filter.getBusinessCategories());
//        businessOccupationText.setText(filter.getBusinessOccupation());
        addressText.setText(filter.getAddress());

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.frag_search_menu, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_frag_search) {
            filter.setIsAndSearch(true);
            new SearchAsyncTask(getActivity()).execute();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {


        filter.setIsAndSearch(true);

        new SearchAsyncTask(getActivity()).execute();

    }

    private SearchFilter populateFilter() {

//        filter.setGender(searchGender.getSelectedIndicies());
        filter.setFirstName(searchFirstName.getText().toString());
        filter.setLastName(searchLastName.getText().toString());

//        int from = ageRangeSeeker.getSelectedMinValue().intValue();


//        if (from > 0) {
//            filter.setAgeFrom(from);
//        } else {
//            filter.setAgeFrom(Integer.MIN_VALUE);
//        }
//
//        int to = ageRangeSeeker.getSelectedMaxValue().intValue();
//
//        if (to < 100) {
//            filter.setAgeTo(to);
//        } else {
//            filter.setAgeTo(Integer.MIN_VALUE);
//        }
//
//        AppLogger.i(getClass(), "Age - " + from + " : " + to);

//Corner case if from =0 but to <100 .. and vice-versa
//        if (from == 0 && to < 100) {
//            filter.setAgeFrom(from);
//        }
//
//        if (to == 100 && from > 0) {
//            filter.setAgeTo(to);
//        }


//        filter.setBloodGroups(bloodGroupSpinner.getSelectedStrings());
//        filter.setBusinessCategories(businessCategorySpinner.getSelectedStrings());
//        filter.setBusinessOccupation(businessOccupationText.getText().toString());
        filter.setFlatNum(addressText.getText().toString());

        return filter;
    }

    private class SearchAsyncTask extends ProgressAsyncTask<String, Integer> {

        private final int NO_RESULTS = -100;
        private List<Person> results;

        public SearchAsyncTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                SearchFilter filter = populateFilter();

                //PreferencesUtil.put(ctx, SEARCH_FILTER, filter);
                PersonDao dao = MasterDBManager.getInstance().getPersonDao();
                results = dao.getFilteredList(filter);

                if (results.isEmpty()) {
                    return NO_RESULTS;
                }

                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return FAILED;
        }

        public void onPostExecute(Integer result) {
            super.onPostExecute(result);

            switch (result) {
                case SUCCESS:
                    ((SearchActivity) getActivity()).loadResults(results);
                    break;
                case FAILED:
                    Utils.showPrompt(getActivity(), "Error", "Some error occured while fetching results.", true);
                    break;
                case NO_RESULTS:
                    Utils.showPrompt(getActivity(), "No Results", "No results found for the query.", true);
                    break;
            }
        }
    }


}
