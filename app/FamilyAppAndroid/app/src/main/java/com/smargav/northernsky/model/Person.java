package com.smargav.northernsky.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.smargav.northernsky.db.PersonDao;
import com.smargav.northernsky.utils.Utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static com.smargav.northernsky.R.array.gender;

/**
 * Created by amu on 25/04/15.
 */
@DatabaseTable(daoClass = PersonDao.class)
public class Person {

    public static final int HEAD = 1;
    public static final int MEMBER = 0;

    public static final int MALE = 1;
    public static final int FEMALE = 0;

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private int parentId;

    @DatabaseField
    private String firstName;

    @DatabaseField
    private String lastName;


    @DatabaseField
    private int isHead = 0;

    @ForeignCollectionField(foreignFieldName = "person", eager = true)
    private ForeignCollection<Contact> contactsList;

    private List<Contact> contacts;

    @DatabaseField
    private String businessCategory;

    @DatabaseField
    private String businessOccupation;

    //family ic_committee
    @DatabaseField
    private String relationWithHead;

    private List<Person> members = new ArrayList<Person>();

    //More personal details.

    @DatabaseField
    private String dateOfMarriage;


    @DatabaseField
    private String mobileNumber;

    @DatabaseField
    private String altnumber;

    @DatabaseField
    private String photo;

    @DatabaseField
    private String emailId;

    @DatabaseField
    private String altMail;

    @DatabaseField
    private int orgId;

    @DatabaseField
    private int role;

    @DatabaseField
    private int userId;
    //Android ContactID
    @DatabaseField
    private String contactId;

    //Android URI
    @DatabaseField
    private String thumbnailUri;

    //Android Lookup Key
    @DatabaseField
    private String lookupKey;

    @DatabaseField
    private boolean favorite;

    private boolean selected = true;

    @DatabaseField
    private String blockName;

    @DatabaseField
    private String flatNo;

    @DatabaseField
    private String flatHolder;

    @DatabaseField
    private String dob;

    @DatabaseField
    private String privacy_mask1;

    @DatabaseField
    private String customAttr;

    private PrivacyMask privacy_mask;



//    privacyMaskObj

    public Person() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public ForeignCollection<Contact> getContactsList() {
        return contactsList;
    }

    public void setContactsList(ForeignCollection<Contact> contactsList) {
        this.contactsList = contactsList;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }


    public String getDateOfMarriage() {
        return dateOfMarriage;
    }

    public void setDateOfMarriage(String dateOfMarriage) {
        this.dateOfMarriage = dateOfMarriage;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public List<Person> getMembers() {
        return members;
    }

    public void setMembers(List<Person> members) {
        this.members = members;
    }

    public void addMember(Person member) {
        this.members.add(member);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusinessOccupation() {
        if (StringUtils.isBlank(businessOccupation)) {
            return "-NA-";
        }
        return businessOccupation;
    }

    public void setBusinessOccupation(String businessOccupation) {
        this.businessOccupation = businessOccupation;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getThumbnailUri() {
        return thumbnailUri;
    }

    public void setThumbnailUri(String thumbnailUri) {
        this.thumbnailUri = thumbnailUri;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public String getBusinessCategory() {

        if (StringUtils.isBlank(businessCategory)) {
            return "Business/Service";
        }
        return businessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        this.businessCategory = businessCategory;
    }



    public String getRelationWithHead() {
        return relationWithHead;
    }

    public void setRelationWithHead(String relationWithHead) {
        this.relationWithHead = relationWithHead;
    }



    public int getIsHead() {
        return isHead;
    }

    public void setIsHead(int isHead) {
        this.isHead = isHead;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getGender() {
        return gender;
    }



    public String toString() {
        return Utils.getFormattedName(this);
    }


    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }



    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAltnumber() {
        return altnumber;
    }

    public void setAltnumber(String altnumber) {
        this.altnumber = altnumber;
    }

    public String getAltMail() {
        return altMail;
    }

    public void setAltMail(String altMail) {
        this.altMail = altMail;
    }

    public String getFlatHolder() {
        return flatHolder;
    }

    public void setFlatHolder(String flatHolder) {
        this.flatHolder = flatHolder;
    }

//    public PrivacyMask getPrivacyMaskObj() {
//        return privacy_mask;
//    }
//
//    public void setPrivacyMaskObj(PrivacyMask privacyMaskObj) {
//        this.privacy_mask = privacyMaskObj;
//    }
//
//    public String getPrivacy_mask() {
//        return privacy_mask1;
//    }
//
//    public void setPrivacy_mask(String privacy_mask) {
//        this.privacy_mask1 = privacy_mask;
//    }

    public String getCustomAttr() {
        return customAttr;
    }

    public void setCustomAttr(String customAttr) {
        this.customAttr = customAttr;
    }

    public String getPrivacy_mask1() {
        return privacy_mask1;
    }

    public void setPrivacy_mask1(String privacy_mask1) {
        this.privacy_mask1 = privacy_mask1;
    }

    public PrivacyMask getPrivacy_mask() {
        return privacy_mask;
    }

    public void setPrivacy_mask(PrivacyMask privacy_mask) {
        this.privacy_mask = privacy_mask;
    }


}
