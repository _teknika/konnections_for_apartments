package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.smargav.northernsky.R;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.ui.activities.BulkMessagingActivity;
import com.smargav.northernsky.ui.activities.GlobalData;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 04/06/15.
 */
public class BulkMessagingForm2Fragment extends Fragment {

    private ViewGroup rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_bulk_2_review, container, false);

        setupForm();
        return rootView;
    }

    private void setupForm() {
        String message = getArguments().getString("message");
        int messageType = getArguments().getInt("messageType");

        ((TextView) rootView.findViewById(R.id.bulk_message_text)).setText(message);

        Button sendButton = (Button) rootView.findViewById(R.id.next_button);

        if (messageType == R.id.menu_bulk_email) {
            sendButton.setText("Send Email");
        } else {
            sendButton.setText("Send SMS");
        }

        List<String> list = new ArrayList<>();
        for (Person person : GlobalData.searchPersonList) {
            if (!person.isSelected()) {
                continue;
            }
            String name = person.getFirstName() + " " + StringUtils.substring(StringUtils.trim(person.getLastName()), 0, 1);
            String info = "";
            if (messageType == R.id.menu_bulk_sms) {
                info = person.getMobileNumber();
            } else {
                info = person.getEmailId();
            }

            list.add(name + " - " + info);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);
        ((ListView) rootView.findViewById(R.id.bulk_total_selected)).setAdapter(adapter);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BulkMessagingActivity) getActivity()).sendMessage();
            }
        });
    }


}
