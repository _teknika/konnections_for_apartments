package com.smargav.northernsky.db;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.smargav.northernsky.model.PersonGrpMap;

import java.sql.SQLException;

/**
 * Created by amu on 26/04/15.
 */
public class PersonGrpMapDao extends BaseDaoImpl<PersonGrpMap, String> {

    public PersonGrpMapDao(ConnectionSource connSource, Class<PersonGrpMap> dataClass) throws SQLException {
        super(connSource, dataClass);
    }


}
