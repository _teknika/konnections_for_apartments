package com.smargav.northernsky.ui.fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.northernsky.R;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.ui.activities.GlobalData;
import com.smargav.northernsky.ui.activities.SetupActivity;
import com.smargav.northernsky.utils.Utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by amu on 26/04/15.
 */
public class SetupForm1Fragment extends Fragment {


    private EditText phoneNumber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_setup_1, container, false);

        setupForm(rootView);
        return rootView;
    }

    private void setupForm(ViewGroup rootView) {
        // setExitTransition(TransitionU);

        phoneNumber = (EditText) rootView.findViewById(R.id.setup_phone);

        phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null && phoneNumber.getError() != null)
                    phoneNumber.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        rootView.findViewById(R.id.setup_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = phoneNumber.getText().toString();
                if (StringUtils.isBlank(number) || StringUtils.length(number) < 10) {
                    phoneNumber.setError("Mobile number is invalid");
                    return;
                }

                confirmToSendSMS(number);

            }
        });
    }

    private void confirmToSendSMS(final String number) {
        Utils.showPrompt(getActivity(), "Confirm Number", "Click OK to receive OTP on " + number, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == Dialog.BUTTON_POSITIVE) {
                    new SetupAsyncTask(getActivity()).execute(number);
                }
            }
        }, new int[]{R.string.alert_ok, R.string.alert_cancel});

    }

    private class SetupAsyncTask extends ProgressAsyncTask<String, Integer> {

        private Person person;
        private String number;
        private String message = "Unknown error occured. Please try again";

        public SetupAsyncTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... strings) {

            try {
                number = strings[0];
                Person person = MasterAPIs.downloadPersonDetail(number);

                if (person == null || !StringUtils.equals(number, person.getMobileNumber())) {
                    message = "No user found with this Phone number. Please check";
                    return FAILED;
                }

//                String regKey = GCMRegister.getRegId(ctx);
//                if (regKey == null) {
//                    GCMRegister.registerToGCM(ctx);
//                    regKey = GCMRegister.getRegId(ctx);
//                }
//
                String otp = MasterAPIs.generateOTP(number);

                if (otp == null) {
                    message = "Failed to send OTP to your number. Please try again";
                    return FAILED;
                }

                PreferencesUtil.putString(getActivity(), GlobalData.OTP, otp);
                PreferencesUtil.putString(getActivity(), GlobalData.PHONE_NUMBER_KEY, number);
                PreferencesUtil.put(getActivity(), GlobalData.USER_INFO, person);
                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return FAILED;
        }

        public void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == FAILED) {
                Utils.showPrompt(getActivity(), "Error", message, true);
                return;
            }

            ((SetupActivity) getActivity()).goToStep2();

        }
    }

}
