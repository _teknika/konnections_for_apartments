package com.smargav.northernsky.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by amu on 25/04/15.
 */
public enum RelationshipType {

    HEAD("NULL"),
    FATHER("Father"),
    MOTHER("Mother"),
    WIFE("Wife"),
    BROTHER("Brother"),
    SISTER("Sister"),
    SON("Son"),
    DIL("Daughter-in-Law"),
    DAUGHTER("Daughter"),
    GS("Grand Son"),
    GD("Grand Daughter"),
    BIL("Brother-in-Law"),
    SIL("Sister-in-Law"),
    GRAND_MOTHER("Grand Mother"),
    NEPHEW("Nephew"),
    NIECE("Niece"),
    UNKNOWN("unknown");

    private String name;

    private RelationshipType(String name) {
        this.name = name;
    }

    private static Map<String, RelationshipType> map = new HashMap<>();

    static {
        map.put(RelationshipType.HEAD.name, RelationshipType.HEAD);
        map.put(RelationshipType.WIFE.name, RelationshipType.WIFE);
        map.put(RelationshipType.FATHER.name, RelationshipType.FATHER);
        map.put(RelationshipType.MOTHER.name, RelationshipType.MOTHER);
        map.put(RelationshipType.BROTHER.name, RelationshipType.BROTHER);
        map.put(RelationshipType.SISTER.name, RelationshipType.SISTER);
        map.put(RelationshipType.SON.name, RelationshipType.SON);
        map.put(RelationshipType.DIL.name, RelationshipType.DIL);
        map.put(RelationshipType.DAUGHTER.name, RelationshipType.DAUGHTER);
        map.put(RelationshipType.GS.name, RelationshipType.GS);
        map.put(RelationshipType.GD.name, RelationshipType.GD);
        map.put(RelationshipType.BIL.name, RelationshipType.BIL);
        map.put(RelationshipType.SIL.name, RelationshipType.SIL);
        map.put(RelationshipType.GRAND_MOTHER.name, RelationshipType.GRAND_MOTHER);
        map.put(RelationshipType.NEPHEW.name, RelationshipType.NEPHEW);
        map.put(RelationshipType.NIECE.name, RelationshipType.NIECE);


    }

    public static RelationshipType get(String name) {
        RelationshipType type = map.get(name);
        if (type == null) {
            return UNKNOWN;
        }
        return type;
    }
}
