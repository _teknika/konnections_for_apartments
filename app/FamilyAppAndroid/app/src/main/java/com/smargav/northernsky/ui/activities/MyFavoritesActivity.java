package com.smargav.northernsky.ui.activities;

import android.os.Bundle;

import com.smargav.northernsky.R;
import com.smargav.northernsky.ui.fragments.FavoritesListFragment;

public class MyFavoritesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_favorites);

        getFragmentManager().beginTransaction().replace(R.id.frame, new FavoritesListFragment()).commit();
        getSupportActionBar().setTitle("My Favorites");
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        switch (id) {
//            case android.R.id.home:
//                finish();
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }


}
