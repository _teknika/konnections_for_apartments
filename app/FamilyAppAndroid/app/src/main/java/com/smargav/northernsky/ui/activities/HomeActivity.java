package com.smargav.northernsky.ui.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.reflect.TypeToken;
import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.api.utils.DialogUtils;
import com.smargav.api.widgets.RemoteImageView;
import com.smargav.northernsky.R;
import com.smargav.northernsky.gcm.GCMRegister;
import com.smargav.northernsky.model.AdBanner;
import com.smargav.northernsky.model.AppUpdate;
import com.smargav.northernsky.model.Article;
import com.smargav.northernsky.model.Contact;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.ui.fragments.HomeGridFragment;
import com.smargav.northernsky.utils.AppConstants;
import com.smargav.northernsky.utils.Utils;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class HomeActivity extends BaseActivity {


    private List<AdBanner> bannersList = new ArrayList<AdBanner>();
    private int currentItem = 0;
    private AdBanner currentBanner = null;
//    private RemoteImageView adImage = null;
    ImageView adImage;
    private static final long SCROLL_DELAY = 7000;
    private List<Article> eventList = new ArrayList<>();
    private MenuItem menuItem;
    private Handler handler = new Handler();
     static Dialog dialogBox = null;
    private Runnable bannerScroller = new Runnable() {

        @Override
        public void run() {
            if (bannersList == null || bannersList.isEmpty()) {
                handler.postDelayed(bannerScroller, SCROLL_DELAY * 2);
                new BannersDownloader().execute();
                return;
            }
            if (currentItem >= bannersList.size() - 1) {
                currentItem = 0;
            } else {
                currentItem++;
            }
            currentBanner = bannersList.get(currentItem);
            bannerImage();
            handler.postDelayed(bannerScroller, SCROLL_DELAY);
        }

    };

    public void bannerImage(){
        Glide.with(this).load(currentBanner.getBannerPath());
        String banner = currentBanner.getBannerPath();
        Glide.with(this)
                .load(banner) // Image URL
                .into(adImage);
//                adImage.setImageUrl(banner);
//            adImage.loadImage();
        YoYo.with(Techniques.ZoomInUp).duration(800).playOn(adImage);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        getFragmentManager().beginTransaction().replace(R.id.frame, new HomeGridFragment()).commit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        new UpdateChecker().execute();

//        new WeatherTask().execute();
        String mobilenum =PreferencesUtil.getString(this, GlobalData.PHONE_NUMBER_KEY,"");
        if (mobilenum.length()>0) {
            new SetupAsyncTask(this).execute(mobilenum);
        }
        new LoadTask(this).execute();

        setupBanners();

        new BannersDownloader().execute();

        setTitle("NorthernSky City");
    }

    private void newNotifications(){

        String lastNotificationTime = PreferencesUtil.getString(HomeActivity.this,AppConstants.LAST_NOTIFICATION_TIME,"");
        for (int i =0;i<eventList.size();i++){

            try {
                String listTime = eventList.get(i).getTransactionTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = null;
                Date date1 = null;
                date = sdf.parse(lastNotificationTime);
                date1 = sdf.parse(listTime);
                long listedTime = date1.getTime();
                long startDate = date.getTime();
                if (listedTime>startDate){
                    Drawable drawable = menuItem.getIcon();
                    if(drawable != null) {
                        drawable.mutate();
                        drawable.setColorFilter(getResources().getColor(R.color.amber_50), PorterDuff.Mode.SRC_ATOP);
                    }
                    Toast.makeText(this,"Received new Notification",Toast.LENGTH_SHORT).show();
                }


            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

    }



    private void setupBanners() {
//        adImage = (RemoteImageView) findViewById(R.id.ad_image);
        adImage = (ImageView) findViewById(R.id.ad_image);

//        adImage.setErrorDrawable(getResources().getDrawable(R.drawable.ns_city_logo));
        adImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (currentBanner != null) {
                        String description = currentBanner.getDescription();
                        if (Patterns.WEB_URL.matcher(description.trim()).matches()) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(description));
                            startActivity(intent);
                        } else {
                            DialogUtils.showPrompt(HomeActivity.this, "Ad Details", description, true);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        //bannersList = getStaticBanners();
    }

    @Override
    public void onResume() {
        super.onResume();
        handler.postDelayed(bannerScroller, SCROLL_DELAY);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(bannerScroller);
    }

    private class UpdateChecker extends AsyncTask<Void, Void, Void> {

        private boolean isUpdateAvailable = false;

        @Override
        protected Void doInBackground(Void... arg0) {

            try {
                GCMRegister.registerToGCM(HomeActivity.this);
                String regId = PreferencesUtil.getString(HomeActivity.this, AppConstants.REGISTRATION_ID, null);
                if (regId != null) {
                    String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    Person user = PreferencesUtil.get(HomeActivity.this, GlobalData.USER_INFO, Person.class);
                    MasterAPIs.updateGcm(regId, user.getId(),user.getMobileNumber(),deviceId);

                }
                isUpdateAvailable = MasterAPIs.isUpdateAvailable(HomeActivity.this);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(Void v) {
            super.onPostExecute(v);
            if (isUpdateAvailable) {
                if (PreferencesUtil.contains(HomeActivity.this, AppConstants.APP_UPDATE_RESULT)) {
                    AppUpdate details = PreferencesUtil.get(HomeActivity.this, AppConstants.APP_UPDATE_RESULT,
                            AppUpdate.class);
                    if (!details.isCritical()) {
                        Utils.showYesNoPrompt(HomeActivity.this, "Update Available",
                                "We have added more exciting features. Click to update the App.", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if (i == DialogInterface.BUTTON_POSITIVE) {
                                            Intent in = new Intent(Intent.ACTION_VIEW);
                                            in.setData(Uri.parse(MasterAPIs.PLAY_STORE_URL + getPackageName()));
                                            startActivity(in);
                                        }
                                    }
                                });
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.base_menu, menu);
       menuItem = menu.findItem(R.id.menu_refresh);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_refresh:
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.clockwise);
                 startActivity(new Intent(this, ArticlesActivity.class));

                break;
            case R.id.menu_call:
                Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(MasterAPIs.CALL_NUMBER));
                startActivity(tel);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

//    private class WeatherTask extends AsyncTask<Void, Void, Weather> {
//
//
//        @Override
//        protected void onPostExecute(Weather weather) {
//            super.onPostExecute(weather);
//            if (weather != null) {
//                ((TextView) findViewById(R.id.weather)).setText("Temp: " + (weather.getMain().temp) + "\u00B0 C");
//            }
//        }
//
//        @Override
//        protected Weather doInBackground(Void... voids) {
//            try {
//                return MasterAPIs.getWeather();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }


    private class BannersDownloader extends AsyncTask<Void, Void, List<AdBanner>> {
        @Override
        protected void onPostExecute(List<AdBanner> adBanners) {
            super.onPostExecute(adBanners);
            bannersList = GlobalData.banners;
            if (bannersList != null) {
                setupBanners();
            }
        }

        @Override
        protected List<AdBanner> doInBackground(Void... voids) {
            try {
                if (!com.smargav.api.utils.Utils.hasNetwork(HomeActivity.this)) {
                    GlobalData.banners = PreferencesUtil.get(HomeActivity.this, AppConstants.BANNERS_LIST, new TypeToken<List<AdBanner>>() {
                    }.getType());
                    return GlobalData.banners;
                }
                if (GlobalData.banners == null) {
                    GlobalData.banners = MasterAPIs.getBanners();
                    if (GlobalData.banners != null) {
                        PreferencesUtil.put(HomeActivity.this, AppConstants.BANNERS_LIST, GlobalData.banners);
                    }
                    return GlobalData.banners;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    private class SetupAsyncTask extends ProgressAsyncTask<String, Integer> {

        private Person person;
        private String number;
        private String message = "No internet connection. Please check the connection and try again";
//        private String message = "Unknown error occured. Please try again";

        public SetupAsyncTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... strings) {

            try {
                number = strings[0];
                Person person = MasterAPIs.downloadPersonDetail(number);

                if (person == null || !StringUtils.equals(number, person.getMobileNumber())) {
                    message = "No user found with this Phone number. Please check with Admin";
                    return FAILED;
                }

//                String regKey = GCMRegister.getRegId(ctx);
//                if (regKey == null) {
//                    GCMRegister.registerToGCM(ctx);
//                    regKey = GCMRegister.getRegId(ctx);
//                }
////
//                String otp = MasterAPIs.generateOTP(number);
//
//                if (otp == null) {
//                    message = "Failed to send OTP to your number. Please try again";
//                    return FAILED;
//                }

//                PreferencesUtil.putString(HomeActivity.this, GlobalData.OTP, otp);
                PreferencesUtil.putString(HomeActivity.this, GlobalData.PHONE_NUMBER_KEY, number);
                PreferencesUtil.put(HomeActivity.this, GlobalData.USER_INFO, person);
//                PreferencesUtil.put(getActivity(), AppConstants.CHURCH_REGISTRATION,person);
                Contact contact = new Contact();
                contact.setPerson(person);

                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return FAILED;
        }

        public void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (result == FAILED) {
                showYesNoPrompt(HomeActivity.this, "Error",
                       message, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (i == DialogInterface.BUTTON_POSITIVE) {
                                    dialogBox.dismiss();
                                    finish();
                                }
                            }
                        });


                return;
            }


        }
    }
    public static void showYesNoPrompt(final Activity ctx, String title, String message,
                                       DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View customTitleView = ctx.getLayoutInflater().inflate(R.layout.dialog_title, null);
        ((TextView) customTitleView.findViewById(R.id.dialog_title)).setText(title);
        builder.setCancelable(false);
        builder.setCustomTitle(customTitleView).setMessage(message).setPositiveButton("Ok", listener)
                ;
        dialogBox = builder.create();
        customTitleView.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialogBox.dismiss();

            }
        });
        dialogBox.show();
    }


    private class LoadTask extends ProgressAsyncTask<String, Integer> {
        public LoadTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... strings) {
            try {
                eventList = MasterAPIs.getArticles();
                if (eventList.isEmpty()) {
                    return NO_RESULT;
                }

                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return FAILED;
        }

        @Override
        public void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == SUCCESS) {
                newNotifications();

            } else if (result == NO_RESULT) {
//                DialogUtils.showPrompt(HomeActivity.this, "No Notifications", "There are no new notifications as of now.", false);
            } else {
                DialogUtils.showPrompt(HomeActivity.this, "Error loading", "Unable to download list.", true);
            }
        }
    }
}

