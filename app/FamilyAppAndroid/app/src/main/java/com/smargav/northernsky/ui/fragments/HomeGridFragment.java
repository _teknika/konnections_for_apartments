package com.smargav.northernsky.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.northernsky.R;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.ui.activities.DirectoryActivity;
import com.smargav.northernsky.ui.activities.EditProfile;
import com.smargav.northernsky.ui.activities.GlobalData;
import com.smargav.northernsky.ui.activities.SearchActivity;
import com.smargav.northernsky.ui.activities.WebPageDisplayActivity;
import com.smargav.northernsky.ui.activities.WriteUsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 27/04/15.
 */
public class HomeGridFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ViewGroup rootView;
    private int selectedIndex;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_home_grid, container, false);

        setupList();
        return rootView;
    }

    private void setupList() {

        GridView drawerListView = (GridView) rootView.findViewById(R.id.drawer_items);

        List<NavBarItem> list = getNavBarItemsList();
        selectedIndex = findSelectedIndex(list);

        NavBarAdapter adapter = new NavBarAdapter(list, getActivity());
        drawerListView.setAdapter(adapter);

        drawerListView.setOnItemClickListener(this);

    }

    private int findSelectedIndex(List<NavBarItem> list) {
        for (int i = 0; i < list.size(); i++) {
            NavBarItem item = list.get(i);
            if (getActivity().getClass().equals(item.className)) {
                return i;
            }
        }
        return 0;
    }


    private List<NavBarItem> getNavBarItemsList() {
        List<NavBarItem> list = new ArrayList<NavBarItem>();
        list.add(new NavBarItem("Members", R.drawable.ic_member, DirectoryActivity.class));
//        list.add(new NavBarItem("My Favorites", R.drawable.ic_favorite, MyFavoritesActivity.class));
        list.add(new NavBarItem("Search", R.drawable.ic_nav_search, SearchActivity.class));
        //list.add(new NavBarItem("SMS services", R.drawable.ic_sms_service));
        list.add(new NavBarItem("Events", R.drawable.ic_events, WebPageDisplayActivity.class, MasterAPIs.EVENTS_URL));
       // list.add(new NavBarItem("Calendar", R.drawable.ic_calendar, WebPageDisplayActivity.class, MasterAPIs.CALENDAR_URL));
//        list.add(new NavBarItem("Articles", R.drawable.ic_articles, ArticlesActivity.class));
        list.add(new NavBarItem("Facilities", R.drawable.ic_magazine, WebPageDisplayActivity.class));
        list.add(new NavBarItem("Committee", R.drawable.ic_committee, WebPageDisplayActivity.class, MasterAPIs.MEMBERS_URL));
        list.add(new NavBarItem("Services",R.drawable.services,WebPageDisplayActivity.class));
        list.add(new NavBarItem("Vendor Listing",R.drawable.vendor,WebPageDisplayActivity.class,MasterAPIs.VENDOR_LISTING));
        list.add(new NavBarItem("My Account", R.drawable.rupee, WriteUsActivity.class, MasterAPIs.ABOUT_APP));
        list.add(new NavBarItem("Edit Profile",R.drawable.editprofile, EditProfile.class));
        list.add(new NavBarItem("About NorthernSky ", R.drawable.ic_aboutus, WebPageDisplayActivity.class,MasterAPIs.ABOUT_APP));

        return list;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        NavBarItem item = (NavBarItem) view.getTag();

        switch (item.icon) {

            case R.drawable.ic_directory: //directory
//            case R.drawable.ic_favorite:
            case R.drawable.ic_nav_search:
              launchScreen(item.className);
                break;
            case R.drawable.rupee:
//                Toast.makeText(getActivity(),"This feature is under development",Toast.LENGTH_SHORT).show();
                Person user1 = PreferencesUtil.get(getActivity(), GlobalData.USER_INFO, Person.class);
                String id1 = user1.getId();
                String parentId1 = String.valueOf(user1.getParentId());
                String url1 = MasterAPIs.MAINTANENCE+"userId="+id1+"&parentId="+parentId1;
                Intent intent2 = new Intent(getActivity(),WebPageDisplayActivity.class);
                intent2.putExtra("url",url1);

                startActivity(intent2);
                break;
            case R.drawable.ic_member:
                Intent intent = new Intent(getActivity(), DirectoryActivity.class);
//                intent.setAction(DirectoryActivity.ACTION_CONTACT_DETAILS);
                startActivity(intent);
//                launchScreen(item.className);


                break;
            case R.drawable.ic_magazine:
//              Toast.makeText(getActivity(),"This feature is under development",Toast.LENGTH_SHORT).show();
                Person user = PreferencesUtil.get(getActivity(), GlobalData.USER_INFO, Person.class);
                String id = user.getId();
                String parentId = String.valueOf(user.getParentId());
                String url = MasterAPIs.FACILITIES+"userId="+id+"&parentId="+parentId;
               Intent intent1 = new Intent(getActivity(),WebPageDisplayActivity.class);
               intent1.putExtra("url",url);

               startActivity(intent1);
//                launchBrowser(item.url);
                break;
            case R.drawable.ic_committee://;committee ic_committee
                launchWebPage(item);
                break;
            case R.drawable.services:
//                Toast.makeText(getActivity(),"This feature is under development",Toast.LENGTH_SHORT).show();
                Person userId = PreferencesUtil.get(getActivity(), GlobalData.USER_INFO, Person.class);
                String idString = userId.getId();
                String parentId2 = String.valueOf(userId.getParentId());
                String url2 = MasterAPIs.SERVICES+"userId="+idString+"&parentId="+parentId2;
                Intent intent3 = new Intent(getActivity(),WebPageDisplayActivity.class);
                intent3.putExtra("url",url2);
                startActivity(intent3);

                break;
            case R.drawable.vendor:
                launchWebPage(item);
//                getActivity().overridePendingTransition(R.anim.right_in,R.anim.left_out);
                break;
            case R.drawable.ic_aboutus://about
                launchWebPage(item);
                break;
//            case R.drawable.ic_articles:
            case R.drawable.ic_events:
                launchWebPage(item);
//                getActivity().overridePendingTransition(R.anim.right_in,R.anim.left_out);
                break;
            case R.drawable.editprofile:
                launchScreen(item.className);
//                startActivity(new Intent(getActivity(),EditProfile.class));
//                getActivity().finish();
            case R.drawable.ic_calendar:
                launchWebPage(item);
                break;
        }

    }

    private void launchBrowser(String url) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    private void launchWebPage(NavBarItem item) {
        Intent intent = new Intent(getActivity(), item.className);
        intent.putExtra("url", item.url);
        intent.putExtra("title", item.name);
        getActivity().startActivity(intent);

    }


    private void launchScreen(Class clazz) {
        getActivity().startActivity(new Intent(getActivity(), clazz));

    }


    private class NavBarAdapter extends BaseAdapter {

        private List<NavBarItem> NavBarItemsList = new ArrayList<NavBarItem>();
        private Activity ctx;

        public NavBarAdapter(List<NavBarItem> NavBarItemsList, Activity ctx) {
            this.NavBarItemsList = NavBarItemsList;
            this.ctx = ctx;
        }

        @Override
        public View getView(int position, View view, ViewGroup group) {

            NavBarItem e = (NavBarItem) getItem(position);

            View layout = ctx.getLayoutInflater().inflate(
                    R.layout.item_home, null);
            TextView v = ((TextView) layout.findViewById(R.id.navBarText));
            ImageView i = ((ImageView) layout.findViewById(R.id.navBarIcon));


            i.setImageResource(e.icon);
//            Animation animation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
//                    R.anim.fade);
//            i.startAnimation(animation);
//            YoYo.with(Techniques.ZoomInUp).duration(2000).playOn(i);
//            YoYo.with(Techniques.FlipInX).duration(3000).playOn(v);

            v.setText(e.name);
            layout.setTag(e);
            return layout;

        }

        @Override
        public int getCount() {
            return NavBarItemsList.size();
        }

        @Override
        public Object getItem(int position) {
            return NavBarItemsList.get(position);
        }

        @Override
        public long getItemId(int position) {

            return position;
        }
    }

    private class NavBarItem {

        public int icon;
        public String name;
        public Class className;
        public String url;

        public NavBarItem() {
        }

        public NavBarItem(String name, int icon) {
            this.icon = icon;
            this.name = name;
        }

        public NavBarItem(String name, int icon, Class c) {
            this.icon = icon;
            this.name = name;
            className = c;
        }

        public NavBarItem(String name, int icon, Class c, String url) {
            this.icon = icon;
            this.name = name;
            className = c;
            this.url = url;
        }
    }

//    private class WeatherTask extends AsyncTask<Void, Void, Weather> {
//
//
//        @Override
//        protected void onPostExecute(Weather weather) {
//            super.onPostExecute(weather);
//            if (weather != null && weather.getMain() != null) {
//                ((TextView) rootView.findViewById(R.id.weather)).setText((weather.getMain().temp) + "\u00B0 C");
//            }
//        }
//
//        @Override
//        protected Weather doInBackground(Void... voids) {
//            try {
//                return MasterAPIs.getWeather();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//    }
}

