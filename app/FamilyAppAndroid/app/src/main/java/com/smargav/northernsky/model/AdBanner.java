package com.smargav.northernsky.model;

import java.io.Serializable;

public class AdBanner implements Serializable {

    private String description;
    private String subText;
    private String bannerPath;
    private String website;
    private String mainPath;

    public AdBanner() {

    }

//    public AdBanner(String m, String g, boolean hasLink) {
//        this.description = m;
//        this.bannerPath = g;
//    }


    public AdBanner(String mainline, String subline) {
        this.description = mainline;
        this.subText = subline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubText() {
        return subText;
    }

    public void setSubText(String subText) {
        this.subText = subText;
    }

    public String getBannerPath() {
        return bannerPath;
    }

    public void setBannerPath(String bannerPath) {
        this.bannerPath = bannerPath;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMainPath() {
        return mainPath;
    }

    public void setMainPath(String mainPath) {
        this.mainPath = mainPath;
    }
}
