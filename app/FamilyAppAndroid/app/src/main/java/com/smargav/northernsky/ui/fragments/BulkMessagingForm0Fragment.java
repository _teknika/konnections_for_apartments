package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.smargav.northernsky.R;
import com.smargav.northernsky.ui.activities.BulkMessagingActivity;
import com.smargav.northernsky.utils.Utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by amu on 04/06/15.
 */
public class BulkMessagingForm0Fragment extends Fragment {


    private ViewGroup rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_bulk_0_message, container, false);

        setupForm();

        return rootView;
    }

    private void setupForm() {
        Button next = (Button) rootView.findViewById(R.id.next_button);
        final EditText textView = (EditText) rootView.findViewById(R.id.bulk_message_text);

        textView.setText(getArguments().getString("message"));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = textView.getText().toString();
                if (StringUtils.isBlank(message)) {
                    Utils.showPrompt(getActivity(), "Error", "Cannot send empty message.", true);
                    return;
                }

                ((BulkMessagingActivity) getActivity()).onMessageEntry(message);

            }
        });
    }


}
