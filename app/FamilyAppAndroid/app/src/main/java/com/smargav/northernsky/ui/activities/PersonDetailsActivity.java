package com.smargav.northernsky.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;
import com.smargav.api.logger.AppLogger;
import com.smargav.northernsky.R;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.model.RelationshipType;
import com.smargav.northernsky.ui.fragments.PersonDetailsFragment;
import com.smargav.northernsky.utils.Utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PersonDetailsActivity extends BaseActivity implements TwoWayAdapterView.OnItemClickListener {

    private Person selectedPerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_details);
        selectedPerson = GlobalData.selectedPerson;
        setTitle(Utils.getFormattedName(selectedPerson));
        //setupToolbar();
        setupActivity();
    }

    private void setupToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(Utils.getFormattedName(selectedPerson));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setupActivity() {

        TwoWayGridView gridView = (TwoWayGridView) findViewById(R.id.members_grid);
        Person selectedPerson = GlobalData.selectedPerson;
        List<Person> list = new ArrayList<Person>();
        list.add(selectedPerson);
        List<Person> members = MasterDBManager.getInstance().getPersonDao().getFamilyMembers(selectedPerson);

        Collections.sort(members, new Comparator<Person>() {
            @Override
            public int compare(Person person, Person t1) {
                RelationshipType rt1 = RelationshipType.get(person.getRelationWithHead());
                RelationshipType rt2 = RelationshipType.get(t1.getRelationWithHead());
                int result = rt1.ordinal() - rt2.ordinal();
//                if(result == 0){
//                    result = Utils.sortByDob(person, t1);
//                }
                return result;
            }
        });
        list.addAll(members);

        MembersAdapter adapter = new MembersAdapter(this, R.layout.item_person_grid_small, list);

        gridView.setAdapter(adapter);
//
        loadPersonFragment(selectedPerson);
        gridView.setOnItemClickListener(this);

    }

    private View previousView = null;

    @Override
    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
        Person person = (Person) view.getTag();
        AppLogger.i(getClass(), "Called? " + person.getFirstName());
        loadPersonFragment(person);
        if (previousView != null) {
            previousView.setBackgroundResource(android.R.color.transparent);
            ((TextView) previousView.findViewById(R.id.person_name)).setTextColor(getResources().getColor(R.color.white));
        }
        view.setBackgroundResource(R.color.person_selected_color);
        ((TextView) view.findViewById(R.id.person_name)).setTextColor(getResources().getColor(R.color.black));
        previousView = view;
    }


    private void loadPersonFragment(Person person) {

        try {
            MasterDBManager.getInstance().getPersonDao().refreshPersonInfo(person);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        PersonDetailsFragment fragment = new PersonDetailsFragment();
        fragment.setSelectedPerson(person);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, fragment)
                .commit();
    }


    private class MembersAdapter extends ArrayAdapter<Person> {

        private Context context;

        public MembersAdapter(Context context, int resource, List<Person> objects) {
            super(context, resource, objects);
            this.context = context;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Person person = (Person) getItem(i);

            if (view == null) {
                view = View.inflate(context, R.layout.item_person_grid_small, null);
            }

            ImageView photo = (ImageView) view.findViewById(R.id.person_photo);
            Utils.fillPersonPhoto(photo, person);
            //((TextView) view.findViewById(R.id.person_name)).setText(person.getFirstName());
            ((TextView) view.findViewById(R.id.person_name)).setText(Utils.getFormattedName(person));
            ((TextView) view.findViewById(R.id.person_name)).setTextColor(getResources().getColor(R.color.white));

            view.setTag(person);

            if (previousView == null) {
                previousView = view;
                view.setBackgroundResource(R.color.person_selected_color);
            }

            if (i == 0 && previousView == view) {
                ((TextView) view.findViewById(R.id.person_name)).setTextColor(getResources().getColor(R.color.black));
            }


            return view;
        }
    }

}
