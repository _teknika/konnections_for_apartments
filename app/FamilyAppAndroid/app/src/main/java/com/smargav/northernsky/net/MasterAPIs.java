package com.smargav.northernsky.net;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.smargav.api.net.WebSession;
import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.api.utils.GsonUtil;
import com.smargav.northernsky.model.AdBanner;
import com.smargav.northernsky.model.AppUpdate;
import com.smargav.northernsky.model.Article;
import com.smargav.northernsky.model.BasicInfo;
import com.smargav.northernsky.model.BusinessCategory;
import com.smargav.northernsky.model.City;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.model.State;
import com.smargav.northernsky.model.Weather;
import com.smargav.northernsky.utils.AppConstants;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by amu on 27/05/15.
 */
public class MasterAPIs {

    private static final Gson gson = new Gson();
//    35.165.225.196/northernSky
//    http://35.165.225.196/NSTEST

//    private final static String BASE_URL = "http://192.168.1.102/northernSky/server";
    private final static String BASE_URL = "http://35.165.225.196/northernSky/server";
    private final static String BASE_LINK = "http://35.165.225.196/northernSky/admin";
    private final static String SINGLE_USER_URL = BASE_URL + "/getContactByMobile.php";
//    private final static String ALL_CONTACTS_URL = BASE_URL + "/getAllContacts.php";
    private final static String ALL_CONTACTS_URL = BASE_URL +"/getContactsOfAppartment.php";

    private final static String ALL_STATES = BASE_URL + "/state.php";
    private final static String ALL_CITIES = BASE_URL + "/city.php";

    private final static String GENERATE_OTP = BASE_URL + "/otpMessage.php";
    private final static String UPDATE_GCM_ID = BASE_URL+"/updateGcm.php";
    private final static String SEND_MESSAGE = BASE_URL + "/sendMessage.php";
    private final static String GET_BANNERS = BASE_URL + "/getBanners.php";
    private final static String GET_CATEGORIES = BASE_URL + "/getCategoriesList.php";

    private final static String GET_ARTICLES = BASE_URL + "/getNotifications.php";

    public final static String EVENTS_URL = BASE_LINK+"/extra/events.html";
    public final static String AD_URL = BASE_LINK+"/extra/adv.html";
    public final static String MEMBERS_URL = BASE_LINK+"/extra/committee.html";
    public final static String ABOUT_APP = "https://www.northernsky.in/";
    public final static String CALENDAR_URL = BASE_LINK+"/extra/calendar.html";
    public final static String APP_UPDATE_URL = "http://smargav.com/applications/srmpjs/server/data/config.json";

    public final static String VENDOR_LISTING = BASE_LINK+"/extra/vendorList.html";

    public final static String PLAY_STORE_URL = "https://play.google.com/store/apps/details?id=";

    public static final String WEATHER_API = "http://api.openweathermap.org/data/2.5/weather?q=Coimbatore&appid=ea44facc8db1ea18ed29e71b2eefe7f3&units=metric";

    public final static String MAGAZINE_URL = "http://www.sindhitimes.in";

    public static final String CALL_NUMBER = "tel:+919606471124";

    public static final String FACILITIES = BASE_URL+"/bookAppointmentCalender.php?";

    public static final String MAINTANENCE = BASE_LINK+"/extra/payMaintenance.php?";

    public static final String SERVICES = BASE_LINK+"/extra/services.php?";

    public static final String EDIT_PROFILE = BASE_URL+"/editProfileApp.php";

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private static class Request {
        public String mobileNumber;
        public String orgId = "1";
        public String gcmId;
        public String userId;
        public int android = 1;
        //used by all contacts API
        public int pageNo;
        public int count;
        public String deviceId;

        //used by messages api
        public String message;
        public String title;


    }

    public static class RegRequest{
        private BasicInfo basicInfo;

        public BasicInfo getBasicInfo() {
            return basicInfo;
        }

        public void setBasicInfo(BasicInfo basicInfo) {
            this.basicInfo = basicInfo;
        }
    }

    private static class Response {
        public String otp;

    }

    public static List<City> getCities() throws Exception {
        WebSession session = new WebSession();

        String responseString = session.get(ALL_CITIES);
        NetResponse<List<City>> response = new Gson().fromJson(responseString, new TypeToken<NetResponse<List<City>>>() {
        }.getType());

        return response.getResp();
    }

    public static List<State> getStates() throws Exception {
        WebSession session = new WebSession();

        String responseString = session.get(ALL_STATES);
        NetResponse<List<State>> response = new Gson().fromJson(responseString, new TypeToken<NetResponse<List<State>>>() {
        }.getType());

        return response.getResp();
    }

    public static List<AdBanner> getBanners() throws Exception {
        WebSession session = new WebSession();
        String responseString = session.get(GET_BANNERS);
        List<AdBanner> response = new Gson().fromJson(responseString, new TypeToken<List<AdBanner>>() {
        }.getType());

        return response;
    }

    public static Person downloadPersonDetail(String number) throws Exception {
        WebSession session = new WebSession();

        NetRequest netRequest = new NetRequest();
        Request request = new Request();
        request.mobileNumber = number;

        netRequest.setRequest(request);

        String responseString = session.post(SINGLE_USER_URL, gson.toJson(netRequest));
        NetResponse<List<Person>> response = new Gson().fromJson(responseString, new TypeToken<NetResponse<List<Person>>>() {
        }.getType());

        if (response != null && response.getResp() != null && !response.getResp().isEmpty()) {
            return response.getResp().get(0);
        }
        return null;
    }


    public static String generateOTP(String number) {

        try {
            WebSession session = new WebSession();
            Request r = new Request();
            r.mobileNumber = number;

            NetRequest request = new NetRequest();
            request.setRequest(r);

            String responseString = session.post(GENERATE_OTP, gson.toJson(request));

            NetResponse<String> response = new Gson().fromJson(responseString, new TypeToken<NetResponse<String>>() {
            }.getType());
            if (response != null && StringUtils.length(response.getOtp()) >= 4) {
                return response.getOtp();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static List<Person> downloadAllContacts(String userId, int pageNo, int count) throws Exception {

        WebSession session = new WebSession();
        Request r = new Request();
        r.userId = userId;
        r.pageNo = pageNo;
        r.count = count;

        NetRequest request = new NetRequest();
        request.setRequest(r);

        String responseString = session.get(ALL_CONTACTS_URL);

        NetResponse<List<Person>> response = new Gson().fromJson(responseString, new TypeToken<NetResponse<List<Person>>>() {
        }.getType());

        return response.getResp();

    }

    public static String editProfile(BasicInfo basicInfo) throws IOException {
        WebSession session = new WebSession();
        RegRequest regRequest = new RegRequest();
        regRequest.setBasicInfo(basicInfo);
//        NetRequest request = new NetRequest();
//        request.setRequest(regRequest);
        String responseString = session.post(EDIT_PROFILE,gson.toJson(regRequest));
        Log.d("json" ,""+regRequest);
//        NetResponse<String> response = new Gson().fromJson(responseString,new TypeToken<String>(){
//        }.getType());

        return responseString;

    }

    public static boolean isUpdateAvailable(Context ctx) {
        try {
            WebSession session = new WebSession();
            String r = session.get(APP_UPDATE_URL);

            AppUpdate update = gson.fromJson(r, AppUpdate.class);
            PreferencesUtil.put(ctx, AppConstants.APP_UPDATE_RESULT, update);
            PackageInfo info = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(),
                    PackageManager.GET_META_DATA);
            if (update.getVersionCode() > info.versionCode) {
                AppUpdate details = PreferencesUtil.get(ctx, AppConstants.APP_UPDATE_RESULT, AppUpdate.class);
                details.setUpdatePending(false);
                PreferencesUtil.put(ctx, AppConstants.APP_UPDATE_RESULT, details);
            }
            return update.getVersionCode() > info.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static void updateGcm(String regId, String id,String mobileNumber,String deviceId) {
        try {
            WebSession session = new WebSession();
            Request r = new Request();
            r.gcmId = regId;
            r.userId = id;
            r.deviceId = deviceId;

            NetRequest request = new NetRequest();
            request.setRequest(r);

            String responseString = session.post(UPDATE_GCM_ID, gson.toJson(request));

            NetResponse<String> response = new Gson().fromJson(responseString, new TypeToken<NetResponse<String>>() {
            }.getType());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void sendMessage(String[] strings) throws IOException {
        String t = strings[0];
        String m = strings[1];
        String u = strings[2];
        WebSession session = new WebSession();

        NetRequest netRequest = new NetRequest();
        Request request = new Request();
        request.title = t;
        request.message = m;
        request.userId = u;

        netRequest.setRequest(request);

        String responseString = session.post(SEND_MESSAGE, gson.toJson(netRequest));

    }

    public static Weather getWeather() throws IOException {
        WebSession session = new WebSession();
        String responseString = session.get(WEATHER_API);

        Weather w = new Gson().fromJson(responseString, Weather.class);
        return w;
    }

    public static List<Article> getArticles() {
        try {
            WebSession session = new WebSession();

            String r = session.get(GET_ARTICLES, null, null);
            List<Article> response = GsonUtil.gson.fromJson(r, new TypeToken<List<Article>>() {
            }.getType());

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    public static List<BusinessCategory> getCategories() {
        try {
            WebSession session = new WebSession();

            String r = session.get(GET_CATEGORIES, null, null);
            List<BusinessCategory> response = GsonUtil.gson.fromJson(r, new TypeToken<List<BusinessCategory>>() {
            }.getType());

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }


    private String path;

    private MasterAPIs(String path) {
        this.path = path;
    }



    public String getFullPath() {
        return BASE_URL;
    }

    public URI getURI() {
        return URI.create(getFullPath());
    }

    public URI getURI(Map<String, String> params) {
        String url = getFullPath();
        for (String key : params.keySet()) {
            url = StringUtils.replaceOnce(url, "{" + key + "}", params.get(key));
        }
        return URI.create(url);
    }
}
