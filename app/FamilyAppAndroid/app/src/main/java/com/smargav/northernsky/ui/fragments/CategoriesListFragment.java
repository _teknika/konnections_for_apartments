package com.smargav.northernsky.ui.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.utils.BaseAdapter2;
import com.smargav.api.utils.DialogUtils;
import com.smargav.api.utils.Utils;
import com.smargav.northernsky.R;
import com.smargav.northernsky.db.CategoryDao;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.model.BusinessCategory;
import com.smargav.northernsky.net.MasterAPIs;
import com.smargav.northernsky.ui.activities.BusinessDirActivity;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Amit S on 01/07/16.
 */
public class CategoriesListFragment extends Fragment {
    private static List<BusinessCategory> categories;

    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        listView = new ListView(getActivity());

        listView.setPadding(10, 10, 10, 10);
        new CategoriesLoaderTask(getActivity()).execute();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ((BusinessDirActivity) getActivity()).searchUsers(((BusinessCategory) view.getTag()).getName());
            }
        });
        return listView;


    }


    private class CategoriesAdapter extends BaseAdapter2<BusinessCategory> {
        public CategoriesAdapter(Context context, List<BusinessCategory> objects) {
            super(context, objects);
        }

        @Override
        public View bindView(View view, int i) {
            return null;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.item_category, null);
            }
            BusinessCategory category = (BusinessCategory) getItem(position);
            ((TextView) convertView).setText(category.getName());
            convertView.setTag(category);

            return convertView;
        }
    }

    private class CategoriesLoaderTask extends ProgressAsyncTask<Void, Integer> {

        public CategoriesLoaderTask(Context ctx) {
            super(ctx);
        }

        @Override
        public void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == SUCCESS) {
                listView.setAdapter(new CategoriesAdapter(getActivity(), categories));
            } else {
                DialogUtils.showPrompt(getActivity(), "Error", "Error loading all categories. Please ensure internet is ON", true);
            }
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            try {


                CategoryDao dao = MasterDBManager.getInstance().getCategoryDao();
                categories = dao.queryForAll();

                if (categories == null || categories.isEmpty()) {
                    categories = MasterAPIs.getCategories();
                    for (BusinessCategory c : categories) {
                        dao.createOrUpdate(c);
                    }
                }

                if (categories == null || categories.isEmpty()) {
                    return FAILED;
                }


                Collections.sort(categories, new Comparator<BusinessCategory>() {
                    @Override
                    public int compare(BusinessCategory businessCategory, BusinessCategory t1) {
                        if (StringUtils.isBlank(businessCategory.getName()) || StringUtils.isBlank(t1.getName())) {
                            return 0;
                        }
                        return businessCategory.getName().compareTo(t1.getName());
                    }
                });


                BusinessCategory showAll = new BusinessCategory("Show All");
                if (!categories.contains(showAll)) {
                    categories.add(0, showAll);
                }

                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return FAILED;
        }
    }
}
