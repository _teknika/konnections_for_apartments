package com.smargav.northernsky.ui.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;

import com.smargav.northernsky.R;
import com.smargav.northernsky.ui.fragments.BulkMessagingForm0Fragment;
import com.smargav.northernsky.ui.fragments.BulkMessagingForm1Fragment;
import com.smargav.northernsky.ui.fragments.BulkMessagingForm2Fragment;

/**
 * Created by amu on 04/06/15.
 */
public class BulkMessagingActivity extends AppCompatActivity {

    private BulkMessagingForm0Fragment messageFragment;
    private BulkMessagingForm1Fragment listSelectionFragment;
    private BulkMessagingForm2Fragment reviewFragment;

    public String message;
    private int messageType;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_bulk_messaging);
        setupToolbar();

        loadMessageEntry();
    }

    private void setupToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Send Message");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void loadMessageEntry() {
        if (messageFragment == null) {
            messageFragment = new BulkMessagingForm0Fragment();
        }
        Bundle bundle = new Bundle();
        bundle.putString("message", message);
        messageFragment.setArguments(bundle);

        FragmentTransaction txn = getFragmentManager().beginTransaction();
        //txn.setCustomAnimations(R.anim.slide_from_right, 0);
        //txn.setCustomAnimations(android.R.animator.fade_in, R.anim.slide_to_left);
        txn.replace(R.id.frame, messageFragment).commit();
    }

    public void loadListSelection() {
        if (listSelectionFragment == null) {
            listSelectionFragment = new BulkMessagingForm1Fragment();
        }
        FragmentTransaction txn = getFragmentManager().beginTransaction();
        //txn.setCustomAnimations(R.anim.slide_from_right, android.R.animator.fade_out);
        txn.replace(R.id.frame, listSelectionFragment).commit();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    public void loadReviewFragment() {
        if (reviewFragment == null) {
            reviewFragment = new BulkMessagingForm2Fragment();
        }

        Bundle bundle = new Bundle();
        bundle.putString("message", message);
        bundle.putInt("messageType", messageType);
        reviewFragment.setArguments(bundle);
        FragmentTransaction txn = getFragmentManager().beginTransaction();
//        txn.setCustomAnimations(R.anim.slide_from_right,
//                android.R.animator.fade_out);
        txn.replace(R.id.frame, reviewFragment).commit();
    }

    public void onMessageEntry(String message) {
        this.message = message;
        loadListSelection();
    }

    public void goToReview(int messageType) {
        this.messageType = messageType;
        loadReviewFragment();
    }

    public void sendMessage() {
        finish();
    }
}
