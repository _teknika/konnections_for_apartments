package com.smargav.northernsky.ui.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smargav.northernsky.R;
import com.smargav.northernsky.db.GroupDao;
import com.smargav.northernsky.db.MasterDBManager;
import com.smargav.northernsky.model.Contact;
import com.smargav.northernsky.model.Group;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.model.PrivacyMask;
import com.smargav.northernsky.utils.Utils;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amu on 27/04/15.
 */
public class PersonDetailsFragment extends Fragment {

    private ViewGroup rootView;
    private Person person;

    private Handler handler = new Handler();
    private List<Group> partOfGroupsList;
    private List<Group> groups;
    String email,mobile,flat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (person.getParentId() == -1) {
            rootView = (ViewGroup) inflater.inflate(
                    R.layout.fragment_person_details_head, container, false);
            loadHeadLayout();
        } else {
            rootView = (ViewGroup) inflater.inflate(
                    R.layout.fragment_person_details_member, container, false);
            loadMemberLayout();
        }

        setHasOptionsMenu(true);
        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_person_details, menu);

//        MenuItem item = menu.findItem(R.id.menu_favorite);
//        if (person.isFavorite()) {
//            item.setIcon(R.drawable.ic_fav_icon_white);
//        } else {
//            item.setIcon(R.drawable.ic_favorite);
//        }
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
//            case R.id.menu_favorite:
//                person.setFavorite(!person.isFavorite());
//                Utils.toggleFavoriteStatus(person);
//                if (person.isFavorite()) {
//                    item.setIcon(R.drawable.ic_fav_icon_white);
//                } else {
//                    item.setIcon(R.drawable.ic_favorite);
//                }
//                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void loadMemberLayout() {

        fillPersonalSection();
//        fillRelationshipSection();
        fillEventsSection();
//        fillFamilyHeadName();
    }


    private void loadHeadLayout() {

        fillPersonalSection();
        fillEventsSection();
        fillBusinessSection();
//        fillAddressSection();
//        fillGroupsSection();
//        fillFatherNameSection();

    }

    private void fillFatherNameSection() {
        // ((TextView) get(R.id.person_fatherName)).setText(person.getFatherName());
    }

//    private void fillFamilyHeadName() {
//        try {
//            PersonDao dao = MasterDBManager.getInstance().getPersonDao();
//            Person head = dao.queryForId(person.getParentId());
//            ((TextView) get(R.id.person_familyHead)).setText(Utils.getFormattedName(head));
//        } catch (Exception e) {
//            get(R.id.person_famHead_section).setVisibility(View.GONE);
//        }
//
//    }

//    private void fillGroupsSection() {
//        try {
//            final MultiSelectionSpinner groupSpinner = (MultiSelectionSpinner) get(R.id.person_group_spinner);
//            GroupDao dao = MasterDBManager.getInstance().getGroupDao();
//            groups = dao.queryForAll();
//
//            List<String> groupsString = new ArrayList<String>();
//            for (Group g : groups) {
//                groupsString.add(g.getLabel());
//            }
//
//            partOfGroupsList = dao.getAllGroupsFor(person);
//
//            List<String> indices = new ArrayList<String>();
//            for (Group partOfGrp : partOfGroupsList) {
//                indices.add(partOfGrp.getLabel());
//            }
//            if (!groupsString.isEmpty())
//                groupSpinner.setItems(groupsString);
//            groupSpinner.setSelection(indices);
//            groupSpinner.setDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface dialogInterface) {
//                    List<Integer> indices = groupSpinner.getSelectedIndicies();
//                    updateGroupsForUser(indices);
//                }
//            });
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * 1. Get all selected groups.
     * 2. Update selected groups for user.
     * 3. Compare and get deleted groups.
     * 4. Remove the Map between user and group.
     *
     * @param indices
     */
    private void updateGroupsForUser(List<Integer> indices) {
        try {
            List<Group> selectedGrps = new ArrayList<Group>();
            for (Integer index : indices) {
                selectedGrps.add(groups.get(index));
            }

            GroupDao grpDao = MasterDBManager.getInstance().getGroupDao();
            grpDao.addPersonToGroups(person, selectedGrps);

            List<Group> deletedGroups = new ArrayList<>();
            for (Group group : groups) {
                if (!selectedGrps.contains(group)) {
                    deletedGroups.add(group);
                }
            }

            grpDao.removePersonFromGroups(person, deletedGroups);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

//    private void fillAddressSection() {
//        ListView contactsView = (ListView) get(R.id.person_contacts_list);
//        List<Contact> list = new ArrayList<Contact>();
//        if (!person.getContactsList().isEmpty()) {
//            list.addAll(person.getContactsList());
//            ContactsAdapter adapter = new ContactsAdapter(getActivity(), list);
//            contactsView.setAdapter(adapter);
//        } else {
//            get(R.id.person_contacts_list).setVisibility(View.GONE);
//        }
//    }

    private void fillBusinessSection() {

//        if (StringUtils.isBlank(person.getBusinessCategory()) && StringUtils.isBlank(person.getBusinessOccupation())) {
//            get(R.id.person_business_section).setVisibility(View.GONE);
//            return;
//        }

        PrivacyMask privacyMask = person.getPrivacy_mask();

        if (privacyMask != null) {
//            try {
//                JSONObject json = new JSONObject(privacy);
//                email = json.optString("email");
//                mobile = json.optString("mobileNumber");
//                flat = json.optString("flat");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        ((TextView) get(R.id.person_business_category)).setText(person.getBusinessCategory());
            String occupation = person.getBusinessOccupation();
            if (privacyMask.isEmail() == false) {
                String email1 = person.getEmailId();


                if (StringUtils.isNotBlank(email1)) {
                    occupation = occupation + "\nEmail:" + email1;
                }
                ((TextView) get(R.id.person_emailId)).setText("Email:" + email1);
            }
        }

    }

//    private void fillRelationshipSection() {
//        ((TextView) get(R.id.person_relationship)).setText(person.getRelationWithHead());
//    }

    private void fillEventsSection() {
        if (person.getDob()!=null) {
            ((TextView) get(R.id.person_birth_date)).setText(person.getDob());

            ((TextView) get(R.id.person_birth_date)).setText(person.getDob());
        }

        if (person.getDateOfMarriage() != null) {
            ((TextView) get(R.id.person_anniversary)).setText(person.getDateOfMarriage());
        } else {
            get(R.id.person_anniversary_section).setVisibility(View.GONE);
        }

    }

    private void fillPersonalSection() {

        PrivacyMask privacyMask = person.getPrivacy_mask();
        if (privacyMask == null){

            String privacyString = person.getCustomAttr();

                    try {
                JSONObject json = new JSONObject(privacyString);
                String privacyMasks = json.optString("privacy_mask");
                JSONObject object = new JSONObject(privacyMasks);
                email = object.optString("email");
                mobile = object.optString("mobileNumber");
                flat = object.optString("flat");

                        if (mobile.equals("false") ) {
                            ((TextView) get(R.id.person_phone)).setText(person.getMobileNumber());
                        }
                        if (email.equals("false")) {
                            ((TextView) get(R.id.person_emailId)).setText(person.getEmailId());

                        }
                        if (flat.equals("false")) {
                            if (person.getFlatNo().length() > 0) {
                                String flatNo = person.getFlatNo();
                                ((TextView) get(R.id.person_flatNum)).setText(flatNo);
                            }
                        }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(privacyMask != null) {

            if (privacyMask.isMobileNumber() == false) {
                ((TextView) get(R.id.person_phone)).setText(person.getMobileNumber());
            }
            if (privacyMask.isEmail() == false) {
                ((TextView) get(R.id.person_emailId)).setText(person.getEmailId());

            }
            if (privacyMask.isFlat() == false) {
                if (person.getFlatNo().length() > 0) {
                    String flatNo = person.getFlatNo();
                    ((TextView) get(R.id.person_flatNum)).setText(flatNo);
                }
            }
        }
        if (person.getBlockName().length()>0){
            ((TextView)get(R.id.person_BlockName)).setText(person.getBlockName());

        }
        ((TextView) get(R.id.person_name)).setText(Utils.getFormattedName(person));


//        ((TextView) get(R.id.person_blood_group)).setText(person.getBloodGroup());

        ImageView photo = (ImageView) get(R.id.person_photo);
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
//                Animation scaleUp = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_scale_up);
//                scaleUp.setFillAfter(true);
//                view.startAnimation(scaleUp);
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Animation scaleDown = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_scale_down);
//                        scaleDown.setFillAfter(true);
//                        view.startAnimation(scaleDown);
//                    }
//                }, 2500);

                showPhotoInDialog((ImageView) view);
            }
        });
        Utils.fillPersonPhoto(photo, person);
    }

    private void showPhotoInDialog(ImageView photo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ImageView largePhoto = (ImageView) View.inflate(getActivity(), R.layout.item_photo_large_display, null);
//        Utils.fillPersonPhoto(largePhoto, person);
        builder.setTitle(person.getFirstName());
        builder.setView(largePhoto);
        builder.create().show();
    }


    public void setSelectedPerson(Person person) {
        this.person = person;
    }


    private View get(int id) {
        return rootView.findViewById(id);
    }


    private class ContactsAdapter extends BaseAdapter {

        private List<Contact> list;
        private Context context;

        public ContactsAdapter(Context context, List<Contact> contacts) {
            this.context = context;

            list = new ArrayList<>();
            for (Contact c : contacts) {
                if (StringUtils.isNotBlank(Utils.getFormattedAddress(c))) {
                    list.add(c);
                }
            }
            //this.list = contacts;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Contact contact = (Contact) getItem(i);

            if (view == null) {
                view = View.inflate(context, R.layout.item_contact, null);
            }

            String name = contact.getType().getLabel();
            if (StringUtils.isNotBlank(contact.getName())) {
                name = contact.getType().getLabel() + " - " + contact.getName();
            }
            ((TextView) view.findViewById(R.id.contact_type)).setText(name);
            ((TextView) view.findViewById(R.id.contact_formatted)).setText(Utils.getFormattedAddress(contact));

            view.setTag(view);
            return view;
        }
    }

}
