package com.smargav.northernsky.ui.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;

import com.smargav.northernsky.model.AdBanner;
import com.smargav.northernsky.ui.fragments.BannerFragment;

import java.util.ArrayList;
import java.util.List;

public class BannerFragmentAdapter extends FragmentPagerAdapter {

    private List<AdBanner> bannersList = new ArrayList<AdBanner>();

    public BannerFragmentAdapter(List<AdBanner> bannersList, FragmentManager fm) {
        super(fm);
        this.bannersList = bannersList;
    }

    @Override
    public Fragment getItem(int position) {
        BannerFragment f = new BannerFragment();
        AdBanner b = bannersList.get(position);
        f.setBanner(b);
        Bundle args = new Bundle();
        args.putSerializable("banner", b);
        f.setArguments(args);
        return f;
        // return BannerFragment.newInstance(bannersList.get(position));
    }

    @Override
    public int getCount() {
        return bannersList.size();
    }

}