package com.smargav.northernsky.ui.activities;

import com.smargav.northernsky.model.AdBanner;
import com.smargav.northernsky.model.Group;
import com.smargav.northernsky.model.Person;

import java.util.List;

/**
 * Created by amu on 02/05/15.
 */
public class GlobalData {

    public static Person selectedPerson;
    public static Group selectedGroup;

    public static List<Person> fullPersonsList;
    public static List<AdBanner> banners;

    //By default this variable is used to store the ic_nav_search results for any query.
    //This list can then be instantly used for Bulk SMS list.
    public static List<Person> searchPersonList;

    public static final String PHONE_NUMBER_KEY = "phoneNumberKey";
    public static final String USER_INFO = "userInfo";
    public static final String APP_INITIALIZED = "appInitStatus";
    public static final String PAGE_NO = "pageNo";

    public static final String phoneNumber = null;

    public static final String ORG_ID = "1";

    public static String OTP = "otpNumber";
}
