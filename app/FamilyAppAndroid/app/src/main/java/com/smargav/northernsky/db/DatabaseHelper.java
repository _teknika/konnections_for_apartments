package com.smargav.northernsky.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.smargav.northernsky.model.Person;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    // any time you make changes to your database objects, you may have to
    // increase the database version
    private static final int DATABASE_VERSION = 3;

    private Class[] dbClasses;

    public DatabaseHelper(Context context, Class[] dbClasses, String dbName) {
        super(context, dbName, null, DATABASE_VERSION);
        this.dbClasses = dbClasses;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            createDbTables(database, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void createDbTables(SQLiteDatabase database, ConnectionSource connectionSource)
            throws java.sql.SQLException {

        for (Class clazz : dbClasses) {
            TableUtils.createTable(connectionSource, clazz);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion,
                          int newVersion) {

        if (newVersion > oldVersion){


//        for (Class clazz : dbClasses) {
            try {
                TableUtils.dropTable(connectionSource, Person.class, true);
            } catch (java.sql.SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        onCreate(db, connectionSource);

    }


}
