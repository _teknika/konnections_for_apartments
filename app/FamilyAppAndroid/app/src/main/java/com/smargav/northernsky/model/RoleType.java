package com.smargav.northernsky.model;

/**
 * Created by amu on 01/05/15.
 */
public enum RoleType {

    MANAGER,
    MEMBER,
    ADMIN
}
