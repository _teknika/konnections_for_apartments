package com.smargav.northernsky.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.smargav.api.widgets.CircleImageView;
import com.smargav.northernsky.R;
import com.smargav.northernsky.model.Person;
import com.smargav.northernsky.utils.Utils;

import java.util.List;
import java.util.Random;

/**
 * Created by amu on 29/04/15.
 */
public class PersonGridAdapter extends BaseAdapter implements Filterable {

    protected Context context;
    protected List<Person> items;
    //protected List<String> categories = new ArrayList<String>();
    //protected Map<String, List<Person>> map = new LinkedHashMap<String, List<Person>>();

    public PersonGridAdapter(Context ctx, List<Person> items) {
        this.items = items;//preProcess(items);
        this.context = ctx;
    }

    /**
     * We want grid to be in group of 3. So preprocessing to add padding item.
     */
//    private List<Person> preProcess(List<Person> items) {
//        List<Person> list = new ArrayList<>();
//
//        int charCount = 0;
//        char prevChar = ' ';
//        for (int i = 0; i < items.size(); i++) {
//            Person p = items.get(i);
//
//            char current = p.getFirstName().toUpperCase().charAt(0);
//            if (prevChar != current) {
//                AppLogger.i(getClass(), " " + prevChar + " - " + charCount);
//                if (charCount != 0 && charCount % 3 != 0) {
//                    int rem = 3 - charCount % 3;
//                    for (int j = 0; j < rem; j++) {
//                        Person dummy = new Person();
//                        dummy.setFirstName(DUMMY_NAME);
//                        list.add(dummy);
//                    }
//                }
//                prevChar = current;
//                charCount = 0;
//                list.add(p);
//                ++charCount;
//            } else {
//                list.add(p);
//                ++charCount;
//                prevChar = current;
//            }
//
//        }
//        return list;
//    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        Person person = (Person) getItem(position);

        if (view == null) {
            view = View.inflate(context, R.layout.item_person_grid, null);
        }


        CircleImageView photo = (CircleImageView) view.findViewById(R.id.person_photo);
        Button photoButton = (Button) view.findViewById(R.id.person_photo_button);

//        if (StringUtils.isNotBlank(person.getThumbnailUri()) && StringUtils.isNumeric(person.getContactId())) {
//            Uri uri = ContactsContract.Contacts.getLookupUri(Long.parseLong(person.getContactId()), person.getLookupKey());
//            photo.setImageURI(Uri.parse(person.getThumbnailUri()));
//            photo.setVisibility(View.VISIBLE);
//            photoButton.setVisibility(View.INVISIBLE);
//
//            char firstChar = person.getFirstName().toUpperCase().charAt(0);
//            photo.setBorderColor(Utils.getColorResource(context, firstChar));
//        } else {
            photo.setVisibility(View.INVISIBLE);
            photoButton.setVisibility(View.VISIBLE);
        if (person.getParentId()== -1) {
            char firstChar = person.getFirstName().toUpperCase().charAt(0);
            photoButton.setText("" + firstChar);
            //photo.setImageResource(R.drawable.contact);
            photoButton.setBackgroundColor(Utils.getColorResource(context, firstChar));
            //}
            ((TextView) view.findViewById(R.id.person_name)).setText(Utils.getFormattedName(person));
            view.setTag(person);

        }else {

           photoButton.setVisibility(View.INVISIBLE);
            ((TextView) view.findViewById(R.id.person_name)).setVisibility(View.INVISIBLE);

        }
        //fillRandomColor(photo);

        return view;

    }

    private void fillRandomColor(CircleImageView view) {
        int[] androidColors = context.getResources().getIntArray(R.array.androidcolors);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        view.setBorderColor(randomAndroidColor);
    }


    @Override
    public Filter getFilter() {
        return null;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public void notifyDataSetChanged(List<Person> list) {
        items = list;
        super.notifyDataSetChanged();
    }
}
