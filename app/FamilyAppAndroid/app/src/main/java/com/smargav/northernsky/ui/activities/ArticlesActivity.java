package com.smargav.northernsky.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.smargav.api.asynctasks.ProgressAsyncTask;
import com.smargav.api.utils.DialogUtils;
import com.smargav.api.utils.Utils;
import com.smargav.northernsky.R;
import com.smargav.northernsky.model.Article;
import com.smargav.northernsky.net.MasterAPIs;

import org.joda.time.DateTime;

import java.util.List;


public class ArticlesActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    private ListView listview;
    private List<Article> eventList;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articles);
        setTitle("Notifications");
        listview = (ListView) findViewById(R.id.events_list);
        new LoadTask(this).execute();
        return;
    }


//    public void onClickCallButton(View view) {
//
//        if (view.getTag() == null) {
//            Utils.showPrompt(this, "No Number", "No contact details are provided by this NGO.");
//            return;
//        }
//
//        String number = view.getTag().toString();
//        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
//        startActivity(intent);
//    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Article event = (Article) view.getTag();
        DialogUtils.showPrompt(this, event.getTitle(), event.getMessage(),true);
    }


    private class LoadTask extends ProgressAsyncTask<String, Integer> {
        public LoadTask(Context ctx) {
            super(ctx);
        }

        @Override
        protected Integer doInBackground(String... strings) {
            try {
                eventList = MasterAPIs.getArticles();
                if (eventList.isEmpty()) {
                    return NO_RESULT;
                }

                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return FAILED;
        }

        @Override
        public void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == SUCCESS) {
                loadList();
            } else if (result == NO_RESULT) {
                DialogUtils.showPrompt(ArticlesActivity.this, "No Notifications", "There are no new notifications as of now.", true);
            } else {
                DialogUtils.showPrompt(ArticlesActivity.this, "Error loading", "Unable to download list.", true);
            }
        }
    }

    private void loadList() {
        DisplayListAdapter adapter = new DisplayListAdapter(this, eventList);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(this);
    }


    private class DisplayListAdapter extends BaseAdapter {
        Context context;


        List<Article> articles;
        private LayoutInflater inflater = null;

        public DisplayListAdapter(Context context, List<Article> EventList) {
            // TODO Auto-generated constructor stub

            this.context = context;
            this.articles = EventList;
            inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return articles.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return articles.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            //if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_event, null);
            //}

            Article event = (Article) getItem(position);

            String stringdate = event.getTransactionTime();

            DateTime date = event.getDate();

            String[] date1 = stringdate.split("-");
            String year = date1[0];
            String month = date1[1];
            String date2 = date1[2];
            String[] date3 = date2.split(" ");
            String finalDate = date3[0];
            int convertMonth = Integer.parseInt(month);
            int convertDate= Integer.parseInt(finalDate);


            ((TextView) convertView.findViewById(R.id.event_date)).setText((convertDate > 9 ? "" : "0") + convertDate);
            ((TextView) convertView.findViewById(R.id.event_month)).setText("" + months[convertMonth]);
            ((TextView) convertView.findViewById(R.id.event_year)).setText("" + year);
            ((TextView) convertView.findViewById(R.id.event_title)).setText(event.getTitle());
            ((TextView) convertView.findViewById(R.id.event_description)).setText(Html.fromHtml(event.getMessage()));
            convertView.setTag(event);
            return convertView;
        }

    }

    private String[] months = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

}
