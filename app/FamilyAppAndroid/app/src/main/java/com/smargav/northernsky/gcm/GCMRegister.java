package com.smargav.northernsky.gcm;

import android.content.Context;
import android.content.Intent;

//import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.smargav.api.logger.AppLogger;
import com.smargav.api.prefs.PreferencesUtil;
import com.smargav.northernsky.utils.AppConstants;

import java.io.IOException;

/**
 * Created by ashwin on 18/03/15.
 */
public class GCMRegister {
    public static final String PROJECT_NUMBER = "855417236009"; // Project number refer Google console

    public static String getRegId(Context context) {
        return PreferencesUtil.getString(context, AppConstants.REGISTRATION_ID, null);
    }

    public static void registerToGCM(final Context context) {

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        String regId = null;
        try {
            regId = gcm.register(PROJECT_NUMBER);
            AppLogger.i(GCMRegister.class, "GCM : " + regId);
        } catch (IOException e) {
            AppLogger.e(GCMRegister.class, "Error Registering GCM ", e);
            e.printStackTrace();
        }
        PreferencesUtil.putString(context, AppConstants.REGISTRATION_ID, regId);
//        AppLogger.i(getClass(), "GCM ID " + regId);

    }

//    private static void registerToGCM(final Context context, final Handler handler) {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
//                String regId = null;
//                try {
//                    regId = gcm.register(PROJECT_NUMBER);
//                    final String registrationId = regId;
//                    PreferencesUtil.putString(context, PrefConstants.REGISTRATION_ID, regId);
//                    AppLogger.i(getClass(), "GCM ID: " + regId);
//                } catch (IOException e) {
//                    AppLogger.e(getClass(), "Error registering for GCM", e);
//                    e.printStackTrace();
//                }
//            }
//        }).start();
//
//    }


    public static void sendDummyNotif(Context ctx) {
        Intent intent = new Intent();
        intent.setAction("com.google.android.c2dm.intent.RECEIVE");
        intent.addCategory(ctx.getPackageName());
        intent.putExtra("message_type", "gcm");
        intent.putExtra("message", "{'title':'Title','text':'Hey.. This is from GCM', 'time':123123123123}");
        ctx.sendBroadcast(intent);

    }

}
