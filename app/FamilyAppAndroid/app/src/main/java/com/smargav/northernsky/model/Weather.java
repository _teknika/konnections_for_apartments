package com.smargav.northernsky.model;

/**
 * Created by Amit S on 21/06/16.
 * <p/>
 * <p/>
 * {
 * "coord": {
 * "lon": 77.58,
 * "lat": 12.98
 * },
 * "weather": [{
 * "id": 500,
 * "main": "Rain",
 * "description": "light rain",
 * "icon": "10d"
 * }],
 * "base": "stations",
 * "main": {
 * "temp": 296.28,
 * "pressure": 921.07,
 * "humidity": 88,
 * "temp_min": 296.28,
 * "temp_max": 296.28,
 * "sea_level": 1016.79,
 * "grnd_level": 921.07
 * },
 * "wind": {
 * "speed": 4.22,
 * "deg": 255.002
 * },
 * "rain": {
 * "3h": 0.21
 * },
 * "clouds": {
 * "all": 88
 * },
 * "dt": 1466513083,
 * "sys": {
 * "message": 0.0127,
 * "country": "IN",
 * "sunrise": 1466468693,
 * "sunset": 1466515102
 * },
 * "id": 6695236,
 * "name": "Kanija Bhavan",
 * "cod": 200
 * }
 */

public class Weather {

    public static class Data {
        public double temp;
    }

    private Data main;

    public Data getMain() {
        return main;
    }

    public void setMain(Data main) {
        this.main = main;
    }
}
