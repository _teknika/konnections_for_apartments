<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
 <link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
       
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>

         <!-- Date Range Picker -->
    <script type="text/javascript" src="theme/js/moment.min2.js"></script>
    <script type="text/javascript" src="theme/js/datepicker/daterangepicker.js"></script>

    <!-- Jquery Validate -->
   <script src="js/jquery.validate.js"></script>

    
    <script src="js/utility/editContact.js"></script>
	
    <style type="text/css" media="screen">
      .error {
         color: red;
       }
    </style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
                       <div class="">
                    <form id="editForm" name="editForm" class="form-horizontal" method="POST" action="">
                                       <div class="panel">
                                          <a class="panel-heading" role="tab" id="headingOne" data-toggle="" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                             
                                          </a>
										   <h4 class="panel-title">Basic Information</h4>
                                          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                             <div class="panel-body">
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label class="col-md-12">First Name <span class="required">*</span></label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="firstnameR" name="firstnameR"  class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                 
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Last Name<span>*</span></label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="lastnameR" name="lastnameR" class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-md-3">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Mobile  Number<span>*</span></label>
                                                         <div class="col-md-12">
                                                            <input type="number" id="mobilenumberR" minlength="10" maxlength="20" name="mobilenumberR" class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-3">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Alternative  Number</label>
                                                         <div class="col-md-12">
                                                            <input type="number" id="altNumber" minlength="10" maxlength="20" name="altNumber" class="form-control"/>
                                                         </div>
                                                      </div>
                                                   </div>


                                                      <div class="col-md-3">
                                                        <div class="form-group">
                                                          <label class="col-md-12">Email Id<span>*</span>
                                                          </label>
                                                          <div class="col-md-12">
                                                             <input type="email"   id="emailId" name="emailId" class="form-control" required/>
                                                          </div>
                                                       </div>
                                                    </div>
                                                     <div class="col-md-3">
                                                        <div class="form-group">
                                                          <label class="col-md-12">Alternative Email
                                                          </label>
                                                          <div class="col-md-12">
                                                             <input type="email"   id="altemailId" name="altemailId" class="form-control"/>
                                                          </div>
                                                       </div>
                                                    </div>
                                                </div>
                                        <div class="row">
                                         
                                          
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                          <label class="col-md-12">Flat Number<span>*</span>
                                                          </label>
                                                          <div class="col-md-12">
                                                             <input type="text"   id="flatNo" name="flatNo" class="col-xs-6 col-md-6" disabled />
                                                              
                                                              <span><i class="fa fa-plus addFlat" style="font-size:24px"></i></span>
                                                              <span><i class="fa fa-trash deleteFlat" style="font-size:24px"></i></span>
                                                          </div>
                                                       </div>
                                                    </div>
                                                     <div class="col-md-4">
                                                        <div class="form-group">
                                                          <label class="col-md-12">Flat Holder<span>*</span>
                                                          </label>
                                                          <div class="col-md-12">
                                                             <select   id="flat_holder" name="flat_holder" class="form-control">
                                                              <option value="owner">Owner</option>
                                                              <option value="tenant">Tenant</option>
                                                             </select>
                                                          </div>
                                                       </div>
                                                    </div>
                                         </div> 
                                                <div class="row">
                                                   
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Gender<span>*</span></label>
                                                         <div class="col-md-12">
                                                            <select id="genderR" name="genderR" class="form-control" required>
                                                               <option value="">-- Select the gender --</option>
                                                               <option value="1">Male</option>
                                                               <option value="0">Female</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                     <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Date of Birth</label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="dob" name="dob" class="form-control"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Date of Marriage</label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="dateofmarriageR" name="dateofmarriageR" class="form-control"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                               
                                                <div class="">
                                        <input type="submit" class="btn btn-info pull-right" value="Submit" name="submit" />
                                       </div>
                                             </div>


                                          </div>
                                        </div>
                                    </form>
                </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>


<!-- modal to delete flats -->
 <!-- Modal -->
<div id="deleteFlatsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please select the flats to be deleted.</h4>
      </div>
      <div class="modal-body">
        <p class="error-msg text-danger hide" >Please check anyone to continue.</p>
       <form id="deleteFlatsForm">
        <div class="flatsContainer">
         <div class="checkbox">
           <!-- <label><input type="checkbox" value="">Option 1</label> -->
         </div>
        </div>
       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="deleteFlats"  onclick="deleteFlats()">Delete</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>

<!-- modal to delete flats ends -->

<!-- shilpa/modal to add flats -->
 <!-- Modal -->
<div id="addFlatsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <form id="addflat">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please select the flats to be add.</h4>
      </div>
      <div class="modal-body">
                             <div class="form-group">
                                                      <label class="col-md-12">Block Name
                                                      </label>
                                                      <div class="col-md-12">
                                                         <select id="blockName" name="blockName"  class="form-control blockName_dropdown">
                                                         </select>
                                                      </div>
                                                   </div>
                                            <div class="form-group">
                                                      <label class="col-md-12" style="margin-top: 12px;">Flat Number
                                                      </label>
                                                      <div class="col-md-12">
                                                      <input type="number"   id="flatNumber" name="flatNumber"  class="form-control" >
                                                      </div>
                                                   </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default" id="addFlats" style="margin-top: 20px;">ADD</button>
         <input type="button"  id="addbtn" value="CANCEL" class="btn btn-default" style="margin-top: 20px;"  data-dismiss="modal">
      </div>
    </div>
	</form>
    </div>
</div>

<!-- modal to add flats ends -->
  
</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>
</body>
</html>
