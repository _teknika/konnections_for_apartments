<?php
include("../server/libs/session.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>TypCbe - SideMenu</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style type="text/css">
      
         li > a {   
            display : flex !important;
            align-items: center !important;
        }
        i.material-icons{
            font-size: 20px !important;
            margin-right: 4px
        }
        i.fa{
            font-size: 14px
        }
        .sidebar-menu > li:hover {
            text-decoration: underline;
            border-left: ;
          }
          .skin-blue .sidebar a{
            text-shadow: 1px 1px 2px #eae5e578;
        }

    </style>

</head>
<body>  
  
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
  
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
                 
                    <li><a href="register.php" style="padding-left: 17px;"><i class="fa fa-edit"></i>Register</a></li>
                    <li><a href="search.php" style="padding-left: 15px;"><i class="fa fa-search"></i>Search</a></li>
                    <li><a href="gcm.php" style="padding-left: 11px;"><i class="material-icons">&#xe7f4;</i>Send Notifications</a></li>
                    <li><a href="buildingInfo.php"><i class="glyphicon glyphicon-home"></i>Building Info</a></li>
                     <li><a href="banners.php"><i class="fa fa-desktop"></i>Manage Banners</a></li>
                     <li><a href="manageCommittee.php" style="padding-left: 13px;"><i class="material-icons">&#xe8f7;</i>Manage Committee</a></li>
                     <li><a href="manageEvents.php" style="padding-left: 13px;"><i class="material-icons">&#xe878;</i>Events</a></li>
                     <li><a href="manageVendors.php" style="padding-left: 13px;"><i class="material-icons">&#xe878;</i>Vendors</a></li>
                     <li><a href="manageServices.php"><i style="font-size: 18px !important" class="material-icons">&#xe895;</i>Services</a></li>

                      <li> <!-- Recreational facilities option is showing -->
                      <a href="#">
                        <i class="fa fa-inr" style="font-size:20px;margin-left:5px"></i>
                        <span>Recreational Facility</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="display: none;">
                        <li><a href="facilityReport.php"><i class="fa fa-circle-o"></i>View Reports</a></li>
                        <li><a href="manageFacilities.php"><i class="fa fa-circle-o"></i>Manage Facilities</a></li>
                        <li><a href="manageFacilityCards.php"><i class="fa fa-circle-o"></i>Manage Cards</a></li>
                      </ul>
                    </li>
                    
                     <li ><!-- Maintenance option is showing -->
                      <a href="#">
                        <i class="fa fa-inr" style="font-size:20px;margin-left:5px"></i>
                        <span>Maintenance</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" style="display: none;">
                         <li><a href="manageMaintenanceservice.php"><i class="fa fa-circle-o"></i>Upload Maintenance</a></li>
                         <!-- <li><a href="manageElectricityBill.php"><i class="fa fa-circle-o"></i>Upload Electricity bill</a></li> -->
                        <li><a href="manageMinimumMaintenance.php"><i class="fa fa-circle-o"></i>Minimum maintenance</a></li>
                        <!-- <li><a href="manageMaintenanceservice.php"><i class="fa fa-circle-o"></i>Add Maintenance</a></li> -->
                      </ul>
                    </li>
            
      </ul>
    </section>
 
</body>
</html>