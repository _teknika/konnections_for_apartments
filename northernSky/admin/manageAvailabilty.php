<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
 <link href="theme/css/animate.min.css" rel="stylesheet">
  <link href="js/popup/NSPopup.css" rel="stylesheet">

 <!-- Stylesheets etc -->
  <link href="manageCalendar/fullcalendar-3.6.2/fullcalendar.print.min.css" rel="stylesheet" media="print" />
<script src="manageCalendar/fullcalendar-3.6.2/lib/moment.min.js"></script>

    <link rel="stylesheet" type="text/css" href="manageCalendar/css/jquery-ui-1.9.2.custom.css" />
    <link rel="stylesheet" type="text/css" href="manageCalendar/css/fullcalendar.css" />
    <link rel="stylesheet" type="text/css" href="manageCalendar/css/view_calendar.css" />
    <link rel="stylesheet" type="text/css" href="manageCalendar/css/jquery.miniColors.css" />
<!-- Custom styling plus plugins -->    
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
         <script src="js/popup/NSPopup.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
		<script src="js/utility/facility.js"></script>
        <script src="js/validation.js"></script>
    <style type="text/css" media="screen">
         .bookedSlot{
          border:1px inset #b7745e;
         }
         .bookedSlot .fc-event-head.fc-event-skin{
           background-color: #d23434;
           width: 65px
         }
         .bookedSlot .fc-event-vert.fc-event-bg{
          background-color:#cc1f1fd4;
         }
         .bookedSlot .fc-event-inner.fc-event-skin{
          background-color:#d05529 
         }
          .calendar-trash{
            height: 60px;
            width: 60px;
            background-color: yellow;
            font-size: 40px;
            padding: 8px;
           margin: auto;
            clear: both;
          }
         
          .availabityFrom row{
            margin: 0px
          }
          .modal-header{
            background: linear-gradient(90deg, #25b9af 92%,#30cdde 56%);
          }
          .modal-body{
                background: #d3d3d34d;
          }
         input[type=number]::-webkit-inner-spin-button{
            display: none;
         }
         .container-title{
               background: lightcoral;
               padding: 10px;
         }
         .bookedSlotInfoContainer{
           padding: inherit;
           margin-top: 10px;
           background: #ef8235;
           border-radius: 3px;
           color: #fff6f6;
           padding-top: 10px;
           padding-bottom: 10px;
         }


         .error{
          color:red;
         }
         #deleteUnAvailableSlot,#deleteBookedSlot{
              background: red !important;
              border: none;
        }
	

       </style>   
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="overflow: scroll;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
   <h1>Recreational facility services</h1>
   	 <?php
require_once('../server/libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$id=$_GET['facilityId'];
$sql= mysqli_query($con, "SELECT facility_name FROM facilities where id='$id'");
  if (mysqli_num_rows($sql) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($sql)) {

     echo "<h2>"."<span class='label label-default'>" . $row["facility_name"] . "</span>"."</h2>";
	 
    }
	}
$connection->closeConnection();
?>

 
	  </section>

<!-- Main content -->
    <section class="content">
      
      <!-- Calendar div -->
	
      <div id="calendar" class="col-xs-9">
      </div>

      <!-- Event generation -->
     
      <!-- Booking types list -->
      <div class="col-xs-3">
      <div class="addAvailabilityContainer col-xs-12" style="padding: 4px;">
        <button type="button" class="btn btn-warning" id="addAvailabilityBtn" style="margin: auto;
            display: block;min-width: 135px;">Add Availability</button>
      </div>
      <div class="addUnAvailabilityContainer col-xs-12 hide" style="padding: 4px;">
        <button type="button" class="btn btn-warning" id="addUnAvailabilityBtn" style="margin: auto;
            display: block;min-width: 135px;">Add Unavailability</button>
      </div>
      <div class="addBookingContainer col-xs-offset-2 col-xs-6">
        <button type="button" class="btn btn-warning" id="addBookingBtn" style="margin: auto;
            display: block;">Book appointment</button>
      </div>
    
      <div id='external_events' class="col-xs-offset-3 col-xs-6">
        <div id='external_event_template' class='external-event ui-draggable'>One Hour</div>
        <div id="1513059966337" class="external-event ui-draggable" style="position: relative; background-color: rgb(39, 149, 195); border-color: rgb(106, 179, 211); color: rgb(255, 255, 255);" data-background="#2795c3" data-border="#6ab3d3" data-text="#ffffff" data-title=""></div>
      </div>
      <div  id="calendarTrash" class="calendar-trash">
        <span class="glyphicon glyphicon-trash"></span>
      </div>
      <div  id="bookedSlotInfoContainer" class="bookedSlotInfoContainer row hide"> 
            <h4 class="text-center container-title">Booked slot details</h4>    
           <h4>Name : <span id="bName"></span></h4>
           <h4>Flat : <span id="bFlat"></span></h4>
           <h4>Mobile : <span id="bMobile"></span></h4>
           <h4>Date : <span id="bDate"></span></h4>
           <h4>Time : <span id="bTime"></span></h4> 
           <h4>Amount : <span id="bAmount"></span></h4>
          <div><button type="button" class="btn btn-danger pull-right" id="deleteBookedSlot">Delete</button></div>
      </div>
       <div  id="unAvailableSlotInfoContainer" class="bookedSlotInfoContainer row hide">
           <h4 class="text-center container-title">Unavailable slot details</h4>         
           <h4>Date : <span id="unAvailbleDate"></span></h4>
           <h4>Time : <span id="unAvailbleTime"></span></h4> 
          <div><button type="button" class="btn btn-danger pull-right" id="deleteUnAvailableSlot">Delete</button></div>
      </div>
      </div>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>

<!-- Modal -->
<div id="addAvailabity" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Manage Availability</h4>
      </div>
      <div class="modal-body">
       <form action="#" class="" id="availabityFrom">
          <div class="row">
            <div class="form-group col-xs-6">
              <label for="fromDate">From :</label>
              <input type="date" name="fromDate" class="form-control" id="fromDate" >
            </div>
            <div class="form-group col-xs-6">
              <label for="toDate">To :</label>
              <input type="date" name="toDate" class="form-control" id="toDate" required>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-xs-6">
              <label for="startTime">Start Time :</label>
              <input type="time" name="startTime" class="form-control" id="startTime" value="10:00:00" required>
            </div>
            <div class="form-group col-xs-6">
              <label for="endTime">End Time:</label>
              <input type="time" name="endTime" class="form-control" id="endTime" value="20:00:00" required>
            </div>
          </div>
          <div class="row" style="padding-left:15px;">
            <div class="checkbox">
              <label><input type="checkbox" name="sat" value=""> Saturday</label>
              <label><input type="checkbox" name="sun" value=""> Sunday</label>
            </div>
          </div>
           <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-default">Submit</button>
          </div>
        </form>
      </div>
     
     
    </div>

  </div>
 </div> 

<!-- Modal -->
<div id="addUnAvailabity" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Manage UnAvailability</h4>
      </div>
      <div class="modal-body">
       <form action="#" class="" id="unAvailabityFrom">
          <div class="row">
            <div class="form-group col-xs-6">
              <label for="startDate">From :</label>
              <input type="date" name="startDate" class="form-control" id="startDate" >
            </div>
            <div class="form-group col-xs-6">
              <label for="endDate">To :</label>
              <input type="date" name="endDate" class="form-control" id="endDate" required>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-xs-6">
              <label for="starttime">Start Time :</label>
              <input type="time" name="starttime" class="form-control" id="starttime" value="10:00:00" required>
            </div>
            <div class="form-group col-xs-6">
              <label for="endtime">End Time:</label>
              <input type="time" name="endtime" class="form-control" id="endtime" value="20:00:00" required>
            </div>
          </div>
          <div class="row" style="padding-left:15px;">
            <div class="checkbox">
              <label><input type="checkbox" name="sat" value=""> Saturday</label>
              <label><input type="checkbox" name="sun" value=""> Sunday</label>
            </div>
          </div>
           <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-default">Submit</button>
          </div>
        </form>
      </div>
     
     
    </div>

  </div>
 </div> 


 <!-- Modal -->
<div id="addBookingFormModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
     
          <div class="input-group">
            <input type="text" class="form-control" min="10" placeholder="flat number.." id="flatNum">
            <div class="input-group-btn" id="searchByMobileNumBtn" style="font-size: 17px;white-space: nowrap;top: -10px;">
              <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
          </div>
           <div class="seachedContainer" style="background: white;">
            <div class="searchedInfo">
             <div class="row">             
                <select name="ownerName" id="ownerName" class="col-xs-offset-1 col-xs-3 ownerName"></select>
                <h4 class="col-xs-5 mail">Kiran@gmail.com</h4>
             </div>
             <div class="row">
                <h4 class="col-xs-offset-1 col-xs-3">flat no : <span id="flatNo">1</span></h4>
                <h4 class=" col-xs-6 ">block no : <span id="blockNo">10</span></h4>
               
             </div>
             <div class="row" style="margin: 0px 0px; ">
               <h4 class="pull-right"><a href="#" data-dismiss="modal">Close</a></h4>
               <h4 class="pull-right"><a href="#" id="appointmentBookBtn" style="margin-right: 10px;"><u>Continue to Book</u></a></h4>
             </div>
              
            </div>
            
          </div>
      
      </div>
    </div>
  </div>
 </div> 


<!-- Include scripts at bottom to aid dom loading and prevent hangs -->
    <script type='text/javascript' src='manageCalendar/js/jquery-ui-1.9.2.custom.min.js'></script>
    <script type='text/javascript' src='manageCalendar/js/fullcalendar.js'></script>
    <script type='text/javascript' src='manageCalendar/js/view_calendar.js'></script>
    <script type='text/javascript' src='manageCalendar/js/jquery.miniColors.js'></script>
    <script type='text/javascript' src='manageCalendar/js/availability.js'></script>
</body>
</html>