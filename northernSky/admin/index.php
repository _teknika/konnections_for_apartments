<?php
    session_start();
    if(isset($_SESSION['userId'])){
        session_regenerate_id();
        //$_SESSION['last_activity']= time();
       // header('Location: dashboard.php');
        //exit;
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,height=device-height">
    <title>Welcome to My Family</title>
    <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'>
    <link href="theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="theme/css/animate.min.css" rel="stylesheet">

   
    <link href="theme/css/icheck/flat/green.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <style type="text/css">
        html,body{
            width: 100%;
            height: 100%
        }
       body{
       	    background: url(img/bodyBg.png);
            overflow: hidden;
           /*background: radial-gradient( rgb(218, 223, 230),rgba(231, 231, 239, 0.89));*/
       }
      .church-container1{
              background: linear-gradient( to top right, rgba(0, 255, 153, 0.61), rgba(0, 137, 255, 0.64));
           border-radius: 5px;
           margin: 10% 35%;
               box-shadow: inset 0px 0px 16px 6px rgba(122, 151, 156, 0.48);
      }
      	input:focus{
	      transform:scale(1.1,1.1);
	      transition:transform 1s;
        }
      input{
       min-height: 20px;
       padding: 10px;
       
      }
      #username,#password{
        border-radius: 25px
      }
      input:-webkit-autofill {
            -webkit-box-shadow: 0 0 0 1000px white inset !important;
      }
      #loginForm{
        padding: 50px 70px;
        
      }
     #submit{
         margin-top: 10px;
         border-radius: 25px;
         margin-left: 35%;
         text-shadow: 5px 5px 15px rgb(0,0,0);
      }
      .labelText{
        text-transform: uppercase;
        font-weight: normal;
        font-size: xx-small;
      }
      .onloginError{
      	color:red;
      }
    </style>
</head>

<body >
    
    <div class="church-container1">

        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <img src="img/northern logo.bmp" class="img-responsive" style="
                       background: #b2dbff;width: 100%;height: 140px">
                    <form name="loginForm" id="loginForm" method="post" action="" >
                        
                       
                      <div class="onloginError hide">Please enter valid credentials</div>

                        <div>
                            <label for="username" class="labelText">User name</label>
                       
                            <input type="text" class="form-control " autocomplete="off" placeholder="Username" name="username" id="username"  required/>
                        </div>
                        <div>
                            <label for="username" class="labelText">password</label>
                            <input type="password" class="form-control" autocomplete="off" placeholder="Password" name="password" id="password" required/>
                        </div>
                        <div>
                            <input type="submit" class="btn btn-info" name="submit" id="submit" value="Submit">
                            <a class="reset_pass hide" href="#">Lost your password?</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link hide">New to site?
                                <a href="#toregister" class="to_register"> Create Account </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            
                        </div>
                    </form>
                </section>
            </div>

            <div id="register" class="animate form hide">
                <section class="login_content">
                    <form>
                        <h1>Create Account</h1>
                        <div>
                            <input type="text" class="form-control" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="email" class="form-control" placeholder="Email" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" placeholder="Password" required="" />
                        </div>
                        <div>
                            <a class="btn btn-default submit" href="index.php">Submit</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">Already a member ?
                                <a href="#tologin" class="to_register"> Log in </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> My Family</h1>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

     <!-- Loader -->
      <div class="fader">
         <div class="loader"></div>
      </div>
</body>


    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/url.js"></script>
    <script src="js/utility/utility.js"></script>
    <script src="js/validation.js"></script>
    <script src="js/utility/login.js"></script>

</html>