<?php
include("../server/libs/session.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>My Family - SMS Tracker</title>
    <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'/ >
    
    <!-- Bootstrap core CSS -->
    <link href="theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="theme/css/animate.min.css" rel="stylesheet">

    <link href="theme/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="theme/css/custom.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="theme/js/bootstrap.min.js"></script>
    <script src="theme/js/custom.js"></script>

  

 <!-- Datatables -->
    <script src="theme/js/datatables/jquery.dataTables.js"></script>
    <script src="theme/js/datatables/dataTables.bootstrap.js"></script>
    
    <!-- Custom JS -->
    <script src="js/jquery.validate.js"></script>
    <script src="js/url.js"></script>
    <script src="js/user.js"></script>
    <script src="js/utility/utility.js"></script>
    <script src="js/validation.js"></script>
    
    <script src="js/utility/smsTracker.js"></script>
</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

        <div class="col-md-3 left_col" id="sidemenu_section"></div>
        <div class="top_nav" id="header_section"></div>
        


            <div class="right_col" role="main">
            <h4>SMS Tracker</h4>
    
            <div class="">
              <table id="smsTrackerTable" class="table table-responsive table-striped responsive-utilities jambo_table" style="width:100%">
               <!--  <thead><tr><th>Person Name</th><th>Pack Name</th><th>Purchased Date</th><th>Total SMS</th><th>Sent SMS</th><th>Balance SMS</th></tr></thead>
                <tbody>
                  
                </tbody> -->
              </table>
            </div>

            <div style="height:40px"></div>
              
                

                <footer>
                    <div class="">
                        <p class="pull-right">All Rights Reserved. |
                            <span class="lead"> <i class="fa fa-paw"></i>My Family</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
            </div>
        </div>


    </div>

     <!-- Loader -->
      <div class="fader">
         <div class="loader"></div>
      </div>

    
</body>

</html>