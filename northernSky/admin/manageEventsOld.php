<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
 <link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
        
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>
        <style>
          /*  table.db-table    { border-right:1px solid #ccc; border-bottom:1px solid #ccc;width:100%; }
            table.db-table th { background:#eee; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
            table.db-table td { padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }*/
            .table-bordered{
                border: 4px solid #ddd ;
               }
        </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Events</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
                      <div class="right_col" role="main" id="gcmList">
                    <br />
                    <div class="" id="hacker-list">
                        <div class="row top_tiles">
                            <form id="eventForm" name="eventForm" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                                <div class="panel">

                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Title</label>
                                                        <div class="col-md-12">
                                                            <textarea type="text" id="title" name="title"  class="form-control" rows="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Details</label>
                                                        <div class="col-md-12">
                                                            <textarea type="text" id="details" name="details" class="form-control" rows="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 hide">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Event Date</label>
                                                        <div class="col-md-12">
                                                            <input type="date" id="datetime" name="datetime"  class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Event Co-Ordinator</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="name" name="name" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Event Venue</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="location" name="location" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Start Date</label>
                                                        <div class="col-md-12">
                                                            <input type="date" id="eventdate" name="eventdate" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                     <label class="col-md-12">Start Time</label>
                                                     <div class="col-md-12">
                                                        <input type="text" class="form-control startTime"  name="startTime" />
                                                    </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                               <div class="col-md-3"> 
                                                <div class="form-group">
                                                        <label class="col-md-12">End Time</label>
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control endTime"  name="endTime" />
                                                        </div>
                                                        
                                                    </div>
                                                 </div>   
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Event Photos Link</label>
                                                        <div class="col-md-12">
                                                            <input type="file" id="photos" name="photos" class="" accept="image/*" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                        <div class="text-right">
                                           <!-- <input type="submit" class="btn btn-success" value="Add Event" name="submit"/> -->
                                           <input type="submit" class="btn btn-success" value="Add Event" name="submit" onclick=" addEvent();" />
                                        </div>
                                    </div>
                                </div>
                            </form> 
                        </div>
                    </div>
                   </div> 

                    
                 
                     <div class="table-responsive"> 
                      <table class="db-table table table-bordered table-hover table-condensed" >
                        <thead>
                            <tr class="success">
                                <th>Title</th>
                                <th>Details</th>
                                <th>Event Co-ordinator</th>
                                <th>Event Date</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Event Venue</th>
                                <th>Event Photos Link</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            require_once('../server/libs/dbConnection.php');
                            $connection = new dbconnection();
                            $con = $connection->connectToDatabase();
                            $sql = mysqli_query($con, "SELECT e.*,FROM_UNIXTIME(e.start_time/1000,'%h:%i %p') as startTime,FROM_UNIXTIME(e.end_time/1000,'%h:%i %p') as endTime FROM events e");
                            $rows_count = mysqli_num_rows($sql);
                            
                            while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                                echo '<tr class="active">';
                                echo "<td>" . $rows_fetch['title'] . "</td>";
                                echo "<td>" . $rows_fetch['details'] . "</td>";
                                echo "<td>" . $rows_fetch['name'] . "</td>";
                                echo "<td>" . explode(" ", $rows_fetch['event_date'])[0] . "</td>";
                               // echo "<td>" . date("d/m/Y", ($rows_fetch['start_time'])/1000) . "</td>";
                               // echo "<td>" . date("d/m/Y", ($rows_fetch['end_time'])/1000)  . "</td>";
                                 echo "<td>" .$rows_fetch['startTime'] . "</td>";
                                echo "<td>" .$rows_fetch['endTime']. "</td>";
                                echo "<td>" . $rows_fetch['location'] . "</td>";
                                echo "<td>" . $rows_fetch['photos'] . "</td>";
                                echo '<td><button class="deleteEventClass" id="' . $rows_fetch['id'] . '">Delete</button></td>';
                                echo '</tr>';
                            }
                            $connection->closeConnection();
                            ?>
                        </tbody>
                    </table></div>

                    <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
                        <div class="cbp-vm-options">
                            <a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid">Grid View</a>
                            <a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list">List View</a>
                        </div>

                        <ul class="list" id="contactsDashboard">
                        </ul>
                    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>




 <!-- Grid Switch Mode -->
        <link rel="stylesheet" type="text/css" href="grid/css/component.css" />
        <script src="grid/js/classie.js"></script>
        <script src="grid/js/cbpViewModeSwitch.js"></script>
        <!-- Grid Switch Mode -->

        <script src="grid/list.js"></script>
        <script>
            $(function () {
                $("#main-header").load("headerMenu.php");
                $("#main-sidebar").load("sidebarMenu.php");
                $("#main-footer").load("footerMenu.php");
                $("#cbp-vm").hide();
                initSearch();
                enableTimepickers();
            });

            function enableTimepickers(){
                  $('.startTime').timepicker({
                        timeFormat: 'h:mm p',
                        interval: 60,
                        minTime: '6:00',
                        maxTime: '11:00pm',
                        defaultTime: '6',
                        startTime: '6:00',
                        dynamic: false,
                        dropdown: true,
                        scrollbar: true
                  });
                  $('.endTime').timepicker({
                        timeFormat: 'h:mm p',
                        interval: 60,
                        minTime: '6:00',
                        maxTime: '11:30pm',
                        defaultTime: '6',
                        startTime: '6:00',
                        dynamic: false,
                        dropdown: true,
                        scrollbar: true
                  });
            }

            function initSearch() {

                $('.deleteEventClass').click(function (evt) {
                    deleteEvent(this.id);
                });
            }
            function addEvent() {
                
                //var details = {title: title, details: details, datetime: datetime, name: name, type: type, location: loc, photos: photos};
                showLoader();
                   var submitUrl=getUrl(api.ADD_EVENT_URL);
                    var form = document.forms.namedItem("eventForm");
                        form.addEventListener('submit', function(ev) {     
                            oData = new FormData(form);
                            var eventDate=$('#eventdate').val();
                            oData.append("startTime",covertTimeToDateTimeMillSec(eventDate,$(".startTime").val()));  
                            oData.append("endTime",covertTimeToDateTimeMillSec(eventDate,$(".endTime").val()));  
                          var oReq = new XMLHttpRequest();
                          oReq.open("POST", submitUrl, true);
                          oReq.onload = function(oEvent) {
                           hideLoader();
                            if (JSON.parse(oReq.response).isSuccessful == true) {
                                alert("Event added successfully");
                                location.reload();
                            } else {
                              alert("Please try again");
                            }
                            
                          };

                          oReq.send(oData);
                          ev.preventDefault();
                        }, false);
           
            }
        function covertTimeToDateTimeMillSec(eventdate,time){ 
            //time value will be like "6:30 AM" |  "02:00 PM" 
                  var timePartStr=time;
                   var timePartArr=timePartStr.split(/[(" ")|(:)]/);
                   var hourPart=Number(timePartArr[0]);
                  if(timePartStr.match(/(PM)$/ig)){
                     hourPart+=12;
                  }

                  var eventDateTime=new Date(eventdate);
                  eventDateTime.setHours(hourPart);
                  eventDateTime.setMinutes(0);
                  eventDateTime.setSeconds(0);
                 return eventDateTime.getTime();
             }
            function deleteEvent(id) {
                showLoader();
                $.ajax({
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    url: getUrl(api.DELETE_EVENT_URL),
                    data: {id: id},
                }).done(function (response) {
                    hideLoader();
                    if (response.isSuccessful === true) {
                        alert("Event deleted successfully");
                        location.reload();
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                })
                        .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});

            }
        </script>
</body>
</html>
