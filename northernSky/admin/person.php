<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
 <link href="theme/css/animate.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="grid/css/component.css" />
<!-- Custom styling plus plugins -->
       
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>

        <script src="js/utility/person.js"></script>
        <script src="grid/js/classie.js"></script>
    <script src="grid/js/cbpViewModeSwitch.js"></script>
    <!-- Date Range Picker -->
    <script type="text/javascript" src="theme/js/moment.min2.js"></script>
    <script type="text/javascript" src="theme/js/datepicker/daterangepicker.js"></script>
    <!-- Grid Switch Mode -->

    <script src="grid/list.js"></script>
     <style type="text/css">
    .table-bordered{
        border: 4px solid #ddd !important;
    }
    label.error {
      color: red;
     } 
    .daterangepicker.dropdown-menu{
        z-index:1051;
    }          
  </style>  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
                <div class="edit-content">
                    <div class="page-title">
                        <div class="title_left">
						       <div class="row">
                            <h3 class="col-sm-8">User Profile</h3>
							    <?php
                               require_once('../server/libs/dbConnection.php');
                                $connection = new dbconnection();
                                $con = $connection->connectToDatabase();
                                $parentId=$_GET['userId'];
                                $sql = mysqli_query($con, "SELECT * FROM person where id='$parentId'");
                                $rows_count = mysqli_num_rows($sql);
                                
                                while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                                   if($rows_fetch['flat_holder'] == 'rental')
								   {
									    echo "<h3 class='col-sm-4'>Owner Details</h3>";
										$qry = mysqli_query($con, "select p.* from person p left JOIN person_flats pf on p.id=pf.personId WHERE (p.flatNo='".$rows_fetch['flatNo']."' and p.parentId=-2) or (pf.flat_number='".$rows_fetch['flatNo']."' and pf.flat_holder_type=-2) limit 1");
                                         $rows_count = mysqli_num_rows($qry);
                                
                                         while ($rows_fetch = mysqli_fetch_assoc($qry)) {
											 echo "<div class='row'>";
											 echo "<h4 class='col-sm-8'></h4>";
											 echo "<h4 class='col-sm-4'><label>Name: ".$rows_fetch['firstName']."</label></h4>";
											 echo "</div>";
											 echo "<div class='row'>";
											 echo "<h4 class='col-sm-8'></h4>";
                                             echo  "<h4 class='col-sm-4'><label>MobileNumber: ".$rows_fetch['mobileNumber']."</label></h4>";
											 echo "</div>";
											 echo "<div class='row'>";
											 echo "<h4 class='col-sm-8'></h4>";
                                             echo "<h4 class='col-sm-4'><label>Email Id: ".$rows_fetch['emailId']."</label></h4>";
											 echo "</div>";
											 
                                              											  
										  }										  
								  }
								   
                                      
                                     }
                                $connection->closeConnection(); 
                                ?> 
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            </div>
                        </div>
                    </div>
					
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                 
                                   <!--  <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
                                    </ul>
 -->                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <div class="col-md-4 col-sm-4 col-xs-12 profile_left">

                                        <div class="profile_img">

                                            <div id="crop-avatar">
                                                <div class="avatar-view" title="" data-original-title="Change the avatar" id="user_photo"></div>

                                            </div>
                                        </div>
                                        <h3 id="user_name"></h3>
                                        <ul class="list-unstyled user_data">
                                            <li>
                                              <span >
                                                <ul id="user_flats_detail" style="padding:0px;text-align:center">
                                                 <table class="table table-bordered">
                                                  <caption>Flat detail :</caption>
                                                  <thead>
                                                    <tr>
                                                    <th style="height:200%">Flat</th>
                                                    <th style="height:200%">Maitenance</th>
													<th class="lesse_colmn" style="height:400%">Lessee</th>
													
                                                    </tr>
                                                  </thead> 
                                                  <tbody class="maintenance_detail_body">
                                                   
                                                  <tbody> 
                                                 </table>
                                                </ul>
                                              </span>
                                            </li>
                                            <li style="margin-bottom:25px;">Mobile :<span id="user_phone"></span></li>
                                         </ul>   

                                        <a class="btn btn-warning" href="addMember.php" id="user_add" title="Add Member to Family"><i class="fa fa-plus-circle m-right-xs"></i></a>

                                        <a class="btn btn-success" href="editContact.php" id="user_edit" title="Edit Profile"><i class="fa fa-edit m-right-xs"></i></a>

                                       
                                        <div class="btn btn-danger deleteContact" title="Delete Profile"><i class="fa fa-close m-right-xs"></i></div>

                                        <br>
                                    </div>
                                

                                    <div class="col-md-12" id="memberList">
                                        <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-list">
                                            <div class="cbp-vm-options">
                                               <!--  <a href="#" class="cbp-vm-icon cbp-vm-grid" data-view="cbp-vm-view-grid">Grid View</a>
                                                <a href="#" class="cbp-vm-icon cbp-vm-list cbp-vm-selected" data-view="cbp-vm-view-list">List View</a> -->
                                            </div>
                                            <!-- <input class="search filterContact form-control" placeholder="Search.." /> -->
                                            <ul class="list" id="contactsDashboard">
                                            </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

          
            </div>
                       <div class="table-responsive"> 
                          <table class="db-table table table-bordered table-hover table-condensed">
                      
                            <thead>
                                <tr class="success">
                                    <th>Name</th>
                                    <th>Number</th>
                                    <th>Alt Number</th>
                                    <th>Email</th>
                                    <th>Alt Email</th>
                                    <th>Dob</th>     
                                    <th>Gender</th>     
                                    <th>Date of marriage</th>                 
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                require_once('../server/libs/dbConnection.php');
                                $connection = new dbconnection();
                                $con = $connection->connectToDatabase();
                                $parentId=$_GET['userId'];
                                $sql = mysqli_query($con, "SELECT * FROM person where parentId='$parentId'");
                                $rows_count = mysqli_num_rows($sql);

                                while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                                    echo '<tr class="active">';
                                    echo "<td>" . $rows_fetch['firstName'] . "</td>";
                                    echo "<td>" . $rows_fetch['mobileNumber'] . "</td>";
                                     echo "<td>" . $rows_fetch['alt_number'] . "</td>";
                                    echo "<td>" . $rows_fetch['emailId'] . "</td>";
                                     echo "<td>" . $rows_fetch['alt_mail'] . "</td>";
                                    echo "<td>" . $rows_fetch['dob'] . "</td>";
                                    $gender=$rows_fetch['gender'] ? "Male":"Female";
                                    echo "<td>" . $gender . "</td>";  
                                      echo "<td>" . $rows_fetch['dateOfMarriage'] . "</td>";

                                    echo '<td>
                                    <i class="ui-tooltip fa fa-pencil editSubMember"  style="font-size:22px;margin-left:12px;" data-id="'. $rows_fetch['id'].'" data-original-title="Edit"></i>
                                    <i class="ui-tooltip fa fa-trash-o deleteSubMember" style="font-size: 22px;margin-left:9px" id="'. $rows_fetch['id'].'" data-original-title="Delete"></i>
                                    </td>';
                                    echo '</tr>';
                                }
                                $connection->closeConnection();
                                ?>
                            </tbody>
                        </table>        
                       </div>

      <div class="modal fade bs-example-modal-lg" id="deleteMember" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Contact</h4>
                </div>
                <div class="modal-body">
                   Do you want to delete the user - <span class="user_popupInfo"></span>?
                   <input type="hidden" class="usreIdDelete" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <div class="btn btn-primary" id="deleteConfirm">Delete</div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="deleteSubMember" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Contact</h4>
                </div>
                <div class="modal-body">
                   Do you want to delete the user - <span class="user_popupInfo"></span>?
                   <input type="hidden" class="usreIdDelete" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <div class="btn btn-primary" id="deleteConfirmSubMember">Delete</div>
                </div>

            </div>
        </div>
    </div>
	<!-- modal to delete lessee    -->
<div class="modal fade bs-example-modal-lg" id="deletelessee" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete lessee</h4>
                </div>
                <div class="modal-body">
                   Do you want to delete the lessee <span class="user_popupInfo"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <div class="btn btn-primary" id="deletelesseeConfirm">Delete</div>
                </div>

            </div>
        </div>
    </div>
	<!--modal ends here-->
    <div class="modal fade bs-example-modal-lg" id="editFamilySubMember" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" >Edit</h4>
                </div>
                <div class="modal-body">
                                     <form id="editFamilyMemberForm" name="editFamilyMemberForm" class="form-horizontal" method="POST" action="">
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label class="col-md-12">First Name <span class="required">*</span></label>
                                                         <div class="col-md-12">
                                                            <input type="hidden" id="userId" name="userId" value=""/>
                                                            <input type="text" id="firstnameR" name="firstnameR"  class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                 
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Last Name<span>*</span></label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="lastnameR" name="lastnameR" class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-md-3">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Mobile  Number<span>*</span></label>
                                                         <div class="col-md-12">
                                                            <input type="number" id="mobilenumberR" minlength="10" maxlength="20" name="mobilenumberR" class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-3">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Alternative  Number</label>
                                                         <div class="col-md-12">
                                                            <input type="number" id="altNumber" minlength="10" maxlength="20" name="altNumber" class="form-control"/>
                                                         </div>
                                                      </div>
                                                   </div>


                                                      <div class="col-md-3">
                                                        <div class="form-group">
                                                          <label class="col-md-12">Email Id<span>*</span>
                                                          </label>
                                                          <div class="col-md-12">
                                                             <input type="email"   id="emailId" name="emailId" class="form-control" required/>
                                                          </div>
                                                       </div>
                                                    </div>
                                                     <div class="col-md-3">
                                                        <div class="form-group">
                                                          <label class="col-md-12">Alternative Email
                                                          </label>
                                                          <div class="col-md-12">
                                                             <input type="email"   id="altemailId" name="altemailId" class="form-control"/>
                                                          </div>
                                                       </div>
                                                    </div>
                                                </div>
                                         
                                                <div class="row">
                                                   
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Gender<span>*</span></label>
                                                         <div class="col-md-12">
                                                            <select id="genderR" name="genderR" class="form-control" required>
                                                               <option value="">-- Select the gender --</option>
                                                               <option value="1">Male</option>
                                                               <option value="0">FeMale</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                     <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Date of Birth</label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="dob" name="dob" class="form-control"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Date of Marriage</label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="dateofmarriageR" name="dateofmarriageR" class="form-control"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                 <div class="footer-btn" style="overflow: auto;">
                                                   
                                                    <button type="submit"  class="btn btn-primary pull-right" id="editConfirmSubMember">Done</button>
                                                     <button class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                                                 </div>
                                    </form>
                        </div>
               

            </div>
        </div>
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>
</body>
</html>
