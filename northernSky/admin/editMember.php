<?php
include("../server/libs/session.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>My Family - Edit Contact</title>
    <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'/ >
<!-- Bootstrap core CSS -->
    <link href="theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="theme/css/animate.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="theme/css/custom.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="theme/js/bootstrap.min.js"></script>
    <script src="theme/js/custom.js"></script>

    <!-- Custom JS -->
    <script src="js/jquery.validate.js"></script>
    <script src="js/url.js"></script>
    <script src="js/user.js"></script>
    <script src="js/utility/utility.js"></script>
    <script src="js/validation.js"></script>

    <!-- Date Range Picker -->
    <script type="text/javascript" src="theme/js/moment.min2.js"></script>
    <script type="text/javascript" src="theme/js/datepicker/daterangepicker.js"></script>


    <!-- Jquery Validate -->
   <script src="js/jquery.validate.js"></script>

    <script src="js/utility/editMember.js"></script>
</head>


<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col" id="sidemenu_section"></div>
            <div class="top_nav" id="header_section"></div>

            <div class="right_col" role="main">
                <br />
                <div class="">
                    <form id="editMemeberForm" name="editMemeberForm" class="form-horizontal" method="POST" action="">
                                       <div class="panel">
                                          <a class="panel-heading" role="tab" id="headingOne" data-toggle="" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                             <h4 class="panel-title">Basic Information</h4>
                                          </a>
                                          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                             <div class="panel-body">
                                                <div class="row">
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">First Name <span class="required">*</span></label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="firstnameR" name="firstnameR"  class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Middle Name</label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="middlenameR" name="middlenameR" class="form-control"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Last Name<span>*</span></label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="lastnameR" name="lastnameR" class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Mobile  Number</label>
                                                         <div class="col-md-12">
                                                            <input type="number" id="mobilenumberR" minlength="10" maxlength="10" name="mobilenumberR" class="form-control" />
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Business Category</label>
                                                         <div class="col-md-12">
                                                            <select id="businesscategoryR" name="businesscategoryR" class="form-control businessCategory_dropdown" >
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Business Occupation</label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="businessoccupationR" name="businessoccupationR" class="form-control" />
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">DOB<span>*</span></label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="dobR" name="dobR" class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Gender<span>*</span></label>
                                                         <div class="col-md-12">
                                                            <select id="genderR" name="genderR" class="form-control" required>
                                                               <option value="">-- Select the gender --</option>
                                                               <option value="1">Male</option>
                                                               <option value="0">Female</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Date of Marriage</label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="dateofmarriageR" name="dateofmarriageR" class="form-control"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Blood Group</label>
                                                         <div class="col-md-12">
                                                            <select id="bloodgroupR" name="bloodgroupR" class="form-control bloodGroup_dropdown"></select>
                                                         </div>
                                                      </div>
                                                   </div>

                                                   

                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Relationship With Head</label>
                                                         <div class="col-md-12">
                                                            

                                                            <select class="form-control" id="relationwithheadR" name="relationwithheadR">
                                                              <option value="">-- Select a relationship --</option>  
                                                              <option value="Father">Father</option>
                                                              <option value="Mother">Mother</option>
                                                              <option value="Husband">Husband</option>
                                                              <option value="Wife">Wife</option>
                                                              <option value="Son">Son</option>
                                                              <option value="Daughter">Daughter</option>
                                                              <option value="Brother">Brother</option>
                                                              <option value="Sister">Sister</option>
                                                              <option value="Grand Mother">Grand Mother</option>
                                                              <option value="Grand Daughter">Grand Daughter</option>
                                                              <option value="Grand Son">Grand Son</option>
                                                              <option value="Daughter-in-Law">Daughter-in-Law</option>
                                                              <option value="Sister-in-Law">Sister-in-Law</option>
                                                              <option value="Brother-in-Law">Brother-in-Law</option>
                                                              <option value="Nephew">Nephew</option>
                                                              <option value="Niece">Niece</option>
                                                              <option value="Great-GrandSon">Great-GrandSon</option>
                                                              <option value="Great-Daughter-In-Law">Great-Daughter-In-Law</option>
                                                              <option value="Great-Grand-Daughter">Great-Grand-Daughter</option>

                                                              <option value="Great-Son-In-Law">Great-Son-In-Law</option>
                                                              <option value="Great-Grand-Nephew">Great-Grand-Nephew</option>
                                                              <option value="Great-Grand-Niece">Great-Grand-Niece</option>
                                                              <option value="Great-Grand-Mother">Great-Grand-Mother</option>
                                                              <option value="Great-Grand-Father">Great-Grand-Father</option>
                                                            </select>

                                                         </div>
                                                      </div>
                                                   </div>

                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Photo</label>
                                                         <div class="col-md-12">
                                                         <input type="text" id="photoR" name="photoR" class="form-control"/>
                                                            
                                                         </div>
                                                      </div>
                                                   </div>

                                                </div>

                                             </div>
                                          </div>
                                       </div>
                                    

                                       <div class="">
                                        <input type="submit" class="btn btn-success" value="Submit" name="submit" />
                                       </div>



                                    </form>
                </div>

                <footer>
                    <div class="">
                        <p class="pull-right">All Rights Reserved.|
                            <span class="lead"> <i class="fa fa-paw"></i>My Family</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
            </div>
        </div>
    </div>

    <!-- Loader -->
      <div class="fader">
         <div class="loader"></div>
      </div>

</body>

</html>