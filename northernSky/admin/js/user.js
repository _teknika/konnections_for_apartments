var User = function(info){
    this.contacts = info;
    localStorage.setItem("contacts",JSON.stringify(this.contacts));
};

function getAllContacts(){
	var contacts = JSON.parse(localStorage.getItem("contacts"));
    return contacts;
}

function getContactById(id){
	var contacts = JSON.parse(localStorage.getItem("contacts"));
    for(var i=0; i<contacts.length;i++){
        if(contacts[i].id == id){
            return contacts[i];
        }
    }
};

function updateContactById(data,id){
  var contacts = JSON.parse(localStorage.getItem("contacts"));
    for(var i=0; i<contacts.length;i++){
        if(contacts[i].id == id){
            contacts[i] = data.basicInfo;
            contacts[i].contacts = []
            contacts[i].contacts.push(data.nativeAddInfo);
            contacts[i].contacts.push(data.homeAddInfo);
            contacts[i].contacts.push(data.otherAddInfo);
        }
    }  
     localStorage.setItem("contacts", JSON.stringify(contacts))
}


function addContact(data){
    var contacts = JSON.parse(localStorage.getItem("contacts"));
    contacts =  data.basicInfo;
    contacts.contacts = [data.nativeAddInfo, data.officeAddInfo, data.homeAddInfo];
    contacts.push(contact);
    localStorage.setItem("contacts", JSON.stringify(contacts));
}
var user;