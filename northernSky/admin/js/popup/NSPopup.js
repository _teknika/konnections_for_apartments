function footerBtns(ButtonsArray){
	if(ButtonsArray.length==1){
	   return '<div class="modal-footer ">'+						
		    '<button id="popBtn-1" class="modal-footer-btns" style="width:100%;background-color:#10a0a0;color:white">'+ButtonsArray[0]+'</button>'+
	      '</div>';
	  };
  if(ButtonsArray.length==2){
	  return '<div class="modal-footer ">'+
						'<button id="popBtn-1" class="modal-footer-btns" style="width:50%;float: left;background-color:#64ccb7;color:green">'+ButtonsArray[0]+'</button>'+
					    '<button id="popBtn-2" class="modal-footer-btns" style="width:50%;background-color:#10a0a0;color:white">'+ButtonsArray[1]+'</button>'+
		      '</div>';
	   };
}
 
 function showNSPopup(callback){
 $('<div class="modal" id="NSPopUpModal" role="dialog" data-backdrop="static" data-keyboard="false">'+
		    '<div class="modal-dialog modal-sm">'+
		         '<div class="modal-content" style="background: linear-gradient(to right , #25b9af, #30cdde);width:100%">'+
		                '<div class="modal-header">'+
		                    /*'<button type="button" class="close popUpcloseBtn" data-dismiss="modal">&times;</button>'+*/
		                '</div>'+
		                '<div class="modal-body" style="min-height: 100px;display: flex;justify-content: center;align-items: center;">'+
	                       '<div>'+                   
                            '<h4 id="counsellorDescriptionInPopup" style="padding-right:15px;padding-left:15px; text-align: center;color:white">'+callback.message+'</h4>'+		        
		                   '</div>'+		        
		                '</div>'+
		                 footerBtns(callback.ButtonNames)+
		          '</div>'+
		    '</div>'+
    '</div>').appendTo('body').trigger('create');
   callbackFunctionref= callback.callbackFunction;
   customParam=callback.customParam;
   $("#popBtn-1,#popBtn-2,.popUpcloseBtn").unbind('click').click(function(event){
   	   event.preventDefault();
   	   event.stopPropagation();
       if(callbackFunctionref != undefined && !($(this).hasClass('popUpcloseBtn')) ){       	
       	callbackFunctionref((this.id).split("-")[1],customParam);

      };
       $('#NSPopUpModal').modal("hide");
       $('#NSPopUpModal').remove();
       callback={};
       return false;
  });
  $('#NSPopUpModal').modal();
 
}
