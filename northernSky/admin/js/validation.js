
// Admin Login Page
function validationOnLogin(data, response){
	$(".onloginError").hasClass("hide") ? "":$(".onloginError").addClass("hide");
	hideLoader();
	if(response.isSuccessful){

		localStorage.setItem("adminInfo", JSON.stringify(response.userInfo));
		location.href = "dashboard.php";
	}else{
		$(".church-container1").css({'animation': 'shake 1s 1'});
		$(".onloginError").removeClass("hide");
		//alert("Please enter the valid credentials");
	}
}


// Registration Page
function validationOnBasicInfolesseeRegister(details, response){
	hideLoader();
	if(response.isSuccessful){
		alert("User registered succesfully");
		$('form')[0].reset();
		location.href="lessee/registerLessee.php";
	}
	else{
		alert(response.msg);
	}
}
//lessee Registration Page
function validationOnBasicInfoRegister(details, response){
	hideLoader();
	if(response.isSuccessful){
		alert("User registered succesfully");
		$('form')[0].reset();
		location.href="register.php";
	}
	else{
		alert(response.msg);
	}
}

// Dashboard Page
function validationOnGetContactsOnDashboard(details,response){
	hideLoader();
	if(response.isSuccessful){
		user = new User(response.resp);
		generateContactsDashboard(response);
	}else{
		alert("Failed");
	}
}

function validationOnGetMemberStats(orgId,response){
	hideLoader();
	if(response.resp.isSuccessful){
		$("#allMemberCount").html(response.resp.memberCount);
		$("#parentMemberCount").html(response.resp.parentCount);
		$("#notificationCount").html(response.resp.notificationCount);
		$("#cityCount").html(response.resp.cityCount);
	}
}

// Single User Page

function validationOnSingleContact(details,response){
	hideLoader();
	if(response.isSuccessful){
		generateUserContact(response.resp[0]);
		var members = response.resp[0].members;
		if(members){
			generateMemberContacts(response.resp[0].members);
		}else{
			$("#contactsDashboard").html("Family members list");
		}
	}else{
		alert("Failed");
	}
}

function validationOnDeleteMember(details, response){
	hideLoader();
	if(response.isSuccessful){
		alert("Member Deleted Successfully");
		location.href="dashboard.php";
	}else{
		alert("failed");
	}
}

//  Add Memeber Page

function validationOnAddMember(details,response){
	hideLoader();
	if(response.isSuccessful){
		alert("Member Added Successfully");
		$("form")[0].reset();
		location.href = "dashboard.php";
	}else{
		alert("Failed");
	}
}

// Edit Contact Page

function validationOnEditSingleContact(details, response){
	hideLoader();
	if(response.isSuccessful){
		generateValuesforEdit(response);
		//location.href="person.php";
	}else{
		alert("Failed");
	}
}

// Edit Member Page
function validationOnEditMemberGetList(details,response){
	hideLoader();
	if(response.isSuccessful){
		generateValuesforMember(response.resp[0]);
	}else{
		alert("Failed");
	}
}

function validationOnEditMember(details,response){
	hideLoader();
	if(response.isSuccessful){
		alert("Member info updated succesfully");
		$('form')[0].reset();
		location.href="person.php";
	}else{
		alert("Failed");
	}
}

function validationOnEditContact(details, response){
	hideLoader();
	if(response.isSuccessful){
		alert("User information updated succesfully");
		$('form')[0].reset();
		updateContactById(details, response.userId);
		//location.href="person.php";
		window.history.back();
	}else{
		alert("Please enter proper details");
	}
}


// Search Page

function validationOnSearch(details,response){
	hideLoader();
	if(response.isSuccessful){
		generateContactsInSearch(details, response);
	}else{
		alert("Please enter proper details");
	}
}



// SMS Tracker Page 

function validationOnSmsTracker(orgId, response){
	hideLoader();
	if(response.isSuccessful){
		generateSmsTrackerTable(response.resp);
	}else{
		alert("Please enter proper details");
	}
}

// SMS History Page 

function validationOnSmsHistory(details, response){
	hideLoader();
	if(response.isSuccessful){
		generateSmsHistoryTable(response.resp);
	}else{
		//alert("Please enter proper details");
	}
}

// Notifications Tracker Page

function validationOnNotificationsTracker(details, response){
	hideLoader();
	if(response.isSuccessful){
		generateNotificationsTrackerTable(response.resp);
	}else{
		//alert("Please enter proper details");
	}
}