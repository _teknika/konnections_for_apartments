var dataTable_smsTracker;
$(function(){
    $("#header_section").load("header.php");
    $("#sidemenu_section").load("side_menu.php");
    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    var orgId = adminInfo.orgId;
    
    getSmsTracker(orgId);
    generateSmsTrackerTable("");

    // Show Popup For All Details of Lead
    $(dataTable_smsTracker).on('click', '.showHistory', function(event) {
        var row = $(this).closest("tr").get(0);
        var rowIndex = dataTable_smsTracker.fnGetPosition(row);
        var smsData = dataTable_smsTracker.fnGetData(rowIndex);
        localStorage.setItem("smsUser", JSON.stringify(smsData));
        location.href="smsHistory.php";
    });
});


function getSmsTracker(orgId){
    showLoader();
    var dataR = {"request":{"orgId":orgId}};
    $.ajax({
        type: "POST",
        //contentType: 'application/json',
        dataType: 'json',
        url: getUrl(api.GETSMSTRACKER_URL),
        data: JSON.stringify(dataR)
    }).done(function (response){
        validationOnSmsTracker(orgId, response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
    return false;
}

function generateSmsTrackerTable(datas){
    console.log(datas);
    if (typeof dataTable_smsTracker == 'undefined'){
        dataTable_smsTracker = $("#smsTrackerTable").dataTable({
            "iDisplayLength": -1,
            "aaSorting": [[3, 'desc']], //desc
            "bDestroy" : true,
            "bRetrieve":true,
            "bProcessing": true,
            "bDeferRender": true,
            "aaData": datas,
            "iDisplayLength": 50,
            "bPaginate": false,
            "aoColumns": [
            {
                "sTitle":"Person Name",
                "mData": "firstName",
                "mRender": function(data, type, row){
                    return row.firstName+" "+row.middleName+" "+row.lastName;
                } 
            },{
                "sTitle":"Pack Name",
                "mData": "name"
            }, {
                "sTitle":"Purchased Date",
                "mData": "purchaseDate",
                "mRender": function(data, type, row) {
                    var date = formDate(row.purchaseDate);
                    return date;
                }
            },{
                "sTitle":"Total SMS",
                "mData": "noOfSms"
            },{
                "sTitle":"Sent SMS",
                "mData": "sentSms"
            },{
                "sTitle":"Balance SMS",
                "mData": "sentSms",
                "mRender": function(data, type, row) {
                    return (row.noOfSms - row.sentSms);
                }
            },{
                "sTitle":"History",
                "mData": "sentSms",
                "mRender": function(data, type, row) {
                    return "<span class='showHistory'>Show History</span>";
                }
            }]
        });
    }else{
        dataTable_smsTracker.dataTable().fnClearTable();
        dataTable_smsTracker.fnAddData(datas)
    }

}
