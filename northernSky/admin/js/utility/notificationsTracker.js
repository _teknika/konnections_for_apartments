var dataTable_smsTracker;
$(function(){
    $("#header_section").load("header.php");
    $("#sidemenu_section").load("side_menu.php");

    

    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    var userId = adminInfo.personId;
    var orgId = adminInfo.orgId;

    var type = 0; //GCM
    var details = {"orgId":orgId, "userId":userId, "type":type};
    getNotificationsTracker(details);
    generateNotificationsTrackerTable("");

    // Show Popup For All Details of Lead
    $(dataTable_smsTracker).on('click', '.showHistory', function(event) {
        var row = $(this).closest("tr").get(0);
        var rowIndex = dataTable_smsTracker.fnGetPosition(row);
        var smsData = dataTable_smsTracker.fnGetData(rowIndex);
        localStorage.setItem("smsUser", JSON.stringify(smsData));
        location.href="smsHistory.php";
    });
});


function getNotificationsTracker(details){
    showLoader();
    var dataR = {"request":details};
    $.ajax({
        type: "POST",
        //contentType: 'application/json',
        dataType: 'json',
        url: getUrl(api.GETSMSHISTORY_URL),
        data: JSON.stringify(dataR)
    }).done(function (response){
        validationOnNotificationsTracker(details, response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
    return false;
}

function generateNotificationsTrackerTable(datas){
    if (typeof dataTable_Notifications == 'undefined'){
        dataTable_Notifications = $("#notificationsTrackerTable").dataTable({
            "iDisplayLength": -1,
            "aaSorting": [[2, 'desc']], //desc
            "bDestroy" : true,
            "bRetrieve":true,
            "bProcessing": true,
            "bDeferRender": true,
            "aaData": datas,
            "iDisplayLength": 50,
            "bPaginate": false,
            "aoColumns": [
            {
                "sTitle":"Sender Name",
                "mData": "sFname",
                 "mRender": function(data, type, row){
                    return row.sFname+" "+row.sMname+" "+row.sLname;
                }
            }, {
                "sTitle":"Message",
                "mData": "message"
            },{
                "sTitle":"Transaction Time",
                "mData": "transactionTime",
                 "mRender": function(data, type, row){
                    return formDateTime(row.transactionTime);
                }
            }]
        });
    }else{
        dataTable_Notifications.dataTable().fnClearTable();
        dataTable_Notifications.fnAddData(datas)
    }

}
