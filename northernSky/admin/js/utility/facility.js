$(function(){
	$("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");
    $("#facilityForm").validate({
        messages: {
               
        },
        submitHandler: function (facilityForm) {
            addfacility();
        }           
    });
    enableVendorSearch();
    $(document).on("click",".cancelBtn",function(e){
        e.preventDefault();
        $(".facility-btn-grp >button[type=reset]").addClass("hide");
        $('#facilityForm')[0].reset();
        $(".facility-btn-grp > button[type=submit]").text("add");
    });
});
 function  enableVendorSearch(){
      $("#SearchQry").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#facilityTable tbody >tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
   }  
function addfacility(){
	var facilityFormArr=$('#facilityForm').serializeArray();
	var facilityFormObj={};
	facilityFormArr.forEach(function(obj,index,fullArray){
        facilityFormObj[obj.name]=obj.value;
    });
    facilityFormObj['rowId']=$("#rowId").val();
	 $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "../server/addFacility.php",
        data: JSON.stringify(facilityFormObj)
    }).done(function(response) {
      hideLoader();
      var alertTxt="Facility added successfully";
      var btnText=$(".facility-btn-grp > button[type=submit]").text();
      if(btnText!="add"){
        alertTxt="Facility updated successfully";
      }
      alert(alertTxt);
      location.reload();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
function viewFacility(facilityId){
   window.location.href="manageAvailabilty.php?facilityId="+facilityId; 
}
function editFacility(id){
  console.log(id);
  var rowData=$(this).closest("tr").find("td");
  var facilityName=rowData[0].innerText;
  var facilityCharge=rowData[1].innerText;
  $("#facilityName").val(facilityName);
  $("#chargePerHour").val(facilityCharge);
  $("#rowId").val(id);
  $(".facility-btn-grp >button[type=submit]").text("update");
  $(".facility-btn-grp >button[type=reset]").removeClass("hide");
}
function deleteFacility(facilityId){
     $.ajax({
        type: "GET",
        url: "../server/deleteFacility.php?id="+facilityId,
        }).done(function(response) {
          hideLoader();
          alert("Facility deleted successfully");
          location.reload();
        }).fail(function(jqXHR, textStatus, errorThrown) {
            hideLoader();
            alert("Server failed");
        })
        .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
