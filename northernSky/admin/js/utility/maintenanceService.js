$(function(){
  
  //to avoid the error=>when i click on any one button it will show dropdown in both button
  $("#dropdownMenu1").click(function(){
    $('#dropdown-menu2').css("display","none");
  });
  $("#dropdownMenu2").click(function(){
    $('#dropdown-menu1').css("display","none");
  });
  
	   $('#maintenaceServiceForm').validate({ 
    rules: {
	  fNumber: {
        required: true,
		 minlength: 5,
		 maxlength: 5,
      },

    },
    messages: {
      fNumber: {
        required: "Please enter flat number",
		minlength: "Please check format and Enter proper flat Number",
		maxlength: "Please check format and Enter proper flat Number",
      },
     
    },

    errorPlacement: function(error, element) {
      var divObj=document.createElement("div");
      divObj.setAttribute("class","error-messageColor");
       element.parent().append(divObj);
     error.appendTo(element.next());
    },

    /**this code snippit causing double click (model will open after clicking on button twice) */
    // submitHandler: function(maintenaceServiceForm) {
    //    $("#maintenaceServiceForm").submit(function(e){
    // 	e.preventDefault();
    // 	var basicServiceInfoObj={};
    // 	$(this).serializeArray().forEach(function(obj,val){
    		 
    //           basicServiceInfoObj[obj.name]=obj.value;
    // 	});
    // 	basicServiceInfoObj["createdBy"]=JSON.parse(localStorage.adminInfo).personId;
    // 	getOwner(basicServiceInfoObj);
    // }); 
    // }
    
     submitHandler: function() {
      var flatNo=$("#fNumber").val()
      var createdBy=localStorage.adminInfo.personId;
    	getOwner({"fNumber":flatNo,"createdBy":createdBy}); 
    }

  }); 
  
});
$(function(){
	$("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");

   
    $(document).on("click","#getHistory",function(e){     
        getMaintenanceHistory();
    });
    $("#uploadMaintenaceExcel").click(function(e){
       e.preventDefault();
       var uploadingFor=$(this).attr("data-uploadingFor");
       //$("#uploadingFor").val(uploadingFor);
       uploadMaintenanceExcel();
    });
    
  //   $(document).on("click",".uploadExcel",function(e){     
  //     e.preventDefault();
  //     resetUploadForm();
  //     var text="Upload maintenance file";
  //     if(isElectricityBillupload()){
  //         $("#uploadMaintenance .modal-title").html("Upload Electricity Bill");
  //     }else{
  //         $("#uploadMaintenance .modal-title").html("Upload Maintenance Bill");
  //     }
  //     $("#uploadMaintenance").modal();
  // });

  //created two upload bytton in one page only
    $(document).on("click",".uploadExcelMain",function(e){     
        e.preventDefault();
        resetUploadForm();
        $("#uploadMaintenance").modal();
    });
    $(document).on("click",".uploadExcelElec",function(e){     
      e.preventDefault();
      resetUploadForm();
      $("#uploadElectricity").modal();
  });
    $(document).on("click","#failedFilePath",function(e){ 
        e.preventDefault(); 
        window.open('data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,' + encodeURIComponent($(this).attr("href")));
    }); 
     $(document).on("click",".openCollapse",function(e){
           $(".collapsiblePanel").collapse("hide");
           if($(this).attr("id")=="addBalance"){
                  $("#balanceForm")[0].reset();
                  $("#addBalanceContainer").collapse("show");
           }else if($(this).attr("id")=="expenses"){
                 $("#expensesContainer").collapse("show");
           }else{
               $("#historyContainer").collapse("show");
               getMaintenanceHistory();
           }
     });
     $(document).on("change","#paymentMode",function(e){
        console.log(e);
        //$("#balanceForm")[0].reset();
        $("#paymentMode").val() == "DebitCard" || $("#paymentMode").val() == "CreditCard"? $(".auth-code").removeClass('hide'): $(".auth-code").addClass('hide');
        $("#paymentMode").val() == "Cheque"? $(".checkDetail").removeClass('hide'): $(".checkDetail").addClass('hide');
     });
    EnableValidationForAddMaintanence();//this validates fields the add Balance or add Maintenance form
});
function resetUploadForm(){
    $("#uploadMaintenanceForm")[0].reset();
    $("#uploadMaintenaceExcel").removeClass("hide");
    $(".upload-success").addClass("hide");
    $(".upload-failed").addClass("hide");
}
//return maintenanceBill or electricitybill
function isElectricityBillupload(){
    if($("#uploadingFor").val()=="Electricity Bill"){
        return true;
    } 
    return false;
}
//below function upload maintenance excel to server
function uploadMaintenanceExcel(){
    var formData=new FormData($("#uploadMaintenanceForm")[0]);
    formData.updatedBy=JSON.parse(localStorage.adminInfo).personId;
    var api='../server/uploadExcel/maintenaceBill.php';
    if(isElectricityBillupload()){
        api='../server/uploadExcel/ElectricityBill.php';
    }
    showLoader();
    $.ajax({
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        url:api ,     
    }).done(function(response) {
        hideLoader();
        var index=response.indexOf("{");
        var resultObj=JSON.parse(response.slice(index));
        if(resultObj.isSuccessful === false){
            $(".upload-success").removeClass("hide");
        }else{
             if(resultObj.failedRows==0){
                $(".upload-success").removeClass("hide");
             }else{
                $(".failedRows").text(resultObj.failedRows);
                $(".totalRows").text(resultObj.totalRows);
                var filePathArr=resultObj.filePath.split("/");
                filePathArr.splice(1,0,"server");
                var finalFilePathArr=filePathArr.join("/");
                $(".failedFilePath").attr("href",finalFilePathArr);
                //$("#downloadXLS").attr("action",finalFilePathArr);
                
                $(".upload-failed").removeClass("hide");
             }
            //$("#uploadMaintenance").modal('hide')
        }
        $("#uploadMaintenanceForm")[0].reset();
        $("#uploadMaintenaceExcel").addClass("hide");
        //$("#uploadMaintenance").modal('hide')
      // if(JSON.parse(response).isSuccessful == true){
       // alert("Banner added successfully");
       // location.reload();
     // }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
 
}

function EnableValidationForAddMaintanence(){
    $('#balanceForm').validate({ //...................validating registration form starts
    rules: {
      name: {
        required: true,
      },
      amount: {
         required: true,
      },
      checkNum: {
         required: true,
      },
      expireDate: {
         required: true,
         isExpireDateValid:true
      }

    },
    messages: {
      name: {
        required: "Please enter First name."
      },
      amount: {
       required: "Please enter the amont."
      },
       checkNum: {
        required: "Please enter check number."
      },
       expireDate: {
        required: "Please enter the expire date.",
        isExpireDateValid:"This cheque has already expired."
      },
    },

    errorPlacement: function(error, element) {
      var divObj=document.createElement("div");
      divObj.setAttribute("class","error-messageColor");
       element.parent().append(divObj);
     error.appendTo(element.next());
    },

    submitHandler: function(form) {
        if($("#amount").val() > 0){
          
        var ownerId=$("#searchedOwnerInfo").attr('data-userid');
        var  updatedBy=JSON.parse(localStorage.adminInfo).personId;
        var amountEntered=$("#amount").val();
        var basicMaintenanceServiceObj={};
        basicMaintenanceServiceObj.ownerId=ownerId;
        basicMaintenanceServiceObj.parentId="-1";
        basicMaintenanceServiceObj.personId=ownerId;
        basicMaintenanceServiceObj.mobileNumber=$("#searchedOwnerInfo #mobileNo").text();
        basicMaintenanceServiceObj.updated_by=updatedBy;
        basicMaintenanceServiceObj.transactionType="credit";
        basicMaintenanceServiceObj.perticular="Maintenance";
        basicMaintenanceServiceObj.from="Web";
        basicMaintenanceServiceObj.paymentMode=$("#paymentMode").val();
        var amountdetailArr=$("#balanceForm").serializeArray();
        amountdetailArr.forEach(function(elementObj,i){
            console.log(elementObj.name,elementObj.value);
            basicMaintenanceServiceObj[elementObj.name]=elementObj.value;
        });
        updateBasicService(basicMaintenanceServiceObj);
       }else{
       if($("[name='amount']").val().trim().length < 1){
            $("[name='amount']").css('animation', 'errorPulse 1s infinite');//if nothing entered show error
          }
        }
    }
  }); 

  $.validator.addMethod("isExpireDateValid", function (value, element, options){
    var res=new Date(value)-new Date();        
    return res > 0;
        },"This cheque has already expired.");
}

function getOwner(basicServiceInfoObj){
      showLoader();
      console.log(basicServiceInfoObj);
	    $.ajax({
	        type: "POST",
	        contentType: 'application/json; charset=utf-8',
	        dataType: 'json',
	        url: "../server/getOwnerDetailsByFlat.php",
	        data: JSON.stringify(basicServiceInfoObj)
	    }).done(function(response) {
	        hideLoader();
	       console.log(response);	     
          if(!$.isEmptyObject(response)){
               var ownerInfo= response.ownerInfo;
          	 var maintenanceInfo= $.isEmptyObject(response.maintenanceInfo) ? "Nill":response.maintenanceInfo.balance;
            $("#searchedOwnerInfo").removeClass('hide');
            $(".collapsiblePanel").collapse("hide");
          	$("#fullName").html(ownerInfo.firstName+" "+ownerInfo.lastName);
            $(".emailId").html(ownerInfo.emailId);
            $(".BlockName").html(ownerInfo.blockName);
	        $(".flatNo").html($("#fNumber").val());
	        $(".mobileNo").html(ownerInfo.mobileNumber);
            $(".flatHolder").html(ownerInfo.flat_holder);
            $(".currMaintenance").html(maintenanceInfo);  
            
	        $("#searchedOwnerInfo").attr('data-userid', ownerInfo.id);
	           // document.getElementById("searchedOwnerInfo").style.animation='shake 1s 1';
            $("#flat").empty();
            $(".flatNo").text().split(",").forEach(function(element){
            $("#flat").append("<option value='"+element+"'>"+element+"</option>");
           });
            }else{
          	$("#searchedOwnerInfo").addClass('hide');
            $(".collapsiblePanel").collapse("hide");
           }
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	        hideLoader();
	        //alert("Unable to load categories");
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
    }

    function updateBasicService(basicServiceInfoObj){
        showLoader();
	    $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/addMaintenance_WEB.php",
	        data: JSON.stringify(basicServiceInfoObj)
	    }).done(function(response) {
	        hideLoader();
           $("#balanceForm")[0].reset();
          $("#getHistory").click();
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	        hideLoader();
	        //alert("Unable to load categories");
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
    }
   function getMaintenanceHistory(){
        var ownerId=$("#searchedOwnerInfo").attr('data-userid');
        var flatNumber=$("#balanceForm #flat").val();
        showLoader();
	    $.ajax({
	        type: "POST",
	        contentType: 'application/json; charset=utf-8',
	        dataType: 'json',
	        url: "../server/getMaintenanceHistory_WEB.php",
	        data: JSON.stringify({ownerId:ownerId,flat:flatNumber})
	    }).done(function(response) {
	        hideLoader();
	       console.log(response);            
            $("#currentBalance").html(response.maintenaceTableData[0].balance); 
            $(".currMaintenance").text(response.maintenaceTableData[0].balance);
            generateHistoryTable(response.traceMaintenaceTableData);
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	        hideLoader();
	        //alert("Unable to load categories");
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
   }
   function generateHistoryTable(resp){
    $('#maintenaceHistory').DataTable( {
        data:resp,
        "bDestroy": true, 
        columns: [
            { title: "Date",
              data : "created_at",
            },
            { title: "Perticulars",
              data : "perticulars",
              width:"20%",
              render : function(datam, type, row) {
                var detail=JSON.parse(row.details);
                var perticulars="";
                if(datam.search(/facility/i) > -1){
                    perticulars+=detail.facilityName+" ("+detail.name+") ";
                    perticulars+=detail.start+"-"+detail.end.split(" ")[1];
                }else if(datam.search(/[Maintenance Bill | Electricity Bill]/i) > -1){
                	 perticulars+=datam;
                } else{
                    perticulars+=detail.paymentMode+" ("+detail.name+")";
                }       
                return perticulars;
               }
            },
            { title: "Cheq No",
              data : "details",
              render : function(datam, type, row) {
                var detail=JSON.parse(datam);
                var cheque_detail="";
                if(detail.checkNum){
                    cheque_detail=detail.checkNum+" ("+detail.expireDate+")";
                }               
                return cheque_detail;
               }
            },
            { title: "Debit",
              data : "debit",
              render : function(datam, type, row) {
                if(datam != 0){
                    return datam;
                } 
                return "";               
               } 
            },
            { title: "Credit", 
              data : "credit",
              render : function(datam, type, row) {
                if(datam != 0){
                   return datam;
                } 
                return "";              
               }
            },
            { title: "Authentication Code",
              data : "details",
              render : function(datam, type, row) {
                var detail=JSON.parse(datam);
                if(detail.authCode){
                    return detail.authCode;
                }               
                return "";
               }
            },
            { title: "Balance", 
              data : "Balance",
            },
           /* { title: "Balance",
              data : "orgName",
              orderable : false,
              render : function(datam, type, row) {
                 return '<button type="button" class="btn btn-default btn-flat btn-sm editInfo"><i class="fa fa-edit "></i></button>'  
             } 
            } */
        ]
    } );
   }
   