$(function(){

    // var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    // if(adminInfo != undefined){
    //     $(".username").html(adminInfo['firstName']+" " + adminInfo['lastName']);
    //     if((adminInfo.photo != null) || (adminInfo.photo != undefined) || (adminInfo.photo != "") || (adminInfo.photo != " ")){
    //         $(".adminPhoto").html("<img src='"+user_img+"/"+adminInfo.photo+"' class='img-circle profile_img '>");
    //         $(".adminPhotoHead").html("<img src='"+user_img+"/"+adminInfo.photo+"' >");
    //     }else{
    //         $(".adminPhoto").html("<img src='img/role.png' class='img-circle profile_img'/>");
    //         $(".adminPhotoHead").html("<img src='img/role.png' />");
    //     }
    // }
});

function on_click_sign_out() {
    localStorage.clear();
    window.location.replace("logout.php");
}

function showLoader(){
    $(".fader").show();
	$(".loader").show();
}
function hideLoader(){
    $(".fader").hide();
	$(".loader").hide();
}

// Register Pages
function getBuildingInfo(){
    showLoader();
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
         async: false,
        url: "../server/getBuildingInfo.php",
        data: ""
    }).done(function(response) {
        hideLoader();
        generateBuildingInfoDropdown(response);
        
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        //alert("Unable to load categories");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
function generateBuildingInfoDropdown(response){
    $(".blockName_dropdown").empty();
    var defoption = $("<option value=''>-- Select the Block --</option>")
    $(".blockName_dropdown").append(defoption);
    for(var i=0;i<response.length;i++){
            var option = $("<option value='"+response[i].buildingName+"'>"+response[i].buildingName+"</option>")
            $(".blockName_dropdown").append(option);
    }

}
function generateBusinessCategory(response){
	var businessCategory = response.businessCategory;
	$(".businessCategory_dropdown").empty();
	var defoption = $("<option value=''>-- Select the Category --</option>")
    $(".businessCategory_dropdown").append(defoption);
    for(var i=0;i<businessCategory.length;i++){
            var option = $("<option value='"+businessCategory[i]+"'>"+businessCategory[i]+"</option>")
            $(".businessCategory_dropdown").append(option);
    }
}

function generateBloodGroup(response){
	var bloodGroup = response.bloodGroup;
	$(".bloodGroup_dropdown").empty();
	var defoption = $("<option value=''>-- Select the Blood Group --</option>")
    $(".bloodGroup_dropdown").append(defoption);
    for(var i=0;i<bloodGroup.length;i++){
            var option = $("<option value='"+bloodGroup[i]+"'>"+bloodGroup[i]+"</option>")
            $(".bloodGroup_dropdown").append(option);
    }
}

function generateCity(response){
	$(".city_dropdown").empty();
	var defoption = $("<option value=''>-- Select the City --</option>")
    $(".city_dropdown").append(defoption);
    for(var i=0;i<response.length;i++){
            var option = $("<option value='"+response[i].id+"'>"+response[i].name+"</option>")
            $(".city_dropdown").append(option);
    }
}







function formName(value){
    return checkNull(value.firstName)+" "+checkNull(value.middleName)+" "+checkNull(value.lastName);
}

function checkNull(value){
    if(value == null){
        return "";
    }else{
        return value;
    }
}

function checkEmpty(value){
    if(value == ""){
        return null;
    }else{
        return value;
    }
}



function dateFormat(value){
    if(value == null){
        return "";
    }else{
        var dateVal = new Date(value);
        var month = dateVal.getMonth()+1;
        var day = dateVal.getDate();
        var date = ((''+day).length<2 ? '0' : '') + day+"-"+((''+month).length<2 ? '0' : '') + month +"-"+dateVal.getFullYear();
        return date; 
    }
}

function formDate(date) {
    var data = new Date(1000*date);
     var month = data.getMonth()+1;
     var day = data.getDate();
     var ret = ((''+day).length<2 ? '0' : '') + day+"-"+((''+month).length<2 ? '0' : '') + month +"-"+data.getFullYear()
     return ret;
}

function formDateTime(date) {
    var data = new Date(1000*date);
     var month = data.getMonth()+1;
 var day = data.getDate();
 var hours = data.getHours();
 var minutes = data.getMinutes();
 var seconds = data.getSeconds();
 var mseconds = data.getMilliseconds();
 
 var ret = ((''+day).length<2 ? '0' : '') + day+"-"+((''+month).length<2 ? '0' : '') + month +"-"+data.getFullYear()+" "+((''+hours).length<2 ? '0' : '')+hours+":"+((''+minutes).length<2 ? '0' : '')+minutes+":"+((''+seconds).length<2 ? '0' : '')+seconds+"."+((''+mseconds).length<2 ? '0' : '')+mseconds; 
 return ret;

}



function getCategoryInfo(){
    showLoader();
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.CATEGORY_URL),
        data: ""
    }).done(function(response) {
        hideLoader();
        generateBusinessCategory(response);
        generateBloodGroup(response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        //alert("unable to download categories. Refresh...");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}


function getCities(){
    showLoader();
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.CITY_URL),
        data: ""
    }).done(function(response) {
       generateCity(response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Unable to fetch cities list");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
//shilpa@ Function to split blockname and adds a flatnumber 
function concatBlokAndFlat(blockName,flatNum){
	var flatNolength = flatNum.length;
	if(flatNolength == 3)
	{
        var blockWithFlat=blockName.split(" ")[1]+"0"+flatNum;	
	}
	else{
		  var blockWithFlat=blockName.split(" ")[1]+flatNum;	
		   
	    }
		
return blockWithFlat;
}

