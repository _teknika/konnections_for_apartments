var issuedDate,chargePerHour;
$(function(){
    $("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");
    
    $('#issueCardFrom').validate({ //...................validating eventForm form starts
        rules: {
          flat: {
            required: true,
          },
          memberName: {
            required: true,
          },
          cardsList: {
             required: true,
          },
        },
        messages: {
            flat: {
            required: "Please select flat."
          },
          memberName: {
           required: "Please select name."
          },
          cardsList: {
           required: "Please select the card"
          }
        },

        errorPlacement: function(error, element) {
          
        },

        submitHandler: function(form) {
           var formDataArr=$(form).serializeArray();
           var formObj={};
           formDataArr.forEach(function(obj,indx){
            formObj[obj.name]=obj.value;
           });
           issueCard(formObj);
        }
}); //...............eventForm form validation ends
$('.RecieveCardFrom').validate({ //...................validating eventForm form starts
  rules: {
    flat: {
      required: true,
    },
    memberName: {
      required: true,
    },
    card: {
       required: true,
    },
    issuedAt: {
      required: true,
   },
  },
  messages: {
      flat: {
      required: "Please select flat."
    },
    memberName: {
     required: "Please select name."
    },
    card: {
     required: "Please select the card"
    },
    issuedAt: {
      required: "Please check the issued date"
     }
  },

  errorPlacement: function(error, element) {
    
  },

  submitHandler: function(form) {
     var formDataArr=$(form).serializeArray();
     var formObj={};
     formDataArr.forEach(function(obj,indx){
      formObj[obj.name]=obj.value;
     });
    onRecieveCard(formObj);
  }
}); //...............eventForm form validation ends
$("#issueCardSearch").on("keyup", function() {
         var value = $(this).val().toLowerCase();
         $(".issued-cards-detail-tb tbody>tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
         });
});
     $("select[name=flat]").change(function(){
        getMembersOfFlat( $("select[name=flat]").val());
     });
     $(".editAmount").click(function(){
        $("#totalCharge").removeAttr("disabled");
     });
     getFlatsList();
     getCardsCatagoryList();
     getIssuedCardsList();
     getCurrentPeopleInfo();
     $(document).on("change","select[name=catagoryList]",function(){
       var facilityId=$(this).val();
       if(facilityId.length < 1){
        $('select[name="cardsList"]').addClass("hide");
        return false;
       }
        getCardsList(facilityId);
     });
});

//getting all flats list to show in dropdown
function getFlatsList(){
    $.ajax({
        type: "GET",
        url: "../server/getAllFlats.php",
    }).done(function(response) {
      hideLoader();
      $("select[name=flat]").empty();
      JSON.parse(response).forEach(function(num,indx){
        var optnEle="<option value="+num+">"+num+"</option>";
        $("select[name=flat]").append(optnEle);
      });
     
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
//ends
//getting all members list for selected dropdown
function getMembersOfFlat(flatNum){
    $.ajax({
      type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
      url: "../server/getOwnerDetailsByFlat.php",
      data:JSON.stringify({fNumber:flatNum}),
     }).done(function(response) {
      hideLoader();
      //
      if(!$.isEmptyObject(response)){
	    	console.log(JSON.stringify(response));
	    	var ownerInfo=response.ownerInfo;
	    	var familyMemberList=response.membersList;
	    	//var maintenanceInfo=response.maintenanceInfo;
	    	$(".memberName").empty().append("<option value='"+ownerInfo.firstName+"'>"+ownerInfo.firstName+"</option>");
			    familyMemberList.forEach(function(item,indx){
            $(".memberName").append("<option value='"+item.firstName+"'>"+item.firstName+"</option>");
			  });
      // 
      }  
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }

  //getting all cards list to show in dropdown
  function getCardsCatagoryList(){
    $.ajax({
        type: "GET",
        url: "../server/facilitycards/getCardsCatagoryList.php",
    }).done(function(response) {
      var selectEle="select[name=catagoryList]";
      $(selectEle).empty();
      $(selectEle).append('<option value="">Select Category</option>');
      var cardsCatagory=JSON.parse(response);
      cardsCatagory.forEach(function(ele,indx){
        var optionEle="<option value='"+ele.facility_id+"'>"+ele.facility_name+"</>";
        $(selectEle).append(optionEle);
      });
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
//ends
  //getting all cards list to show in dropdown
function getCardsList(facilityId){
    $.ajax({
        type: "GET",
        url: "../server/facilitycards/getAllCardsByFacilityId.php?facilityId="+facilityId,
    }).done(function(response) {
      var resp=JSON.parse(response);
      var availableCards=resp.availableCards;
      var numberOfPeople=resp.numberOfPeople;
      hideLoader();
      $(".issued").html(numberOfPeople);
      $("select[name=cardsList]").empty();
     availableCards.forEach(function(obj,indx){
        var optnEle="<option value="+obj.id+">"+obj.card+"</option>";
        $("select[name=cardsList]").append(optnEle);
      });
      $('select[name="cardsList"]').removeClass("hide");
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
//ends

//get people in gym or swimming pool info
function getCurrentPeopleInfo(){
  $.ajax({
    type: "GET",
    url: "../server/facilitycards/getCurrentPeopeInfo.php",
  }).done(function(response) {
    hideLoader();
    var detailObj=JSON.parse(response);
    detailObj.numberOfPeople.forEach(function(info,indx){
        if(info.facilityName=="Gym"){
            $(".gymCnt").html(info.currentPeople+"/"+info.totalCards);
        }else{
          $(".swimmingCnt").html(info.currentPeople+"/"+info.totalCards);
        }
    });
  
    
  }).fail(function(jqXHR, textStatus, errorThrown) {
      hideLoader();
      alert("Server failed");
  })
  .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}

//ends
//issue card  starts
  function issueCard(formObj){
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "../server/facilitycards/issueCard.php",
        data: JSON.stringify(formObj)
    }).done(function(response) {
      hideLoader();
      getIssuedCardsList();
      getCardsList($('select[name=catagoryList]').val());
      getCurrentPeopleInfo();      
      alert("Card added successfully");
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
//

function getIssuedCardsList(){
    $.ajax({
        type: "GET",
        url: "../server/facilitycards/getIssuedCardsList.php?facilityId=4",
    }).done(function(response) {
      hideLoader();
      $(".issued-cards-detail-tb > tbody").empty(); 
      JSON.parse(response).forEach(function(Obj,indx){
        var rowData="<tr>"+
                        "<td class='flat'>"+Obj.card+"</td>"+   //card
                        "<td class='issuedAt'>"+Obj.issued_date+"</td>"+    //issued_at
                        "<td>"+Obj.facility_name+"</td>"+  
                        "<td class='card'>"+Obj.flat+"</td>"+   //flat
                        "<td class='personName'>"+Obj.name+"</td>"+  //name
                        "<td class='onRecieveCard' onclick='getDetailsOfIssuedCard("+Obj.id+")'>"+"Close"+"</td>"+  //action
                     "</tr>";
        $(".issued-cards-detail-tb > tbody").append(rowData);             
      });
   
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
//
//recieve te card code  starts
function onRecieveCard(formObj){
  var changedTime=getRecievedTime();
  var formObj={
               id:$("#RecieveCardModal").attr("data-id"),
               dateChangedInDashboard:$("#recievedAt").val(),
               totalCharge:$("#totalCharge").val()
              };
  $.ajax({
      type: "POST",
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      url: "../server/facilitycards/recieveCard.php",
      data: JSON.stringify(formObj)
  }).done(function(response) {
    hideLoader();
    $("#RecieveCardModal").modal("hide");
    getIssuedCardsList();
    getCardsList();
    getCurrentPeopleInfo();
  }).fail(function(jqXHR, textStatus, errorThrown) {
      hideLoader();
      alert("Server failed");
  })
  .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
//recieve te card code  ends

//getting all cards list to show in dropdown

  function getDetailsOfIssuedCard(id){
    $.ajax({
        type: "GET",
        url: "../server/facilitycards/getDetailsOfIssuedCard.php?id="+id,
    }).done(function(response) {
      hideLoader();
      var detail=JSON.parse(response);
      detail=detail[0];
      $("#flat").val(detail.flat);
      $("#personName").val(detail.name);
      $("#card").val(detail.cardName);
      $("#issuedAt").val(detail.issued_date);
      issuedDate=detail.issued_date;
      chargePerHour=detail.charge_per_hour;
     
      //$("recievedAt").val($("#recievedAt").val(currentDateTime.toISOString().split("T")[0]));
      /*
      var currentDateTime=new Date(); 
      var hours=currentDateTime.getHours();
      hours.length <1 ?hours="0"+hours : hours;
      var min=currentDateTime.getMinutes();
      min.length < 1 ?min="0"+min : min; 
      hrmin=hours+":"+min; 
      var recievedDate=currentDateTime.toISOString().split("T")[0]; 
      $("#recievedAt").val(recievedDate+" "+hrmin+":00");   
       
      */
     $("#recievedAt").val(detail.recievedTime); 
      var totalCharge=calculateTotalCharge(detail.issued_date,detail.charge_per_hour);
      $("#totalCharge").val(totalCharge);
      $("#RecieveCardModal").attr("data-id",detail.id);
      $("#RecieveCardModal").modal();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
//ends

function calculateTotalCharge(issuedDate,chargePerhour){
    var recievedTime=getRecievedTime();
    var issuedTime=new Date(issuedDate);
    issuedTime.setSeconds(0);
     var timeDiff=recievedTime-new Date(issuedTime);//time diffrence in ms
     var totalMinutes=timeDiff/(60*1000);//converting to minutes
     var hours=totalMinutes/60;
     var hours=hours.toFixed(2).toString();//this gives hour:min part
     var hourPart=hours.split(".")[0];
     var minutesPart=hours.split(".")[1];
     if(minutesPart > 59 ){
      hourPart++;
      minutesPart=minutesPart % 60;
     }
     Number(minutesPart) > 10 ? hourPart++ : "" ;//if min is greater than 10 min then we are increaing adding one hour  extra.
     return hourPart * chargePerhour;//calcualating charge per hour
}
function getRecievedTime(){
  var datenow=new Date($("#recievedAt").val());
  return datenow;
}
function msToTime(duration) {
  var milliseconds = parseInt((duration % 1000) / 100),
    seconds = parseInt((duration / 1000) % 60),
    minutes = parseInt((duration / (1000 * 60)) % 60),
    hours = parseInt((duration / (1000 * 60 * 60)) % 24);
     // hours = (hours < 10) ? "0" + hours : hours;
 // minutes = (minutes < 10) ? "0" + minutes : minutes;
 // seconds = (seconds < 10) ? "0" + seconds : seconds;

  return hours;
}
