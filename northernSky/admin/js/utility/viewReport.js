var faciltyTableObj={},
    multiFaciltyTableObj={};
$(function(){
	$("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");
    getFacilityList();
    //below function enables the click events for edit and delete option in viewReport datatables

     $(document).on("click",".slotEdit",function(e){
        var bookedStatusVal=$(this).closest("tr").find("span.bookStatus").attr("attr-val");
        var paymentStatusVal=$(this).closest("tr").find("span.payStatus").attr("attr-val");
         
        $('#editBookedSlotForm').attr("attr-id",$(this).parent().attr("data-id"));
        $('.booking-status input[type=radio][value='+bookedStatusVal+']').prop('checked', 'checked');
          $('.payment-status input[type=radio][value='+paymentStatusVal+']').prop('checked', 'checked');
                    //we are storing this context in editedrow andits golbal
          editedrow=this;
          $("#editBookedSlotModal").modal();
       });
    $(document).on("click",".submitEditedBookedSlot",function(e){
          e.preventDefault();
          var editedObj={};
          $("#editBookedSlotForm").serializeArray().forEach(function(ele,i){
              console.log(ele.name,ele.value,i);
              editedObj[ele.name]=ele.value;
          });
          editedObj.id=$('#editBookedSlotForm').attr("attr-id");
          editBookedSlotInfo(editedObj);
       });
 //ends
      //unAvailability for validation
      $("#viewReportForm").validate({
        rules:{
            startDate: {
                        required: true,
                      },
            endDate: {
                        required: true,
                        endDate:true
                         
                      },                      

        },
        messages: {
           startDate: {
                        required: "Please enter start date."
                      },
              endDate: {
                        required: "Please enter end date.",
                        
                      }, 
        },
        submitHandler: function (Form) {
             var obj={};
            $(Form).serializeArray().forEach(function(element,i){
             obj[element.name]=element.value;
            });
            var facilityType=$("#facility>option:selected").attr("data-facilityType");
            var urlForReport="../server/facilities/getFacilityReports.php";
            if(facilityType == 1){ //0=> single slot ,1=>multi slot
              urlForReport="../server/facilitycards/getCadsReports.php";
            }
            getReports(obj,urlForReport,facilityType);
        }           
    });
    // validation ends here  
      $.validator.addMethod("endDate", function(value, element) {
            var startDate = $('#startDate').val();
            return Date.parse(startDate) <= Date.parse(value) || value == "";
        }, "* End date must be after start date");
    
    
});
 function  enableVendorSearch(){
      $("#SearchQry").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#facilityTable tbody >tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
   }  
function getFacilityList(){ 
   $.ajax({
        type: "GET",
        url: "../server/getFacilityList.php",
    }).done(function(response) {    
      var resultArray=JSON.parse(response);
      resultArray.forEach(function(obj,i,fullArr){
        console.log(obj,i);
        $("#facility").append("<option  value='"+obj.id+"' data-facilityType='"+obj.type+"'>"+obj.facility_name+"</option>");
      });
    }).fail(function(jqXHR, textStatus, errorThrown) { 
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
function getReports(formObj,urlForReport,facilityType){
    $.ajax({
          type: "POST",
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          url:urlForReport,
          data:JSON.stringify(formObj),
      }).done(function(response) {
         if(facilityType == 1){
            $("#reportTable").addClass("hide");
            $("#reportMultiFacilityTable").removeClass("hide");
            multitypeFacilityTable(response);
           }else{
            $("#reportTable").removeClass("hide");
            $("#reportMultiFacilityTable").addClass("hide");
            createReportDataTable(response);
           }
          hideLoader();
          //         
      }).fail(function(jqXHR, textStatus, errorThrown) {
          hideLoader();
          alert("Unable to proccess");
      })
      .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
//below function initializes the data tables
function createReportDataTable(objFordataTable){
   if(!jQuery.isEmptyObject(multiFaciltyTableObj)){
    //$("#reportMultiFacilityTable").DataTable().destroy(true);
    multiFaciltyTableObj.clear();
    multiFaciltyTableObj.destroy();
   }
   faciltyTableObj= $("#reportTable").DataTable({
     "destroy": true,
     "dom": 'Bfrtip',
        "buttons": [
           /* 'copy',*/
            {
                extend: 'excel',
                messageTop: 'The Recreatioal facility information.'
            },
            {
                extend: 'pdf',
                messageBottom: null
            },         
        ],
     "data" : objFordataTable,
     "columns":[
                {
                  "title":"Date", "data": "DateFormat"
                },
               
                {
                  "title":"Time", "data": "TimeFormat"
                },

                {
                  "title":"Amount", "data": "Amount"
                },
                 {
                  "title":"Booking Status", 
                  "data": "isCancelled",
                  render:   function(e)
                  {
                    var bookingStatus="";
                    e == 0 ? bookingStatus='Booked':bookingStatus='Cancelled';
                    return "<span class='bookStatus' attr-val="+e+">"+bookingStatus+"</span>"; 
                  }
                },
                {
                  "title":"Name", "data": "Name"
                },
                 {
                  "title":"Mobile",
                   "data": "mobileNum",
                   render:function(number){
                   	    number.endsWith("/") ? number=number.replace(/\//g,""):"";
                        return number;
                   }
                },
                {
                  "title":"Flat", 
                  "data": "payment_detail",
                  render:   function(paymentDetail)
                  { 
                    var flat=JSON.parse(paymentDetail).flat;
                    return "<span class='flatNum' attr-val="+flat+">"+flat+"</span>"; 
                  }
                
                },
               
              /*  { 
                  "title":"Action",
                  "data": "id",
                  render:   function(id)
                  {return '<span data-id='+id +'><i class="ui-tooltip fa fa-pencil slotEdit"  style="font-size:22px;margin-left:12px;" data-original-title="Edit"></i><span>'; }
                } 
              */  
              ]
   });
}
//initializing data table ends
 //below function initializes the data table for multi type facility Like Gym,Swimming pool
function multitypeFacilityTable(objFordataTable){
  if(!jQuery.isEmptyObject(faciltyTableObj)){
   // $("#reportMultiFacilityTable").DataTable().destroy(true);
   faciltyTableObj.clear();
   faciltyTableObj.destroy();
   }
  multiFaciltyTableObj= $("#reportMultiFacilityTable").DataTable({
   "destroy": true,
   "dom": 'Bfrtip',
      "buttons": [
         /* 'copy',*/
          {
              extend: 'excel',
              messageTop: 'The Recreatioal facility information.'
          },
          {
              extend: 'pdf',
              messageBottom: null
          },
         
      ],
   "data" : objFordataTable,
   "columns":[{
                "title":"Issued At", "data": "issued_date"
              },
              {
                "title":"Flat", "data": "flat"
              },
              {
                "title":"Name", "data": "name"
              },
               {
                "title":"Recieved At", 
                "data": "changed_end_time",
              },{
                "title":"Amout", "data": "total_charge"
              },
            ]
 });
//initializing data table ends
}

//below function updates the booked slot information in db
function editBookedSlotInfo(editedObj){
    $.ajax({
          type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
          url: "../server/facilities/updateBookedSlot.php",
          data:JSON.stringify(editedObj),
      }).done(function(response) {
          hideLoader();
          var checkedInputs= $("#editBookedSlotForm input:checked");
          var checkedInputsArr= Array.prototype.slice.call(checkedInputs);
          $(editedrow).closest("tr").find("span.bookStatus").html(checkedInputsArr[0].nextSibling);
          $(editedrow).closest("tr").find("span.payStatus").html(checkedInputsArr[1].nextSibling);
           $("#editBookedSlotModal").modal("hide");
          //
          
      }).fail(function(jqXHR, textStatus, errorThrown) {
          hideLoader();
          alert("Unable to proccess");

      })
      .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
} 
