var registerData;
$(function(){
     $("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");

    // Calendar
    $('#dobR').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        format:'DD/MM/YYYY'
    }, function (start, end, label) {
        
    });

    $('#dateofmarriageR').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        format:'DD/MM/YYYY'
    }, function (start, end, label) {        
    });
     $('#dob').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        format:'DD/MM/YYYY'
    }, function (start, end, label) {        
    });
     getBuildingInfo();
    // Assign Old Values
    
   var userInfo = JSON.parse(localStorage.getItem("userInfo"));
    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    var orgId = adminInfo.orgId;

    var details = {"orgId":orgId, "userId":userInfo.userId};
    getContactInfo(details);
    editFormValidation(userInfo);

    $(".addFlat").click(function(){
    //var inputElement='<input type="text"  class="flatArr col-xs-6 col-md-6"   required/>';
     //$(this).before(inputElement);
	  addingFlats();
    });
    $(".deleteFlat").click(function(){
       generateFlatsForDeleting();
    });

    $("#addbtn").click(function(){
   //Single line Reset function executes on click of Reset Button 
    $("#addflat")[0].reset();
   });
    
});



function getContactInfo(details){
    showLoader();
    var dataR = {"request":details};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        url: getUrl(api.GETCONTACTINFO_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
       validationOnEditSingleContact(details,response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
//below flatsArr variable stores total flats belongs to current owner
var flatsArr=[];
function getFlatsArray(flatsObj){
      if(flatsObj){
        Object.keys(flatsObj).forEach(function(key,i){
           console.log(flatsObj[key].flat_number);
           flatsArr.push(flatsObj[key].flat_number);        
         });
      }
      return flatsArr;
}

function generateValuesforEdit(contactData){
    var contact= contactData.resp[0];
    var flatsArr=getFlatsArray(contactData.flatsArray);
    $("#firstnameR").val(contact.firstName);
    $("#lastnameR").val(contact.lastName);
    $("#mobilenumberR").val(contact.mobileNumber);
    $("#altNumber").val(contact.alt_number);
    $("#genderR").val(contact.gender);
    $("#dateofmarriageR").val(contact.dateOfMarriage);
    $("#dob").val(contact.dob);
    $("#flat_holder").val(contact.flat_holder);
    $("#emailId").val(contact.emailId);
     $("#altemailId").val(contact.alt_mail);
    //$("#blockName").val(contact.blockName);
    $("#blockName option[value='"+contact.blockName+"']").attr('selected', 'selected');
    $("#flatNo").val(contact.flatNo);
    flatsArr.forEach(function(ele,i){
       var inputElement='<input type="text"  class="col-xs-6 col-md-6"   value="'+ele+'" disabled/>';
       
        $(".addFlat").before(inputElement);
    });
}

function editFormValidation(userInfo){
    var editForm = $("#editForm");
    $("#editForm").validate({
       messages: {
              
       },
        submitHandler: function (editForm) {
            editFormCall(userInfo)
        }           
    });
	
	
}
$(function(){
	   $('#addflat').validate({ 
    rules: {
	  flatNumber: {
        required: true,
		 minlength: 3,
		 maxlength: 4,
      },

    },
    messages: {
      flatNumber: {
        required: "Please enter flat number.",
		minlength: "Please enter 3 numbers",
		maxlength: "Please enter only 4 numbers",
      },
     
    },

    errorPlacement: function(error, element) {
      var divObj=document.createElement("div");
      divObj.setAttribute("class","error-messageColor");
       element.parent().append(divObj);
     error.appendTo(element.next());
    },

    submitHandler: function(addflat) {
      addFlats();
    }
  }); 
  
});


function editFormCall(userInfo) {


    // Basic Info
    var firstname = $("#firstnameR").val();
    var lastname = $("#lastnameR").val();

    var mobilenumber = $("#mobilenumberR").val();
    var altnumber =$("#altNumber").val();
    var gender = $("#genderR").val();
    var dateofmarriage = $("#dateofmarriageR").val();
     var dob=$("#dob").val().replace(/\//g,"-");
   /* if($("#dob").val().search("-")>0 || $("#dob").val().length<1){
          var dob=$("#dob").val();
    }else{
        var tempDateArr=$("#dob").val().split(/\/|-/g);
        var dob=tempDateArr[2]+"-"+tempDateArr[1]+"-"+tempDateArr[0];  
    }*/
    //getting  newly added flat numbers
    var FlatArr=[];
    Array.prototype.slice.call($(".flatArr")).forEach(function(e,i){
       console.log($(e).val());
       FlatArr.push($(e).val());
    });
    //
    
    var flatHolder= $("#flat_holder").val();
    var emailId=$("#emailId").val() ;
    var altMail=$("#altemailId").val();
    var blockName= $("#blockName").val();
    var flatNo=$("#flatNo").val() ;

    var parentId = -1;
    var role=0;
    

    if(dateofmarriage == ""){
        dateofmarriage = null;
    }

    adminInfo = JSON.parse(localStorage.getItem("adminInfo"));

    var basicInfo = {"userId":userInfo.userId,"parentid":parentId,"ishead":1,"orgId":adminInfo.orgId,"role":role,"relationwithhead":"","firstname":firstname,"lastname":lastname,"mobilenumber":mobilenumber,"gender":gender,"dateofmarriage":dateofmarriage,"emailId":emailId,"blockName":blockName,"flatNo":flatNo,"dob":dob,"flatHolder":flatHolder,"altnumber":altnumber,"altMail":altMail,"FlatArr":FlatArr};


    registerData = {"basicInfo":basicInfo};

    showLoader();
    var dataR = {"request":registerData};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.UPDATE_CONTACT_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
       validationOnEditContact(registerData, response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}

function generateFlatsForDeleting(){
   $(".flatsContainer>div").empty(); 
  var  faltsArr=getFlatsArray(); 
  faltsArr.forEach(function(val){  
  var ele='<label><input type="checkbox" name='+val+' value='+val+'>'+val+'</label><br>';
  $(".flatsContainer>div").append(ele);
  });//forEach ends hre
  $("#deleteFlatsModal").modal();
}
//shilpa@ function to get addflat model
function addingFlats(){
  
  $("#addFlatsModal").modal();
}

function deleteFlats(){
   var selectedFlatsArrObj= Array.prototype.slice.call($(".flatsContainer  input:checked"));
   if(selectedFlatsArrObj.length < 1){
     $(".error-msg").removeClass('hide');
     return false; 
    }
   var selectedFlatsArr=[];
    selectedFlatsArrObj.forEach(function(ele){
     selectedFlatsArr.push(ele.value);
    }) ; 
   var userInfo = JSON.parse(localStorage.getItem("userInfo")); 
   var userDetails = {"userId":userInfo.userId};
   var dataObj={
       flats:selectedFlatsArr,
       userDetails:userDetails
   }
  console.log(dataObj);
  //sending details to server
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.DELETE_Flats_URL),
        data: JSON.stringify(dataObj)
    }).done(function(response) {
      window.location.reload();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {});
  //ajax ends
}
//shilpa@ function to send flatno and userId to add_flats file to store 
function addFlats()
{
    var blockName= $("#blockName").val();
	var flatNumber=concatBlokAndFlat(blockName,$("#flatNumber").val()) ;
    var userInfo = JSON.parse(localStorage.getItem("userInfo")); 
    var usersId = userInfo.userId;
	
	//sending details to server 
 $.ajax({
            type:'POST',
            url:'../server/add_flat.php',
            dataType: "json",
            data:{flatNumber:flatNumber,usersId:usersId},
           success: function(response){
		   if(response.isSuccessful)
		   {
				alert("Flat Number Added Successfully");
		           $('#addFlatsModal').modal('hide');
				   window.location.reload();
		   }
				 else 
				 {
	           alert("Flat number already exists! Please add another.");
				 }
		   }
			})
		       
	 
	//ajax ends
    
	           

}


