$(function(){

        $("#main-header").load("headerMenu.php");
        $("#main-footer").load("footerMenu.php");
        $("#main-sidebar").load("sidebarMenu.php");

    $("#cbp-vm").hide();

   initSearch();


});

function initSearch(){
    console.log("Init GCM form - " + $('#gcmForm'));
    var gcmForm = $("#gcmForm");
    $("#gcmForm").validate({
        rules:{
            title:{
                required:true
                },
            message:{
                required:true
            }    

        },messages: { 
             title:{
                required:"Please fill out the title field"
                },
            message:{
                required:"Please fill message the title field"
            }

        },
        submitHandler: function (gcmForm) {
            var live = $("#category").val();
            var result = live.match(/live/i);

            if(result){
                bootbox.prompt("Type 'yes' in Caps to send notification to All ", function(result){ 
                    if (result === null) {
                        // Prompt dismissed
                    } 
                    else if(result.match(/YES/)){
                        sendGcm();
                    }else{
                        alert("Text does not match");
                        return false;
                    }
                 });
                return;
            }else{
                sendGcm();
            }


            
        }           
    });
}

function sendGcm(){
   
    var title = $("#title").val();
    var message = $("#message").val();
    var type = $("#type").val();
    var live = $("#category").val();
    var isTest = live.match(/live/i)?false:true;
    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));

    var details = {"title": title, "message":message, "type":type, "personId":adminInfo.personId, "isTest":isTest};
    console.log(details);
    showLoader();
    var dataR = details;
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.GCM_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
        hideLoader();
       validateResponse(response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 

}




function validateResponse(response){
    //{"multicast_id":6610759997490520196,"success":1,"failure":0,"canonical_ids":0,"results":[{"message_id":"0:1460867632280940%3de1791ef9fd7ecd"}]}
    if(response.success >0){
        $("#gcmForm")[0].reset();
        alert("Notification sent successfully");
    }
}


