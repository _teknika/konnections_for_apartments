
var registerData;
$(function(){
    $("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");
   
   getBuildingInfo();
    // Calendar
    $('#dob').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        //format:'YYYY-MM-DD'
        format:'DD-MM-YYYY'
    }, function (start, end, label) {
        
    });

    $('#dateofmarriage').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        //format:'YYYY-MM-DD'
        format:'DD-MM-YYYY'
    }, function (start, end, label) {        
    });

      $('#registerBasicForm').validate({ //...................validating registration form starts
    rules: {
      firstname: {
        required: true,
      },
      lastname: {
         required: true,
      }

    },
    messages: {
      firstname: {
        required: "Please enter your First name."
      },
      lastname: {
       required: "Please enter your last name."
      }
    },

    errorPlacement: function(error, element) {
      var divObj=document.createElement("div");
      divObj.setAttribute("class","error-messageColor");
       element.parent().append(divObj);
     error.appendTo(element.next());
    },

    submitHandler: function(form) {
       onFinishCallback();
    }
  }); //...............registration form validation ends.
});

function onFinishCallback() {
    $('#previewRegForm').find('input, textarea, button, select').attr('disabled','disabled');
    // Basic Info
    var firstname = $("#firstname").val();
    var lastname = $("#lastname").val();

    var mobilenumber = $("#mobilenumber").val();
    var altnumber = $("#altnumber").val();

    var gender = $("#gender").val();
    var dateofmarriage = $("#dateofmarriage").val();
    var tempDateArr= $("#dob").val().split("-");
    var dob=false;
     tempDateArr.length > 2 ? dob=tempDateArr[2]+"-"+tempDateArr[1]+"-"+tempDateArr[0]:"";
    var emailId=$("#emailId").val() ;
    var altmailId = $("#altmailId").val();
    var blockName= $("#blockName").val();
    var flatholder=$("#flatHolder").val();
	//calling concatBlokAndFlat() function to get the flatNo
    var flatNo=concatBlokAndFlat(blockName,$("#flatNo").val()) ;
    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    var orgId = adminInfo.orgId;

    var basicInfo = {"parentid":"-1","ishead":"1","orgId":orgId,"role":"-1","relationwithhead":"","firstname":firstname,"lastname":lastname,"mobilenumber":mobilenumber,"gender":gender,"dob":dob,"dateofmarriage":dateofmarriage,"emailId":emailId,"blockName":blockName,"flatNo":flatNo,"flatHolder":flatholder,"altnumber":altnumber,"altmailId":altmailId};
    registerData = {"basicInfo":basicInfo};

    showLoader();
    var dataR = {"request":registerData};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "../server/webAppRegister.php",
        data: JSON.stringify(dataR)
    }).done(function(response) {
         if(!response.isSuccessful){
            alert(response.msg);
            hideLoader();
            return false;
           }
         alert("Registered Successfully");
        location.reload();
       //validationOnBasicInfoRegister(registerData, response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log("fail:"+jqXHR, textStatus, errorThrown);
        hideLoader();
        alert("Server failed");
        return false;
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}

function getBuildingInfo(){
    showLoader();
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "../server/getBuildingInfo.php",
        data: ""
    }).done(function(response) {
        hideLoader();
        generateBuildingInfoDropdown(response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        //alert("Unable to load categories");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}

/*function getCities(){
    showLoader();
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.CITY_URL),
        data: ""
    }).done(function(response) {
       generateCity(response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}*/


