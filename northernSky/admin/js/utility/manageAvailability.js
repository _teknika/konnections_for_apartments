var facilityId=undefined;
$(function(){
  
   $("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");
    getFacilityList();
    $(document).on("click",".showView",function(e){
    	e.preventDefault();
       facilityId=$("#facilityId").val();
       generateCalender();
    });
});

function getFacilityList(){
	showLoader();
	 $.ajax({
        type: "GET",
        url: "../server/getFacilityList.php",
    }).done(function(response) {
      hideLoader();
      var resultArray=JSON.parse(response);
      resultArray.forEach(function(obj,i,fullArr){
      	console.log(obj,i);
      	$("#facilityId").append("<option value="+obj.id+">"+obj.facility_name+"</option>");
      });
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
function generateCalender(){
		/* initialize the external events
		-----------------------------------------------------------------*/

		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true ,// maintain when user navigates (see docs on the renderEvent method
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/

		$('#calendar').fullCalendar({
			 header:{
                left: 'prev,next today',
                center: 'title',
                right: 'agendaWeek,agendaDay'
            },
            defaultView: 'agendaWeek',
            editable: true,
            selectable: true,
            allDaySlot: false,
            eventOverlap: false,
			droppable: true, // this allows things to be dropped onto the calendar
			drop: function(date, jsEvent, ui, resourceId) {
				
				console.log(date, jsEvent, ui, resourceId);
				var availabilityObj={};
                	availabilityObj.startTime=date.format();
                	var datetimeObj=date;
                	datetimeObj._d.setHours(datetimeObj._d.getHours()+2);//adding 2 hrs to current dragged element to get endtime
                	endTimestr=datetimeObj.format();
                	availabilityObj.endTime=endTimestr;
                	availabilityObj.facilityId=facilityId;
                	availabilityObj.weekId=datetimeObj._d.getDay();//days number sun->0 mon->1 ...
                	addAvailability.call(this,availabilityObj,ui);
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
			},
			  eventClick:  function(event, jsEvent, view) {  // when some one click on any event
                endtime = $.fullCalendar.moment(event.end).format('h:mm');
                starttime = $.fullCalendar.moment(event.start).format('dddd, MMMM Do YYYY, h:mm');
                var mywhen = starttime + ' – ' + endtime;
                 alert("clicked");
               console.log(event.title,mywhen,event.id);
            },
            select: function(start, end, jsEvent) {  // click on empty time slot
            	alert("click on empty time slot");
                endtime = $.fullCalendar.moment(end).format('h:mm');
                starttime = $.fullCalendar.moment(start).format('dddd, MMMM Do YYYY, h:mm');
                var mywhen = starttime + ' – ' + endtime;
                start = moment(start).format();
                end = moment(end).format();
              
               // $('#createEventModal').modal('toggle');
           },
           eventDrop: function(event, delta){ // event drag and drop
               alert("is this ok");
              /* $.ajax({
                   url: 'index.php',
                   data: 'action=update&title='+event.title+'&start='+moment(event.start).format()+'&end='+moment(event.end).format()+'&id='+event.id ,
                   type: "POST",
                   success: function(json) {
                   //alert(json);
                   }
               });*/
           },
           eventResize: function(event) {  // resize to increase or decrease time of event
           	alert("event resize1");
              /* $.ajax({
                   url: 'index.php',
                   data: 'action=update&title='+event.title+'&start='+moment(event.start).format()+'&end='+moment(event.end).format()+'&id='+event.id,
                   type: "POST",
                   success: function(json) {
                       //alert(json);
                   }
               });*/
           },
           eventResize: function(event, delta, revertFunc) {
                	alert("event resize2");
                alert(event.title + " end is now " + event.end.format());

                if (!confirm("is this okay?")) {
                    revertFunc();
                }else{
                	var updateObj={};
                	updateObj.startTime=event.start.format();
                	updateObj.endTime=event.end.format();
                	updateObj.id=event._id;
                	updateObj.facilityId=1;
                	updateObj.weekId=new Date(event.start.format()).getDay();
                	updateAvailability(updateObj,revertFunc);
                }

            },
            eventReceive: function(event) {
              // console.log(this.calendar.options._id);
              },
                eventRender: function(event, element) {
                	
			      /*  element.qtip({
			            content: event.description
			        });*/
			    }
		});


	
}