var vendorCategoryList=[];
$(function(){
     $("#main-header").load("headerMenu.php");
     $("#main-sidebar").load("sidebarMenu.php");
     $("#main-footer").load("footerMenu.php");
     initSearch();
     enableVendorSearch();
     getVendorCategoryList();
     getVendors();
});
  function initSearch() {
                
                   $('#vendorForm').validate({ //...................validating eventForm form starts
                    rules: {
                      nameR: {
                        required: true,
                      },
                      
                      mobilenumberR: {
                         required: true,
                      },
                      
                      dealsin: {
                        required: true,
                      }
                    },
                    messages: {
                      nameR: {
                        required: "Please enter name of vendor."
                      },
                      mobilenumberR: {
                       required: "Please enter mobile number"
                      },
                      dealsin: {
                        required: "Please enter dealsIn"
                      }
                    },

                    errorPlacement: function(error, element) {
                      var divObj=document.createElement("div");
                      divObj.setAttribute("class","error-messageColor");
                      element.parent().append(divObj);
                      error.appendTo(element.next());
                    },

                    submitHandler: function(form) {
                       addVendor(form);
                    }
            }); //...............eventForm form validation ends
         $('#editVendorForm').validate({ //...................validating editForm form starts
                    rules: {
                      bname: {
                        required: true,
                      },
                      
                      contactNo: {
                         required: true,
                      },
                      
                      dealsIn: {
                        required: true,
                      }
                    },
                    messages: {
                      bname: {
                        required: "Please enter name of vendor."
                      },
                      contactNo: {
                       required: "Please enter mobile number"
                      },
                      dealsIn: {
                        required: "Please enter dealsIn"
                      }
                    },

                    errorPlacement: function(error, element) {
                      var divObj=document.createElement("div");
                      divObj.setAttribute("class","error-messageColor");
                      element.parent().append(divObj);
                      error.appendTo(element.next());
                    },

                    submitHandler: function(form) {
                       console.log(form);
                       var editFormObj={};
                       editFormObj.vendorId=$("#vendorId").val();
                        var formFiledsArr = $("#editVendorForm").serializeArray();
                        $.each(formFiledsArr, function(i, field){
                            editFormObj[field.name]=field.value;
                        });
                        updateVendor({request:editFormObj});
                    }
            }); //...............eventForm form validation ends
            }
   function  enableVendorSearch(){
      $("#SearchQry").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".db-table tbody >tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
   }         

   function addVendor(form) {
                showLoader();
                oData = new FormData(form);
                $.ajax({
                    url: "../server/addVendor.php",
                    data: oData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST', // For jQuery < 1.9
                }).done(function (response) {
                    hideLoader();
                    if (JSON.parse(response).isSuccessful == true) {
                                alert("Vendor added successfully");
                                location.reload();
                            } else {
                              alert("Please try again");
                            }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                }).always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});

            }
  function getVendorCategoryList(){
    showLoader(); 
    $.ajax({
      url: '../server/getVendorsCategory.php',
      type: 'GET',
    })
    .done(function(response) {
      vendorCategoryList=JSON.parse(response);
      hideLoader();
    })
    .fail(function() {
      hideLoader(); 
      alert("could'nt able to fetch vendors category.");
    })
    .always(function() {
      console.log("complete");
    });
  }
function deleteVendor(){
   $(".deleteVndr").attr('data-vendorid', this.getAttribute("id"));
   $("#deleteVendorModal").modal();
  
}
function deleteVendorOnConfirm(){
  showLoader(); 
  var idTobeDeleted=this.getAttribute("data-vendorid");
  $.ajax({
    url: '../server/deleteVendor.php?id='+idTobeDeleted,
    type: 'GET',
  })
  .done(function(response) {
   console.log("deleted vendor obj :"+JSON.stringify(response));
    alert("Selected vendor deleted successfuly.");
    location.reload();
  })
  .fail(function() {
    hideLoader(); 
    alert("try again.");
  })
  .always(function() {
    console.log("complete");
  });
}
function editVendor(){  
  //var vendorInfo=JSON.parse($(this).closest("td").attr("data-vendor"));
  var rowDetailArr=$(this).closest("tr").find("td");
  $("#vendorId").val($(this).attr("data-id"));
  $("#nameR").val($(rowDetailArr[0]).text());
  $("#emailR").val($(rowDetailArr[1]).text());
  var numbers=$(rowDetailArr[2]).text();
  $("#mobilenumberR").val(numbers.split(" ")[0]);
  $("#altNumber").val(numbers.split(" ")[1]);

  //below code for displaying category of vendor in vendor edit form
  $("#editVendorForm  [name='category']").empty();
  vendorCategoryList.forEach(function(categoryObj,i){
     var id=categoryObj['id'];
     var category=categoryObj['category'];
     if($(rowDetailArr[3]).text() == category){
      $Ele="<option selected value='"+id+"'>"+category+"</option>";
     }else{
      $Ele="<option value='"+id+"'>"+category+"</option>";
     }
    
     $("#editVendorForm  [name='category']").append($Ele);
  });//vendor category ends
  $("#link").val($(rowDetailArr[4]).text());
  $("#dealsin").val($(rowDetailArr[5]).text());
  $("#addressR").val($(rowDetailArr[6]).text());
  $("#editVendor").modal();
}
function updateVendor(vendorInfo){
  $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.UPDATE_VENDOR_URL),
        data: JSON.stringify(vendorInfo)
    }).done(function(response) {
        location.reload();
       //validationOnAddMember(registerData, response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
function createVendorTb(obj){
  console.log(obj);
        VendorTableObj= $("#vendorTable").DataTable({
    "destroy": true,
    "data" : obj,
    "columns":[
               {
                 "title":"Name", "data": "name"
               },
              
               {
                 "title":"Email", "data": "email"
               },

               {
                 "title":"Contact", "data": "person_contact"
               },
                {
                 "title":"Category", 
                 "data": "category",
                },
               {
                 "title":"Link", "data": "website_link"
               },
                {
                 "title":"DealsIn",
                  "data": "dealsIn",
               },
               {
                 "title":"Flat", 
                 "data": "address",
               },
              
              { 
                 "title":"Action",
                 "data": "id",
                 render:   function(id)
                 {return '<span data-id='+id +'><i class="ui-tooltip fa fa-pencil editVendor"  style="font-size:22px;margin-left:12px;"  data-original-title="Edit" data-id="'+ id +'" onclick="editVendor.call(this)"></i><span>' +
                 '<i class="ui-tooltip fa fa-trash-o deleteSubMember" style="font-size: 22px;margin-left:9px" onclick="deleteVendor.call(this)" id="'+ id +'" data-original-title="Delete"></i>';
               } 
              }
             ]
            
  });
}
function getVendors(){
  $.ajax({
    url:'../server/getVendors.php',
    type:'GET',
  }).done(function(response){
    console.log("vendors list displayed successfully:"+JSON.parse(response));
    createVendorTb(JSON.parse(response));
  }).fail(function(){
    console.log("failed")
  })
  
}

