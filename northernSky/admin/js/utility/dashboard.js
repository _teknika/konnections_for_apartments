$(function(){

    $("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");


    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    var orgId = adminInfo.orgId;

	// Check 
    var details = {"orgId":orgId, "count":-1, "pageNo":1, "head":true, "parentId":""};
	getContactsMinimalInfo(details);

    var userId = adminInfo.personId;
    // Get Member Statistics
    getMemberStats(orgId, userId);

	// Click on Single User

	$(document).on('click','#contactsDashboard li:not(.phone)',function(){
		var userIndex = $(this).attr('userIndex');
		var userId = $(this).attr("userId");
		var userInfo = {"userIndex":userIndex, "userId":userId};
		localStorage.setItem("userInfo", JSON.stringify(userInfo));
		location.href="person.php?userId="+userId;
	});
    $(document).on('click','#totalFamilies',function(){
        getAllMembers();
    });
     $(document).on('click','#onlyFamilies',function(){
        // Check 
    var details = {"orgId":orgId, "count":-1, "pageNo":1, "head":true, "parentId":""};
    getContactsMinimalInfo(details);
    });
     $("#notifications").click(function(event) {
         /* Act on the event */
         alert("This feature is under development.");
     });
    
});

function getContactsMinimalInfo(details){
	showLoader();
    var dataR = {"request":details};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.GETCONTACTSMINIMALINFO_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
       validationOnGetContactsOnDashboard(details,response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}

function getMemberStats(orgId, userId){
    showLoader();
    var type = 0;
    var dataR = {"request":{"orgId":orgId, "userId":userId, "type":type}};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.GETMEMBERSTATS_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
       validationOnGetMemberStats(orgId,response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}

function getAllMembers(){
      showLoader();
    $.ajax({
        type: "GET",
        url: "../server/getContactsOfAppartment.php",
    }).done(function(response) {
        var dataObj=JSON.parse(response);
       generateContactsDashboard(dataObj);
        hideLoader();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}

function generateContactsDashboard(response){
    $("#contactsDashboard").empty();
    var data = response.resp;
    for(var i=0; i<data.length;i++){
        var list = $("<li userIndex="+i+" userId="+data[i].id+"></li>");
        if((data[i].photo == null) || (data[i].photo == " ") || (data[i].photo == undefined) || (data[i].photo == "")){
            var photo = $("<a class='cbp-vm-image contactPhotoIcon'>"+data[i].firstName.charAt(0).toUpperCase()+"</a>");    
        }else{
            var photo = $("<a class='cbp-vm-image'><img src='"+user_img+"/"+data[i].photo+"'></a>");    
        }
        
        var name = $("<h3 class='name cbp-vm-title'>"+formName(data[i])+"</h3>");
        var phone = $("<div class='cbp-vm-price phone'>"+data[i].mobileNumber+"</div>");
        //var business = $("<div class='cbp-vm-details'>"+data[i].businessCategory+" "+data[i].businessOccupation+"</div>");
        $(list).append(photo);
        $(list).append(name);
        $(list).append(phone);
       // $(list).append(business);
        $("#contactsDashboard").append(list);

    }
   
    var options = {
        valueNames: ['name', 'phone']
    };
    var hackerList = new List('hacker-list', options);
    
}

