var registerData;
$(function(){
   $("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");

    // Calendar
    $('#dobR').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        format:'YYYY-MM-DD'
    }, function (start, end, label) {
        
    });

    $('#dateofmarriageR,#dob').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        format:'YYYY-MM-DD'
    }, function (start, end, label) {        
    });

    // Assign Old Values
    var userInfo = JSON.parse(localStorage.getItem("userInfo"));
    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    var orgId = adminInfo.orgId;
    
    //initilize BlockName and FlatNo
    var details = {"orgId":orgId, "userId":userInfo.userId};
     getFamilyHeadInfo(details);
    //ends

    addMembervalidation(userInfo, orgId);
});



function addMembervalidation(userInfo, orgId){
    var addMemeberForm = $("#addMemeberForm");
    $("#addMemeberForm").validate({
        messages: {
               
        },
        submitHandler: function (addMemeberForm) {
            addMemberCall(userInfo, orgId);
        }           
    });
}


function addMemberCall(userInfo, orgId) {


    // Basic Info
    var firstname = $("#firstnameR").val();
    var lastname = $("#lastnameR").val();

    var mobilenumber = $("#mobilenumberR").val();
    var altNumber = $("#altNumber").val();
    var gender = $("#genderR").val();
    var dateofmarriage = $("#dateofmarriageR").val();
    var dob = $("#dob").val() ? $("#dob").val() : false;
    var emailId=$("#emailId").val() ;
    var altmail=$("#altmail").val() ;
    var blockName= $("#blockName").val();
    var flatNo=$("#flatNo").val() ;
    var parentId = userInfo.userId;
    var role= 0;
    var relationwithhead= $("#relationwithheadR").val();
    if(dateofmarriage == ""){
        dateofmarriage=null;
    }

    var basicInfo = {"parentid":parentId,"ishead":"1","orgId":orgId,"role":"-1","relationwithhead":"","firstname":firstname,"lastname":lastname,"mobilenumber":mobilenumber,"gender":gender,"dob":dob,"dateofmarriage":dateofmarriage,"emailId":emailId,"blockName":blockName,"flatNo":flatNo,"flatHolder":"owner","altnumber":altNumber,"altmailId":altmail};

   

    registerData = {"basicInfo":basicInfo};

    showLoader();
    var dataR = {"request":registerData};
	console.log(JSON.stringify(dataR));
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.ADD_MEMBER_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
       if(response.isSuccessful){
        alert("Member added successfully");
        location.reload();
       } else{
         alert(response.msg);
         hideLoader();
       }
        
       //validationOnAddMember(registerData, response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {
		console.log("..textStatus.."+textStatus);
	}); 
}
  function getFamilyHeadInfo(details){
    showLoader();
    var dataR = {"request":details};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.GETCONTACTINFO_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
        $("#blockName").val(response.resp[0].blockName);
        $("#flatNo").val(response.resp[0].flatNo) ;
        hideLoader();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
    
  }