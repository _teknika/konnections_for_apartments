$(function(){

   $("#main-header").load("headerMenu.php");
        $("#main-sidebar").load("sidebarMenu.php");
        $("#main-footer").load("footerMenu.php");

    $("#cbp-vm").hide();

    // Get Local Category Data
    //getCategoryInfo();
    getBuildingInfo();

    // Get All Cities

    //getCities();


    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    var orgId = adminInfo.orgId;
    
    searchValidation(orgId);

	
   /* $(document).on('click', '.searchParentView', function(){
        var userIndex = $(this).parent("#contactsDashboard li").attr('userIndex');
        var userId = $(this).parent("#contactsDashboard li").attr('userId');
        var userInfo = {"userIndex":userIndex, "userId":userId};
         localStorage.setItem("userInfo", JSON.stringify(userInfo));
         location.href="person.php";
    });*/

    /*$(document).on('click', '.searchMemberView', function(){
        var userIndex = $(this).parent("#contactsDashboard li").attr('userIndex');
        var userId = $(this).parent("#contactsDashboard li").attr('parent');
        var userInfo = {"userIndex":userIndex, "userId":userId};
         localStorage.setItem("userInfo", JSON.stringify(userInfo));
         location.href="person.php";
    });*/
    $(document).on('click', '#contactsDashboard li', function(){
        var userIndex = $(this).parent("#contactsDashboard li").attr('userIndex');
        //var userId = $(this).parent("#contactsDashboard li").attr('parent');
         var parentId = $(this).attr('data-qry-id');
        var userInfo = {"userIndex":userIndex, "userId":parentId};
         localStorage.setItem("userInfo", JSON.stringify(userInfo));
         
         location.href="person.php?userId="+parentId;
    });
});

function searchValidation(orgId){
    var searchForm = $("#searchForm");
    $("#searchForm").validate({
        messages: { 

        },
        submitHandler: function (searchForm) {
          $("#searchForm").serializeArray().filter(function(e,i){
                return e.value.trim().length >0;
             }).length > 1 ? searchContactt(orgId):"" ;
             
            
        }           
    });
}

function searchContactt(orgId){
    var name = $("#nameR").val().replace(/\s/g,'');//removes all white spaces
    var mobilenumber = $("#mobilenumberR").val();
    var blockName = $("#blockName").val();
    var flatNo = $("#flatNo").val();
 
    var details = {"orgId":orgId,"name":name,"mobilenumber":checkEmpty(mobilenumber),"blockName":checkEmpty(blockName),"flatNo":checkEmpty(flatNo)};

  

    showLoader();
    var dataR = {"request":details};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.SEARCHCONTACT_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
       validationOnSearch(details,response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 

}






function generateContactsInSearch(details, response){
    $("#cbp-vm").show();
    $("#contactsDashboard").empty();
    var data = response.resp;
    for(var i=0; i<data.length;i++){
        var list = $("<li userIndex="+i+" userId="+data[i].id+" parent="+data[i].parentId+"></li>");
        if((data[i].photo == null) || (data[i].photo == " ") || (data[i].photo == undefined) || (data[i].photo == "")){
            var photo = $("<a class='cbp-vm-image contactPhotoIcon'>"+data[i].firstName.charAt(0).toUpperCase()+"</a>");    
        }else{
            var photo = $("<a class='cbp-vm-image'><img src='"+user_img+"/"+data[i].photo+"'></a>");    
        } 
        
        var name = $("<h3 class='name cbp-vm-title'>"+formName(data[i])+"</h3>");
        var phone = $("<div class='cbp-vm-price phone'>"+data[i].mobileNumber+"</div>");
        //var business = $("<div class='cbp-vm-details'><span class='glyphicon glyphicon-calendar' aria-hidden='true'> "+data[i].dob+"</span></div>");
        if(data[i].parentId == -1){
            //var view = $("<a class='cbp-vm-icon cbp-vm-add searchParentView'>View</a>"); 
            var data_qryId= data[i].id;  
        }else{
           // var view = $("<a class='cbp-vm-icon cbp-vm-add searchMemberView'>View on parent</a>");
            var data_qryId= data[i].parentId; 
        }
        
        
        
        $(list).append(photo);
        $(list).append(name);
        $(list).append(phone);
        //$(list).append(view);
        $(list).attr("data-qry-id",data_qryId);
        $("#contactsDashboard").append(list);

    }
   
    var options = {
        valueNames: ['name', 'phone']
    };
    var hackerList = new List('hacker-list', options);
    
}

