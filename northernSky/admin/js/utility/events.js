$(function(){
  $("#main-header").load("headerMenu.php");
  $("#main-sidebar").load("sidebarMenu.php");
  $("#main-footer").load("footerMenu.php");
  $('#endTime+span.error-messageColor').addClass('hide');
  initSearch();
  enableTimepickers();
  getEvents();
});
function enableTimepickers(){
  $('.startTime').timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
        minTime: '6:00',
        maxTime: '11:00pm',
        defaultTime: '6',
        startTime: '6:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
  });
  $('.endTime').timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
        minTime: '6:00',
        maxTime: '11:30pm',
        defaultTime: '6',
        startTime: '6:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
  });
}
function initSearch() {
  $("#eventForm").validate({
    rules: {
      title: {
          required: true
        },
      details: {
          required: true
        },
      name: {
          required: true
        },
      location:{
        required: true
      },
      eventdate:{
        required: true,
      },
      endTime:{
        required:true
    },
      // photos:{
      //   required: true
      // }
      },
      messages: {
        title: {
          required: "Please enter the title"
        },
        details: {
          required: "Please enter the  details"
        },
      name: {
        required: "Please enter the name"
        },
      location:{
        required: "Please enter the location"
      },
      eventdate:{
        required: "Please enter the eventdate",
      },
      endTime:{
        required:"end time must be entered",
      },
      // photos:{
      //   required: "Please choose the file"
      // }
      },
  
      errorPlacement: function(error, element) {
        var divObj=document.createElement("div");
        divObj.setAttribute("class","error-messageColor");
         element.parent().append(divObj);
       error.appendTo(element.next());
     
      },
      submitHandler : function( form) { 
        // var form = document.forms.namedItem("eventForm");
        addEvent(form);
      }
      
  });
}

function endTimeValidator(){
  $('#endTime+span.error-messageColor').removeClass('hide');
}
function addEvent(form) {
  // $('#endTime+span.error-messageColor').addClass('hide');
  oData = new FormData(form);
  var eventDate=$('#eventdate').val();
  oData.append("startTime",covertTimeToDateTimeMillSec(eventDate,$(".startTime").val()));  
  oData.append("endTime",covertTimeToDateTimeMillSec(eventDate,$(".endTime").val())); 
  var startTime=covertTimeToDateTimeMillSec(eventDate,$(".startTime").val());
  var endTime=covertTimeToDateTimeMillSec(eventDate,$(".endTime").val());
  if(startTime>= endTime){
        endTimeValidator();
        return false;
      }
  showLoader();
  $.ajax({
    url: "../server/addEvent.php",
    data: oData,
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST', // For jQuery < 1.9
}).done(function (response) {
    hideLoader();
      alert("Event added successfully");
     location.reload();
}).fail(function (jqXHR, textStatus, errorThrown) {
    hideLoader();
    alert("Server failed");
}).always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});
}

function covertTimeToDateTimeMillSec(eventdate,time){ 
//time value will be like "6:30 AM" |  "02:00 PM" 
    var timePartStr=time;
     var timePartArr=timePartStr.split(/[(" ")|(:)]/);
     var hourPart=Number(timePartArr[0]);
    if(timePartStr.match(/(PM)$/ig)){
       hourPart+=12;
    }

    var eventDateTime=new Date(eventdate);
    eventDateTime.setHours(hourPart);
    eventDateTime.setMinutes(0);
    eventDateTime.setSeconds(0);
   return eventDateTime.getTime();
}

function deleteEvent(){
  $(".delEvent").attr('eventid', this.getAttribute("id"));
  $("#deleteEventModal").modal();
  
}
function deleteEventOnConfirm(){
  showLoader(); 
  var idTobeDeleted=$(this).attr("eventid");
  $.ajax({
    url: '../server/deleteEvent.php?id='+idTobeDeleted,
    type: 'GET',
  })
  .done(function(response) {
   console.log("deleted event obj :"+response);
    alert("Selected event deleted successfuly.");
    location.reload();
  })
  .fail(function() {
    hideLoader(); 
    alert("try again.");
  })
  .always(function() {
    console.log("complete");
  });
}

function createEventTb(eventObj){
        EventTableObj= $("#eventTable").DataTable({
    "destroy": true,
    "data" : eventObj,
    "columns":[
               {
                 "title":"Title", "data": "title"
               },
              
               {
                 "title":"Details", "data": "details"
               },

               {
                 "title":"Event Co-ordinator", "data": "name"
               },
                {
                 "title":"Event Date", 
                 "data": "event_date",
                },
               {
                 "title":"Start Time", "data": "startTime"
               },
                {
                 "title":"End Time",
                  "data": "endTime",
               },
               {
                 "title":"Event Venue", 
                 "data": "location",
               },
               {
                  "title":"Event Photos Link", 
                  "data": "photos",
                },
               
              { 
                  "title":"Action",
                  "data": "id",
                  render:   function(id)
                  {
                      console.log(id);
                  //    return '<span data-id='+id +'><i class="ui-tooltip fa fa-trash-o deleteSubMember" style="font-size: 22px;margin-left:9px" onclick="deleteEvent.call(this)" id="'+ id +'" data-original-title="Delete"></i> </span>';
                  return '<button onClick="deleteEvent.call(this)" id='+ id +'>delete</button></span>'
                } 
               }
             ]
            
  });
}
function getEvents(){
    $.ajax({
        url:'../server/getEvents.php',
        type:'GET'
    }).done(function(response){
      console.log("Events list displayed successfully:"+response);
      createEventTb(JSON.parse(response));
  }).fail(function(){
      console.log("failed")
    })
}