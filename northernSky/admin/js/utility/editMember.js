var registerData;
$(function(){
    $("#header_section").load("header.php");
    $("#sidemenu_section").load("side_menu.php");

    // Get Local Category Data
    getCategoryInfo();

    // Get All Cities
    getCities();

    // Calendar
    $('#dobR').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        format:'DD/MM/YYYY'
    }, function (start, end, label) {
        
    });

    $('#dateofmarriageR,#dob').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        format:'DD/MM/YYYY'
    }, function (start, end, label) {        
    });

    // Assign Old Values
    var userInfo = JSON.parse(localStorage.getItem("userMemberInfo"));
    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    var orgId = adminInfo.orgId;

    var details = {"orgId":orgId, "userId":userInfo.userId};
    
    getContactInfo(details);

    editMemberValidation(userInfo, orgId);
});

function getContactInfo(details){
    showLoader();
    var dataR = {"request":details};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.GETCONTACTINFO_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
       validationOnEditMemberGetList(details,response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}



function generateValuesforMember(contact){
    $("#firstnameR").val(contact.firstName);
    $("#middlenameR").val(contact.middleName);
    $("#lastnameR").val(contact.lastName);
    $("#mobilenumberR").val(contact.mobileNumber);
    $("#businesscategoryR").val(contact.businessCategory);
    $("#businessoccupationR").val(contact.businessOccupation);
     $("#dob").val(contact.dob);
    $("#dobR").val(contact.dob);
    $("#genderR").val(contact.gender);
    $("#dateofmarriageR").val(contact.dateOfMarriage);
    $("#bloodgroupR").val(contact.bloodGroup);
    $("#relationwithheadR").val(contact.relationWithHead);
    //$("#photoR").val(contact.photo);
}

function editMemberValidation(userInfo, orgId){
    var editMemeberForm = $("#editMemeberForm");
    $("#editMemeberForm").validate({
        messages: {
               
        },
        submitHandler: function (editMemeberForm) {
            
            editMemberCall(userInfo, orgId)
        }           
    });
}


function editMemberCall(userInfo, orgId) {


    // Basic Info
    var firstname = $("#firstnameR").val();
    var middlename = $("#middlenameR").val();
    var lastname = $("#lastnameR").val();
    var mobilenumber = $("#mobilenumberR").val();
    var businesscategory = $("#businesscategoryR").val();
    var businessoccupation = $("#businessoccupationR").val();
    var dob = $("#dobR").val();
    var gender = $("#genderR").val();
    var dateofmarriage = $("#dateofmarriageR").val();
    var bloodgroup= $("#bloodgroupR").val();
    //var photo= $("#photoR").prop('files');
    var photo= $("#photoR").val();

   
    var relationwithhead= $("#relationwithheadR").val();


    var parent = JSON.parse(localStorage.getItem("userInfo"));
    var parentId = parent.userId;
    var role=0;
    

    if(dateofmarriage == ""){
        dateofmarriage = null;
    }

    var basicInfo = {"userId":userInfo.userId,"parentid":parentId,"ishead":0,"orgId":orgId,"role":role,"relationwithhead":relationwithhead,"firstname":firstname,"middlename":middlename,"lastname":lastname,  "mobilenumber":mobilenumber, "businesscategory":businesscategory, "businessoccupation":businessoccupation, "dob":dob, "gender":gender, "dateofmarriage":dateofmarriage, "bloodgroup":bloodgroup, "photo":photo};

   

    registerData = {"basicInfo":basicInfo};

    showLoader();
    var dataR = {"request":registerData};
    
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.UPDATE_MEMBER_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
       validationOnEditMember(registerData, response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
