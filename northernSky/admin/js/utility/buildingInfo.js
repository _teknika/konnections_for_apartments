$(function(){
  
   $("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");
      $("#addBuildingInfoForm").validate({
        messages: {
               
        },
        submitHandler: function (loginForm) {
            addBuildingName();
        }           
    });
      enableVendorSearch();
      $(document).on("click",".deleteBlockName",function(e){
         deleteBlockName($(this)[0].getAttribute("id"));
      });
});
 function  enableVendorSearch(){
      $("#SearchQry").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#buildningInfoTable tbody >tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
   }  
  function addBuildingName(){
    
    if($("#blockName").val().trim().length <=0){
      return false;
    };
    showLoader();
    var dataR = {"blockName":$("#blockName").val()};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "../server/addBuildingInfo.php",
        data: JSON.stringify(dataR)
    }).done(function(response) {
      hideLoader();
      alert("Block name added successfully");
      location.reload();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 

  }
    function deleteBlockName(deleteID){
      showLoader();
      var dataR = {"blockName":$("#blockName").val()};
      $.ajax({
          type: "POST",
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          url: "../server/deleteBuildingInfo.php?id="+deleteID,
          data: JSON.stringify(dataR)
      }).done(function(response) {
        hideLoader();
        alert("Block name deleted successfully");
        location.reload();
      }).fail(function(jqXHR, textStatus, errorThrown) {
          hideLoader();
          alert("Server failed");
      })
      .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 

   }