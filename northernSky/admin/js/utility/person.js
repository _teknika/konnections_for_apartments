$(function(){
     $("#main-header").load("headerMenu.php");
     $("#main-sidebar").load("sidebarMenu.php");
     $("#main-footer").load("footerMenu.php");

    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    var orgId = adminInfo.orgId;

    var userInfo = JSON.parse(localStorage.getItem("userInfo"));

   //Generate Particular User Info

    var details = {"orgId":orgId, "userId":userInfo.userId};
    getContactInfo(details);

    $(document).on('click', '.member_edit', function(){
        var userIndex = $(this).attr("userIndex");
        var userId = $(this).attr("userId");
        var userMemberInfo = {"userIndex":userIndex, "userId":userId};
        localStorage.setItem("userMemberInfo", JSON.stringify(userMemberInfo));
        location.href="editMember.php";
    });
	//add lessee
	$(document).on('click','.addlessee',function(e){
	   e.preventDefault();
		var flatNo=$(this).closest("tr").attr("data-id");
		var managelessee='0';
		var flatInfo = {"flatNo":flatNo, "managelessee":managelessee};
		localStorage.setItem("flatInfo", JSON.stringify(flatInfo));
		window.location.href="lesseeRegister.php";
		
	});
	//change lessee 
	$(document).on('click','.changelesse',function(e){
	   e.preventDefault();
		var flatNo=$(this).closest("tr").attr("data-id");
		var managelessee='1';
		var flatInfo = {"flatNo":flatNo, "managelessee":managelessee};
		localStorage.setItem("flatInfo", JSON.stringify(flatInfo));
		window.location.href="lesseeChange.php";
		
	});
	//delete lessee
  	$(document).on('click','.deletelessee',function(e){
		e.preventDefault();
		var flatNo=$(this).closest("tr").attr("data-id");
		var managelessee='2';
		var flatInfo = {"flatNo":flatNo, "managelessee":managelessee, "userId":userInfo.userId};
		localStorage.setItem("flatInfo", JSON.stringify(flatInfo));
		$('#deletelessee').modal('show');
		});
	$(document).on('click','#deletelesseeConfirm',function(e){
	   e.preventDefault();
      	//	var flatInfo = {"flatNo":flatNo};
		var flatInfo=JSON.parse(localStorage.getItem("flatInfo"));
		deleteLessee(flatInfo);
	}); 
 	/*$(document).on('click','.deletelessee',function(e){
		  //console.log("modal1");
	   e.preventDefault();
      	//	var flatInfo = {"flatNo":flatNo};
		var flatNo=$(this).closest("tr").attr("data-id");
		var managelessee='2';
		var flatInfo = {"flatNo":flatNo, "managelessee":managelessee, "userId":userInfo.userId};
  	  
		localStorage.setItem("flatInfo", JSON.stringify(flatInfo));
		deleteLessee(flatInfo);
		
	}); */

    // Delete Contact

    $(document).on('click', '.deleteContact', function(){
        var name =$("#user_name").html();
        var phone =$("#user_phone").html();
        var userInfo = JSON.parse(localStorage.getItem("userInfo"));
        $(".usreIdDelete").val(userInfo.userId);
        $(".user_popupInfo").html(name+"-"+phone+" ");
        var elementObj=document.getElementById("deleteConfirm");
        if(elementObj.hasAttribute("personID")){
            elementObj.removeAttribute("personID");    
        }
        elementObj.setAttribute("personID",userInfo.userId);
        $('#deleteMember').modal('show');
    });

    $(document).on('click', '.deleteSubMember', function(){
        console.log("modal1");
        var name =this.parentElement.parentElement.firstChild.innerText;
        var userId = $(this).attr("userId");
        var elementObj=document.getElementById("deleteConfirmSubMember");
        if(elementObj.hasAttribute("personID")){
            elementObj.removeAttribute("personID");    
        }
        elementObj.setAttribute("personID",this.getAttribute("id"));
        $(".user_popupInfo").html(name);
        $('#deleteSubMember').modal('show');
    });
    //submember edit
    
    $(document).on('click', '.editSubMember', function(){
         editFamilyMemberFormValidation();
         generateEditForm($(this).attr("data-id"));
         
    });
    //submember edit
    $(document).on('click', '#deleteConfirm', function(){
        var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
        var orgId = adminInfo.orgId;    
        var userId = document.getElementById("deleteConfirm").getAttribute("personID");;
        var details = {"orgId":orgId, "userId":userId};
        deleteHeadOfFamily(details);
    });

    $(document).on('click', '#deleteConfirmSubMember', function(){
        var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
        var orgId = adminInfo.orgId;    
        var userId = document.getElementById("deleteConfirmSubMember").getAttribute("personID");;
        var details = {"orgId":orgId, "userId":userId};
        deleteSubMember(details);
    });

    enableDatePickers();
});

function enableDatePickers(){
    $('#dob').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        //format:'YYYY-MM-DD'
        format:'DD-MM-YYYY'
    }, function (start, end, label) {
        
    });
    
    $('#dateofmarriageR').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        //format:'YYYY-MM-DD'
        format:'DD-MM-YYYY'
    }, function (start, end, label) {
        
    });
}
//function to delete lessee
function deleteLessee(flatInfo){
	 showLoader();
    var dataR = {"request":flatInfo};
    $.ajax({
         type: "POST",
        contentType: 'application/json',
        url: "../server/lessee/manageLesse.php",
        data: JSON.stringify(dataR)
    }).done(function(response){
		hideLoader();
		alert("Lessee Record Deleted Successfully");
		location.reload();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log("fail:"+jqXHR, textStatus, errorThrown);
		 hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
function deleteHeadOfFamily(details){
    showLoader();
    var dataR = {"request":details};
	console.log(JSON.stringify(dataR));
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "../server/deleteHeadOfFamily.php",
        data: JSON.stringify(dataR)
    }).done(function(response) {
       validationOnDeleteMember(details,response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
//sub member related functions
 //submember form edit form validation
 function editFamilyMemberFormValidation(){
    $("#editFamilyMemberForm").validate({
        rule:{

        },
        messages: {
               
        },
        submitHandler: function (editForm) {
            console.log(editForm);
            var formdata={};
           var formDataArr= $(editForm).serializeArray();
           formDataArr.forEach(function(obj,i){
              formdata[obj.name]=obj.value;
            });
            updateSubMember(formdata);
        }           
    });
} 
 function generateEditForm(userId){
   showLoader();
    var dataR = {"userId":userId,"orgId":1};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.GETCONTACTINFO_URL),
        data: JSON.stringify({"request":dataR})
    }).done(function(response) { 
        var userInfo=response.resp[0];
        $("#userId").val(userId);        
        $("#firstnameR").val(userInfo.firstName);
        $("#lastnameR").val(userInfo.lastName);
        $("#mobilenumberR").val(userInfo.mobileNumber);
        $("#altNumber").val(userInfo.alt_number);
        $("#emailId").val(userInfo.emailId);
        $("#altemailId").val(userInfo.alt_mail);
        $("#dob").val(userInfo.dob);
        $("#genderR > option[value="+userInfo.gender+"]").attr("selected","selected")
         $("#dateofmarriageR").val(userInfo.dateOfMarriage);
        $('#editFamilySubMember').modal('show');
        hideLoader();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
 }
 function updateSubMember(submemberObj){
    showLoader();
    var dataR = {"basicInfo":submemberObj};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "../server/updateSubMember_web.php",
        data: JSON.stringify(dataR)
    }).done(function(response) {
        if(response.isSuccessful){
            alert("Updated successfully");
            $("#editFamilySubMember").modal("hide");
            window.location.reload();
        }
        hideLoader();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
 }
//below function delete summembers of family
function deleteSubMember(details){
    showLoader();
    var dataR = {"request":details};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.DELETECONTACT_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
       validationOnDeleteMember(details,response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}


function getContactInfo(details){
    showLoader();
    var dataR = {"request":details};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "../server/getOwnerlesseInfo.php",
        data: JSON.stringify(dataR)
    }).done(function(response) {
       validationOnSingleContact(details,response);
       var flatsDetailObj={"ownerId":response.resp[0].id,"flat_holder":response.resp[0].flat_holder,"parentId":response.resp[0].parentId,"flatNo":response.resp[0].flatNo}; 
       var flatsArrOfObject=[],flatsArr=[flatsDetailObj.flatNo];
	   var extaFlatsArr=response.flatsArray;
       Object.keys(extaFlatsArr).forEach(function(val,indx){
		   var flatDetail={};
		var flatDetail = {"flat_number":extaFlatsArr[val].flat_number,"flat_holder_type":extaFlatsArr[val].flat_holder_type};
        flatsArrOfObject.push(flatDetail);
		flatsArr.push(extaFlatsArr[val].flat_number);
       });
       flatsDetailObj['flatsArr']=flatsArr;
       flatsDetailObj['flatsArrOfObject']=flatsArrOfObject;  	   
       getMaintenanceAmountByFlats(flatsDetailObj);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}

function generateUserContact(contact){
   
    var address = contact.contacts;

    if((contact.photo == null) || (contact.photo == " ") || (contact.photo == undefined) || (contact.photo == "")){
        var photo = $("<a class='contactPhotoIconPerson'>"+contact.firstName.charAt(0).toUpperCase()+"</a>");    
    }else{
        var photo = $("<a class=''><img src='"+user_img+"/"+contact.photo+"'></a>");    
    }
    $("#user_photo").html(photo);
    $("#user_flats").html(contact.flatNo);
    $("#user_name").html(contact.firstName+" "+contact.lastName);
    $("#user_phone").html(contact.mobileNumber);
    


   

}

function generateMemberContacts(datas){

    
    prop = "firstName";
    asc=true;
    data = datas.sort(function(a, b) {
        if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
    });


    $("#contactsDashboard").empty();
    for(var i=0; i<data.length;i++){
        var list = $("<li userIndex="+i+" userId="+data[i].id+"></li>");
        if((data[i].photo == null) || (data[i].photo == " ") || (data[i].photo == undefined) || (data[i].photo == "")){
            var photo = $("<a class='cbp-vm-image contactPhotoIcon'>"+data[i].firstName.charAt(0).toUpperCase()+"</a>");    
        }else{
            var photo = $("<a class='cbp-vm-image'><img src='"+user_img+"/"+data[i].photo+"'></a>");    
        }
        
        var name = $("<h3 class='namea cbp-vm-title'><div id='user_nameR'>"+formName(data[i])+" - "+data[i].relationWithHead+"</div><br><div id='user_phoneR'>"+data[i].mobileNumber+"</div></h3>");
        var phone = $("<div class='cbp-vm-price'><span class='glyphicon glyphicon-calendar' aria-hidden='true'> "+data[i].dob+"</span><br /><span class='glyphicon glyphicon-gift' aria-hidden='true'> "+data[i].dateOfMarriage+"</span><br /><span class='glyphicon glyphicon-hand-up' aria-hidden='true'> <span style='font-family:verdana'>"+checkNull(data[i].bloodGroup)+"</span></span></div>");
        var profile = $("<div class='cbp-vm-details'><a class='btn btn-success member_edit' userIndex="+i+" userId="+data[i].id+"  title='Edit Profile'><i class='fa fa-edit m-right-xs'></i></a><span class='btn btn-danger deleteMember user_edit' title='Delete Profile' userIndex="+i+" userId="+data[i].id+" userName='"+data[i].firstName+"'><i class='fa fa-close m-right-xs'></i></span></div>");
        $(list).append(photo);
        $(list).append(name);
        $(list).append(phone);
        $(list).append(profile);
        $("#contactsDashboard").append(list);

    }
   
    var options = {
        valueNames: ['namea', 'phonea']
    };
    var hackerList = new List('hacker-list', options);
    
}
//below fn is to get maintenance charge for person by flats number
function getMaintenanceAmountByFlats(flatsDetailObj){
    showLoader();
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "../server/getMaintenanceAmountByFlats.php",
        data: JSON.stringify(flatsDetailObj)
    }).done(function(response) { 
	     var flatholder = flatsDetailObj.flat_holder;
		 var parentId = flatsDetailObj.parentId;
		 var flatno = flatsDetailObj.flatNo;
		 lesseeModification(flatholder,parentId,flatno);
		flatsDetailObj.flatsArrOfObject.forEach(function(obj,indx){ 
		 lesseeModification(flatholder,obj.flat_holder_type,obj.flat_number);
		 
       });
	   //function to design lessee buttons 
	 function lesseeModification(flatholder,parentId,data){
		      
	         if(flatholder == 'owner' && parentId==-1){
                 var  trInStr="<tr data-id='"+data+"'>"+
                      "<td>"+data+"</td>"+
                      "<td class='curr_maintenance'>"+'Nill'+"</td>"+
					  "<td><a href='#' class='addlessee' title='Add Lessee'><button class='btn btn-success'>Add</button></a></td>"+
					  
				  "</tr>";
			 }else if(flatholder == 'owner' && parentId==-2){
                 var  trInStr="<tr data-id='"+data+"'>"+
                      "<td>"+data+"</td>"+
                      "<td class='curr_maintenance'>"+'Nill'+"</td>"+
					  "<td><a href='#' class='changelesse' title='Change Lessee'><button class='btn btn-success'>Change</button></a>   <a href='#' class='deletelessee' title='delete Lessee'><button class='btn btn-success'>Delete</button></a></td>"+
					  
				  "</tr>";
			 }
		if(flatholder == 'rental'){
			   var  trInStr="<tr data-id='"+data+"'>"+
                      "<td>"+data+"</td>"+
                      "<td class='curr_maintenance'>"+'Nill'+"</td>"+
					  	 $('.lesse_colmn').remove();
				  "</tr>"
				   
			 }		 
		
			  $(".maintenance_detail_body").append(trInStr);    		  
	   }
       response.forEach(function(data,indx){   
       $("tr[data-id="+data.flat.toUpperCase()+"] .curr_maintenance").html(data.balance);
        console.log("sadasd..");
        // $("li data-id["++"]").       
       });
       hideLoader();
      // flatsDetailObj
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
 }
//ends here