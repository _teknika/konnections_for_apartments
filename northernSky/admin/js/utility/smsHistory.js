var dataTable_smsTracker;
$(function(){
    $("#header_section").load("header.php");
    $("#sidemenu_section").load("side_menu.php");
    var adminInfo = JSON.parse(localStorage.getItem("adminInfo"));
    var orgId = adminInfo.orgId;
    
    var smsUser = JSON.parse(localStorage.getItem("smsUser"));
    $("#smsUser").html("Sender: "+smsUser.firstName+" "+smsUser.middleName+" "+smsUser.lastName);
    var type = 1; //sms
    var details = {"orgId":orgId, "userId":smsUser.id, "type":type};
    generateSmsHistoryTable("");

    getSmsHistory(details);

});


function getSmsHistory(details){
    showLoader();
    var dataR = {"request":details};
    $.ajax({
        type: "POST",
        //contentType: 'application/json',
        dataType: 'json',
        url: getUrl(api.GETSMSHISTORY_URL),
        data: JSON.stringify(dataR)
    }).done(function (response){
        validationOnSmsHistory(details, response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
    return false;
}

function generateSmsHistoryTable(datas){
    
    if (typeof dataTable_smsHistory == 'undefined'){
        dataTable_smsHistory = $("#smsHistoryTable").dataTable({
            "iDisplayLength": -1,
            "aaSorting": [[2, 'asc']], //desc
            "bDestroy" : true,
            "bRetrieve":true,
            "bProcessing": true,
            "bDeferRender": true,
            "aaData": datas,
            "iDisplayLength": 50,
            "bPaginate": false,
            "aoColumns": [
            {
                "sTitle":"Receiver Name",
                "mData": "rFname",
                 "mRender": function(data, type, row){
                    return row.rFname+" "+row.rMname+" "+row.rLname;
                }
            }, {
                "sTitle":"Message",
                "mData": "message"
            },{
                "sTitle":"Transaction Time",
                "mData": "transactionTime",
                 "mRender": function(data, type, row){
                    return formDateTime(row.transactionTime);
                }
            }]
        });
    }else{
        dataTable_smsHistory.dataTable().fnClearTable();
        dataTable_smsHistory.fnAddData(datas)
    }

}
