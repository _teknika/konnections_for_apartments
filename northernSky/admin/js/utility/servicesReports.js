$(function(){
	$("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");
    getServiceList();
    //unAvailability for validation
    $("#serviceReportForm").validate({
        rules:{
            startDate: {
                        required: true,
                      },
            endDate: {
                        required: true,
                        endDate:true                        
                      },                      
        },
        messages: {
           startDate: {
                        required: "Please enter start date."
                      },
              endDate: {
                        required: "Please enter end date.",
                        
                      }, 
        },
        submitHandler: function (Form) {
             var obj={};
            $(Form).serializeArray().forEach(function(element,i){
             obj[element.name]=element.value;
            });
            getReports(obj);
        }           
    });
    // validation ends here  
      $.validator.addMethod("endDate", function(value, element) {
            var startDate = $('#startDate').val();
            return Date.parse(startDate) <= Date.parse(value) || value == "";
        }, "* End date must be after start date");
    
    
});

function getServiceList(){
  
   $.ajax({
        type: "GET",
        url: "../server/getServices.php",
    }).done(function(response) {     
      var resultArray=JSON.parse(response);
      $("#service").append("<option value="+0+" selected>All</option>");
      resultArray.forEach(function(obj,i,fullArr){
        console.log(obj,i);
        $("#service").append("<option value="+obj.id+">"+obj.service_name+"</option>");
      });
    }).fail(function(jqXHR, textStatus, errorThrown) {
        
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
function getReports(formObj){
    $.ajax({
          type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
          url: "../server/getServicesReports.php",
          data:JSON.stringify(formObj),
      }).done(function(response) {
          createReportDataTable(response);
          hideLoader();
          //
          
      }).fail(function(jqXHR, textStatus, errorThrown) {
          hideLoader();
          alert("Unable to proccess");

      })
      .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}

//below function initializes the data tables

function createReportDataTable(objFordataTable){
    manageBooksDataTable= $("#reportTable").DataTable({
     "destroy": true,
     "dom": 'Bfrtip',
        "buttons": [
           /* 'copy',*/
            {
                extend: 'excel',
                messageTop: 'The Recreatioal facility information.'
            },
            {
                extend: 'pdf',
                messageBottom: null
            },
           
        ],
     "data" : objFordataTable,
     "columns":[
                {
                   "title":"Ccreated At", "data": "created_at"
                },
                {
                  "title":"Closed on", "data": "closed_at"
                },
               
                {
                  "title":"Name", "data": "name"
                },

                {
                  "title":"Flat", "data": "flat"
                },
                {
                  "title":"Service", "data": "serviceName"
                }, 
              ]
   });
}
//initializing data table ends



//below function updates the booked slot information in db
function editBookedSlotInfo(editedObj){
    $.ajax({
          type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
          url: "../server/facilities/updateBookedSlot.php",
          data:JSON.stringify(editedObj),
      }).done(function(response) {
          hideLoader();
          var checkedInputs= $("#editBookedSlotForm input:checked");
          var checkedInputsArr= Array.prototype.slice.call(checkedInputs);
          $(editedrow).closest("tr").find("span.bookStatus").html(checkedInputsArr[0].nextSibling);
          $(editedrow).closest("tr").find("span.payStatus").html(checkedInputsArr[1].nextSibling);
           $("#editBookedSlotModal").modal("hide");
          //
          
      }).fail(function(jqXHR, textStatus, errorThrown) {
          hideLoader();
          alert("Unable to proccess");

      })
      .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
} 
