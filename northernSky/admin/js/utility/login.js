$(function(){

    loginValidation();
});

function loginValidation(){
    var loginForm = $("#loginForm");
    $("#loginForm").validate({
        messages: {
                username: "Please enter the username",
                password:"Please enter the password"
        },
        submitHandler: function (loginForm) {
            
            var username = $("#username").val();
            var password = $("#password").val();
            var details = {"username":username,"password":password};
            loginCall(details);
        }           
    });
}

function loginCall(details){
    showLoader();
    var dataR = {"request":details};
    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.LOGIN_URL),
        data: JSON.stringify(dataR)
    }).done(function(response) {
        validationOnLogin(details, response);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}