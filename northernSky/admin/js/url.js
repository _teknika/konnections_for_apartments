// Get Current Url and Path 
var host = window.location.origin;
var path = window.location.pathname.split('/')[1];

var address = host + "/" + path;
//var address = "http://smargav.com/applications/typcbe"

//var images_path = address+"/resources/images";
var extraImage = address + "/server/adv";
var user_img = address + "/server/user";

var phpInfo = "../server";
var dataInfo = "../server/data";
var api = {
    "LOGIN_URL": phpInfo + "/login.php",
    "REGISTER_URL": phpInfo + "/register.php",
    "CITY_URL": phpInfo + "/city.php",
    "GETCONTACTSMINIMALINFO_URL": phpInfo + "/getContactsMinimalInfo.php",
    "GETCONTACTINFO_URL": phpInfo + "/getContactInfo.php",
    "ADD_MEMBER_URL": phpInfo + "/webAppRegister.php",
    "UPDATE_MEMBER_URL": phpInfo + "/updateMember.php",
    "MEMBER_UPDATE_COM_URL":phpInfo +"/updateCommitteMember.php", 
    "GETCONTACTS_URL": phpInfo + "/getContacts.php",
    "GETMEMBERSTATS_URL": phpInfo + "/getMemberStats.php",
    "DELETECONTACT_URL": phpInfo + "/deleteMember.php",
    "DELETE_Flats_URL": phpInfo + "/deleteFlats.php",
    "SEARCHCONTACT_URL": phpInfo + "/search.php",
    "GCM_URL": phpInfo + "/sendNotifications.php",
    "ADD_CATEGORY_URL": phpInfo + "/addCategory.php",
    "ADD_BANNER_URL": phpInfo + "/addBanner.php",
    "DELETE_BANNER_URL": phpInfo + "/deleteBanner.php",
    "ADD_COM_URL": phpInfo + "/addCommittee.php",
    "DELETE_COM_URL": phpInfo + "/deleteCommittee.php",
    "ADD_EVENT_URL": phpInfo + "/addEvent.php",
    "DELETE_EVENT_URL": phpInfo + "/deleteEvent.php",
    "DELETE_THOUGHT_URL": phpInfo + "/deleteThought.php",
    "DELETE_BULLETIN_URL": phpInfo + "/deleteBulletin.php",
    "ADD_THOUGHT_URL": phpInfo + "/addThought.php",
    "ADD_BULLETIN_URL": phpInfo + "/addBulletin.php",
    "ADD_PREACHINGS_URL": phpInfo + "/addPreachings.php",
    "ADD_PASTOR_REQUEST_URL": phpInfo + "/addPastorRequest.php",
    "DELETE_PREACHINGS_URL": phpInfo + "/deletePreachings.php",
     "DELETE_BOOK_URL": phpInfo + "/deleteBook.php",
     "DELETE_EVENGELISM_URL": phpInfo + "/deleteEvengelism.php",
      "DELETE_PASTOR_REQUEST_URL": phpInfo + "/deletePastorRequest.php",
    "ADD_MAPS_URL": phpInfo + "/addMaps.php",
    "DELETE_MAPS_URL": phpInfo + "/deleteMaps.php",

    "GETHEADCONTACTS_URL": phpInfo + "/getHeadContacts.php",
    "UPDATE_CONTACT_URL": phpInfo + "/updateContact.php",
    "UPDATE_VENDOR_URL": phpInfo + "/updateVendor.php",
    
    "CATEGORY_URL": phpInfo + "/getCategories.php",
    "GET_COM_URL": "../" + phpInfo + "/getCommittee.php",
    "GET_EVENT_URL": "../" + phpInfo + "/getEvents.php",
    //"GET_THOUGHT_URL": "../" + phpInfo + "/getThoughts.php",
    //"GET_BULLETIN_URL": "../" + phpInfo + "/getBulletin.php",
    //"GET_PREACHINGS_URL": "../" + phpInfo + "/getPreachings.php",

    //"GET_MAPS_URL": "../" + phpInfo + "/getMaps.php",

    //"CATEGORY_URL":dataInfo+"/category.json",

    "GETSMSTRACKER_URL": phpInfo + "/getSmsTracker.php",
    "GETSMSHISTORY_URL": phpInfo + "/getSmsHistory.php",
    "MaintainaceHistory_URL": "../" + phpInfo + "/getOwnerServiceInfo.php",
    // "GET_ADV": dataInfo + "/adv.json",
    // "GET_EVENTS": dataInfo + "/events.json",
    // "GET_MEMBERS": dataInfo + "/members.json",
    // "GET_THOUGHTS": dataInfo + "/thoughts.json",
    // "GET_BULLETIN": dataInfo + "/bulletin.json",
    // "GET_PREACHINGS": dataInfo + "/preachings.json",
    // "GET_MAPS": dataInfo + "/maps.json"
   
   "getFamilySubMembers":phpInfo + "/getFamilySubMembers.php",
};

// Method for Get absolute Url
function getUrl(name) {
    return name;
}
