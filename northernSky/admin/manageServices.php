<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->


  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>
  
 <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon' >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
    
        <link href="css/style.css" rel="stylesheet">
        <link href="css/common.css" rel="stylesheet">
<script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
 <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>

<style>
 .closeServiceRequest,.approve,.responses{
          border: 0px;
          padding: 4px 7px;
          background-color: transparent;
          text-decoration:underline;
      }
   #viewResponsesModal th{
       text-align:left;
   }   
      
 </style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="display: flex;justify-content: space-between;">
      <h1>Manage Services</h1>
      <a href="servicesReports.php"><u>Services Reports</u></a>
    </section>

    <!-- Main content -->
    <section class="content">
       <!-- add service starts -->
                <!-- Info boxes -->
        <div class="box  box-default box-solid collapsed-box" data-widget="box-widget"> 
          <div class="box-header">
            <h3 class="box-title">Add Service</h3>
            <div class="box-tools">
              <!-- This will cause the box to collapse when clicked -->
              <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus" data-widget="collapse"></i></button>
              
            </div> 
          </div>
          <div class="box-body">
             <form class="addServiceForm" id="addServiceForm" method="POST"  enctype="multipart/form-data" >  
                 <div class="row"> 
                  <div class="col-xs-6">
                    <div class="form-group">
                        <label for="bname">Name</label>
                        <input type="text" class="form-control" id="bname" name="bname" required>
                    </div>
                 </div>
                <div class="col-xs-3">
                    <button type="submit" class="btn btn-primary" style="margin-top: 25px;
                       margin-right: 15px;margin-bottom: 10px" >ADD</button>             
                </div>
                </div>      
              </form>
            </div>
          </div>  
       <!-- add service ends -->
  <div class="box  box-default box-solid" data-widget="box-widget"> 
    <div class="box-header">
        <h3 class="box-title">Services</h3>
        <div class="box-tools">
            <!-- This will cause the box to collapse when clicked -->
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus" data-widget="collapse"></i></button>             
        </div> 
    </div>
    <div class="box-body" style="display: block;">
        <table class="table ns-table">
            <thead>
            <tr>
            <th>Created At</th>    
            <th>Name</th>
            <th>Flat</th>
            <th>Service</th>
            <th>Message</th>
            <th>Available</th>                                       
            <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
                require_once('../server/libs/dbConnection.php');
                $connection = new dbconnection();
                $con = $connection->connectToDatabase();
                $sql = mysqli_query($con, "SELECT req.*,srvs.service_name as serviceName FROM requests req inner join services srvs on req.service=srvs.id where service_status!=1 order by created_at desc");
                $rows_count = mysqli_num_rows($sql);
                while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                    echo '<tr class="active">';
                    echo "<td>" . $rows_fetch['created_at'] . "</td>";
                    echo "<td>" . $rows_fetch['name'] . "</td>";
                    echo "<td>" . $rows_fetch['flat'] . "</td>";
                    echo "<td>" . $rows_fetch['serviceName'] . "</td>";
                    echo "<td>" . $rows_fetch['message'] . "</td>";
                    $availability=json_decode($rows_fetch['availability_detail']);
                    echo "<td>" .$availability->date." ".$availability->from."-".$availability->to ."</td>";
                    // echo "<td>" . $rows_fetch['service_status'] . "</td>";
                    echo '<td><button class="approve" id="' . $rows_fetch['id'] . '">SMS</button>  
                    <button class="responses" id="' . $rows_fetch['id'] . '">History</button>
                    <button class="closeServiceRequest" id="' . $rows_fetch['id'] . '">Close</button>
                    </td>';
                    echo '</tr>';
                }
                $connection->closeConnection();
                ?>                            
            </tbody>
        </table>    
            
        </div>      
    </div>                                          
    <div class="box  box-default box-solid collapsed-box" data-widget="box-widget"> 
        <div class="box-header">
        <h3 class="box-title">Closed Services</h3>
        <div class="box-tools">
            <div class="input-group pull-left">
            <input type="text" class="form-control" id="searchClosedServices" placeholder="Search">
            </div>
            <!-- This will cause the box to collapse when clicked -->
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-plus" data-widget="collapse"></i></button>             
            </div> 
        </div>
        <div class="box-body">
            <table class="table ns-table table-fixed" id="closedServiceTable">
                <thead>
                <tr>
                    <th>Created At</th>
                    <th>Closed on</th>
                    <th>Name</th>
                    <th>Flat</th>
                    <th>Service</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    require_once('../server/libs/dbConnection.php');
                    $connection = new dbconnection();
                    $con = $connection->connectToDatabase();
                    $sql = mysqli_query($con, "SELECT req.*,service.service_name as serviceName FROM requests req inner join services service on req.service=service.id where service_status=1 order by created_at desc");
                    $rows_count = mysqli_num_rows($sql);
                    while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                        echo '<tr class="active">';
                        echo "<td>" . $rows_fetch['created_at'] . "</td>";
                        echo "<td>" . $rows_fetch['closed_at'] . "</td>";
                        echo "<td>" . $rows_fetch['name'] . "</td>";
                        echo "<td>" . $rows_fetch['flat'] . "</td>";
                        echo "<td>" . $rows_fetch['serviceName'] . "</td>";
                        echo '</tr>';
                    }
                $connection->closeConnection();
            ?>                             
                </tbody>
            </table>         
    </div>      
   </div>
 </div>        
</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>
  
    <!--approve  Modal starts-->
    <div class="modal fade" id="approveModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
    <div class="modal-content">
     <form action="#"  name="approveRequest_form" id="approveRequest_form">
        <div class="modal-body">
         
            <div class="form-group">
             <!-- Message field -->
             <label class="control-label" for="message">Message</label>
              <textarea name="message" id="message" class="form-control" cols="20" rows="5" required></textarea>
            </div>
           
        </div>
        <div class="modal-footer">
          <button type="submit sendApprovement" class="btn btn-info" >Send</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
     </form> 
    </div>
      
    </div>
  </div>
<!-- approve  Modal ends -->

    <!--view detail  Modal starts-->
   <div class="modal fade" id="viewResponsesModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <table class="table table-condensed" style="margin-bottom: 0px;">
            <thead>
            <tr>
                <th>Date</th>
                <th>Responses</th>
            </tr>
            </thead>
            <tbody id="viewResponseBody">
            </tbody>
        </table>
       </div> 
        <div class="modal-footer">
          
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
      
    </div>
  </div>
<!-- view detail Modal ends -->
</div>
<!-- ./wrapper -->



 <!-- Grid Switch Mode -->
            <link rel="stylesheet" type="text/css" href="grid/css/component.css" />
            <script src="grid/js/classie.js"></script>
            <script src="grid/js/cbpViewModeSwitch.js"></script>
            <!-- Grid Switch Mode -->

            <script src="grid/list.js"></script>
            <script>
                $(function () {
                    $("#main-header").load("headerMenu.php");
                    $("#main-sidebar").load("sidebarMenu.php");
                     $("#main-footer").load("footerMenu.php");
                    $("#cbp-vm").hide();
                    initPage();
                    $("#searchClosedServices").on("keyup",onClosedServices);
                });

        
                function initPage() {
                    
                    $('.closeServiceRequest').click(function (evt) {
                        var isConfirm=confirm("Are you sure that you want to close this request?");
                        isConfirm ? closeRequest(this.id):"";
                    });
                  
                    $('.approve').click(function (evt) {
                        //approveMessage(this.id);
                        $("#approveRequest_form")[0].reset();
                        $("#approveModal").attr("data-id",this.id);
                        $("#approveModal").modal();
                    });
                    $("#approveRequest_form").submit(function(e){
                        e.preventDefault();
                        approveMessage($("#approveModal").attr("data-id"),$("#message").val());
                    });
                    $('.responses').click(function (evt) {
                        getResponsesFromAdmin(this.id);                       
                    });

                     //validation in add service 
                   $('#addServiceForm').validate({ 
                     rules: {
                         bname: {
                            required: true,
                            }
                            },
                    messages: {
                            bname: {
                   required: "Please enter the service name."
                        },
                      },
                 errorPlacement: function(error, element) {
                 var divObj=document.createElement("div");
                divObj.setAttribute("class","error-messageColor");
                 element.parent().append(divObj);
                  error.appendTo(element.next());
                  },
                submitHandler: function () {
                        addService({"serviceName":$("#bname").val()});
                }
                })
                }
                
                function deleteMessage(id) {
                    showLoader();

                    //php info is found in urls.js.
                    //Amit - 10th August
                    var finalUrl = phpInfo + "/deleteRequest.php"
                    $.ajax({
                        type: "GET",
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        url: finalUrl,
                        data: {id: id},
                    }).done(function (response) {
                        hideLoader();
                        if (response.isSuccessful === true) {
                            alert("Request has been deleted");
                            location.reload();
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        hideLoader();
                        alert("Server failed");
                    })
                            .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});

                }

                function approveMessage(id,message) {
                    showLoader();
                     var finalUrl = phpInfo + "/approveRequest.php"
                    $.ajax({
                        type: "GET",
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        url: finalUrl,
                        data: {id: id,messageR:message},
                    }).done(function (response) {
                        hideLoader();
                        if (response.isSuccessful === true) {
                            $("#approveRequest_form")[0].reset();
                            $("#approveModal").modal("hide");
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        hideLoader();
                        alert("Server failed");
                    })
                            .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});

                }
            function closeRequest(id) {
                    showLoader();
                     var finalUrl = phpInfo + "/closeServiceRequest.php"
                    $.ajax({
                        type: "GET",
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        url: finalUrl,
                        data: {id: id},
                    }).done(function (response) {
                        hideLoader();
                        if (response.isSuccessful === true) {
                            
                            location.reload();
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        hideLoader();
                        alert("Server failed");
                    })
                            .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});

                }
            //add service to database
            function addService(serviceInfo){
                $.ajax({
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    url: "../server/addServices.php",
                    data: JSON.stringify(serviceInfo)
                }).done(function(response) {
                hideLoader();
                alert("Service added successfully");
                location.reload();
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                })
                .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
            }
            //below function gets all responses of the admin to the perticular service request
            function  getResponsesFromAdmin(id){
                $.ajax({
                    type: "GET",
                    url: "../server/getResponsesForServiceRequest.php?id="+id,
                    contentType: 'application/json; charset=utf-8',
                   
                }).done(function(response) {
                   hideLoader();
                   $("#viewResponseBody").empty();
                   JSON.parse(response).forEach(function(obj,indx){
                       console.log(JSON.stringify(obj));
                       var rowdata="<tr>"+
                                    "<td>"+obj.created_at+"</td>"+
                                    "<td>"+obj.resp_message+"</td>"+
                                    "</tr>";
                       $("#viewResponseBody").append(rowdata);
                   });
                   $("#viewResponsesModal").modal();
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                })
                .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
            }

            function onClosedServices(){
                    var value = $(this).val().toLowerCase();
                    $("#closedServiceTable >tbody > tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });    
            }

            </script>

</body>
</html>
