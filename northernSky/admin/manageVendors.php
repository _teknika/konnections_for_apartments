<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
 <link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
       
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>
        <script src="js/utility/vendors.js"></script>
        
        <style>
          #vendorForm{
                background: linear-gradient(90deg, #ded6d9 50%,#d9e6e6 50%);
                padding: 4px;
          }
         .table-bordered{
               border: 2px solid #2980B9;
               border-top: 0;
         }
          .tbl-header{
                background-color: #827f7f;
               color: white;
          }
		   tbody tr:nth-child(odd) {
		    background: white;
		   }
		  tr:nth-child(even) {
		    background: linear-gradient(to right, #dcc760 ,#715f5f);
		    background: lightyellow;
		  }
		   caption{
	      padding:8px;
	      background: #3a444882;
	    }
	    .caption-title{
	    	    margin: 0px;
		    color: white;
		    padding: 5px;
	    }
          .custom-form-control{
                    font-size: 14px;
                    line-height: 2;
                    color: #555;
                    border-radius: 2px;
                    border-style: none;
          }
         .deleteVendor{
            background:transparent;
            border-width: 0px;
          color: red;
          padding: 2px 15px;
          border-radius: 1px;
         } 
        </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Vendors</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
          <div class="box  box-warning box-solid collapsed-box" data-widget="box-widget"> 
          <div class="box-header">
            <h3 class="box-title">Add Vendor</h3>
            <div class="box-tools">
              <!-- This will cause the box to collapse when clicked -->
              <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus" data-widget="collapse"></i></button>
              
            </div> 
          </div>
          <div class="box-body">
             <form class="vendorForm" id="vendorForm" method="POST"  enctype="multipart/form-data">  
                <div class="row"> 
                 <div class="col-xs-6">
                   <div class="form-group">
                    <label for="bname">Name</label>
                    <input type="text" class="form-control" id="bname" name="bname" required>
                  </div>
                </div>
                <div class="col-xs-6">             
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email">
                </div> </div>
                </div>
              <div class="row"> 
                    <div class="col-xs-6"><div class="row">
                      <div class="form-group col-xs-6">
                          <label for="contactNo">Contact number</label>
                          <input type="number" class="form-control" id="contactNo" name="contactNo" minlength="10" required>
                      </div>
                      <div class="form-group col-xs-6">
                          <label for="altNo">alternate number</label>
                          <input type="number" class="form-control" id="altNo" name="altNo">
                      </div></div> 
                    </div>
                    <div class="col-xs-3">
                       <div class="form-group">
                         <label for="websiteLink">Website link</label>
                         <input type="text" class="form-control" id="websiteLink" name="websiteLink">
                       </div> 
                    </div>
                    <div class="col-xs-3">
                       <div class="form-group">
                         <label for="category">Category</label>
                         <select  class="form-control" id="category" name="category">
                          <?php
                             require_once('../server/libs/dbConnection.php');
                             $connection = new dbconnection();
                             $con = $connection->connectToDatabase();
                             $sql = mysqli_query($con, "SELECT * FROM vendor_category vc order by vc.category");
                             $rows_count = mysqli_num_rows($sql);

                             while ($rows_fetch = mysqli_fetch_assoc($sql)) {
  
                                 echo "<option value=".$rows_fetch['id'].">" . $rows_fetch['category'] . "</option>";
                                                                
                             }
                             $connection->closeConnection();
                          ?>
                         </select>
                       </div> 
                    </div>
               </div> 
                <div class="row">
                <div class="col-xs-6">
                <div class="form-group">
                  <label for="dealsIn">Deals in</label>
                  <textarea class="form-control" id="dealsIn" name="dealsIn" rows="3" required></textarea>
                </div> </div>
                <div class="col-xs-6" style="padding-top: 20px">
                <div class="form-group">
                  <label for="address">Address</label>
                  <input type="text" class="form-control" id="address" name="address" rows="3">
                </div> </div></div>
                <br/>
                <div>
                <button type="submit" class="btn btn-success" style="margin-top: 20px;
                       float: right;margin-right: 15px;margin-bottom: 10px" >Done</button>
                </div>
              </form>
            </div>
          </div>  
        
        <hr style="border-top: 2px solid darkgrey;clear: both">
        <table id="vendorTable" ></table>

 <!-- Modal -->
  <div class="modal fade" id="deleteVendorModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <h4>Are you sure you want to delete this row?</h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default deleteVndr" onclick="deleteVendorOnConfirm.call(this)">Delete</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>
  

      <div class="modal fade bs-example-modal-lg" id="editVendor">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" >Edit</h4>
                </div>
                <div class="modal-body">
                                     <form id="editVendorForm" name="editVendorForm" class="form-horizontal" method="POST" action="">
                                              <input type="text" id="vendorId" name="vendorId"  class="form-control hide"/>
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Name </label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="nameR" name="nameR"  class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                 
                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Email</label>
                                                         <div class="col-md-12">
                                                            <input type="email" id="emailR" name="emailR" class="form-control" />
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                 <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Category</label>
                                                         <div class="col-md-12">
                                                            <select   name="category" class="form-control" required/>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Mobile  Number</label>
                                                         <div class="col-md-12">
                                                            <input type="number" id="mobilenumberR" minlength="10" maxlength="20" name="mobilenumberR" class="form-control" required/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Alternative  Number</label>
                                                         <div class="col-md-12">
                                                            <input type="number" id="altNumber" minlength="10" maxlength="20" name="altNumber" class="form-control"/>
                                                         </div>
                                                      </div>
                                                   </div> 
                                                </div>
                                                <div class="row">   
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                          <label class="col-md-12">Address
                                                          </label>
                                                          <div class="col-md-12">
                                                             <input  type="text" id="addressR" name="addressR" class="form-control">
                                                          </div>
                                                       </div>
                                                   
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                          <label class="col-md-12">Link
                                                          </label>
                                                          <div class="col-md-12">
                                                             <input type="text"   id="link" name="link" class="form-control" />
                                                          </div>
                                                       </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                 <div class="col-md-12">
                                                  <div class="form-group">
                                                          <label class="col-md-12">Deals in<span>*</span>
                                                          </label>
                                                          <div class="col-md-12">
                                                             <textarea  rows="4" cols="50"   id="dealsin" name="dealsin" class="form-control" required></textarea >
                                                          </div>
                                                  </div>
                                                  </div>    
                                                </div>
                                                 <div class="footer-btn" style="overflow: auto;">
                                                   
                                                     <button type="submit" class="btn btn-primary pull-right" id="editConfirmVendor">Done</button>
                                                     <button type="submit" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                                                 </div>
                                    </form>
                        </div>
               

            </div>
        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
</body>
</html>
