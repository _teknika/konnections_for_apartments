<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
 <link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
       
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>
        <style>
            table.db-table    { border-right:1px solid #ccc; border-bottom:1px solid #ccc;width:100%; }
            table.db-table th { background:#eee; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
            table.db-table td { padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }

            .error{
                color: red
            }
        </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Thought For The Day</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
                       <div class="right_col" role="main" id="gcmList">
                    <br />
                    <div class="" id="hacker-list">
                        <div class="row top_tiles">
                            <form id="thoughtsForm" name="thoughtsForm" class="form-horizontal" method="POST" action="">
                                <div class="panel">

                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Title</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="title" name="title"  class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Message</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="message" name="message" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-md-12">Posted By<span class="required">*</span></label>
                                                    <div class="col-md-12">
                                                        <input type="text" id="name" name="name" class="form-control" required/>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- <div class="col-md-4" style="display:none">
                                                <div class="form-group">
                                                    <label class="col-md-12">Photos Link</label>
                                                    <div class="col-md-12">
                                                        <input type="text" id="photos" name="photos" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->

                                    </div>
                                   <div class="text-right">
                                       <input type="submit" class="btn btn-success" value="AddThought" name="submit" /> 
                                   </div>
                                </div>
                        </div> </form>
                    </div></div></div>


                    <div class="pull right" >
                        <table class="db-table " cellpadding="0" cellspacing="0" style="width-left:25%" >
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Message</th>
                                    <th>Posted By</th>
                                    <th>Date time</th>
                                    <th>Action</th>

                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                require_once('../server/libs/dbConnection.php');
                                $connection = new dbconnection();
                                $con = $connection->connectToDatabase();

                                $sql = mysqli_query($con, "SELECT * FROM thoughts");
                                $rows_count = mysqli_num_rows($sql);

                                while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                                    echo '<tr>';
                                    echo "<td>" . $rows_fetch['title'] . "</td>";
                                    echo "<td>" . $rows_fetch['message'] . "</td>";
                                    echo "<td>" . $rows_fetch['name'] . "</td>";
                                    echo "<td>" . $rows_fetch['datetime'] . "</td>";
                                    echo '<td><button class="deleteThoughtClass" id="' . $rows_fetch['id'] . '">Delete</button></td>';
                                    echo '</tr>';
                                    
                                }
                                $connection->closeConnection();
                                ?>
                            </tbody>
                        </table>
                    </div>


                    <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
                        <div class="cbp-vm-options">
                            <a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid">Grid View</a>
                            <a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list">List View</a>
                        </div>
                        <input class="gcm filterContact form-control hide" placeholder="Notify.." />
                        <ul class="list" id="contactsDashboard">

                        </ul>
                    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>


<!-- Grid Switch Mode -->
        <link rel="stylesheet" type="text/css" href="grid/css/component.css" />
        <script src="grid/js/classie.js"></script>
        <script src="grid/js/cbpViewModeSwitch.js"></script>
        <!-- Grid Switch Mode -->

        <script src="grid/list.js"></script>


        <script>

            $(function () {

                $("#main-header").load("headerMenu.php");
                $("#main-sidebar").load("sidebarMenu.php");
                $("#main-footer").load("footerMenu.php");
                $("#cbp-vm").hide();

                initSearch();
            });

            function initSearch() {
                var thoughtsForm = $("#thoughtsForm");
                $("#thoughtsForm").validate({
                    messages: {
                    },
                    submitHandler: function (thoughtsForm) {
                        addThought();
                    }
                });

                $('.deleteThoughtClass').click(function (evt) {
                    deleteThought(this.id);
                });
            }
            function addThought() {
                var title = $("#title").val();
                var message = $("#message").val();
                var photos = $("#photos").val();
                var name = $("#name").val();
                var details = {title: title, message: message, photos: photos, name: name};
                showLoader();
                var dataR = details;
                $.ajax({
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    url: getUrl(api.ADD_THOUGHT_URL),
                    data: dataR,
                }).done(function (response) {
                    hideLoader();
                    if (response.isSuccessful === true) {
                        alert("Thought for the day added successfully");
                        location.reload();
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                })
                        .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});

            }
            function deleteThought(id) {
                showLoader();
                $.ajax({
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    url: getUrl(api.DELETE_THOUGHT_URL),
                    data: {id: id},
                }).done(function (response) {
                    hideLoader();
                    if (response.isSuccessful === true) {
                        alert("Thought for the day deleted successfully");
                        location.reload();
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                })
                        .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});

            }
        </script>
</body>
</html>
