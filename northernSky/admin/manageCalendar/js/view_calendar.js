var getBookedSlotsTimer;//this timer gets books slots to admin page
function getFacilityList(){
	showLoader();
	 $.ajax({
        type: "GET",
        url: "../server/getFacilityList.php",
    }).done(function(response) {
      hideLoader();
      var resultArray=JSON.parse(response);
      resultArray.forEach(function(obj,i,fullArr){
      	console.log(obj,i);
      	$("#facilityId").append("<option value="+obj.id+">"+obj.facility_name+"</option>");
      });
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
       showNSPopup({message: "Please try again.",ButtonNames: ['OK']});
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
/* Wait for DOM to load etc */
$(document).ready(function(){
     $("#main-header").load("headerMenu.php");
     $("#main-sidebar").load("sidebarMenu.php");
     $("#main-footer").load("footerMenu.php");
     $("#calendar").fullCalendar( 'destroy');
     generateCalender();
         
});
function generateCalender(){

	
	//Initialisations
	initialise_calendar();
	initialise_color_pickers();
	initialise_buttons();
	initialise_event_generation();
	initialise_update_event();
    //
    var facilityIdQry=window.location.href.split("?")[1];
    $(".external-event.ui-draggable").attr('data-facilityId', facilityIdQry.split("=")[1].trim().split("#")[0]);
	 
}

/* Initialise buttons */
function initialise_buttons(){

	$('.btn').button();
}


/* Binds and initialises event generation functionality */
function initialise_event_generation(){

	//Bind event
	$('#btn_gen_event').bind('click', function(){

		//Retrieve template event
	/*	var template_event = $('#external_event_template').clone();
		var background_color = $('#txt_background_color').val();
		var border_color = $('#txt_border_color').val();
		var text_color = $('#txt_text_color').val();
		var title = $('#txt_title').val();
		var description = $('#txt_description').val();
		var price = $('#txt_price').val(); 
		var available = $('#txt_available').val();*/
        var globalEventColor="blue";
		//Edit id
		$(template_event).attr('id', get_uni_id());

		//Add template data attributes
		$(template_event).attr('data-background', globalEventColor);
		$(template_event).attr('data-border', globalEventColor);
		$(template_event).attr('data-text', globalEventColor);
		$(template_event).attr('data-title', "");
		$(template_event).attr('data-description', "");
		$(template_event).attr('data-price', "");
		$(template_event).attr('data-available', available);

		//Style external event
		$(template_event).css('background-color', globalEventColor);
		$(template_event).css('border-color', globalEventColor);
		$(template_event).css('color', globalEventColor);

		//Set text of external event
		$(template_event).text("");

		//Append to external events container
		$('#external_events').append(template_event);

		//Initialise external event
		initialise_external_event('#' + $(template_event).attr('id'));

		//Show
		$(template_event).fadeIn(2000);
	});
}


/* Initialise external events */
function initialise_external_event(selector){

	//Initialise booking types
	$(selector).each(function(){

		//Make draggable
		$(this).draggable({
			revert: true,
			revertDuration: 0,
			zIndex: 999,
			cursorAt: {
				left: 10,
				top: 1
			}
		});

		//Create event object
		var event_object = {
			title: $.trim($(this).text())
		};

		//Store event in dom to be accessed later
		$(this).data('eventObject', event_object);
	});
}


/* Initialise color pickers */
function initialise_color_pickers(){

	//Initialise color pickers
	$('.color_picker').miniColors({
		'trigger': 'show',
		'opacity': 'none'
	});
}


/* Initialises calendar */
function initialise_calendar(){

	//Initialise calendar
	$('#calendar').fullCalendar({
		theme: true,
		firstDay: 1,
		header: {
			left: 'today prev,next',
			center: 'title',
			right: 'agendaWeek,agendaDay'
		},
		defaultView: 'agendaWeek',
		minTime: '4:00am',
		maxTime: '23:30pm',
		allDaySlot: false,
		
		
		editable: true,
		eventStartEditable :false,	

		eventOverlap:true,
		columnFormat: {
			month: 'ddd',
			week: 'ddd dd/MM',
			day: 'dddd M/d'
		},
		droppable: true,
		drop: function(date, all_day){
			external_event_dropped(date, all_day, this);
		},
		eventClick: function(cal_event, js_event, view){
			console.log(cal_event, js_event, view);
			cal_event.className.indexOf('bookedSlot') > -1?getBookedSlotInfo(cal_event): "";
			cal_event.className.indexOf('unAvailable') > -1?getUnAvailableSlotInfo(cal_event): "";
			console.log(cal_event.id, cal_event.backgroundColor, cal_event.borderColor, cal_event.textColor, cal_event.title, cal_event.description, cal_event.price, cal_event.available);
		},
	    eventResize: function(event, delta,a,revertFunc, ui, view) {
                showNSPopup({message: "Are you sure you want update availability?",ButtonNames: ['Cancel','Ok'],callbackFunction: onConfirm});
                function onConfirm(Submit) {
				        if(Submit==2){  
				           var updateObj={};
			                	updateObj.start=event.start;
			                	updateObj.end=event.end;
			                	updateObj.id=event.id;
			                	updateObj.facilityId=event.facilityId;
			                	updateObj.weekId=new Date(event.end).getDay();

			                	if($(this).hasClass("booked")){ //this calss is added in availabilty.js
								       updateBookAppointment(updateObj,revertFunc);
								  }else{
								  	 updateAvailability(updateObj,revertFunc);
								  }
					    }
				        else{
				          revertFunc();
				        }      
				}

            },
			eventDragStop: function(event,jsEvent) {

			    var trashEl = jQuery('#calendarTrash');
			    var ofs = trashEl.offset();

			    var x1 = ofs.left;
			    var x2 = ofs.left + trashEl.outerWidth(true);
			    var y1 = ofs.top;
			    var y2 = ofs.top + trashEl.outerHeight(true);

			    if (jsEvent.pageX >= x1 && jsEvent.pageX<= x2 &&
			       jsEvent.pageY >= y1 && jsEvent.pageY <= y2) {
			         showNSPopup({message: "Are you sure you want delete this availabilty?",ButtonNames: ['Cancel','Ok'],callbackFunction: onDeleteConfirm});
                      function onDeleteConfirm(Submit) {
				        if(Submit==2){  
							isSlotsBookedInAvailability(event);
					    }    
				      }
			    }
			},
			 events: function(start, end, timezone, callback) {
				 console.log(start, end, timezone);
				 $("#bookedSlotInfoContainer").addClass('hide');//hiding booked slot info
				 var facilityIdQry=window.location.href.split("?")[1];
				 getAvailability(facilityIdQry,start, end);
				 getBookedSlots(facilityIdQry);
				 if(getBookedSlotsTimer){    
					 clearInterval(getBookedSlotsTimer);
				 }
				 getBookedSlotsTimer=setInterval(function(){
					getBookedSlots(facilityIdQry);
				 },15000);
			 },

		
	});	

	//Initialise external events
	initialise_external_event('.external-event');
}


/* Handle an external event that has been dropped on the calendar */
function external_event_dropped(date, all_day, external_event){

	//Create vars
	var event_object;
	var copied_event_object;
	var duration = 60;
	var cost;

	//Retrive dropped elemetns stored event object
	event_object = $(external_event).data('eventObject');

	//Copy so that multiple events don't reference same object
	copied_event_object = $.extend({}, event_object);
	
	//Assign reported start and end dates
	copied_event_object.start = date;
	copied_event_object.end = new Date(date.getTime() + duration * 60000);
	copied_event_object.allDay = all_day;

	//Assign colors etc
	copied_event_object.backgroundColor = $(external_event).data('background');
	copied_event_object.textColor = $(external_event).data('text');
	copied_event_object.borderColor = $(external_event).data('border');

	//Assign text, price, etc
	copied_event_object.id = get_uni_id();
	copied_event_object.title = $(external_event).data('title');
	copied_event_object.description = $(external_event).data('description');
	copied_event_object.price = $(external_event).data('price');
	copied_event_object.available = $(external_event).data('available');
    
    //
    copied_event_object.facilityId=$(external_event).attr("data-facilityId");
    copied_event_object.weekId=date.getDay();//days number sun->0 mon->1 ...
    copied_event_object.availabiltyType='0';//0 means weekly based
    if($(external_event).hasClass("book-day")){
    	
        BookAppointment(copied_event_object);
    }
    else{
    	 if($(external_event).hasClass("day-based")){
    	       copied_event_object.availabiltyType='1';//1 means day based
          }
    	addAvailability(copied_event_object);
    }
    

}


/* Set event generation values */
function set_event_generation_values(event_id, bg_color, border_color, text_color, title, description, price, available){

	//Set values
	$('#txt_background_color').miniColors('value', bg_color);
	$('#txt_border_color').miniColors('value', border_color);
	$('#txt_text_color').miniColors('value', text_color);
	$('#txt_title').val(title);
	$('#txt_description').val(description);
	$('#txt_price').val(price);
	$('#txt_available').val(available);
	$('#txt_current_event').val(event_id);
}


/* Generate unique id */
function get_uni_id(){

	//Generate unique id
	return new Date().getTime() + Math.floor(Math.random()) * 500;
}


/* Initialise update event button */
function initialise_update_event(){
	var test = $('#calendar').fullCalendar( 'clientEvents');
	//Bind event
	$('#btn_update_event').bind('click', function(){

		//Create vars
		var current_event_id = $('#txt_current_event').val();

		//Check if value found
		if(current_event_id){

			//Retrieve current event
			var current_event = $('#calendar').fullCalendar('clientEvents', current_event_id);

			//Check if found
			if(current_event && current_event.length == 1){

				//Retrieve current event from array
				current_event = current_event[0];

				//Set values
				current_event.backgroundColor = $('#txt_background_color').val();
				current_event.textColor = $('#txt_text_color').val();
				current_event.borderColor = $('#txt_border_color').val();
				current_event.title = $('#txt_title').val();
				current_event.description = $('#txt_description').val();
				current_event.price = $('#txt_price').val();
				current_event.available = $('#txt_available').val();

				//Update event
				$('#calendar').fullCalendar('updateEvent', current_event);
			}
		}
	});
}