   $(function(){
   	 $("#loginForm").validate({
        rules:{

        },
        messages: {
                username: "Please enter the username",
                password:"Please enter the password"
        },
        submitHandler: function (loginForm) {
            
            var username = $("#username").val();
            var password = $("#password").val();
            var details = {"username":username,"password":password};
            loginCall(details);
        }           
    });

   	 //availability form validation
   	  $("#availabityFrom").validate({
        rules:{
        	  fromDate: {
                        required: true,
                      },
              toDate: {
                        required: true,
                        endDate:true
                         
                      },                      

        },
        messages: {
        	 fromDate: {
                        required: "Please enter availabilty start date."
                      },
              toDate: {
                        required: "Please enter availabilty end date.",
                        
                      }, 
        },
        submitHandler: function (availabityFrom) {
             var obj={};
	   	 	obj.sat="6";
	   	 	obj.sun="0";
	   	 	$("#availabityFrom").serializeArray().forEach(function(element,i){
			  obj[element.name]=element.value;
			});
	        addAvailabilityAllDay(obj);
          
        }           
    });

   	//unAvailability for validation
   	  $("#unAvailabityFrom").validate({
        rules:{
        	  startDate: {
                        required: true,
                      },
              endDate: {
                        required: true,
                        DateEnd:true
                         
                      },                      

        },
        messages: {
        	 startDate: {
                        required: "Please enter availabilty start date."
                      },
              endDate: {
                        required: "Please enter availabilty end date.",
                        
                      }, 
        },
        submitHandler: function (availabityFrom) {
             var obj={};
	   	 	obj.sat="6";
	   	 	obj.sun="0";
	   	 	$("#unAvailabityFrom").serializeArray().forEach(function(element,i){
			  obj[element.name]=element.value;
			});
	        addUnAvailability(obj);
          
        }           
    });
   	// validation ends here  
   	  $.validator.addMethod("endDate", function(value, element) {
            var startDate = $('#fromDate').val();
            return Date.parse(startDate) <= Date.parse(value) || value == "";
        }, "* End date must be after start date");
      
       $.validator.addMethod("DateEnd", function(value, element) {
            var startDate = $('#startDate').val();
            return Date.parse(startDate) <= Date.parse(value) || value == "";
        }, "* End date must be after start date");
   	 //
   	$("#addAvailabilityBtn").click(function(e){
   	 $("#availabityFrom")[0].reset();
     $("#addAvailabity").modal();
   	});
   	$("#addUnAvailabilityBtn").click(function(e){
   	 $("#availabityFrom")[0].reset();
     $("#addUnAvailabity").modal();
   	});
   	$("#addBookingBtn").click(function(e){
   	$(".seachedContainer").addClass('hide');	
     $("#addBookingFormModal").modal();
   	});
  /* $("#availabityFrom").submit(function(e){
   	 	e.preventDefault();
   	 	
   	 });*/
   $("#deleteBookedSlot").click(function(event) {

			 showNSPopup({message: "Are you sure want to delete?",ButtonNames: ['CANCEL','OK'],callbackFunction: onConfirm});
			 function onConfirm(Submit){
				(Submit==2) ? deleteBookedSlot($("#bookedSlotInfoContainer").attr('data-booked-id')):"";	
             				
			}
			  
        });
   $("#deleteUnAvailableSlot").click(function(event) {
             deleteUnAvailableSlot($("#unAvailableSlotInfoContainer").attr('data-unavailable-id'));
		});	

   });
   function updateAvailability(updateObj,revertFunc){
   	updateObj.start=convertToUtc(updateObj.start);
    updateObj.end=convertToUtc(updateObj.end);
    $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/facilities/updateAvailability.php",
	        data: JSON.stringify(updateObj)
	    }).done(function(response) {
	       
	       showNSPopup({message: "Availabilty updated successfully",ButtonNames: ['OK']});
	        
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       showNSPopup({message: "Unable to proccess",ButtonNames: ['OK']});
	        revertFunc();
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 

  }

  function addAvailability(copied_event_object){
  	  var availabilityObj=copied_event_object;
      availabilityObj.start=convertToUtc(availabilityObj.start);
      availabilityObj.end=convertToUtc(availabilityObj.end);
        $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/facilities/addAvailability.php",
	        data: JSON.stringify(availabilityObj)
	    }).done(function(response) {
	        
	        showNSPopup({message: "Availabilty added successfully",ButtonNames: ['OK']});
	        copied_event_object.id=response.id;
	       	//Render event on calendar
	        $('#calendar').fullCalendar('renderEvent', copied_event_object, false);
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       
	        showNSPopup({message: "Unable to proccess",ButtonNames: ['OK']});
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
  function addAvailabilityAllDay(formdataObj){
   Object.getOwnPropertyNames(formdataObj).length==4?formdataObj.maxWeekId=5:"";
   var facilityIdQry=window.location.href.split("?")[1];
   formdataObj.facilityId=facilityIdQry.split("=")[1].split("#")[0];

        $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/facilities/addAvailabilityForAllDay.php",
	        data: JSON.stringify(formdataObj)
	    }).done(function(response) {
	        
	        showNSPopup({message: "Availabilty added successfully",ButtonNames: ['OK']});
	        $("#addBookingFormModal").modal("hide");
	        window.location.reload();
	       /* $("#calendar").fullCalendar( 'destroy');
           generateCalender();*/
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       showNSPopup({message: "Unable to proccess",ButtonNames: ['OK']});
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
    function getAvailability(facilityIdQry,calendarStart,calendarEnd){
    	function dateFormat(date){
           var dateObj=new Date(date);
           var formattedDate=dateObj.getFullYear()+"-"+leadingZero((dateObj.getMonth()+1))+"-"+leadingZero(dateObj.getDate());
           return formattedDate;
    	}
    	function leadingZero(arg){
    	 var value=arg;	
          var temp=arg < 10? value="0"+arg: "";
          return value;
    	}
    	var startDate= dateFormat(calendarStart);
    	var endDate= dateFormat(calendarEnd);
        $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/facilities/getAvailabiltyList.php",
	        data:JSON.stringify({start:startDate,end:endDate,facilityId:facilityIdQry.split("=")[1].split("#")[0]}),
	    }).done(function(response) {
            var calendarStartDate=new Date(calendarStart);
            var calendarEndDate=new Date(calendarEnd);
	    	response.forEach(function(obj,i){
	    		obj.allDay=false;
	    		obj.title="";
	    		 $('#calendar').fullCalendar( 'renderEvent', obj,false);
	    	});
	    	getUnAvailability(facilityIdQry,calendarStart,calendarEnd);
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       showNSPopup({message: "Unable to proccess",ButtonNames: ['OK']});
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
  function BookAppointment(appointmentObj){
  	appointmentObj.start=convertToUtc(appointmentObj.start);
    appointmentObj.end=convertToUtc(appointmentObj.end);
  	appointmentObj.personId=2837;
	       $.ajax({ 
		        type: "POST",
	            contentType: 'application/json; charset=utf-8',
	            dataType: 'json',
	            data:JSON.stringify(appointmentObj),
		        url: "../server/facilities/addBookedSlot.php",
		    }).done(function(response) {
		    	appointmentObj.id=response.id;
		    	appointmentObj.className="booked";
		    	 $('#calendar').fullCalendar('renderEvent', appointmentObj, false);
	    	 	showNSPopup({message: "Slot is booked.",ButtonNames: ['OK']});
		    }).fail(function(jqXHR, textStatus, errorThrown) {
		       showNSPopup({message: "Unable to proccess",ButtonNames: ['OK']});
		    })
		    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
	}
	
     function deleteBookedSlot(bookedSlotId){
	       $.ajax({ 
		        type: "POST",
		        url: "../server/facilities/deleteBookedSlot.php?id="+bookedSlotId,
		    }).done(function(response) {
		       $('#calendar').fullCalendar( 'removeEvents', bookedSlotId);
		       $("#bookedSlotInfoContainer").addClass('hide');//hiding booked slot info
	    	   showNSPopup({message: "Deleted booked slot successfully.",ButtonNames: ['OK','CANCEL']});
		    }).fail(function(jqXHR, textStatus, errorThrown) {
		        showNSPopup({message: "Unable to delete.",ButtonNames: ['OK']});
		    })
		    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
	 } 
	 
	function isSlotsBookedInAvailability(event){
		var eventObj={id:event.id,facilityId:event.facilityId}
		  $.ajax({ 
			   type: "POST",
			   contentType: 'application/json; charset=utf-8',
			   dataType: 'json',
			   data:JSON.stringify(eventObj),
			   url: "../server/facilities/isSlotsBookedInAvailability.php",
		   }).done(function(response) {
			response.isSlotsExists ? 
			 showNSPopup({message: "Slot/s are booked for this availability.Are you sure want to remove?",ButtonNames: ['Cancel','Ok'],callbackFunction: onConfirm}):deleteAvaillabilty(event);
			function onConfirm(Submit){
				(Submit==2) ? deleteAvaillabilty(event):"";			
			}
			function deleteAvaillabilty(){
				deleteSelectedAvailability(event);
			}
		   }).fail(function(jqXHR, textStatus, errorThrown) {
			   showNSPopup({message: "Unable to delete.",ButtonNames: ['OK']});
		   })
		   .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
   } 
	function deleteSelectedAvailability(event){
		 var eventObj={id:event.id,facilityId:event.facilityId}
	       $.ajax({ 
		        type: "POST",
	            contentType: 'application/json; charset=utf-8',
	            dataType: 'json',
	            data:JSON.stringify(eventObj),
		        url: "../server/facilities/deleteAvailabiltiy.php",
		    }).done(function(response) {
		    	$('#calendar').fullCalendar('removeEvents', event.id);
	    	    showNSPopup({message: "Availability deleted.",ButtonNames: ['OK']});
		    }).fail(function(jqXHR, textStatus, errorThrown) {
		        showNSPopup({message: "Unable to delete.",ButtonNames: ['OK']});
		    })
		    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
	} 	
  function getBookedSlots(facilityIdQry){
        $.ajax({
	        type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/facilities/getBookedSlots.php?"+facilityIdQry,
	    }).done(function(response) {
	    	response.forEach(function(obj,i){
	    		obj.allDay=false;
	    		obj.className="bookedSlot";
	    		obj.editable=false;
				obj.title="" ;
				$('#calendar').fullCalendar( 'removeEvents', obj.id);//remove event if already there in availabality calender
	    		$('#calendar').fullCalendar( 'renderEvent', obj,false);
	    	});
	       
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       showNSPopup({message: "Unable to delete.",ButtonNames: ['OK']});
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
  	function getBookedSlotInfo(cal_event){
		
	       $.ajax({  
		        type: "GET",
		        url: "../server/facilities/getBookedSlotInfo.php?bookedSlotId="+cal_event.id,
		    }).done(function(response) {
		    	var resArray=JSON.parse(response);
		    	$("#unAvailableSlotInfoContainer").addClass('hide');//hiding unavailable slot info
		    	$("#bookedSlotInfoContainer").attr('data-booked-id',cal_event.id);
	    	 	$("#bName").text(resArray[1].firstName+" "+resArray[1].lastName);
	    	 	$("#bFlat").text(resArray[1].flatNo);
	    	 	$("#bMobile").text(resArray[1].mobileNumber);
	    	 	$("#bDate").text(resArray[0].bookedDate);
	    	 	$("#bTime").text(resArray[0].bookedtime);
	    	 	$("#bAmount").text(resArray[0].amount);
	    	 	$("#bookedSlotInfoContainer").removeClass('hide');
		    }).fail(function(jqXHR, textStatus, errorThrown) {
		        showNSPopup({message: "Unable to fetch.",ButtonNames: ['OK']});
		    })
		    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
	}


	// unavailabilty related ajax and some methods starts here
	  function addUnAvailability(formdataObj){
  	   Object.getOwnPropertyNames(formdataObj).length==4?formdataObj.maxWeekId=5:"";
	   var facilityIdQry=window.location.href.split("?")[1];
	   formdataObj.facilityId=facilityIdQry.split("=")[1].split("#")[0];

        $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/facilities/addUnAvailability.php",
	        data: JSON.stringify(formdataObj)
	    }).done(function(response) {
	        
	       showNSPopup({message: "Unavailability added successfully.",ButtonNames: ['OK']});
	       $("#addUnAvailabity").modal("hide")
	        $("#calendar").fullCalendar( 'destroy');
           generateCalender();
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       showNSPopup({message: "Please try again.",ButtonNames: ['OK']});
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
    }

    function getUnAvailability(facilityIdQry,calendarStart,calendarEnd){
    	function dateFormat(date){
           var dateObj=new Date(date);
           var formattedDate=dateObj.getFullYear()+"-"+leadingZero((dateObj.getMonth()+1))+"-"+leadingZero(dateObj.getDate());
           return formattedDate;
    	}
    	function leadingZero(arg){
    	 var value=arg;	
          var temp=arg < 10? value="0"+arg: "";
          return value;
    	}
    	var startDate= dateFormat(calendarStart);
    	var endDate= dateFormat(calendarEnd);
        $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/facilities/getUnAvailabiltyList.php",
	        data:JSON.stringify({start:startDate,end:endDate,facilityId:facilityIdQry.split("=")[1].split("#")[0]}),
	    }).done(function(response) {
            var calendarStartDate=new Date(calendarStart);
            var calendarEndDate=new Date(calendarEnd);
	    	response.forEach(function(obj,i){
	    		obj.allDay=false;
	    		obj.title="";
	    		obj.color="black";
	    		obj.textColor="black";
	    		obj.backgroundColor="whitesmoke";
	    		obj.className="unAvailable";
	    		obj.editable=false;
	    		$('#calendar').fullCalendar( 'renderEvent', obj,false);
	    	});
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       showNSPopup({message: "Please try again.",ButtonNames: ['OK']});
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
     	function getUnAvailableSlotInfo(cal_event){
		
	       $.ajax({  
		        type: "GET",
		        url: "../server/facilities/getUnAvailableSlotInfo.php?unAvailableSlotId="+cal_event.id,
		    }).done(function(response) {
		    	var unavailableObj=JSON.parse(response);
		    	$("#unAvailableSlotInfoContainer").attr('data-unavailable-id',cal_event.id);
	    	 	$("#unAvailbleDate").text(unavailableObj.unAvailableDate);
	    	 	$("#unAvailbleTime").text(unavailableObj.unAvailabletime);
	    	 	$("#bookedSlotInfoContainer").addClass('hide');
	    	 	$("#unAvailableSlotInfoContainer").removeClass('hide');
		    }).fail(function(jqXHR, textStatus, errorThrown) {
		        showNSPopup({message: "Please try again.",ButtonNames: ['OK']});
		    })
		    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
	}

     function deleteUnAvailableSlot(unAvailableSlotId){
     	
	       $.ajax({ 
		        type: "POST",
		        url: "../server/facilities/deleteUnAvailableSlot.php?id="+unAvailableSlotId,
		    }).done(function(response) {
		       $('#calendar').fullCalendar( 'removeEvents', unAvailableSlotId);
		       $("#unAvailableSlotInfoContainer").addClass('hide');//hiding unavailable slot info
	    	   showNSPopup({message: "Slot deleted successfully.",ButtonNames: ['OK']});
		    }).fail(function(jqXHR, textStatus, errorThrown) {
		       showNSPopup({message: "Please try again.",ButtonNames: ['OK']});
		    })
		    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
	}
    //unavailability ends

  $("#searchByMobileNumBtn").unbind('click').click(function(e){
	searchByFlatNumber();
  });
  $("#appointmentBookBtn").unbind('click').click(function(e){
	  var userId=$(".ownerName").val();
	  var parentId;
	  if($(".searchedInfo").attr("data-id")==userId){
		parentId=$(".searchedInfo").attr("data-parentid");
	  }else{
		parentId=$(".searchedInfo").attr("data-id"); 
	  }	 
	  //when actual user try to access facilities
	  if(parentId==-2){
	  alert("Only lesse access facities");
	  return false;
	  }  	
	 else {
	  var url="../server/bookAppointmentCalender.php?userId="+userId+"&parentId="+parentId;
  	$("#addBookingFormModal").modal("hide");
  	window.localStorage.facilityId=window.location.href.split("?")[1].split("=")[1].split("#")[0];
	  window.open(url,"_self","menubar=no");	
	 }
  });
  function searchByFlatNumber(){
  	   $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/getOwnerDetailsByFlat.php",
	        data:JSON.stringify({fNumber:$("#flatNum").val()}),
	    }).done(function(response) {
	    	if(!$.isEmptyObject(response)){
	    	console.log(JSON.stringify(response));
	    	var ownerInfo=response.ownerInfo;
	    	var familyMemberList=response.membersList;
	    	var maintenanceInfo=response.maintenanceInfo;
			$(".searchedInfo").attr("data-id",ownerInfo.id);
			$(".searchedInfo").attr("data-parentid",ownerInfo.parentId);
	    	$(".ownerName").empty().append("<option value='"+ownerInfo.id+"'>"+ownerInfo.firstName+"</option>");
			familyMemberList.forEach(function(item,indx){
                $(".ownerName").append("<option value='"+item.id+"'>"+item.firstName+"</option>");
			});
			$(".mail").html(ownerInfo.emailId);
			$("#blockNo").html(ownerInfo.blockName);
			$("#flatNo").html(ownerInfo.flatNo); 
			$(".seachedContainer").removeClass('hide');
		   }else{
		   	alert("No such results found.");
		   }
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       showNSPopup({message: "Please try again.",ButtonNames: ['OK']});
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  	
  }
  
 function convertToUtc(dateTime){
 	var YYYYMMDDFormat=dateTime.getFullYear()+"-"+((dateTime.getMonth()+1)<10? "0"+(dateTime.getMonth()+1):(dateTime.getMonth()+1))+"-"+(dateTime.getDate()<10?"0"+dateTime.getDate():dateTime.getDate());
   return YYYYMMDDFormat+"T"+dateTime.toTimeString().split(" ")[0];
 }
