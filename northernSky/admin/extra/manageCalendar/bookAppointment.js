  function closeBookAppointmntPopup(){
  	$("#BookAppointmentModal").modal("hide");
  }
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	var currentDate = new Date();
	var cuurentDateString = JSON.stringify(currentDate); //alert(currentDate);
	var trimCurrentDate = cuurentDateString.substr(1, cuurentDateString.length - 16); 

	function displayCalender(eventsArray) {

		var today = new Date();
		var dayNumber = today.getDay();
		//alert("displayCalender");
		var calendarHeight = screen.availHeight - 120;
		$('#fullCalendar').fullCalendar({
			header: {
				left: 'prev',
				center: 'title',
				right: 'next'
			},
			defaultDate: $('#fullCalendar').fullCalendar('today'),
			defaultView: 'agendaWeek',
			firstDay: dayNumber,
			allDaySlot: false,
			defaultTimedEventDuration: "00:30:00",
			forceEventDuration: true,
			slotDuration: "00:30:01",
			displayEventEnd: true,
			disableDragging: true,
			editable: true,
			eventDurationEditable: false,
			dragScroll: false,
			height: calendarHeight,
			slotLabelFormat: "HH:mm",
			axisFormat: 'HH:mm',
			firstHour: 9,
			editable: false,
			events: eventsArray,
			eventRender: function(event, element) {
				element.find('.fc-content').empty();

			},
			eventOverlap: false,
			eventClick: function(calEvent, jsEvent, view) {

				var eventStartTime = JSON.stringify(calEvent.start);

				var eventStartTime_format = eventStartTime.substr(1, eventStartTime.length - 10);

				var date = new Date();
				//below code was allowing to bookappoinment at time like  11:40 if current time like is 12:05 
				//date.setMinutes(date.getMinutes()-60);

				var bookingdateArray=eventStartTime.split(/[^0-9]/);
				var check1_InMillSec=new Date(bookingdateArray[1],bookingdateArray[2]-1,bookingdateArray[3],bookingdateArray[4],bookingdateArray[5]);
				var check2_currentTimeInmillSec = date.getTime();
				if (calEvent.backgroundColor == "red" || calEvent.backgroundColor == "gray") {
					
				}

				if (check1_InMillSec < check2_currentTimeInmillSec) {
					showNSPopup({
						message: "Sorry! You cannot book for the past timings.",
						ButtonNames: ['Ok']
					});
					return false;
				} else {
	
					var selectedDateTime=$.fullCalendar.moment(calEvent.start).format('YYYY-MM-DD HH:mm:ss');
					var selectedTimePlusHalfanHour=$.fullCalendar.moment(calEvent.end).format('YYYY-MM-DD HH:mm:ss');
					var tempDateTimeObj= new Date(selectedDateTime);
					var tempVal=tempDateTimeObj.setHours(tempDateTimeObj.getHours()+1);
					var selectedTimePlusOneHour=$.fullCalendar.moment(tempVal).format('YYYY-MM-DD HH:mm:ss');
                    //extratcing only time parts to append to 'endTime' dropdown in modal
                      var selectedTime=$.fullCalendar.moment(calEvent.start).format('HH:mm');
                      var additionalHalfAnHour=$.fullCalendar.moment(calEvent.end).format('HH:mm');
                      var additionalOneHour=$.fullCalendar.moment(selectedTimePlusOneHour).format('HH:mm');
                    //
                    $("#facility_Name").attr("data-facilityid",2);
                    $("#bookingDate").val($.fullCalendar.moment(calEvent.start).format('YYYY-MM-DD'));
                    $("#bookingStartTime").attr("data-startdatetime",selectedDateTime);
                    $("#bookingStartTime").val(selectedTime);
                    $("#bookingEndTime").empty();
                    $("#bookingEndTime").append("<option value='"+selectedTimePlusHalfanHour +"'>"+additionalHalfAnHour+"</option>");
					$("#bookingEndTime").append("<option value='"+selectedTimePlusOneHour +"'>"+additionalOneHour+"</option>"); 
					$("#BookAppointmentModal").modal();	
				}
			},
			viewRender: function(currentView) {
				var minDate = moment(),
					maxDate = moment().add(2, 'weeks');

				// Past
				if (minDate >= currentView.start && minDate <= currentView.end) {
					getCalenderDataForCurrentDay();
					$(".fc-prev-button").prop('disabled', true);
					$(".fc-prev-button").addClass('fc-state-disabled');
				} else {
					$(".fc-prev-button").removeClass('fc-state-disabled');
					$(".fc-prev-button").prop('disabled', false);
				}
			}

		});



		if ($('#fullCalendar').children().length != 0) { // it will remove old events and will add new events
			//alert();
			$('#fullCalendar').fullCalendar('removeEvents');
			$('#fullCalendar').fullCalendar('addEventSource', eventsArray);
			323

			//$('#fullCalendar').fullCalendar('renderEvent');
		}


		//$('#output').trigger('create');
	}

	// To get next weak dates with session availabilities 
	$(document).on('click', ".fc-next-button", function(e) {
		getCalenderData();
	});

	$(document).on('click', ".fc-prev-button", function(e) {
		getCalenderData();
	});

	function addCalanderEvent(response) {
		$('#fullCalendar').fullCalendar('addEventSource', response);
	}

	function getCalenderData() {
		
		var beginingOfTheWeek = $('#fullCalendar').fullCalendar('getView').start;
		var cuurentDateString = JSON.stringify(beginingOfTheWeek); //alert(currentDate);
		var trimCurrentDate = cuurentDateString.substr(1, cuurentDateString.length - 16);

	  //call ajax for availability
	}

	function getCalenderDataForCurrentDay() {
		var currentDate = new Date();
		var cuurentDateString = JSON.stringify(currentDate); //alert(currentDate);
		var trimCurrentDate = cuurentDateString.substr(1, cuurentDateString.length - 16);


	}
	$(function(){
		getBookedSlots(2);
		$(document).on("click","#bookAppointmentConfirm",function(){
			var bookedDate=$("#bookingDate").val();
			var weekId=new Date(bookedDate).getDay();
			var appointmentObj={
						facilityId:$("#facility_Name").attr("data-facilityid"),
						bookedDate:bookedDate,
						weekId:weekId,
						start:$("#bookingStartTime").attr("data-startdatetime"),
						end:$("#bookingEndTime").val(),
					}
          BookAppointment(appointmentObj);
		});
	});
	function BookAppointment(appointmentObj){
	       $.ajax({
		        type: "POST",
	            contentType: 'application/json; charset=utf-8',
	            dataType: 'json',
	            data:JSON.stringify(appointmentObj),
		        url: "../../server/facilities/addBookedSlot.php",
		    }).done(function(response) {
	    	
		    }).fail(function(jqXHR, textStatus, errorThrown) {
		       
		        alert("Unable to proccess");
		    })
		    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
	}



	

