    function getAvailability(facilityId){
        $.ajax({
	        type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../../server/facilities/getAvailabiltyList.php?facilityId="+facilityId,
	    }).done(function(response) {
	        var arrayList=[];
	        response.forEach(function(obj,i){
              arrayList=getEventSlotsGeneratorFn(obj.start,obj.end,"available","#3a87ad");
	       });
	        displayCalender(arrayList);
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       
	        alert("Unable to proccess");
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
  function getBookedSlots(facilityId){
        $.ajax({
	        type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../../server/facilities/getBookedSlots.php?facilityId="+facilityId,
	    }).done(function(response) {
	    	/*response.forEach(function(obj,i){
	    		obj.allDay=false;
	    		obj.className="bookedSlot";
	    		obj.editable=false;
	    		 $('#calendar').fullCalendar( 'renderEvent', obj,true);
	    	});
	       $(".fc-content").empty();*/
	       response.forEach(function(obj,i){
             getEventSlotsGeneratorFn(obj.start,obj.end,"booked","#d23434");
	       });
	       getAvailability(facilityId)

	  
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       
	        alert("Unable to proccess");
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
var getEventSlotsGeneratorFn=new createSlotsTemplate().getEventSlotsGenerator();
function createSlotsTemplate(){
    var arr=[];
    var hash = Object.create(null);
   this.getEventSlotsGenerator= function getEventSlotsGenerator(){

   					return	function eventSlotsArr(startTime,endTime,title,color){
					   	var end=new Date(startTime);
					    do{
					    	//
					    	var temp=new EventObj(end,title,color);
					    	 if (!hash[temp['start']]) {//this if condition is to avoid duplicate object
							        hash[temp['start']] = true;
							        arr.push(temp);
							    }
					    	//
					       // arr.push(new EventObj(end,title));
					      //end.setHours(end.getHours()+1);
					      end.setMinutes(end.getMinutes()+30);
					    }
					    while(end < new Date(endTime));
					    return arr;
					 };
					}
  function EventObj(endTime,title,color){
	   this.start=new Date(endTime);
	   this.title=title;
	   this.backgroundColor=color;
	}					 
}

