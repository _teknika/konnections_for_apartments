<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
              <meta name="theme-color" content="#3c8dbc" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="msapplication-tap-highlight" content="no" />
        <!-- WARNING: for iOS 7, remove the width=device-width and height=device-height attributes. See https://issues.apache.org/jira/browse/CB-4323 -->
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
        <link href="theme/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="../aLTE/bootstrap/css/bootstrap.min.css">
  <!-- jQuery 2.2.3 -->
  <script src="../aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="../aLTE/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/url.js"></script>
<link href="spinner/app.css" rel="stylesheet">
<script src="spinner/app.js"></script>
<script src="spinner/spinner.js"></script>
<title>maintainance</title>
        <style type="text/css">
            html,body{
                height:100%;
                background: rgba(239, 233, 239, 0.25);
            }
            .upper-section{
                min-height: 20px;
                margin-top: 20px;
               
                overflow: auto;
            }
            .body-section{
                background: white;
                border-top: 2px solid darkcyan;
            }
            span#balance::before,.colon-insert::before{
              content: ":";
              font-weight: 700;
              padding: 0px 4px 0px 4px;
            }
            #balance{
                font-weight: 700;
            }
            .mth-selected{
                background-color:lightgrey !important;
            }
         .insert-colon::after{
                padding: 0px 4px 0px 20px;
            }
            .table-responsive{
                border:0px;
            }
            .desc-detail-container {
               
            }
        </style>
    <?php
        require_once('../../server/libs/dbConnection.php');
        $connection = new dbconnection();
        $con = $connection->connectToDatabase();
        $userId=$_GET['userId'];
        $parentId=$_GET['parentId'];
        $parentId == -1 ? $parentId=$userId:"";
        $sql = mysqli_query($con, "SELECT * FROM person where id='$parentId'");
        $rows_count = mysqli_num_rows($sql);
        $userDataArry = array();
        if ($rows_count != 0) {
            while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                        $info = $rows_fetch;  
                    array_push($userDataArry, $info);
            }
        }
        $faltsArray=array();
        $personFlatsQuery = "SELECT flat_number FROM person_flats WHERE  personId='$parentId' ";
    
          $sql_res = mysqli_query($con, $personFlatsQuery);

          $rows_cont = mysqli_num_rows($sql_res);

              if($rows_cont!=0){
                  while($rows_fetch = mysqli_fetch_assoc($sql_res)){
                     array_push($faltsArray, $rows_fetch['flat_number']);
                  }
        }
         array_push($userDataArry, $faltsArray);
        echo "<script>var familyHeadObj = " . json_encode($userDataArry[0]) . ';'.
        "familyHeadObj.flatsArray = " . json_encode($userDataArry[1]) . ';</script>';
        $connection->closeConnection();
     ?>
     
</head>
    <body class="container" style=" padding: 10px;">
        <div class="section head-section">
            <p class="bg-info text-center">My account summary- A/c bal<span id="balance" class="amt">Loading..</span><span>rs</span></p>
            <div class="search-container" style="overflow:auto">
              <div class="form-group col-xs-5">
                <label for="flat">Flat:</label>
                <select type="text" class="form-control" name="flat" id="flat">
                
                </select>
             </div> 
             <div class="form-group col-xs-offset-2 col-xs-5">
                <label for="month">Month:</label>
                <ul class="pager" style="margin:0px">
                    <li ><a href="#" id="previousMonth" >Previous</a></li>
                    <li ><a href="#" id="currentMonth" class="mth-selected">Current</a></li>
                </ul>
             </div>
            </div>
        </div>
        <div class="section body-section">
           <table class="table table-responsive table-hover maintenanceHistory">
            <tbody>
            <tr>
                <td>Opening balance</td>
                
                <td class=" colon-insert openingBal amt">0.00 rs</td>
            </tr>
            <tr>
                <td>Total credit</td>
                
                <td class="credit colon-insert amt">0.00 rs</td>
            </tr>
            <tr>
                <td>Maitenance deduction</td>
                
                <td class="maintenanceUsage colon-insert amt">0.00 rs</td>
            </tr>
            <tr>
                <td>Electricity deduction</td>
                
                <td class="electricityUsage colon-insert amt">0.00 rs</td>
            </tr>
            <tr>
                <td>Facility usage</td>
                
                <td class="facilityUsage colon-insert amt">0.00 rs</td>
            </tr>
            <tr>
                <td>Closing balance</td>
               
                <td class="colon-insert closingBalance amt">0.00 rs</td>
            </tr>
            
            </tbody>
        </table>
        </div>
        <div class="section footer-section" style="margin-top: 10px;">
            <!-- <table id="maintainanceTable" class="display" width="100%"></table> -->
        <div><a href="#" style="float:right;color:green;text-decoration:underline" onclick="payNow()">PayNow</a></div>
        </div>
        <!-- Loader -->
        <div class="fader">
            <div class="loader"></div>
        </div>

        <script>
         $(function(){
            intializeFlatDropDown();
            initializeOnFlatChangeListener();
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
            var dateObj=new Date;
            var currentMonth=dateObj.getMonth()+1;//javascript month range 0-11
            //setting month name
            $("#previousMonth").attr("data-month",currentMonth-1);
            $("#previousMonth").text(monthNames[currentMonth-2]);
            $("#currentMonth").attr("data-month",currentMonth);
            $("#currentMonth").text(monthNames[currentMonth-1]);
            //ends
           // $("#month >option[value="+currentMonth+"]").attr("selected","selected");
            getMaintainanceHistory($("#flat").val(),currentMonth);
         });
         function initializeOnFlatChangeListener(){
             $("#flat").change(function(e){
                 var month=$(".mth-selected").attr("data-month");
                getMaintainanceHistory($("#flat").val(),month);
             });
             $("#previousMonth,#currentMonth").click(function(e){
                  e.preventDefault();
                  $("#previousMonth,#currentMonth").toggleClass("mth-selected");
                  var mnth=$(this).attr("data-month");
                  getMaintainanceHistory($("#flat").val(),mnth);
             });
         }
            //below function initializes the data tables
function getMaintainanceHistory(flatNum,monthNum){
    showLoader();
    var obj={ownerId:familyHeadObj.id,flat:flatNum,month:monthNum};
    $.ajax({
       url:"../../server/getMaintenanceHistory.php" ,
        type:"POST",
        contentType:"application/json;charset=utf-8",
        dataType:"JSON",
        data:JSON.stringify(obj),
        success:function(res){ 
            console.log("res.."+res);
            if($.isEmptyObject(res)){
             $(".amt").html("0.00 rs");
              hideLoader();
             return false;
            }
           createMaintainanceHistoryTable(res);
        },
        error:function(event,xhr,options,exc){
            hideLoader();
        }
    });

}
function createMaintainanceHistoryTable(objForTable){
    //var rowArray=Array.prototype.slice.call($(".maintenanceHistory tr"));
    //below are the keys should be there in response of getMaintenanceHistory.php api
    var keyNamesObj={
        "currentBal":0.00,
        "credit":0.00,
        "facilityUsage":0.00,
        "maintenanceUsage":0.00,
        "electricityUsage":0.00,
        "openingBal":0.00,
        "closingBalance":0.00
    };
    var objForTableMerged = $.extend({}, keyNamesObj, objForTable);
    $("#balance").html(objForTableMerged.currentBal);
    var keyArr=Object.keys(objForTableMerged);
    keyArr.forEach(function(key,indx){
        $(".maintenanceHistory tr > td."+key).html((objForTableMerged[key] || 0 )+" rs");
    }); 

    hideLoader();
}

  function payNow(){
    window.location.href="../../server/payManintenance/payMaintenanceForm.php?userId="+getId().userId+"&parentId="+getId().parentId+"&flat="+$("#flat").val();
  }
  function getId(){
      var queryParam1=location.href.split("?")[1].split("&")[0].split("=");
      var queryParam2=location.href.split("?")[1].split("&")[1].split("=");
      return {
          userId:queryParam1[1],
          parentId:queryParam2[1]
      };
  }
  function intializeFlatDropDown(){
        $("#flat").append('<option value='+familyHeadObj.flatNo+'>'+familyHeadObj.flatNo+'</option>');
		familyHeadObj.flatsArray.forEach(function(ele,i){
            $("#flat").append('<option value='+ele+'>'+ele+'</option>');
		});
 }

</script>

    </body>
</html>



