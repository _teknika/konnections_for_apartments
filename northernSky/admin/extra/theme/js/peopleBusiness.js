$(function(){
  $(document).on("click","#moreInfo",function(event) {
   	event.preventDefault();
   	$("#moreInfoModal #name").html($(this).closest('.business-box').find('.media-heading.name').text());
   	$("#moreInfoModal #dealsIn").html($(this).closest('.business-box').find('.dealsIn-description').text());
   	$("#moreInfoModal").modal();
   });
   getPeopleBusinessesArr('getPeopleBusiness.php');
   $(document).on("click",".onSearch",function(e){
      e.preventDefault();
      getPeopleBusinessesArr('getPeopleBusinessOnSearch.php?key='+$(".serachInput").val().trim().split(" ").join(","));
   });
   $(document).on("click",".addBtn",function(e){
      e.preventDefault();
      $("#addBusinessModal").modal();
       addBusiness();
   });
   
});
  function getPeopleBusinessesArr(fileNameInServer){
       $.ajax({
		   	url: '/server/'+fileNameInServer,
		   	type: 'GET',
		   })
		   .done(function(resp) {
		   	console.log("success");
		   	$(".business-container").empty();
		   	appendbusinessBoxes(resp);
		   })
		   .fail(function() {
		   	console.log("error");
		   })
		   .always(function() {
		   	console.log("complete");
		   });
       }
function appendbusinessBoxes(resp){
	var businessesArr=JSON.parse(resp);
	businessesArr.forEach(function(e,i,fullArr){
	var str='<div class="media business-box">'+
		  '<div class="media-left media-middle">'+
		    '<img src="http://35.165.225.196/Test/server/uploads/Events/home1.jpg" class="media-object" style="width:60px;height: 60px">'+
		  '</div>'+
		  '<div class="media-body">'+
		    '<h5 class="media-heading name" style="display:inline">'+e.name+'</h5><span class="mobile-icon-cntainer"><span class="glyphicon glyphicon-globe"></span><i class="fa fa-mobile" aria-hidden="true"></i></span>'+
		    '<div class="dealsIn-description">'+
             e.dealsIn+
          '</div>'+
         '<a  id="moreInfo">more info</a>'+
		  '</div>'+
    '</div>';
    $(".business-container").append(str);
	});
	if(businessesArr.length < 1){
       var str='<div><p>No such result..<p></div>';
        $(".business-container").append(str);
	}
}
function addBusiness(){
	  var form = document.forms.namedItem("businessForm");
	  
       form.addEventListener('submit', function(ev) {
                          var formObj=new FormData(form);
                          var oReq = new XMLHttpRequest();
                          var submitUrl="/server/addPeopleBusiness.php";
                          oReq.open("POST", submitUrl, true);
                          oReq.onload = function(oEvent) {
                             if (JSON.parse(oReq.response).isSuccessful == true) {
                                alert("Book added successfully");
                                location.reload();
                            } else {
                              alert("Please try again");
                            }

                          };

                          oReq.send(formObj);
                          ev.preventDefault();
                        }, false);

}
