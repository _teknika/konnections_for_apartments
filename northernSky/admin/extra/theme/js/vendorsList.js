$(function(){
  $(document).on("click","#moreInfo",function(event) {
   	event.preventDefault();
   	$("#moreInfoModal #name").html($(this).closest('.business-box').find('.media-heading.name').text());
   	$("#moreInfoModal #dealsIn").html($(this).closest('.business-box').find('.dealsIn-description').text());
    $("#moreInfoModal .websitelink").html("Website : "+$(this).closest('.business-box').find('.websiteLink').attr("href"));
   // $("#moreInfoModal .mob").html("Phone : "+$(this).closest('.business-box').find('.phone').attr("data-phone"));
    $("#moreInfoModal .mob").html(getPhoneContacts.call(this));
    $("#moreInfoModal .address").html("Address : "+JSON.parse($(this).closest(".media-body").attr("data-info")).address);
   	$("#moreInfoModal").modal();
   });
   
   $(document).on("click",".addBtn",function(e){
      e.preventDefault();
      $("#addBusinessModal").modal();
       addBusiness();
   });
   $(document).on("change","#category",function(e){
    e.preventDefault();
    getVendorsByCategory();
   });
   getVendorCategoryList();
   getVendorsByCategory();
   enableVendorSearch();
});
 function getVendorsByCategory(){
  getPeopleBusinessesArr('getVendors.php');
 }
  function getPeopleBusinessesArr(fileNameInServer){
           $.ajax({
    		   	url: '../../server/'+fileNameInServer+'?category='+$("#category").val(),
    		   	type: 'GET',
    		   })
    		   .done(function(resp) {
    		   	console.log("success");
    		   	$(".business-container").empty();
    		   	appendbusinessBoxes(resp);
    		   })
    		   .fail(function() {
    		   	console.log("error");
    		   })
    		   .always(function() {
    		   	console.log("complete");
    		   });
       }
function appendbusinessBoxes(resp){
	var businessesArr=JSON.parse(resp);
	businessesArr.forEach(function(e,i,fullArr){
	var str='<div class="media business-box">'+
		  '<div class="media-left media-middle">'+
		    '<img src="../img/northern logo.bmp" class="media-object vendorImg" style="width:60px;height: 60px">'+
		  '</div>'+
		  "<div class='media-body' data-info='"+JSON.stringify(e)+"'>"+
		    '<h5 class="media-heading name" style="display:inline">'+e.name+'</h5><span class="mobile-icon-cntainer"><a class="websiteLink" href="'+e.website_link+'"><span class="glyphicon glyphicon-globe"></span></a><i class="fa fa-mobile" aria-hidden="true"><span class="phone" data-phone="'+e.person_contact+'"></span></i></span>'+
		    '<div class="dealsIn-description">'+
             e.dealsIn+
          '</div>'+
         '<a  id="moreInfo">more info</a>'+
		  '</div>'+
    '</div>';
    $(".business-container").append(str);
	});
	if(businessesArr.length < 1){
       var str='<div><p>No such result..<p></div>';
        $(".business-container").append(str);
	}
}
function addBusiness(){
	  var form = document.forms.namedItem("businessForm");
	  
       form.addEventListener('submit', function(ev) {
                          var formObj=new FormData(form);
                          var oReq = new XMLHttpRequest();
                          var submitUrl="/server/addPeopleBusiness.php";
                          oReq.open("POST", submitUrl, true);
                          oReq.onload = function(oEvent) {
                             if (JSON.parse(oReq.response).isSuccessful == true) {
                                alert("Book added successfully");
                                location.reload();
                            } else {
                              alert("Please try again");
                            }

                          };

                          oReq.send(formObj);
                          ev.preventDefault();
                        }, false);

}
function getPhoneContacts(){
 var selectedVendorInfo=JSON.parse($(this).closest(".media-body").attr("data-info"));
 var contact='<a href="tel:'+selectedVendorInfo.person_contact+'">'+selectedVendorInfo.person_contact+'</a>';
 if(selectedVendorInfo.alt_number !=null ){
	
    selectedVendorInfo.alt_number.length > 5 ? contact = contact+"/"+'<a href="tel:'+selectedVendorInfo.alt_number+'">'+selectedVendorInfo.alt_number+'</a>': "";
 }
 return contact;
}
function getVendorCategoryList(){
  showLoader(); 
  $.ajax({
    url: '../../server/getVendorsCategory.php',
    type: 'GET',
    asych:false,
  })
  .done(function(response) {
    vendorCategoryList=JSON.parse(response);
    vendorCategoryList.forEach(function(categoryObj,i){
      var id=categoryObj['id'];
      var category=categoryObj['category'];
       $Ele="<option value='"+id+"'>"+category+"</option>";  
    $("#category").append($Ele);
   });
    hideLoader();
  })
  .fail(function() {
    hideLoader(); 
    alert("could'nt able to fetch vendors category.");
  })
  .always(function() {
    console.log("complete");
  });
}

function enableVendorSearch(){
  /** 
   $(document).on("click",".onSearch",function(e){
     e.preventDefault();
     if($(".serachInput").val().trim().length < 1 ){location.reload()}
     getPeopleBusinessesArr('getVendorsOnSearch.php?key='+$(".serachInput").val().trim().split(" ").join(","));
  });
  */
  $(document).on("keyup","#searchVendor",function() {
   var value = $(this).val().toLowerCase();
   $(".media.business-box").filter(function() {
     $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
   });
 });
 }
