<!DOCTYPE html>
<html lang="en">

<head>
  <title>Sevice Form</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="../aLTE/bootstrap/css/bootstrap.min.css" />
  <link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
  <script type="text/javascript" src="../js/jquery-1.11.2.min.js"></script>
  <script type="text/javascript" src="../aLTE/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="../js/url.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
  <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  <?php
        require_once('../../server/libs/dbConnection.php');
        $connection = new dbconnection();
        $con = $connection->connectToDatabase();
        $userId=$_GET['userId'];
        $parentId=$_GET['parentId'];
        $parentId == -1 ? $parentId=$userId:"";
        $sql = mysqli_query($con, "SELECT * FROM person where id='$parentId'");
        $rows_count = mysqli_num_rows($sql);
        $userDataArry = array();
        if ($rows_count != 0) {
            while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                        $info = $rows_fetch;  
                    array_push($userDataArry, $info);
            }
        }
        $faltsArray=array();
        $personFlatsQuery = "SELECT flat_number FROM person_flats WHERE  personId='$parentId' ";
    
          $sql_res = mysqli_query($con, $personFlatsQuery);

          $rows_cont = mysqli_num_rows($sql_res);

              if($rows_cont!=0){
                  while($rows_fetch = mysqli_fetch_assoc($sql_res)){
                     array_push($faltsArray, $rows_fetch['flat_number']);
                  }
        }
         array_push($userDataArry, $faltsArray);
        echo "<script>var familyHeadObj = " . json_encode($userDataArry[0]) . ';'.
        "familyHeadObj.flatsArray = " . json_encode($userDataArry[1]) . ';</script>';
        $connection->closeConnection();
     ?>


</head>

<body>

  <div class="container-fluid" style="margin-top: 30px;">
    <form action="#" method="post" name="services_form">
      <div class="form-group col-xs-6">
          <label for="flat" class="label-custom-css">Flat</label>
          <select class="form-control inputHeight custm-padding" id="flat" name="flat" required>
                                             
          </select> 
      </div>
      <div class="form-group col-xs-6">
        <label for="sel1">Service:</label>
        <select name="serviceName" class="form-control" id="service-drop-down">
          <option value="">Select</option>
         
        </select>
      </div>


      <div class="form-group col-xs-12">
        <!-- Message field -->
        <label class="control-label " for="message">Description</label>
        <textarea name="message" id="message" class="form-control" cols="20" rows="5"></textarea>
      </div>
      
       <div class="form-group col-xs-12">
        <!-- Message field -->
        <label class="control-label " for="availableDate">Date:</label>
        <input type="date" name="availableDate" id="availableDate" class="form-control " />
      </div>
      <div class="form-group col-xs-6">
        <!-- Message field -->
        <label class="control-label " for="fromTime">From:</label>
        <input  id="fromTime" name="fromTime" class="timepicker form-control " />
      </div>
      <div class="form-group col-xs-6">
         <label class="control-label " for="toTime">To:</label>
         <input  type="text" id="toTime" name="toTime" class="timepicker form-control" />
         <span class="error-msg ">Time should be greater than start time</span>
      </div>

      <div class="form-group">
        <button class="btn btn-primary " style="display:block;margin:auto;" name="submit" type="submit">Submit</button>
      </div>
    </form>
    </div>
    <script type="text/javascript">
      $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        initValidation();
        intializeFlatDropDown();
        getServicesList();
        initiateTimePicker();
        endTimeValidator();
      });
     function endTimeValidator(){
       $(document).on("click","li.ui-menu-item",function(){
        var isvalid=$.validator.timeValidation($("#fromTime").val(),$("#toTime").val());
        if(isvalid){
          $("#toTime+span.error-msg").addClass("hide");
        }
        else{
          $("#toTime+span.error-msg").removeClass("hide");
          return false;
        }
      })
     }
      function initValidation() {
        $("form[name='services_form']").validate({
          // Specify validation rules
          rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            serviceName: "required",
            message: "required",
            toTime:{
             // greaterThan:true
            }
            
          },
          // Specify validation error messages
          messages: {
            serviceName: "Please select your service",
            message: "Please enter description",

          },
          // Make sure the form is submitted to the destination defined
          // in the "action" attribute of the form when valid
          submitHandler: function (form) {
            var formObj={},serviceInfo={},reqDetail={},avaialbleDetail=[];           
            $(form).serializeArray().forEach(function(ele,i){
                 formObj[ele.name]=ele.value;
            });
            reqDetail.userId=familyHeadObj.id;
            reqDetail.name=familyHeadObj.firstName;
            reqDetail.flatNo=formObj.flat;
            reqDetail['serviceName']=formObj.serviceName;
            reqDetail['description']=formObj.message;
            reqDetail['avaialbleDetail']=new AvaialableTimeFormat(formObj.availableDate,formObj.fromTime,formObj.toTime);
            
            console.log(reqDetail);
            addRequest(reqDetail);
          }
        });
      }
    function AvaialableTimeFormat(availableDate,fromTime,toTime){
       this.date=availableDate;
       this.from=fromTime;
       this.to=toTime;
    }
    $.validator.timeValidation=function(start,end) { // method for validating DOB[minAge attribute
         function isAMPM(time){
             var timeInStr=time.split(" ")[0];
             var hour_min=timeInStr.split(":");
             var dateObj= new Date();
             time.search("PM") > 0 ?  dateObj.setHours(Number(hour_min[0])+12):dateObj.setHours(Number(hour_min[0])); 
             return dateObj;                                              
         }
         return isAMPM(start) < isAMPM(end);
     }; // ...............................Validating DOB method ends
       
       function initiateTimePicker(){
         $('.timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 60,
            minTime: '6:00',
            maxTime: '9:00pm',
            defaultTime: '6',
            startTime: '6:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
       }
      function intializeFlatDropDown(){
        $("#flat").append('<option value='+familyHeadObj.flatNo+'>'+familyHeadObj.flatNo+'</option>');
		 	  familyHeadObj.flatsArray.forEach(function(ele,i){
               $("#flat").append('<option value='+ele+'>'+ele+'</option>');
		 	  });
      }
      function getServicesList() {
        $.ajax({
            type: "GET",
            url: "../../server/getServices.php",
          }).done(function (response) {
            JSON.parse(response).forEach(function(ele,i){
               var newElement="<option value="+ele.id+">"+ele.service_name+"</option>";
                $("#service-drop-down").append(newElement);
            });
          }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("Server failed");
          })
          .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});
      }
      function addRequest(reqDetail) {
        var isvalid=$.validator.timeValidation($("#fromTime").val(),$("#toTime").val());
        if(isvalid){
          $("#toTime+span.error-msg").addClass("hide");
        }
        else{
          $("#toTime+span.error-msg").removeClass("hide");
          return false;
        }
        $.ajax({
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: "../../server/addRequest.php",
            data:JSON.stringify(reqDetail),
          }).done(function (response) {
            if(response.isSuccessful){
              $("form[name='services_form']")[0].reset();
              // var url="http://localhost/northernSky/admin/extra/services.php?userId=2840&parentId=-1";
              // var customWindow = window.open(url, '_self', '');
              // window.close();
             
            }
          }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("Server failed");
          })
          .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});
      }
    </script>

  

</body>

</html>