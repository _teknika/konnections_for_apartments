  
   function updateAvailability(updateObj,revertFunc){
    $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/facilities/updateAvailability.php",
	        data: JSON.stringify(updateObj)
	    }).done(function(response) {
	       
	        alert("success");
	        
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       
	        alert("Unable to proccess");
	        revertFunc();
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 

  }
  function addAvailability(availabilityObj){
  	var eventBoxObj=this;
        $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "../server/facilities/addAvailability.php",
	        data: JSON.stringify(availabilityObj)
	    }).done(function(response) {
	        
	        alert("success");
	        $('#calendar').fullCalendar('updateEvent', JSON.parse(response));
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	       
	        alert("Unable to proccess");
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
