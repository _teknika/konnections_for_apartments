<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="theme/css/animate.min.css" rel="stylesheet">
<link href="theme/css/custom.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
       
        
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>
         
         <script src="js/jquery.validate.js"></script>
         <script src="js/utility/basicService.js"></script>
 <style type="text/css">
   .addBasicServiceContainer{
    background: white;
    padding: 10px;
    border: 1px solid lightgrey;
    border-radius: 5px;
    box-shadow: 3px 2px 7px 1px rgba(181, 175, 175, 0.38);
   }
   
 </style>      
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Basic Services</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="addBasicServiceContainer">
         <form id="addBasicServiceForm" enctype="multipart/form-data" method="Post">
          <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                  <label class="col-md-12" for="blockName">Block Name</label>
                  <div class="col-md-12">
                  <select id="blockName" name="blockName" class="form-control blockName_dropdown"></select>
                  </div>
                 </div>
            </div>
            <div class="col-xs-4">
               <div class="form-group">
                <label for="fname">Flat No</label>
                <input type="number" class="form-control" id="fname" name="fname">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
               <div class="form-group">
                  <label class="col-md-12" for="fromdate">From</label>
                    <div class="col-md-12">
                      <input type="date" id="fromdate" name="fromdate"  class="form-control"/>
                    </div>
                </div>
            </div> 
            <div class="col-md-4">
               <div class="form-group">
                  <label class="col-md-12" for="todate">To</label>
                    <div class="col-md-12">
                      <input type="date" id="todate" name="todate"  class="form-control"/>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
               <div class="form-group">
                  <label class="col-md-12" for="amount">Amount</label>
                    <div class="col-md-12">
                      <input type="number" id="amount" name="amount"  class="form-control"/>
                    </div>
                </div>
            </div>                                    
          </div>
          <div class="row" style="padding-top: 10px;padding-right: 15px;">
            <button type="submit" class="btn btn-success pull-right">Sumbmit</button>
          </div>
          </form>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>
<style type="text/css">
  input,select{
    border-radius: 5px;
    background: transparent;
    background-image: none;
    box-shadow: inset 0px 0px 1px 0px rgba(177, 171, 177, 0.89);
   }
</style>
</body>
</html>
