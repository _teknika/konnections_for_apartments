<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
 <link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
        <link href="theme/css/custom.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>


    <!-- Date Range Picker -->
      <script type="text/javascript" src="theme/js/moment.min2.js"></script>
      <script type="text/javascript" src="theme/js/datepicker/daterangepicker.js"></script>
      <!-- Form Wizard -->
      <script type="text/javascript" src="theme/js/wizard/jquery.smartWizard.js"></script>
      <!-- Jquery Validate -->
    
      <script src="js/utility/Lessee/registerLessee.js"></script>  
	  
      <style type="text/css">
         #registerBasicForm .error{
            color:red;
         }
         .x_panel{
          outline: 5px groove rgba(193, 184, 184, 0.45);
         }
      </style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h2>Add Member</h2>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
                    <div class="right_col" role="main">
               <br />
               <div class="">
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                           
                           <div class="">
                                 <div id="step-1">
                                    <!-- <h2 class="StepTitle">Basic Information</h2> -->
                                    <form id="registerBasicForm" name="registerBasicForm" method="post" action="" class="form-horizontal">
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="col-md-12" for="firstname">First Name <span class="required">*</span>
                                                </label>
                                                <div class="col-md-12">
                                                   <input type="text" id="firstname" required="required" name="firstname"  class="form-control" />
                                                </div>
                                             </div>
                                          </div>
                                           <div class="col-md-6">
                                             <div class="form-group">
                                                <label class="col-md-12">Last Name<span>*</span>
                                                </label>
                                                <div class="col-md-12">
                                                   <input type="text" id="lastname" name="lastname" class="form-control" required/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                         
                                          <div class="col-md-3">
                                             <div class="form-group">
                                                <label class="col-md-12">Mobile  Number<span>*</span>
                                                </label>
                                                <div class="col-md-12">
                                                   <input type="number" minlength="10" maxlength="20" id="mobilenumber" name="mobilenumber" class="form-control" title="Please enter more than 9 and less than 20  digits" required/>
                                                </div>
                                             </div>
                                          </div>
                                           <div class="col-md-3">
                                             <div class="form-group">
                                                <label class="col-md-12">alternative Number
                                                </label>
                                                <div class="col-md-12">
                                                   <input type="number" minlength="10" maxlength="20" id="altnumber" name="altnumber" class="form-control" />
                                                </div>
                                             </div>
                                          </div>
                                           <div class="col-md-3">
                                              <div class="form-group">
                                                <label class="col-md-12">Email Id<span>*</span>
                                                </label>
                                                <div class="col-md-12">
                                                   <input type="email"   id="emailId" name="emailId" class="form-control" required/>
                                                </div>
                                             </div>
                                          </div>
                                           <div class="col-md-3">
                                              <div class="form-group">
                                                <label class="col-md-12">alternative Email
                                                </label>
                                                <div class="col-md-12">
                                                   <input type="email"   id="altmailId" name="altmailId" class="form-control"/>
                                                </div>
                                             </div>
                                          </div>
                                         </div>
                                       <div class="row">
                                         
                                          <div class="col-md-4" hidden>
                                             <div class="form-group">
                                                <label class="col-md-12">Block Name<span>*</span>
                                                </label>
                                                <div class="col-md-12">
                                                   <select id="blockName" name="blockName" class="form-control blockName_dropdown" title="This field is required." required >
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                           <div class="col-md-4" hidden>
                                              <div class="form-group">
                                                <label class="col-md-12">Flat Number<span>*</span>
                                                </label>
                                                <div class="col-md-12">
                                                   <input type="text"   id="data" name="data"  class="form-control"  required/>
                                                </div>
                                             </div>
                                          </div>
                                            <div class="col-md-4" hidden>
                                              <div class="form-group">
                                                <label class="col-md-12" >Flat holder<span>*</span>
                                                </label>
                                                <div class="col-md-12">
                                                   <select name="flatHolder" id="flatHolder" class="form-control">
                                                     <option value="rental" selected>rental</option>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                         </div> 
                                      <div class="row">
                                          <div class="col-md-4">
                                             <div class="form-group">
                                                <label class="col-md-12">Gender<span>*</span>
                                                </label>
                                                <div class="col-md-12">
                                                   <select id="gender" name="gender" class="form-control" required>
                                                      <option value="">-- Select the gender --</option>
                                                      <option value="1">Male</option>
                                                      <option value="0">Female</option>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-4">
                                             <div class="form-group">
                                                <label class="col-md-12">Date of Birth
                                                </label>
                                                <div class="col-md-12">
                                                   <input type="text" id="dob" name="dob" class="form-control"/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-4">
                                             <div class="form-group">
                                                <label class="col-md-12">Date of Marriage
                                                </label>
                                                <div class="col-md-12">
                                                   <input type="text" id="dateofmarriage" name="dateofmarriage" class="form-control"/>
                                                </div>
                                             </div>
                                          </div>
                                      </div>
                                    <div class="row">
                                      <button type="submit" class="btn btn-success pull-right" >Submit</button>
                                      <button type="reset" class="btn btn-default pull-right">Reset</button>
                                    </div>
                                    </form>
                                 </div>
                       
                              
                               
                          
                                 <!--  <div>
                                    <input type="submit" class="btn btn-success" name="submit" id="submit" value="Submit">
                                    <a class="reset_pass" href="#">Lost your password?</a>
                                    </div> -->
                           
                              <!-- </form> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
              
            </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>
</body>
</html>
