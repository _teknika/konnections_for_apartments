<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- data tables styles -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
 <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script> -->
  
  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
 <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> 
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- data table -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- Morris.js charts -->

     <script type="text/javascript" src="theme/js/moment.min2.js"></script>
      <script type="text/javascript" src="theme/js/datepicker/daterangepicker.js"></script>
      <!-- Form Wizard -->
      <script type="text/javascript" src="theme/js/wizard/jquery.smartWizard.js"></script>
      <!-- Jquery Validate -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="theme/css/animate.min.css" rel="stylesheet">
<link href="theme/css/custom.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
       
        
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>
         
         <script src="js/jquery.validate.js"></script>
         <script src="js/utility/maintenanceService.js"></script>
 <style type="text/css">
   .maintenaceServiceContainer{
    background: white;
   /* background: linear-gradient(90deg, #f7f7f7 50%,#7ba0bf 50%);
            overflow: hidden;*/
    padding: 10px;
    border: 1px solid lightgrey;
    border-radius: 5px;
    box-shadow: 3px 2px 7px 1px rgba(181, 175, 175, 0.38);
    margin-bottom: 20px;
    overflow: hidden;
   }
   .searchedOwnerInfo{
     color: white;
     background: #7ba0bf;
     border-radius: 4px;     
   }
   .currentBalanceInfoContainer{
    display: flex;
    justify-content: space-around;
    background: white;
    margin: 20px 0;
    align-items: baseline;
    border-radius: 5px;
    border-top: 2px solid #52b58c;
    box-shadow: 3px 2px 7px 1px rgba(181, 175, 175, 0.38);
   }
   .enteredBal:blur{
    margin-right: 5px
   }
   .leftHalf{
        background: #d1d5d8a3;
   }
  .gridContainer{
    display: grid;
    grid-template-columns: auto auto;
   
    justify-content: space-around;
    min-height: 150px;

   }
   .balanceContainer{
    grid-column-start: 1;
    grid-column-end: 3;
    display: flex;
    justify-content: space-between;
    font-size: 18px;
    text-decoration: underline;
   }
   .balanceContainer >a{
    color: #000000b5;
   }
   .fullNameContainer,.balenceContainer{
    grid-column-start: 1;
    grid-column-end: 3;
   }
   .historyHead{
    background-color: #3f4146;
    height: 30px;
    line-height: 2em;
    color: white;
    padding-left: 20px;
   }
   .serviceTable{
    margin:10px;
   }
   #addBalanceContainer{
     background-color: #a8aeb1a1;
     overflow: auto;
     border-radius: 4px;
     border-left: 2px solid #f34242;
     color: #4e4040d4;
     padding: 20px;
   }
   #historyContainer{
      background-color: #d8ddde !important;
   }
  
   tr:nth-child(odd) {
    background: white;
    }
    tr:nth-child(even) {
      background: #dae8f7 !important;
      color: black;
    }
    .row-success{
    background-color: #5D6677 !important;
    color: #e8e8e8;
   }
   .uploadExcel{
    background: transparent;
    color: black;
    text-decoration: underline;
    background-image: none;
    border: 0px;
   }
   @keyframes errorPulse{
       to{ 
        box-shadow: 0 0 4px 2px #d25252;
       }
   }
 </style>  
<script>
$(function(){
	   $('#flatSearchForm').validate({ 
			rules: {
			  fNumber: {
				required: true,
				 minlength: 6,
				 maxlength: 6,
			  },

			},
			messages: {
			 fNumber : {
				required: "Please enter flat number.",
				minlength: "Please check format and Enter proper Flat Number",
				maxlength: "Please check format and Enter proper Flat Number",
			  },
			 
			},

			errorPlacement: function(error, element) {
			  
			},

			submitHandler: function(form) {
				alert("success");
			}
  }); 
  
});
</script>
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div style="overflow: auto">
      <h4 class="pull-left">Manage Electricity Bill</h4>
      <button type="button" class="btn btn-lg pull-right uploadExcel" data-toggle="modal" >Upload Electricity</button>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="maintenaceServiceContainer">
        <div class="col-xs-6">
         <form id="maintenaceServiceForm" enctype="multipart/form-data" method="Post">
          <div class="row">
            <div class="col-md-5 hide">
               <div class="form-group">
                  <label class="col-md-12" for="amount">Mobile Number</label>
                    <div class="col-md-12">
                      <input type="number" id="phoneNumber" name="phoneNumber"  class="form-control"/>
                    </div>
                </div>
            </div>       
            <div class="col-md-7">
               <div class="form-group">
                  <label class="col-md-12" for="amount">FlatNo</label>
                    <div class="col-md-12">
                      <input type="text" id="fNumber" name="fNumber"  class="form-control"/>
                    </div>
                </div>
            </div>
            <div class="col-md-2" style="margin-top: 25px;">
              <button type="submit" value="Submit"  class="btn btn-success pull-right">Search</button>
            </div>   
          </div>
        </div>
        <div class="col-xs-6 searchedOwnerInfo hide" id="searchedOwnerInfo">
                 <!-- Media middle -->
        
              
              <div class="rightHalf">
                <div class="gridContainer">
                 <div class="fullNameContainer"><h3 id="fullName"></h3></div>
                 <p>Email : <span class="emailId"></span></p>
                 <p>Ph : <span class="mobileNo" id="mobileNo"></span></p>
                 <p>Block : <span class="BlockName"></span></p>
                 <p>Flat : <span class="flatNo"></span></p>
                 <p>Flat Holder : <span class="flatHolder"></span></p>
                 <p>Maintenance : <span class="currMaintenance"></span></p>
                 <div class="balanceContainer">
                    
                     <a  id="expenses" class="openCollapse" style="visibility: hidden;">Expenses</a>
                    <a  id="addBalance" class="openCollapse">Add Balance</a>
                     <a  id="getHistory" class="openCollapse">History</a>
                 </div>
                </div>
              </div>
            
     
      </div>  
      </div>

    <div id="addBalanceContainer" class="panel-collapse collapse collapsiblePanel">
       <div class="form-group col-xs-4">
            <label  for="paymentMode">Payment mode</label>       
             <select id="paymentMode" name="paymentMode"  class="form-control">
                 <option value="Cash">Cash</option>
                 <option value="Cheque">Cheque</option>
                 <option value="DebitCard">Debit Card</option> 
                 <option value="CreditCard">Credit Card</option>               
             </select>
        </div>
     <form id="balanceForm" action="#">
        
        <div class="form-group col-xs-4">
               <label  for="name">Name</label>         
               <input type="name" id="name" name="name"  class="form-control"/>
        </div>
         <div class="form-group col-xs-4">
               <label  for="amount">Enter Amount</label>         
               <input type="number" id="amount" name="amount"  class="form-control"/>
          </div> 
          <div class="form-group col-xs-4">
               <label  for="flat">Select flat</label>         
               <select id="flat" name="flat"  class="form-control"></select>
          </div>
          <div class="form-group col-xs-4 auth-code hide">
               <label  for="authCode">Authentication Code</label>         
               <input type="text" id="authCode" name="authCode"  class="form-control"/>
          </div>  
        <div class="checkDetail hide">          
            <div class="form-group col-xs-4">
               <label  for="checkNum">Cheque Number</label>            
               <input type="text" id="checkNum" name="checkNum"  class="form-control"/>
            </div>   
          <div class="form-group col-xs-4">
                 <label for="expireDate">Check Expire Date</label>            
                 <input type="date" id="expireDate" name="expireDate"  class="form-control"/>
          </div>             
        </div>  
        <div class="form-group col-xs-4" style="margin-top: 24px;">
          <button type="submit" class="btn btn-default" id="addAmount">Submit</button>
        </div>
     </form>
    </div>

     <div id="expensesContainer" class="panel-collapse collapse collapsiblePanel" style="overflow: auto">
          
          <div class="col-xs-offset-2 col-xs-8">
            <iframe src="../server/bookAppointmentCalender.php?userId=2837&parentId=2837" height="500px" width="100%" style="border: 2px solid #b3afaf;"></iframe>
          </div>
          
    </div>

    <div id="historyContainer" class="panel-collapse collapse collapsiblePanel">
     <div class="historyHead">
       <div class="table-head pull-left">Maintenance History</div>
       <div class="pull-right"><h4 style="margin: 5px 10px;">Current Balance :<span id="currentBalance" style="font-weight:900;"></span></h4></div>
      </div>
      <div class=""> 
            <table  id="maintenaceHistory" class="table table-striped table-bordered" style="width:100%">   
              
            </table>        
      </div>
   
    </div>  


    </section>
    <!-- /.content -->


  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>

 <!-- Modal -->
<div id="uploadMaintenance" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload maintenance file</h4>
      </div>
      <div class="modal-body">
        <form id="uploadMaintenanceForm" action="#"  enctype="multipart/form-data" method="POST">
          <input type="hidden" name="uploadingFor" id="uploadingFor" value="Electricity Bill" style="visibility:hidden">
          <div class="form-group">
            <input type="file" class="form-control" id="file" name="file" required>
          </div>
          <button type="submit" id="uploadMaintenaceExcel" class="btn btn-default pull-right" data-uploadingFor="maintenance">Submit</button>
        </form>
        <h4 class="text-success upload-success hide">All records updated successfully</h4>
        <h4 class="text-danger upload-failed hide"><span class="failedRows">0 records</span > out of <span class="totalRows">0</span> are failed to update Maintenance.Please <a href="#" class="failedFilePath">click here to download failed records.</a></h4>
      </div>
    </div>

  </div>
</div>

</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>
<style type="text/css">
  input,select{
    border-radius: 5px;
    background: transparent;
    background-image: none;
    box-shadow: inset 0px 0px 1px 0px rgba(177, 171, 177, 0.89);
   }
</style>
</body>
</html>
