<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="theme/css/animate.min.css" rel="stylesheet">
<link href="theme/css/custom.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
       
        
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>
         
         <script src="js/jquery.validate.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
         <script src="js/utility/facility.js"></script>
 <style type="text/css">
     .error{
      color: red;
     }
   .facilityContainer{
    background: #eaeaea;
    padding: 10px;
    border: 1px solid lightgrey;
    border-top: 3px solid #efd418;
    border-radius: 5px;
    box-shadow: 3px 2px 7px 1px rgba(181, 175, 175, 0.38);
   }
   .searchedOwnerInfo{
      margin: 10px 2px;
      background-color: white;
      border-top: 2px solid #e8165a78;
      border-radius: 4px;
      box-shadow: 3px 2px 7px 1px rgba(181, 175, 175, 0.38);   
   }
   .currentBalanceInfoContainer{
    display: flex;
    justify-content: space-around;
    background: white;
    margin: 20px 0;
    align-items: baseline;
    border-radius: 5px;
    border-top: 2px solid #52b58c;
    box-shadow: 3px 2px 7px 1px rgba(181, 175, 175, 0.38);
   }
   .table-container{
    margin-top: 30px;
    color: black;
  }
  tbody tr:nth-child(odd) {
    background: #f9f9f97a;
}
  tr:nth-child(even) {
    background: linear-gradient(to right, #dcc760 ,#715f5f);
    background: lightyellow;
  }
  
 
  .viewFacility,.editFacility,.deleteFacility{
   background:transparent;
   border-width: 0px;
    color: #1984a5;
    padding: 2px 15px;
    border-radius: 1px;
  }
  .deleteFacility{
     color:red;
  }
  .custom-form-control{
                    font-size: 14px;
                    line-height: 2;
                    color: #555;
                    border-radius: 2px;
                    border-style: none;
                    background-color: white;
          }
    caption{
      padding:8px;
      background: #f1efed;
    }
   @keyframes errorPulse{
       to{ 
        box-shadow: 0 0 4px 2px #d25252;
       }
   }
 </style>  
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="overflow: hidden;">
      <h1 class="pull-left">Manage Facilities</h1>
      <a href="facilityReport.php" class="pull-right"><u>View Report</u></a>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="facilityContainer">
         <form id="facilityForm" enctype="multipart/form-data" method="Post">
          <div class="row">
            <div class="col-md-5">
               <div class="form-group">
                  <label class="col-md-12" for="facilityName">Facility Name</label>
                    <div class="col-md-12">
                      <input type="text" id="facilityName" name="facilityName"  class="form-control" required />
                    </div>
                </div>
            </div>  
             <div class="col-md-5">
               <div class="form-group">
                  <label class="col-md-12" for="chargePerHour">Charge per hour</label>
                    <div class="col-md-12">
                      <input type="number" id="chargePerHour" name="chargePerHour"  class="form-control col-md-6" required />
                      <input type="hidden" id="rowId" value="" class="form-control col-md-6"/>
                    </div>
                </div>
            </div>                                    
            <div class="col-md-2 facility-btn-grp" style="margin-top: 25px;">
              <button type="submit" class="btn btn-success col-xs-5">Add</button>
              <button type="reset" class="btn btn-default hide cancelBtn">Cancel</button>
            </div>                                    
          </div>
          </form>
      </div>
       <div class="table-container">
           <div class="table-responsive" style="border:#3F4551;border-top-left-radius: 4px;
    border-top-right-radius: 4px;">          
                <table class="table table-hover table-bordered" id="facilityTable"  style="margin-bottom: 0;border-top:0px">
                <caption>
                    <h4 class="col-xs-4" style="margin-bottom: 0px;color: black;">Facilities List</h4>
                    <input class="custom-form-control col-xs-4 pull-right" id="SearchQry" type="text" placeholder="Search..">
                </caption>
                  <thead style="background: lightcoral;color:white;">
                    <tr>
                      
                      <th>Name</th>
                      <th>Charge/Hour</th> 
                      <th>Action</th>                       
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                        require_once('../server/libs/dbConnection.php');
                        $connection = new dbconnection();
                        $con = $connection->connectToDatabase();
                        $sql = mysqli_query($con, "SELECT * FROM  facilities where type=0 order by id");
                        $rows_count = mysqli_num_rows($sql);

                          while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                           echo '<tr >';
                          
                           echo "<td>" . $rows_fetch['facility_name'] . "</td>";
                           echo "<td>" . $rows_fetch['charge_per_hour'] . "</td>";
                           $buttonId=$rows_fetch['id'];
                           echo '<td><button class="viewFacility" onclick="viewFacility('.$buttonId.')">View</button><button class="editFacility" onclick="editFacility.call(this,'.$buttonId.')">Edit</button><button class="deleteFacility" onclick="deleteFacility('.$buttonId.')">Delete</button></td>';
                           echo '</tr>';
                            }
                        //below one gets multi booking faciltieis   
					
                         $sql = mysqli_query($con, "SELECT * FROM  facilities where type=1 order by id");
                         $rows_count = mysqli_num_rows($sql);

                          while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                           echo '<tr>';
                          
                           echo "<td>" . $rows_fetch['facility_name'] . "</td>";
                           echo "<td>" . $rows_fetch['charge_per_hour'] . "</td>";
                           $buttonId=$rows_fetch['id'];
                           echo '<td><button class="viewFacility" style="visibility:hidden">View</button><button class="editFacility" onclick="editFacility.call(this,'.$buttonId.')">Edit</button><button class="deleteFacility" onclick="deleteFacility('.$buttonId.')">Delete</button></td>';
                           echo '</tr>';
                            }
                            $connection->closeConnection();
                    ?>
                  </tbody>
                </table>
          </div>

       </div>
       

    </section>
    <!-- /.content -->


  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>
<style type="text/css">
  input,select{
    border-radius: 5px;
    background: transparent;
    background-image: none;
    box-shadow: inset 0px 0px 1px 0px rgba(177, 171, 177, 0.89);
   }
</style>
</body>
</html>
