<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
 <link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
       
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>
        <style>
           /* table.db-table    { border-right:1px solid #ccc; border-bottom:1px solid #ccc;width:100%; }
            table.db-table th { background:#eee; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
            table.db-table td { padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }*/
              
               .form-container{
               	 
               	 border-top: 2px solid red;
               }
                .table-bordered{
                   border:2px solid lightcoral;
               border-top: 0;

         }
         .cust-line-hight{
           line-height: 4em;
         }
         tbody tr:nth-child(odd) {
            background:white;
        }
          tr:nth-child(even) {
            background: linear-gradient(to right, #dcc760 ,#715f5f);
            background: lightyellow;
          }
          .custom-form-control{
                    font-size: 14px;
                    line-height: 2;
                    color: #555;
                    border-radius: 2px;
                    border-style: none;
                    background-color: white;
          }
        caption{
          padding:8px;
          background: #f1efed;
        }
        .caption-title{
            margin: 0px;
        color: black;
        padding: 5px;
      }
        .deleteBannerClass{
           background:transparent;
            border-width: 0px;
          color: red;
          padding: 2px 15px;
          border-radius: 1px;
        }
        </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Banners</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
                  <div class="form-container">
                       <form id="bannerForm" name="bannerForm" class="form-horizontal" method="POST" action="">
                                             <div class="panel-body">
                                                <div class="row">
                                                   <div class="col-xs-offset-1 col-md-4">
                                                      <div class="form-group">
                                                        <label class="col-md-12"> Banner Link:</label>
                                                        <div class="col-md-12">
                                                            <input type="file" id="bannerPath" name="bannerPath" class=""/>
                                                        </div>
                                                    </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Banner Website or Message</label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="message" name="message" class="form-control"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-2" style="margin-top: 22px;">
                                                     <input type="submit" class="btn btn-success" value="Add Banner" name="submit" />
                                                   </div>
                                                </div>
                                                
                                                </div>
                                               
                         </form>
                  </div>
                   
                   
                   <div class="">
                  
                   <div class="table-responsive"> 
                          <table class="db-table table table-bordered table-hover table-condensed">
                           <caption >
                              <h4 class="col-xs-4 caption-title" >List of Banners<span class="glyphicon glyphicon-arrow-down"></span></h4>
                               <input class="custom-form-control col-xs-4 pull-right" id="SearchQry" type="text" placeholder="Search..">
                           </caption> 
                      <thead style="background-color: #EA6153;color: white">
                      <tr >
                      <th class="cust-line-hight">Banner Photo Link</th>
                      <th>Banner Description or Website</th>
                      <th class="cust-line-hight">Action</th>
                      </tr>
                      </thead>
                      <tbody>
                       <?php
                               
                                $connection = new dbconnection();
                                $con = $connection->connectToDatabase();

                                $sql = mysqli_query($con,"SELECT * FROM banners");
                                $rows_count = mysqli_num_rows($sql);
                                $banners=array();
                                if($rows_count!=0){
                                    while ($rows_fetch = mysqli_fetch_assoc($sql)) { 
                                      echo '<tr >';
                                        echo '<td class="text-left">',$rows_fetch['bannerPath'],'</td>';
                                        echo '<td>',$rows_fetch['description'],'</td>';
                                        echo '<td><button class="deleteBannerClass" id="',$rows_fetch['id'],'">Delete</button></td>';
                                        
                                      echo '</tr>';
                                    }
                                }
                                
                                $connection->closeConnection();

                      ?>
                      </tbody>
                  </table></div></div>
                   
              
                    <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
                    <div class="cbp-vm-options">
                        <a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid">Grid View</a>
                        <a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list">List View</a>
                    </div>
                    <input class="gcm filterContact form-control hide" placeholder="Notify.." />
                    <ul class="list" id="contactsDashboard">
                        
                    </ul>

                
                </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>


 <!-- Grid Switch Mode -->
    <link rel="stylesheet" type="text/css" href="grid/css/component.css" />
    <script src="grid/js/classie.js"></script>
    <script src="grid/js/cbpViewModeSwitch.js"></script>
    <!-- Grid Switch Mode -->

    <script src="grid/list.js"></script>

    <script>

    $(function(){

    $("#main-header").load("headerMenu.php");
    $("#main-sidebar").load("sidebarMenu.php");
    $("#main-footer").load("footerMenu.php");

    $("#cbp-vm").hide();

   initSearch();


});

function initSearch(){
    var bannerForm = $("#bannerForm");
    $('#bannerForm').validate({ //...................validating bannerForm form starts
        rules: {
          bannerPath: {
            required: true,
          },
          message: {
             required: true,
          }

        },
        messages: {
          bannerPath: {
            required: "Please enter photo link."
          },
          message: {
           required: "Please enter message"
          }
        },

        errorPlacement: function(error, element) {
          var divObj=document.createElement("div");
          divObj.setAttribute("class","error-messageColor");
          element.parent().append(divObj);
          error.appendTo(element.next());
        },

        submitHandler: function(form) {
           addBanner();
        }
  }); //...............bannerForm form validation ends
     enableBannerSearch();

    $('.deleteBannerClass').click(function(evt){
        delBanner(this.id);
    });

}
  function  enableBannerSearch(){
      $("#SearchQry").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".db-table tbody >tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
   } 

function addBanner(){
   
    /*var title = $("#bannerPath").val();
    var message = $("#message").val();
    
    var details = {"bannerPath": title, "message":message};*/
    oData = new FormData($("#bannerForm")[0]);
    showLoader();
   // var dataR = details;
    $.ajax({
        type: "POST",
        data: oData,
        cache: false,
        contentType: false,
        processData: false,
        url: getUrl(api.ADD_BANNER_URL),
      
    }).done(function(response) {
        hideLoader();
       if(JSON.parse(response).isSuccessful == true){
        alert("Banner added successfully");
        location.reload();
      }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 

}


function delBanner(id){
   
    showLoader();
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: getUrl(api.DELETE_BANNER_URL),
        data: {id:id},
    }).done(function(response) {
        hideLoader();
       if(response.isSuccessful == true){
        alert("Banner deleted successfully");
        location.reload();
      }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        hideLoader();
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 

}


    </script>
</body>
</html>
