<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!--moment -->
<script src="aLTE/plugins/moment/moment.js"></script>
<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>
 <!-- Date Range Picker -->
 <script type="text/javascript" src="theme/js/moment.min2.js"></script>
      <script type="text/javascript" src="theme/js/datepicker/daterangepicker.js"></script>

<script src="aLTE/plugins/mutistep_form/js/multistep.js"></script>
<link rel="stylesheet" href="aLTE/plugins/mutistep_form/css/multistep.css">

<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
       
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>
 <script src="js/utility/facilityCards.js"></script>
   <style type="text/css" media="screen">
        #cardIssueForm{
        	display: grid;
        }
        table.issue-card-tb>tbody>tr{
        	    height: 42px;
        }
        table.issue-card-tb>tbody>tr>td {
         text-align: center;
         vertical-align: middle !important;
         padding: 4px !important;
        }
        .footer-btn{
          border-left: 2px solid #1ba3c1;
          border-radius: 0px;
        }
        table.issued-cards-detail-tb th,table.issued-cards-detail-tb td{
           text-align: left
        }	
   </style>     
       
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Facility Cards</h1>
    </section>

    <!-- Main content -->
    <section class="content">
            <!-- Info boxes -->
          <div class="box box-default box-solid collapsed-box" data-widget="box-widget"> 
          <div class="box-header">
            <h3 class="box-title">Manage cards</h3>
            
            <div class="box-tools pull-right">
            <button type="button" class="btn btn-primary" style="margin-left:4px;">GYM <span class="badge gymCnt">0</span></button>
            <button type="button" class="btn btn-info">Swimming<span class="badge swimmingCnt">0</span></button>
              <!-- This will cause the box to collapse when clicked -->
              <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus" data-widget="collapse"></i></button>             
            </div> 
          </div>
          <div class="box-body card-issue-container">
           <form id="issueCardFrom" name="issueCardFrom" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
            <table class="table table-bordered issue-card-tb">
              <tbody>
                <tr>
                  <td>Flat</td>
                  <td><select name="flat" class="form-control"/>
                        <option value="">flat</option>
                      </select>
                  </td>
                  <td>Name</td>
                  <td>
                  <select name="memberName" class="form-control memberName"/>
                        <option value="">Select Name</option>
                      </select>
                  </td>
                </tr>
                <tr>
                  <td>Category</td>
                  <td>
                     <select name="catagoryList" class="form-control"/>
                      
                     </select>
                  </td>
                  <td>Card</td>
                  <td>
                      <select name="cardsList" class="form-control hide"/>
                        <option value="">Select Card</option>
                      </select>
                  </td>
                  
                </tr>
                <tr>
                  <td colspan="4">
                    <input type="submit" class="btn btn-default footer-btn" value="Submit">
                  </td>
                </tr>
              </tbody>
			    </table>
         </form> 
           </div>
          </div>  

        <div class="box box-default box-solid" data-widget="box-widget"> 
          <div class="box-header">
            <h3 class="box-title">Issued Cards</h3>
            <div class="box-tools">
              <input type="text" class="form-control" id="issueCardSearch" placeholder="Search">
            </div> 
          </div>
          <div class="box-body issued-cards-container">
              <table class="table table-bordered issued-cards-detail-tb">
                <thead>
                	<tr>
                		<th>Card</th>
                		<th>Issued At</th>
                    <th>Category</th>
                		<th>Flat</th>
                		<th>Name</th>
                		<th>Action</th>
                	</tr>
                </thead>
			    <tbody>
          
			    </tbody>
			  </table>
           </div>
        </div>  
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->

<!-- Modal -->
  <div id="RecieveCardModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Recieve Card</h4>
        </div>
        <div class="modal-body" style="overflow: -webkit-paged-x;">
            <!-- multistep form -->
          <form id="msform" class="RecieveCardFrom">
                <div>
                       <div class="form-group col-xs-6">
                          <label for="flat">Flat:</label>
                          <input type="text" class="form-control" id="flat" name="flat" disabled>
                       </div>
                       <div class="form-group col-xs-6">
                          <label for="personName">Name:</label>
                          <input type="text" class="form-control" id="personName" name="personName" disabled>
                       </div>         
                      </div>
                      <div>
                       <div class="form-group col-xs-6">
                          <label for="card">Card:</label>
                          <input type="text" class="form-control" id="card" name="card" disabled>
                       </div>
                       <div class="form-group col-xs-6">
                          <label for="issuedAt">Issued:</label>
                          <input type="text" class="form-control" id="issuedAt" name="issuedAt" disabled>
                       </div>         
                      </div>
                      <div>
                          <div class="form-group col-xs-6">
                              <label for="recievedAt">Recieved Date:</label>
                              <input type="text" class="form-control" id="recievedAt" name="recievedAt" disabled>
                          </div>
                          <div class="form-group col-xs-6">
                            <label for="totalCharge" class="">Amount:</label>
                            <input type="text" class="form-control col-xs-10" id="totalCharge" name="totalCharge" disabled>
                            <i class="fa fa-fw fa-edit col-xs-2 pull-right editAmount"></i>
                          </div>
                      </div>
                     <input type="submit" name="submit" class="submit action-button" value="Submit" />
          </form>
        </div>
      </div>
    </div>
  </div>


<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>
</body>
</html>
