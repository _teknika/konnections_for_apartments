<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
 <link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
       
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>

        <script src="js/utility/buildingInfo.js"></script>
       <style type="text/css" media="screen">
         .form-container{
         background-color: #f3f3f378 !important;
          border: 1px solid lightgrey;
          border-top: 2px solid #f1ad14;
         }
         .table-bordered{
               border: 2px solid #2980B9;
               border-top: 0;
         }
         tbody tr:nth-child(odd) {
            background: #f9f9f97a;
        }
          tr:nth-child(even) {
            background: linear-gradient(to right, #dcc760 ,#715f5f);
            background: lightyellow;
          }
          .custom-form-control{
                    font-size: 14px;
                    line-height: 2;
                    color: #555;
                    border-radius: 2px;
                    border-style: none;
                    background-color: white;
          }
        caption{
          padding:8px;
          background: #f1efed;
        }
        .caption-title{
            margin: 0px;
        color: black;
        padding: 5px;
      }
        .deleteBlockName{
           background:transparent;
            border-width: 0px;
          color: red;
          padding: 2px 15px;
          border-radius: 1px;
        }
		.editBlockName{
           background:transparent;
            border-width: 0px;
          color: green;
          padding: 2px 15px;
          border-radius: 1px;
        }
       </style>
	    <script>
	 $(document).ready(function(){
    $(".editBlockName").click(function(){var id= $(this).closest("tr").attr("data-id");$(".OkButton").click(function(){

	var blockNm=$('#blockNm').val();
	alert("Block Name updated successfully");
	location.reload();
 $.ajax({
            type:'POST',
            url:'../server/buildingInfoEdit.php',
            dataType: "json",
            data:{blockNm:blockNm,id:id},
            success:function(){
			
                
            }
        });
	
	
	});
    });
});

</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
 <div class="container">
 <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog">
									<div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Enter The Blockname</h4>
        </div>
        <div class="modal-body">
           BlockName:<input type="text" id="blockNm" name="blockNm" required />
        </div>
        <div class="modal-footer">
          <button type="button" class="OkButton" data-dismiss="modal">OK</button>
        </div>
      </div>
      
    </div>
  </div>
  </div>
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <h4 class="panel-title">Building Info</h4>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
                <div class="form-container">
                    <form id="addBuildingInfoForm" name="addBuildingInfoForm" class="form-horizontal" method="POST" action="">  
                                         <div class="row">
                                         
                                                <div class=" col-xs-offset-2 col-md-6">
                                                   <div class="form-group">
                                                      <label class="col-md-12">Block Name<span>*</span> <!-- Bug no. 37 -->
                                                      </label>
                                                      <div class="col-md-12">
                                                         <input type="text" id="blockName" name="blockName" class="form-control" required />
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col-md-2" style="margin-top:24px">
                                                             <input type="submit" class="btn btn-success" value="Add" name="submit"/>
                                                </div>
                                            </div> 
                                     
                                    </form>
                </div>
                <br/>
                      <div class="table-responsive"> 
                          <table class="db-table table table-bordered table-hover table-condensed" id="buildningInfoTable">  
                           <caption >
                              <h4 class="col-xs-4 caption-title" >Blocks list</h4> <!-- Bug no. 37 -->
                               <input class="custom-form-control col-xs-4 pull-right" id="SearchQry" type="text" placeholder="Search..">
                           </caption> 
                           <thead style="background: #2776ae;color: #efebebe8;">
                                <tr >
                                    <th>Block Name</th> 
                               <!--   <th>Action</th>  -->                   
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                                require_once('../server/libs/dbConnection.php');
                                $connection = new dbconnection();
                                $con = $connection->connectToDatabase();
                                $sql = mysqli_query($con, "SELECT * FROM  buildinginfo");
                                $rows_count = mysqli_num_rows($sql);

                                while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                                    echo "<tr data-id=". $rows_fetch['id'].">";
                                   
                                    echo "<td>" . $rows_fetch['buildingName'] . "</td>";
                                    //echo '<td><button class="editBlockName" data-toggle="modal" data-target="#myModal" id="' . $rows_fetch['id'] . '" disabled>Edit</button><button class="deleteBlockName" id="' . $rows_fetch['id'] . '" disabled>Delete</button></td>'; 
                                    echo '</tr>';
                                }
                                 $connection->closeConnection();
                                ?>
                            </tbody>
                        </table>        
                       </div>
					   <h5 align="right"><b>If you want do Any changes. Please contact Neptune</b><br>
             <b>admin@ncpl.co</b></h5>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>

</body>
</html>
