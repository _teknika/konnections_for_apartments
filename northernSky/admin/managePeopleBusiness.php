<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
    .table-bordered{
                border: 4px solid #ddd !important;
               }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Book</h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
                 <div class="right_col" role="main" id="gcmList">
                    <br />
                    <div class="" id="hacker-list">
                        <div class="row top_tiles">
                            <form id="managePeopleBusinessForm" name="managePeopleBusinessForm" class="form-horizontal" method="POST"  enctype="multipart/form-data">
                                <div class="panel">
                                   
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Name</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="name" name="name"  class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Email</label>
                                                        <div class="col-md-12">
                                                            <input type="email" id="email" name="email" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Contact No</label>
                                                        <div class="col-md-12">
                                                            <input type="number" id="contactNo" name="contactNo" class="form-control"/>                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Website Link</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="websiteLink" name="websiteLink" class="form-control"/>                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Deals in</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="dealsIn" name="dealsIn" class="form-control"/>                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12">address</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="address" name="address" class="form-control"/>                                                         
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>                                                                    
                        <div class="text-right">
                            <input type="submit" class="btn btn-success" value="Add" name="submit" onclick="addPeopleBusiness()"/>
                            </form>
                        </div>                     
                        <br><br>
                        <div class="table-responsive"> 
                          <table class="db-table table table-bordered table-hover table-condensed">
                      
                            <thead>
                                <tr class="success">
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Number</th>
                                    <th>Website Link</th>                                    
                                    <th>Deals in</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                require_once('../server/libs/dbConnection.php');
                                $connection = new dbconnection();
                                $con = $connection->connectToDatabase();
                                $sql = mysqli_query($con, "SELECT * FROM people_business");
                                $rows_count = mysqli_num_rows($sql);

                                while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                                    echo '<tr class="active">';
                                    echo "<td>" . $rows_fetch['name'] . "</td>";
                                    echo "<td>" . $rows_fetch['email'] . "</td>";
                                    echo "<td>" . $rows_fetch['person_contact'] . "</td>";
                                    echo "<td>" . $rows_fetch['website_link'] . "</td>";
                                    echo "<td>" . $rows_fetch['dealsIn'] . "</td>";
                                    echo "<td>" . $rows_fetch['address'] . "</td>";
                                    echo '<td><button class="deleteSelectedRow" id="' . $rows_fetch['id'] . '">Delete</button></td>';
                                    echo '</tr>';
                                }
                                $connection->closeConnection();
                                ?>
                            </tbody>
                        </table>        
                       </div>
            
            <!-- Grid Switch Mode -->
            <link rel="stylesheet" type="text/css" href="grid/css/component.css" />
            <script src="grid/js/classie.js"></script>
            <script src="grid/js/cbpViewModeSwitch.js"></script>
            <!-- Grid Switch Mode -->

            <script src="grid/list.js"></script>
          
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer" >
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->
            <!-- Loader -->
            <div class="fader">
                <div class="loader"></div>
            </div>


<!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>

 <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility.js"></script>
        <script src="js/validation.js"></script>
        <style>
           
        </style>

            <script>

                $(function () {
                    $("#main-header").load("headerMenu.php");
                    $("#main-sidebar").load("sidebarMenu.php");
                     $('.deleteSelectedRow').click(function (evt) {
                        deleteSelectedRow(this.id);
                    });
                    initSearch();
                   
                });


                                function initSearch() {
                    var evengelismForm = $("#evengelismForm");
                    $("#evengelismForm").validate({
                        messages: {

                        },
                        submitHandler: function (evengelismForm) {

                            addPreachings();
                        }
                    });

                  
                }
                function addPeopleBusiness() {
                        showLoader();    
                        var submitUrl="../server/addPeopleBusiness.php";
                        var form = document.forms.namedItem("managePeopleBusinessForm");
                        form.addEventListener('submit', function(ev) {
                          oData = new FormData(form);
                          var oReq = new XMLHttpRequest();
                          oReq.open("POST", submitUrl, true);
                          oReq.onload = function(oEvent) {
                           hideLoader();
                             if (JSON.parse(oReq.response).isSuccessful == true) {
                                alert("Business added successfully");
                                location.reload();
                            } else {
                              alert("Please try again");
                            }

                          };

                          oReq.send(oData);
                          ev.preventDefault();
                        }, false);
                
                }

                function deleteSelectedRow(id) {
                    showLoader();
                    $.ajax({
                        type: "GET",
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        url: "../server/deletePeopleBusiness.php",
                        data: {id: id},
                    }).done(function (response) {
                        hideLoader();
                        if (response.isSuccessful === true) {
                            alert("Selected row  deleted successfully.");
                            location.reload();
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        hideLoader();
                        alert("Server failed");
                    })
                            .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});

                }

           </script>     
</body>
</html>
