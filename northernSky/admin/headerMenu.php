<?php
include("../server/libs/session.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
<title>header</title>
 <style type="text/css" media="screen">
   .skin-blue .main-header .navbar{
                background: linear-gradient(146deg, #25b9af 87%,#30cdde 51%);
        }
        .skin-blue .sidebar-menu>li.header{
                background-color: #25a9a0;
        }
      .main-header .logo .logo-lg{
        text-shadow: 5px 3px 8px black;
        
       
      }
      .main-header .logo:hover{
        
         background-color: #11655f !important;
      }
      .skin-blue .main-header .logo{
          background-color: #38aba3;
         
      }
 </style>
</head>
<body>  
  
       <!-- Logo -->
    <a href="dashboard.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini" style="font-size: 11px;"><b>Northern</b>Sky</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">NorthernSky</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="position: relative;">
      <!-- Sidebar toggle button-->
      <a href="dashboard.php" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu" style="float:none;position: absolute;right:0">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu" style="margin: 15px">
            <div onclick="on_click_sign_out()"><a href="#" style="color: white;"> Log Out<i class="fa fa-sign-out" style="margin-left:6px "></i></a></div>
          </li>
         
        </ul>
      </div>
    </nav>
 
</body>
</html>