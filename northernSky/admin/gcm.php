<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <style>
  //changed by cs
  .error{
     color:red;
  }
  </style>
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">

  
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom styling plus plugins -->
       <!-- <link href="theme/css/custom.css" rel="stylesheet"> -->
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>

          <script src="js/utility/gcm.js"></script>
        <style>
            table.db-table    { border-right:1px solid #ccc; border-bottom:1px solid #ccc;width:100%; }
            table.db-table th { background:#eee; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
            table.db-table td { padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
        </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Send Notification</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
                   <div class="form-container">

              

                    
                       <form id="gcmForm" name="gcmForm" class="form-horizontal" method="POST" action="">                                                                                                                     
                                                <div class="row">
                                                   <div class="col-xs-6 col-md-6">
                                                      <div class="form-group">
                                                         <label class="col-md-12 ">Title</label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="title" name="title"  class="form-control formContorl"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-xs-6 col-md-6">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Message</label>
                                                         <div class="col-md-12">
                                                            <input type="text" id="message" name="message" class="form-control formContorl"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-xs-6 col-md-6 hide">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Type</label>
                                                         <div class="col-md-12">
                                                            <select id="type" name="type" class="form-control formContorl">
                                                               <option value="general" selected="selected">General</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>

                                                   <div class="col-xs-6 col-md-6 hide">
                                                      <div class="form-group">
                                                         <label class="col-md-12">Live/Test</label>
                                                         <div class="col-md-12">
                                                            <select id="category" name="category" class="form-control formContorl">
                                                           
                                                            <option value="live" selected="selected">Live</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   
                                                </div>
                                                
                                           
                                             <div class="text-right">
                                                <input type="submit" class="btn btn-success" value="Send Notification" name="submit" />
                                          </div>                                       
                                     
                                </form>
                    </div>

                    

                    <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
                    <div class="cbp-vm-options">
                        <a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid">Grid View</a>
                        <a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list">List View</a>
                    </div>
                    <input class="gcm filterContact form-control hide" placeholder="Notify.." />
                    <ul class="list" id="contactsDashboard">
                        
                    </ul>

                
                </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>


  <!-- Grid Switch Mode -->
    <link rel="stylesheet" type="text/css" href="grid/css/component.css" />
    <script src="grid/js/classie.js"></script>
    <script src="grid/js/cbpViewModeSwitch.js"></script>
    <!-- Grid Switch Mode -->

    <script src="grid/list.js"></script>

</body>
</html>
