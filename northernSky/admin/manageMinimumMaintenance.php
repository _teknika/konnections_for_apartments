<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->


  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>
  
 <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon' >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
    
        <link href="css/style.css" rel="stylesheet">
<script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
 <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Minimum Maintenance</h1>
    </section>

    <!-- Main content -->
    <section class="content">
       <!-- add service starts -->
                <!-- Info boxes -->
        <div class="box  box-warning box-solid collapsed-box" data-widget="box-widget"> 
          <div class="box-header">
            <h3 class="box-title">Current Minimum Balance: <span id="currentMinMaitenance"></span></h3> <!-- label change, bug no. #35 -->
            <div class="box-tools">
              <!-- This will cause the box to collapse when clicked -->
              <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus" data-widget="collapse"></i></button>
              
            </div> 
          </div>
          <div class="box-body">
             <form class="updateMinMaintenanceForm" id="updateMinMaintenanceForm" method="POST"  enctype="multipart/ form-data">  
                 <div class="row"> 
                  <div class="col-xs-6">
                    <div class="form-group">
                        <label for="minMaintenance">Min Maintenance</label>
                        <input type="number" class="form-control" id="minMaintenance" name="minMaintenance" required>
                    </div>
                 </div>
                <div class="col-xs-3">
                    <button type="submit" class="btn btn-success" style="margin-top: 25px;
                       margin-right: 15px;margin-bottom: 10px" >UPDATE</button>             
                </div>
                </div>      
              </form>
            </div>
          </div>  
       <!-- add service ends -->
                    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->



 <!-- Grid Switch Mode -->
            <link rel="stylesheet" type="text/css" href="grid/css/component.css" />
            <script src="grid/js/classie.js"></script>
            <script src="grid/js/cbpViewModeSwitch.js"></script>
            <!-- Grid Switch Mode -->

            <script src="grid/list.js"></script>
            <script>
                $(function () {
                    $("#main-header").load("headerMenu.php");
                    $("#main-sidebar").load("sidebarMenu.php");
                     $("#main-footer").load("footerMenu.php");
                     getMinMaintenance();//get current min maintenance to show
                    initPage();
                });

                function initPage() {
                    $('#updateMinMaintenanceForm').submit(function (evt) {
                        evt.preventDefault();
                        updateMinMaintenance({"minMaintenance":$("#minMaintenance").val(),"updatedBy":"111"});
                    });

                }
                
            //add Min maintenance to database
            function updateMinMaintenance(maintenanceInfo){
                $.ajax({
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    url: "../server/updateMinimumMaintenance.php",
                    data: JSON.stringify(maintenanceInfo)
                }).done(function(response) {
                hideLoader();
                alert("Minimum Maintenance updated successfully");
                $('#updateMinMaintenanceForm').trigger("reset");
                getMinMaintenance();
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                })
                .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
            }
             //add service to database
             function getMinMaintenance(){
                $.ajax({
                    type: "GET",
                    url: "../server/getMinimumMaintenance.php",
                }).done(function(response) {
                    hideLoader();
                    var responseData=JSON.parse(response);
                    if(responseData){
                      var htmlString=responseData.min_maintenance_charge+" ("+responseData.updated_at+")";
                      $("#currentMinMaitenance").html(htmlString);
                    }else{
                      responseData=0;
                      $("#currentMinMaitenance").html(responseData); 
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                })
                .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
            }
            </script>

</body>
</html>
