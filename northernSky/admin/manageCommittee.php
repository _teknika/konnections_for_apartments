<?php
include("../server/libs/session.php");
require_once('../server/libs/dbConnection.php');
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My family</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="aLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="aLTE/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->

  <!-- jQuery 2.2.3 -->
<script src="aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="aLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->

<script src="aLTE/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="aLTE/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="aLTE/dist/js/demo.js"></script>


<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
<link href="theme/fonts/css/font-awesome.min.css" rel="stylesheet">
 <link href="theme/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
       
        <link href="css/style.css" rel="stylesheet">
        <script src="theme/js/custom.js"></script>
        <script src="js/bootbox.js"></script>
        <!-- Custom JS -->
        <script src="js/jquery.validate.js"></script>
        <script src="js/url.js"></script>
        <script src="js/user.js"></script>
        <script src="js/utility/utility.js"></script>
        <script src="js/validation.js"></script>
        <style>
          /*  table.db-table    { border-right:1px solid #ccc; border-bottom:1px solid #ccc;width:100%; }
            table.db-table th { background:#eee; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
            table.db-table td { padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }*/

          .table-bordered{
                border: 4px solid #5D6677 ;
               }
           .tbl-header{
             background-color: #5D6677;
             color: white;
           }
           .table-responsive{
                background-color: #3F4551;
                padding: 20px 10px 10px 10px;
           } 
           .form-container{
                border-top: 2px solid #f54907;
           }  
           .deleteComClass{
           margin-left: 10px; 
           }
        </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

 <header class="main-header" id="main-header">

  </header>
 
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="main-sidebar">

 </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Committee Members</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
                       <div class="form-container">
                            <form id="memberForm" name="memberForm" class="form-horizontal" method="POST" action="">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Name</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="name" name="name"  class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Designation</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="post" name="post" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-md-12">Mobile number</label>
                                                    <div class="col-md-12">
                                                        <input type="text" id="mobile_number" name="mobile_number" class="form-control" minlength="10"  />
                                                    </div>
                                                </div>
                                            </div>

                                            </div>

                                        <div class="text-right">
                                          <input type="submit" class="btn btn-success" value="Add New Member" name="submit" />
                                        </div></div>
                            </form>
                            

                       </div>
                    
             
                  
                    
                    <p> List of Members</p>
                   <div class="table-responsive"> 
                    <table class="db-table table table-bordered table-hover table-condensed" >
                        <thead>
                            <tr class="tbl-header">
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Mobile number</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            require_once('../server/libs/dbConnection.php');
                            $connection = new dbconnection();
                            $con = $connection->connectToDatabase();
                            $sql = mysqli_query($con, "SELECT * FROM managemembers");
                            $rows_count = mysqli_num_rows($sql);

                            while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                                echo '<tr class="active">';
                                echo "<td class='memberName'>" . $rows_fetch['name'] . "</td>";
                                echo "<td class='memberPost'>" . $rows_fetch['post'] . "</td>";
                                echo "<td class='memberPh'>" . $rows_fetch['mobile_number'] . "</td>";
                                echo '<td><button class="editMemberBtn" data-id="' . $rows_fetch['id'] . '">Edit</button><button class="deleteComClass" id="' . $rows_fetch['id'] . '">Delete</button></td>';
                                echo '</tr>';
                            }
                            $connection->closeConnection();
                            ?>
                        </tbody>
                    </table>
                    </div>
                    <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
                        <div class="cbp-vm-options">
                            <a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid">Grid View</a>
                            <a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list">List View</a>
                        </div>
                     
                        <ul class="list" id="contactsDashboard">
                        </ul>
                    </div>
     
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" id="main-footer">

  </footer>



</div>
<!-- ./wrapper -->
<!-- Modal -->
<div id="editCommitteMember" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Member Details</h4>
      </div>
      <div class="modal-body">
       <form action="#" class="" id="editMemberFrom">
          <div class="row">
            <div class="form-group col-xs-6">
              <label for="memberName">Name :</label>
              <input type="text" name="memberName" class="form-control" id="memberName" required>
            </div>
            <div class="form-group col-xs-6">
              <label for="memberPost">Designation :</label>
              <input type="text" name="memberPost" class="form-control" id="memberPost" required>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-xs-6">
              <label for="memberPh">Mobile number :</label>
              <input type="number" name="memberPh" class="form-control" id="memberPh"  required>
            </div>
          </div>
         
           <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-default">Submit</button>
          </div>
        </form>
      </div>
     
     
    </div>

  </div>
 </div> 
<!-- Loader -->
<div class="fader">
   <div class="loader"></div>
</div>

 <!-- Grid Switch Mode -->
        <link rel="stylesheet" type="text/css" href="grid/css/component.css" />
      
        <script src="grid/js/cbpViewModeSwitch.js"></script>
        <!-- Grid Switch Mode -->
        <script src="grid/list.js"></script>
        <script>
            $(function () {
                $("#main-header").load("headerMenu.php");
                $("#main-sidebar").load("sidebarMenu.php");
                $("#main-footer").load("footerMenu.php");
                $("#cbp-vm").hide();
                initSearch();
            });
            function initSearch() {
                var memberForm = $("#memberForm");
                $('#memberForm').validate({ //...................validating memberForm form starts
                    rules: {
                      name: {
                        required: true,
                      },
                      post: {
                        required: true,
                      },
                      mobile_number: {
                         required: true,
                      }

                    },
                    messages: {
                      name: {
                        required: "Please enter name."
                      },
                      post: {
                        post: true,
                      },
                      mobile_number: {
                       required: "Please enter number"
                      }
                    },

                    errorPlacement: function(error, element) {
                      var divObj=document.createElement("div");
                      divObj.setAttribute("class","error-messageColor");
                      element.parent().append(divObj);
                      error.appendTo(element.next());
                    },

                    submitHandler: function(form) {
                       addCom();
                    }
            }); //...............memberForm form validation ends
                $('.deleteComClass').click(function (evt) {
                    deleteCom(this.id);
                });
                
                $('.editMemberBtn').click(function (evt) {
                    //editMember(this.id);
                    var arrayLikeObj=this.closest("tr").getElementsByTagName("td");
                    var tdElementArr=Array.prototype.slice.call(arrayLikeObj);//converting to array object
                    $("#editCommitteMember").modal();
                    tdElementArr.forEach(function(obj){
                        $("#editCommitteMember #"+$( obj).attr("class")).val(obj.innerText);
                    });
                    $("#editCommitteMember").attr("data-userid",this.getAttribute("data-id"));

                    
                });
            }
            function addCom() {
                var name = $("#name").val();
                var post = $("#post").val();
                var mobile = $("#mobile_number").val();
                var details = {name: name, post: post, mobile_number: mobile};
                showLoader();
                var dataR = details;
                $.ajax({
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    url: getUrl(api.ADD_COM_URL),
                    data: dataR,
                }).done(function (response) {
                    hideLoader();
                    if (response.isSuccessful === true) {
                        alert("Member added successfully");
                        location.reload();
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                })
                        .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});
            }

            function deleteCom(id) {
                showLoader();
                $.ajax({
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    url: getUrl(api.DELETE_COM_URL),
                    data: {id: id},
                }).done(function (response) {
                    hideLoader();
                    if (response.isSuccessful === true) {
                      
                        alert("Member deleted successfully");
                        location.reload();
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                })
                        .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});
            }
            $("#editMemberFrom").submit(function(e){
               e.preventDefault();

               showLoader();
               var editFormObj={};
               editFormObj['userId']=$("#editCommitteMember").attr("data-userid");
               $("#editMemberFrom").serializeArray().forEach(function(obj){
                 editFormObj[obj.name]=obj.value
               }); 
                $.ajax({
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    url: getUrl(api.MEMBER_UPDATE_COM_URL),
                    data: JSON.stringify(editFormObj),
                }).done(function (response) {
                    hideLoader();
                    if (response.isSuccessful === true) {
                      $("#editCommitteMember").modal('hide');
                        alert("Member details updated successfully");
                        location.reload();
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    hideLoader();
                    alert("Server failed");
                })
                        .always(function (jqXHROrData, textStatus, jqXHROrErrorThrown) {});
                
            })
          
        </script>
</body>
</html>
