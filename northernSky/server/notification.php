
<?php
require_once('libs/dbConnection.php');
require_once('libs/GcmNotify.php');

$api_key = "AIzaSyDZXPgrv1n36FG4dqR1a8Iy864ZGClmbsE";

$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);


Class Notification{
	public function notify($request, $con, $api_key){

		$senderId = $request->request->senderId;
		$receiverId = $request->request->receiverId;
		$message = $request->request->message;
		$title = $request->request->title;
		$updatedTime = $request->request->updatedTime;
		$notificationType = $request->request->notificatioType;
		$messageType = $request->request->messageType;
		$dataUrl = $request->request->dataUrl;
		$packId = $request->request->packId;

		$gcm_message;
		if($notificationType != 1){
		    $gcmMessageSender = new GcmNotify($api_key);
		    $gcmSet =  $this->getGcmId($receiverId, $con);
		    $GLOBALS['gcm_message'] = array('title'=>$title,'text'=>$message, "time"=>$updatedTime, "messageType"=>$messageType,'url'=>$dataUrl);
		    $gcm_push = array("message"=>$GLOBALS['gcm_message']);
		    $response = $gcmMessageSender->sendNotification($gcmSet, $gcm_push);    
		}else{
			$GLOBALS['gcm_message'] = array('title'=>$title,'text'=>$message, "time"=>$updatedTime, "messageType"=>$messageType,'url'=>$dataUrl);
		}


		foreach ($receiverId as $key => $result) {
			$receiver = $receiverId[$key];
			$val = json_encode($GLOBALS['gcm_message']);
			$sql_gcm = mysqli_query($con, "INSERT INTO notifications(senderId, receiverId, message, type, transactionTime) VALUES ('$senderId', '$receiverId[$key]', '$val', '$notificationType', '$updatedTime')");

			if($notificationType != 0){
				$sql_sms = mysqli_query($con,"UPDATE sms_counter SET sentSms = sentSms+1, lastSentDate='$updatedTime' WHERE personId='$senderId' && packId = '$packId'");
			}
		}

		$response = array("resp" => "SUCCESS", "code"=>200);
		return $response;

	}

	public function getGcmId($personId, $con){
        $gcmId = array();
        foreach ($personId as $key => $value) {
        	$sql_gcm = mysqli_query($con,"SELECT gcmId FROM gcm WHERE personId='".$personId[$key]."' ");
        	 while ($rows_fetch = mysqli_fetch_assoc($sql_gcm))
		    {
		    	array_push($gcmId, $rows_fetch['gcmId']);
		    }
            
        }
         return $gcmId;
    }
}

$notify = new Notification();
$result = $notify->notify($request, $con, $api_key);



$response = json_encode($result);
echo $response;
$connection->closeConnection();

?>
