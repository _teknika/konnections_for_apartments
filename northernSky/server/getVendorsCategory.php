<?php

require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$sql = mysqli_query($con, "SELECT * FROM vendor_category");
$rows_count = mysqli_num_rows($sql);
$events = array();
if ($rows_count != 0) {
    while ($rows_fetch = mysqli_fetch_assoc($sql)) {
        array_push($events, array('id' => $rows_fetch['id'], 'category' => $rows_fetch['category']));
    }
}
$response = json_encode($events);
$connection->closeConnection();
echo $response;
?>
