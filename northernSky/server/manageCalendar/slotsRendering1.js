function getFacilityList(){
	
	$.ajax({
		   type: "GET",
		   url: "../server/getFacilityList.php",
	   }).done(function(response) {
		
		 var resultArray=JSON.parse(response);
		 resultArray.forEach(function(obj,i,fullArr){
			 console.log(obj,i);
			 $("#facilityId").append("<option  value="+obj.id+" data-charge='"+obj.charge_per_hour+"'>"+obj.facility_name+"</option>");
		 });
		 if(localStorage.facilityId){
			 
			 $("option[value="+localStorage.facilityId+"]").attr("selected","selected");
			 //$("option:not(option[value="+localStorage.facilityId+"])").attr("disabled","disabled");
			 $("#viewCalendar").click();
		 }
	   }).fail(function(jqXHR, textStatus, errorThrown) {
		   
		   alert("Server failed");
	   })
	   .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
   }
   //getting available slots from server
   function getAvailability(facilityId,calendarStart,calendarEnd){
		   console.time("Availability_Ajax");
	   $.ajax({
			   type: "POST",
			   contentType: 'application/json; charset=utf-8',
			   dataType: 'json',
			   url: "facilities/getAvailabiltyList.php",
			   data:JSON.stringify({start:calendarStart,end:calendarEnd,facilityId:facilityId}),
		   }).done(function(response) {
			   console.timeEnd("Availability_Ajax");
			   console.time("Availability");
			   var arrayList=[];
			   //
			   var calendarStartDate=new Date(calendarStart);
			   var calendarEndDate=new Date(calendarEnd);
			   var sequence = Promise.resolve();
	
			   response.forEach(function(obj,i){
					var sequence1=sequence.then(function(){
						return getPromise(obj.start,obj.end,"available","#3a87ad");
					});
			      });  
				 //arrayList=EventSlotsGeneratorFn.getEventSlotsArr(obj.start,obj.end,"available","#3a87ad");
		
			   EventSlotsGeneratorFn.resetHash();//this is a function which sets varible hash to null,this hash object is used to avoid duplicates
			   console.timeEnd("Availability");
			   hideLoader();
			   //
			   
		   }).fail(function(jqXHR, textStatus, errorThrown) {
			   hideLoader();
			   alert("Unable to proccess");
   
		   })
		   .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
   } //getting available slots ends here
   
   //getting unavailable slots from server
   function getUnAvailability(facilityId,calendarStart,calendarEnd){
   
		   console.time("UnAvailability_Ajax");
		   $.ajax({
			   type: "POST",
			   contentType: 'application/json; charset=utf-8',
			   dataType: 'json',
			   url: "facilities/getUnAvailabiltyList.php",
			   data:JSON.stringify({start:calendarStart,end:calendarEnd,facilityId:facilityId}),
		   }).done(function(response) {
			   console.timeEnd("UnAvailability_Ajax");
			   console.time("UnAvailability");
			   var arrayList=[];
			   //
			   var calendarStartDate=new Date(calendarStart);
			   var calendarEndDate=new Date(calendarEnd);
			
			   response.forEach(function(obj,i){
				 arrayList=EventSlotsGeneratorFn.unavailableSlotsArr(obj.start,obj.end,"unavailable","#ffffff");
			  });
			   console.timeEnd("UnAvailability");
			   getAvailability(facilityId,calendarStart,calendarEnd);
			  
			
			   
		   }).fail(function(jqXHR, textStatus, errorThrown) {
			   hideLoader();
			   alert("Unable to proccess");
   
		   })
		   .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
   } //getting unavailable slots ends 
   
   //get  booked slots from server
   function getBookedSlots(facilityId,start,end){
		   console.time("BookedSlots_Ajax");
		   showLoader();
		   $.ajax({
			   type: "GET",
			   contentType: 'application/json; charset=utf-8',
			   dataType: 'json',
			   url: "facilities/getBookedSlots.php?facilityId="+facilityId,
		   }).done(function(response) {
				console.timeEnd("BookedSlots_Ajax");
			  console.time("BookedSlots");	 
			  response.forEach(function(obj,i){
				EventSlotsGeneratorFn.getEventSlotsArr(obj.start,obj.end,"booked","#d23434");
			  });
			   console.timeEnd("BookedSlots");
			  getUnAvailability(facilityId,start,end);
   
		 
		   }).fail(function(jqXHR, textStatus, errorThrown) {
			   hideLoader();
			   alert("Unable to proccess");
		   })
		   .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
   } //getting booked slots ends
   
   var EventSlotsGeneratorFn=new CreateSlotsTemplate().getEventSlotsGenerator;
   //below template function 
   function CreateSlotsTemplate(){
	  
	   var hash = Object.create(null);
	   var arr=[];
	  this.getEventSlotsGenerator= {
					   resetHash:function(){
							  hash = Object.create(null);
						   },
						arrayLs:function(slot){
                           arr.push(slot);
						},
						getArrLs:function(){
                           return arr;
						},
						  getEventSlotsArr:function eventSlotsArr(startTime,endTime,title,color){
							  var end=new Date(startTime);
						   do{
							   //
							   var temp=new EventObj(end,title,color);
								if (!hash[temp['start']]) {//this if condition is to avoid duplicate object
									   hash[temp['start']] = true;
									   //this.arrayLs(temp);
									   setTimeout(function(){
										$("#fullCalendar").fullCalendar('renderEvent',temp,false);
									 },0) 
								   }
							   //
							  // arr.push(new EventObj(end,title));
							 //end.setHours(end.getHours()+1);
							 end.setMinutes(end.getMinutes()+30);
						   }
						   while(end < new Date(endTime));  
						},
						unavailableSlotsArr: function unavailableSlots(startTime,endTime,title,color){
								 var end=new Date(startTime);
							   do{
							   //
							   var temp=new EventObj(end,title,color);
								if (!hash[temp['start']]) {//this if condition is to avoid duplicate object
									   hash[temp['start']] = true;
								   }
							   //
							  // arr.push(new EventObj(end,title));
							 //end.setHours(end.getHours()+1);
							 end.setMinutes(end.getMinutes()+30);
						   }
						   while(end < new Date(endTime)); 
						}
				   }//generator ends
									
   }
   function EventObj(endTime,title,color){
					   this.start=new Date(endTime);
					   this.title=title;
					   this.backgroundColor=color;
					   this.className=title;
				   }
	var getPromise=function promise(start,end,available,color){

					return new Promise(function(resolve,reject){
				   
				  var arr=	EventSlotsGeneratorFn.getEventSlotsArr(start,end,available,color);
						if(arr){
                            resolve(start);
						}else{

						}
					   /* function resolvePromise(temp){
						  resolve(temp);
						}*/
					});
				}
   function showLoader(){
	$.busyLoadFull("show", { 
		spinner: "cube-grid"
	 });
   }
   function hideLoader(){
	$.busyLoadFull("hide", { 
		spinner: "cube-grid"
	});
   }
   