  function closeBookAppointmntPopup(){
  	$("#BookAppointmentModal").modal("hide");
  }
  function closeAppointmntDeatilPopup(){
  	$("#bookedSlotDetailModal").modal("hide");
  } 

// below currentfacilityObj is updated with its properties when user clicks next button on change of facility names dropdown
  var currentfacilityObj={
		facilityId:undefined,
		facilityName:undefined,
		chargePerHour:undefined
	}
  //
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	var currentDate = new Date();
	var cuurentDateString = JSON.stringify(currentDate); //alert(currentDate);
	var trimCurrentDate = cuurentDateString.substr(1, cuurentDateString.length - 16); 

	function displayCalender() {

		var today = new Date();
		var dayNumber = today.getDay();
		//alert("displayCalender");
		var calendarHeight = screen.availHeight - 120;
		$('#fullCalendar').fullCalendar({
			header: {
				left: 'prev',
				center: 'title',
				right: 'next'
			},
			defaultDate: $('#fullCalendar').fullCalendar('today'),
			defaultView: 'agendaWeek',
			firstDay: dayNumber,
			allDaySlot: false,
			defaultTimedEventDuration: "00:30:00",
			forceEventDuration: true,
			slotDuration: "00:30:01",
			displayEventEnd: true,
			disableDragging: true,
			editable: true,
			eventDurationEditable: false,
			dragScroll: false,
			height: calendarHeight,
			slotLabelFormat: "HH:mm",
			axisFormat: 'HH:mm',
			firstHour: 9,
			editable: false,
			
			eventRender: function(event, element) {
				element.find('.fc-content').empty();

			},
			eventOverlap: false,
			eventClick: function(calEvent, jsEvent, view) {
				if ($(this).hasClass('booked')) {
					alert("already booked");
					return false;
				}
                if($(this).hasClass('unavailable')){
                	return false;
                }
				var eventStartTime = JSON.stringify(calEvent.start);

				var eventStartTime_format = eventStartTime.substr(1, eventStartTime.length - 10);

				var date = new Date();
				//below code was allowing to bookappoinment at time like  11:40 if current time like is 12:05 
				//date.setMinutes(date.getMinutes()-60);

				
				var check1_InMillSec=new Date(calEvent.start._d).getTime();
				var check2_currentTimeInmillSec = date.getTime();
				

				if (check1_InMillSec < check2_currentTimeInmillSec) {
					showNSPopup({
						message: "Sorry! You cannot book for the past timings.",
						ButtonNames: ['Ok']
					});
					return false;
				} else {
	                 selectedEventHolder=selectedEventFn();
	                 selectedEventHolder().selectedEventObj=this;
					var selectedDateTime=$.fullCalendar.moment(calEvent.start).format('YYYY-MM-DD HH:mm:ss');
					var selectedTimePlusHalfanHour=$.fullCalendar.moment(calEvent.end).format('YYYY-MM-DD HH:mm:ss');
					var tempDateTimeObj= new Date(selectedDateTime);
					var tempVal=tempDateTimeObj.setHours(tempDateTimeObj.getHours()+1);
					var selectedTimePlusOneHour=$.fullCalendar.moment(tempVal).format('YYYY-MM-DD HH:mm:ss');
                    //extratcing only time parts to append to 'endTime' dropdown in modal
                      var selectedTime=$.fullCalendar.moment(calEvent.start).format('HH:mm');
                      var additionalHalfAnHour=$.fullCalendar.moment(calEvent.end).format('HH:mm');
                      var additionalOneHour=$.fullCalendar.moment(selectedTimePlusOneHour).format('HH:mm');
                    //
                    $("#facility_Name").attr("data-facilityid",currentfacilityObj.facilityId);
                     $("#facility_Name").val(currentfacilityObj.facilityName);
                    $("#bookingDate").val($.fullCalendar.moment(calEvent.start).format('YYYY-MM-DD'));
                    $("#bookingStartTime").attr("data-startdatetime",selectedDateTime);
                    $("#bookingStartTime").val(selectedTime);
                    $("#bookingEndTime").empty();
                    $("#bookingEndTime").append("<option data-id='1' value='"+selectedTimePlusHalfanHour +"'>"+additionalHalfAnHour+"</option>");
					if (!$(this).next().hasClass('booked')) {
					  $("#bookingEndTime").append("<option data-id='2' value='"+selectedTimePlusOneHour +"'>"+additionalOneHour+"</option>"); 
					 }
					selectedEventHolder().counter=1;
					$("#BookAppointmentModal").modal();	
				}
			},
			viewRender: function(currentView) {
				var minDate = moment(),
					maxDate = moment().add(2, 'weeks');

				// Past
				if (minDate >= currentView.start && minDate <= currentView.end) {
					getCalenderDataForCurrentDay();
					$(".fc-prev-button").prop('disabled', true);
					$(".fc-prev-button").addClass('fc-state-disabled');
				} else {
					$(".fc-prev-button").removeClass('fc-state-disabled');
					$(".fc-prev-button").prop('disabled', false);
				}
			},
			events: function(start, end, timezone, callback) {
			 	console.log(start, end, timezone);
				 var arr=getBookedSlots($("#facilityId").val(),start,end,callback);						
			 },
		
		});



		if ($('#fullCalendar').children().length != 0) { // it will remove old events and will add new events
			//alert();
			$('#fullCalendar').fullCalendar('removeEvents');
			var eventsArray=[];
			$('#fullCalendar').fullCalendar('addEventSource', eventsArray);
			//$('#fullCalendar').fullCalendar('renderEvent');
		}


		//$('#output').trigger('create');
	}

	
	function addCalanderEvent(response) {
		$('#fullCalendar').fullCalendar('addEventSource', response);
	}

	function getCalenderDataForCurrentDay() {
		var currentDate = new Date();
		var cuurentDateString = JSON.stringify(currentDate); //alert(currentDate);
		var trimCurrentDate = cuurentDateString.substr(1, cuurentDateString.length - 16);


	}
	function selectedEventFn(){ //this function set or gets value of "selectedEventObj" property
	   var selectedEventInfo={
	   	      selectedEventObj:null,
	          counter:0
	   }
	  return function(){
             return selectedEventInfo;
	   }
	  	
	  
	}
	$(function(){
		getFacilityList(initializeCalendar);

		$("#proceedToBookBtn").click(function(event) {
			event.preventDefault();
			var bookedDate=$("#bookingDate").val();
		    //modal generation 
		    $("#owner_name").val(familyHeadObj.firstName);
		 	$("#mobile_no").val(familyHeadObj.mobileNumber);
		 	$("#block").val(familyHeadObj.blockName);
		 	//$("#flat").val(familyHeadObj.flatNo);
            $("#bookedSlotDetailModal #flat").empty();
		 	$("#bookedSlotDetailModal #flat").append('<option value='+familyHeadObj.flatNo+'>'+familyHeadObj.flatNo+'</option>');
		 	familyHeadObj.flatsArray.forEach(function(ele,i){
               $("#bookedSlotDetailModal #flat").append('<option value='+ele+'>'+ele+'</option>');
		 	});
		 	$("#facility_name").val(currentfacilityObj.facilityName)
		 	$("#bookDate").val(bookedDate);
            $("#bookTiming").val(leadingZero($("#bookingStartTime").attr("data-startdatetime"))+" - "+leadingZero($("#bookingEndTime").val()));		
			var selectedTimeDetail={
				start:$("#bookingStartTime").attr("data-startdatetime"),
				end:$("#bookingEndTime").val()
			}
			var timeDiff=calculateBookedSlotCharge(selectedTimeDetail);
			$("#slotCharge").val(currentfacilityObj.chargePerHour * timeDiff);
		    $("#bookedSlotDetailModal").modal();
		    // 			
		}); 
		$(document).on("change","#facilityId",function(e){
			var facilityType = $('option:selected', this).attr('data-facilitytype');//facilitytype 0=>single 1=>multi
				initializeCalendar();
		});
		
		$(document).on("click","#bookConfirm",function(e){
			e.preventDefault();
        var bookDate=$("#bookDate").val();
		var weekId=new Date(bookDate).getDay();	
		var ownerId=familyHeadObj .parentId !=-1 ? familyHeadObj.parentId:familyHeadObj.id; 
		 currentAppointmentObj={
				        personId:familyHeadObj.id,
						facilityId:$("#facility_Name").attr("data-facilityid"),
						facilityName:$("#facility_Name").val(),
						mobileNumber:familyHeadObj .mobileNumber,//familyHeadObj is global object 
						name:familyHeadObj .firstName,
						lastName:familyHeadObj .lastName,
						parentId:familyHeadObj .parentId,
						ownerId:ownerId,
						perticular:"facility",
						flat:$("#flat").val(),
						bookedDate:bookDate,
						weekId:weekId,
						start:$("#bookingStartTime").attr("data-startdatetime"),
						end:$("#bookingEndTime").val(),
					}
		  currentAppointmentObj.chargePerHour=$("#slotCharge").val();
		  currentAppointmentObj.isCancel=false;
          BookAppointment(currentAppointmentObj);
		});
		$("#bookingEndTime").change(function(e){
			var optionSelected = $("option:selected", this)[0];
			selectedEventHolder().counter=$(optionSelected).attr("data-id");
		});
	});
	function initializeCalendar() {
		currentfacilityObj.facilityId=$("#facilityId").val();
		currentfacilityObj.facilityName=$("option[value='"+$("#facilityId").val()+"']")[0].innerText;
		currentfacilityObj.chargePerHour=$("option[value='"+$("#facilityId").val()+"']").attr("data-charge");
		
		$("#fullCalendar").fullCalendar( 'destroy');
		displayCalender();
	 };
	function BookAppointment(appointmentObj){
		  showLoader();	
	       $.ajax({  
		        type: "POST",
	            contentType: 'application/json; charset=utf-8',
	            dataType: 'json',
	            data:JSON.stringify(appointmentObj),
		        url: "facilities/addBookedSlot.php",
		    }).done(function(response) {
		     	
              if(response.isSuccessful || response.isSuccessful==null){
	    	  selectedEventHolder().selectedEventObj.style.backgroundColor="rgb(210, 52, 52)";
			  $(selectedEventHolder().selectedEventObj).removeClass("available").addClass("booked");
			  while(selectedEventHolder().counter > 1){
                  --selectedEventHolder().counter;
                  selectedEventHolder().selectedEventObj.nextSibling.style.backgroundColor="rgb(210, 52, 52)";
				  $(selectedEventHolder().selectedEventObj.nextSibling).removeClass("available").addClass("booked"); 
				  selectedEventHolder().selectedEventObj=selectedEventHolder().selectedEventObj.nextSibling;
	    	  }
	    	  $("#bookedSlotDetailModal").modal("hide");	
	    	  $("#BookAppointmentModal").modal("hide");
	    	  $("#paymentSuccesModal").attr('data-bookedId',response.id);
	    	  $("#paymentSuccesModal").modal();
	    	 }else{
	    	 	
	    	 	$("#paymentFailureModal").modal();
	    	 }
	    	  hideLoader();	
		    }).fail(function(jqXHR, textStatus, errorThrown) {
		        hideLoader();	
		        alert("Unable to proccess");
		    })
		    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
	}
	function leadingZero(datetimeObj){
	    var datetimeObj=new Date(datetimeObj); 	    
	    var hourpart= datetimeObj.getHours() < 10 ? "0"+datetimeObj.getHours() : datetimeObj.getHours();
	    var minutepart= datetimeObj.getMinutes() < 10 ? "0"+datetimeObj.getMinutes() : datetimeObj.getMinutes();
	    var ampm;
	    datetimeObj.getHours() <= 12 ? ampm="am":ampm="pm";          
	  return hourpart+":"+minutepart+ampm;
  
	} 
	function calculateBookedSlotCharge(currentAppointmentObj){
		var milliSecDiff=new Date(currentAppointmentObj.end)-new Date(currentAppointmentObj.start);
        var diffInMin=(milliSecDiff)*(1/60)*(1/1000);//this return minute diiference
        return (diffInMin/60);//converting to hour
	}

// 	
	$(".payLater-btn").click(function(event) {
		 $("#paymentSuccesModal").modal("hide");
	});
	$(".payNow-btn").click(function(event) {
		 $("#paymentSuccesModal").modal("hide");
		 var additionalParams="facilityName="+currentfacilityObj.facilityName+"&facilityHeadId="+familyHeadObj.parentId+"&bookedBy="+familyHeadObj.id+"&BookedId="+$("#paymentSuccesModal").attr('data-bookedId');
		window.payuWindowObj= window.open("payu/PayUMoney_form.php?"+additionalParams);
	});




	

