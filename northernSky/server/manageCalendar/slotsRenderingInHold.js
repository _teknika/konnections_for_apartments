    function getFacilityList(){
	
	 $.ajax({
        type: "GET",
        url: "../server/getFacilityList.php",
    }).done(function(response) {
     
      var resultArray=JSON.parse(response);
      resultArray.forEach(function(obj,i,fullArr){
      	console.log(obj,i);
      	$("#facilityId").append("<option  value="+obj.id+" data-charge='"+obj.charge_per_hour+"'>"+obj.facility_name+"</option>");
      });
      if(localStorage.facilityId){
      	
      	$("option[value="+localStorage.facilityId+"]").attr("selected","selected");
      	//$("option:not(option[value="+localStorage.facilityId+"])").attr("disabled","disabled");
      	$("#viewCalendar").click();
      }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        
        alert("Server failed");
    })
    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
}
    function getAvailability(facilityId,calendarStart,calendarEnd,callback){
    	console.time("beforeAjax");
        $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "facilities/getAvailabiltyList.php",
	        data:JSON.stringify({start:calendarStart,end:calendarEnd,facilityId:facilityId}),
	    }).done(function(response) {
	    	console.timeEnd("beforeAjax");
	    	console.time("start");
	        var arrayList=[];
	        //
            var calendarStartDate=new Date(calendarStart);
            var calendarEndDate=new Date(calendarEnd);	
			var PromisesArr=response.map(function(obj,i){
					getPromise(obj.start,obj.end,"available","#3a87ad");
				  }); 
			Promise.all(PromisesArr).then(function(ele){
					console.log("array..."+ele);
					//callback(getEventSlotsGeneratorFn.getEvents());
				});
	        getEventSlotsGeneratorFn.resetHash();//this is a function which sets varible hash to null,this hash object is used to avoid duplicates
	        console.timeEnd("start");
	        hideLoader();
	        //
	        
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	        hideLoader();
	        alert("Unable to proccess");

	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
      function getUnAvailability(facilityId,calendarStart,calendarEnd,callback){

    	console.time("beforeAjax");
        $.ajax({
	        type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "facilities/getUnAvailabiltyList.php",
	        data:JSON.stringify({start:calendarStart,end:calendarEnd,facilityId:facilityId}),
	    }).done(function(response) {
	    	console.timeEnd("beforeAjax");
	    	console.time("start");
	        var arrayList=[];
	        //
            var calendarStartDate=new Date(calendarStart);
            var calendarEndDate=new Date(calendarEnd);
	
			response.forEach(function(obj,i){
				 var sequence1=sequence.then(function(){
					  getPromise(obj.start,obj.end,"available","#3a87ad");
				 });
			   });
	        getAvailability(facilityId,calendarStart,calendarEnd,callback);
	        console.timeEnd("start");
	     
	        
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	        hideLoader();
	        alert("Unable to proccess");

	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
  function getBookedSlots(facilityId,start,end,callback){
        showLoader();
        $.ajax({
	        type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
	        url: "facilities/getBookedSlots.php?facilityId="+facilityId,
	    }).done(function(response) {
	       response.forEach(function(obj,i){
             getEventSlotsGeneratorFn.getEventSlotsArr(obj.start,obj.end,"booked","#d23434");
	       });
	       getUnAvailability(facilityId,start,end,callback);

	  
	    }).fail(function(jqXHR, textStatus, errorThrown) {
	        hideLoader();
	        alert("Unable to proccess");
	    })
	    .always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) {}); 
  }
var getEventSlotsGeneratorFn=new createSlotsTemplate().getEventSlotsGenerator;
function createSlotsTemplate(){
   
	var hash = Object.create(null);
	var events=[];
   this.getEventSlotsGenerator= {
                    resetHash:function(){
                           hash = Object.create(null);
                        },
   					getEventSlotsArr:function eventSlotsArr(startTime,endTime,title,color){
						   var end=new Date(startTime);
							do{
								//
								var temp=new EventObj(end,title,color);
								 if (!hash[temp['start']]) {//this if condition is to avoid duplicate object
										hash[temp['start']] = true;															
											//$("#fullCalendar").fullCalendar('renderEvent',temp,false);																													
								//
								  events.push(temp);
								 }
							  //end.setHours(end.getHours()+1);
							  end.setMinutes(end.getMinutes()+30);
							
						}
							while(end < new Date(endTime));

					//	});
					     
					 },
					 unavailableSlotsArr: function unavailableSlots(startTime,endTime,title,color){
					 	 	var end=new Date(startTime);
					        do{
					    	//
					    	var temp=new EventObj(end,title,color);
					    	 if (!hash[temp['start']]) {//this if condition is to avoid duplicate object
							        hash[temp['start']] = true;
							    }
					    	//
					       // arr.push(new EventObj(end,title));
					      //end.setHours(end.getHours()+1);
					      end.setMinutes(end.getMinutes()+30);
					    }
					    while(end < new Date(endTime)); 
					 },
					 getEvents:function(){
						 return events;
					 }
					}
  function EventObj(endTime,title,color){
	   this.start=new Date(endTime);
	   this.title=title;
	   this.backgroundColor=color;
	   this.className=title;
	}					 
}
var getPromise=function promise(start,end,available,color){

	return new Promise(function(resolve,reject){
   
  var arr=	getEventSlotsGeneratorFn.getEventSlotsArr(start,end,available,color);
		
			resolve(start);
		
	   /* function resolvePromise(temp){
		  resolve(temp);
		}*/
	});
}
function showLoader(){
 $.busyLoadFull("show", { 
 	spinner: "cube-grid"
  });
}
function hideLoader(){
 $.busyLoadFull("hide", { 
 	spinner: "cube-grid"
 });
}
