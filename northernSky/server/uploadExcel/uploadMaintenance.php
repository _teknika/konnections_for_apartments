<?php
 require_once('../addMaintenanceAmount.php');
  class uploadMaintenance{
      var $con=null;
      function __construct(){
        require_once('../libs/dbConnection.php');
        $connection = new dbconnection();
        $this->con = $connection->connectToDatabase();
      }
      function isUserID_flatExists($name,$flatId,$amount,$uploadingFor){
        $selectUserIDqry="SELECT DISTINCT(p.id),p.parentId,p.mobileNumber FROM person p LEFT JOIN person_flats pf on p.id=pf.personId WHERE p.parentId=-1 and (p.flatNo='$flatId' OR pf.flat_number='$flatId');";
        $flatOwnerInfo=mysqli_query($this->con, $selectUserIDqry);
        if ($flatOwnerInfo->num_rows > 0) {
            // output data of each row           
            while($row = $flatOwnerInfo->fetch_assoc()) {
                //creating transaction object to process further
                $txDetailObj=new stdClass();
                $txDetailObj->ownerId=$row['id'];
                //extra detail
                $txDetailObj->personId=$row['id']; 
                $txDetailObj->parentId=$row['parentId']; 
                // extra detail ends              
                $txDetailObj->amount=$amount;
                $txDetailObj->flat=$flatId;
                $txDetailObj->updated_by=2837;
                $txDetailObj->perticular=$uploadingFor;
                $txDetailObj->mobileNumber=$row['mobileNumber'];
                $txDetailObj->name=$name;
                //json_encode($txDetailObj)
                $mainteanceObj=new Maintenance(json_encode($txDetailObj));
                $mainteanceObj->connectDataBase();
                $res=$mainteanceObj->deductFromMaintainanceTB();
                $isSMSent=$mainteanceObj->sendSMS($txDetailObj);
                return $res;
            }
                       
        }
        else{
            return false;
        }  
      }
      public function exportAction($folderName,$fileName) {
        header('Content-Description: File Transfer');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=\"".basename($folderName.$fileName)."\"");
        header("Content-Transfer-Encoding: binary");
        header("Expires: 0");
        header("Pragma: public");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Length: ' . filesize($folderName.$fileName)); //Remove

       

        readfile($folderName.$fileName);
        exit();
    }
  }
?>