
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$basicInfo = $req->basicInfo;
$qry="";
Class UpdateContacts{

	public function updateBasicIfo($basicInfo, $con){

		// Basic Info
		$parentid = $basicInfo->parentId;
		$ishead = $basicInfo->ishead;
		$orgId = $basicInfo->orgId;
		$role = $basicInfo->orgId;
		$relationwithhead = $basicInfo->relationwithhead;
		$firstname = $basicInfo->firstname;
		$firstname = mysqli_real_escape_string($con, $firstname);//removed special charecters
		$lastname = $basicInfo->lastname;
		$lastname = mysqli_real_escape_string($con, $lastname);//removed special charecters

		$mobilenumber = $basicInfo->mobilenumber;
        $altnumber = $basicInfo->altnumber;
		$gender = $basicInfo->gender;
		$dateofmarriage = $basicInfo->dateofmarriage;
	    $emailId=$basicInfo->emailId ;
        $altMail = $basicInfo->altMail;
		$blockName=$basicInfo->blockName ;
		
		$dob=$basicInfo->dob ;
		$user_id = $basicInfo->userId;

		$privacyMask =json_encode( $basicInfo->privacyMask);
		$query = "UPDATE person SET ";
		$query = $query." orgId = '$orgId',parentId ='$parentid',firstName='$firstname',lastName='$lastname',isHead='$ishead',gender='$gender',mobileNumber='$mobilenumber',alt_number='$altnumber', dateOfMarriage='$dateofmarriage', role='$role', relationWithHead='$relationwithhead',emailId='$emailId',alt_mail='$altMail',blockName='$blockName',dob='$dob',privacy_mask='$privacyMask' ";
		// if(!empty($dateofmarriage)){
		// 	$query = $query.",dateOfMarriage='$dateofmarriage'";
		// } else{
		// 	$query = $query.",dateOfMarriage=NULL";
		// }
		$query = $query." WHERE id=$user_id";
        $GLOBALS['qry']=$query;
		$sql_basic = mysqli_query($con,$query);
		
		 return $sql_basic;
	}
}

$user_id = $basicInfo->userId;

$contactsObj = new UpdateContacts();
$basicRes = $contactsObj->updateBasicIfo($basicInfo, $con);



if($basicRes){
	$isSuccessful = true;
	$error="";
}else{
	$isSuccessful = false;
	$error=mysqli_error($con);
}

$response = array('isSuccessful'=> $isSuccessful , 'userId'=> $user_id,'error'=>$error,'qry'=>$qry);
$response = json_encode($response);
echo $response;
$connection->closeConnection();
?>
