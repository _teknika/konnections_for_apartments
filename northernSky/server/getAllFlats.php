<?php

require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$flatqry="SELECT distinct(flatNo) as flat FROM person where flatNo IS NOT NULL AND isHead!=2 AND TRIM(flatNo) <> '' order by LENGTH(flatNo),flatNo";
$personSql = mysqli_query($con, $flatqry);
$data = array();
$rows_count=mysqli_num_rows($personSql);

if ($rows_count > 0) {
    while ($rows_fetch = mysqli_fetch_assoc($personSql)) {
                $info = $rows_fetch;	
		        array_push($data, $info['flat']);
    }
}
$personFlatSql = mysqli_query($con, "SELECT distinct(flat_number) as flat FROM person_flats order by LENGTH(flat_number),flat_number");
$rows_count = mysqli_num_rows($personFlatSql);
if ($rows_count != 0) {
    while ($rows_fetch = mysqli_fetch_assoc($personFlatSql)) {
                $info = $rows_fetch;	
		        array_push($data, $info['flat']);
    }
}
$response = json_encode($data);
$connection->closeConnection();
echo $response;
?>