
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

Class Contacts{

	public function getPersonalInfo($request, $con){
		$orgId = $request->request->orgId;
		$limit = $request->request->count;
		$pageNo = $request->request->pageNo;
		$userId = $request->request->userId;
		
		
		$sql_user = mysqli_query($con, "SELECT * from person WHERE id = '".$userId."'"); 
		$rows_count_user = mysqli_num_rows($sql_user);
		if($rows_count_user != 0){
				$query = "SELECT * FROM person WHERE orgId='".$orgId."' ";

				$query_count = mysqli_query($con, "SELECT id FROM person WHERE orgId='".$orgId."'");
				$rowsTotal_count = mysqli_num_rows($query_count);				
				

				if($limit != -1){

					$page = ($pageNo-1)*$limit;
					$query = $query." LIMIT $page,$limit";
				}

				
				$sql = mysqli_query($con, $query);

				$rows_count = mysqli_num_rows($sql);

				$data=array();
				if($rows_count!=0){
					while ($rows_fetch = mysqli_fetch_assoc($sql))
					{
						$contacts = $this->getAddressInfo($rows_fetch['id'], $con);	
						$info = $rows_fetch;
						$info['contacts'] = $contacts;
						array_push($data, $info);
						$isSuccessful = true;
						$code = 200;
						$totalCount = $rowsTotal_count;
					}
				}else{
					$isSuccessful = true;
					$code = 202;
					$totalCount = $rowsTotal_count;
				}

				$response = array("resp"=>$data, "code"=>$code, "isSuccessful"=>$isSuccessful, "totalCount"=>$totalCount);
				return $response;
		}else{
				$data=array();
				$response = array("resp"=>$data, "code"=>202, "isSuccessful"=>false, "totalCount"=>0);
				return $response;
		}
		


		
	}




	public function getAddressInfo($personId, $con){
		$sql_address = mysqli_query($con,"SELECT * FROM contact WHERE personId='".$personId."' ");
		$rows_count = mysqli_num_rows($sql_address);
		$address=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql_address))
		    {
		    	$cityId = $rows_fetch['cityId'];
		    	$sql_city = mysqli_query($con, "SELECT * FROM city WHERE id = '".$cityId."'");
		    	while ($rows_fetch1 = mysqli_fetch_assoc($sql_city))
			    {
			    	$cityName = $rows_fetch1['name'];
		    		$rows_fetch['city'] = $cityName;
			    }
			    array_push($address, $rows_fetch);
		    }
		    return $address;
		}
	}

}

$contactsObj = new Contacts();
$contacts = $contactsObj->getPersonalInfo($request, $con);

$response = json_encode($contacts);
echo $response;
$connection->closeConnection();

?>
