<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$query='SELECT v.*,vc.category as category FROM vendor v INNER JOIN vendor_category vc ON v.category=vc.id ';
$sql=mysqli_query($con,$query);
$rows_count=mysqli_num_rows($sql);
$vendors=array();
if($rows_count!=0){
while($rows_fetch=mysqli_fetch_assoc($sql)) {
    array_push($vendors, array('name' => $rows_fetch['name'], 'email' => $rows_fetch['email'], 'person_contact' => $rows_fetch['person_contact'],'alt_number' => $rows_fetch['alt_number'], 'website_link' => $rows_fetch['website_link'], 'dealsIn' => $rows_fetch['dealsIn'], 'address' => $rows_fetch['address'],'category' => $rows_fetch['category'],'id'=> $rows_fetch['id']));
}
}
$response=json_encode($vendors);
$connection->closeConnection();
echo $response;
?>