<?php
header('Content-Type: application/json; charset=utf-8');
header("Access-Control-Allow-Origin: *");
ob_start();
session_start();

require_once('libs/dbConnection.php');

$connection = new dbconnection();
$con        = $connection->connectToDatabase();

// API access key from Google API's Console
define('API_ACCESS_KEY', 'AIzaSyDZXPgrv1n36FG4dqR1a8Iy864ZGClmbsE');

$gcmMessage = file_get_contents("php://input");
$parsedMsg  = json_decode($gcmMessage);

$result = androidPush($con, $parsedMsg);
$r = json_decode($result);
$iosRes = applePush($con, $parsedMsg);
//$r->success= $r->success + $iosRes->success;
//$r->failure= $r->failure + $iosRes->failure;
echo json_encode($r);
//echo json_encode($iosRes);



//Code for Android push...
function androidPush($con, $parsedMsg)
{
    $registrationIds = array();
    
    //Test
    if ($parsedMsg->isTest == true) {
        $id  = $parsedMsg->personId;
        $sql = "select g.gcmId from gcm g where g.personId='$id' AND g.android=1";
        
        $result          = mysqli_query($con, $sql);
        $registrationIds = array();
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($registrationIds, $row['gcmId']);
        }
    }
    
    //Live
    else {
        //$memberType = $parsedMsg->memberType;
        $sql= "select g.gcmId from gcm g where g.gcmId!='' AND g.android=1";
        
        $result= mysqli_query($con, $sql);
        $registrationIds = array();
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($registrationIds, $row['gcmId']);
        }
        $notification_msg=mysqli_real_escape_string($con, $parsedMsg->message);
        $notification_title=mysqli_real_escape_string($con, $parsedMsg->title);
        $insQuery = "INSERT INTO `notifications` (`senderId`, `message`,`title`,`type`) VALUES ($parsedMsg->personId, '$notification_msg', '$notification_title',0);";
        $result   = mysqli_query($con, $insQuery);
        $insertedId = mysqli_insert_id($con);
        $selectQryForDate="select g.transactionTime from notifications g where g.id='$insertedId'";
        $QryForDateResult=mysqli_query($con, $selectQryForDate);
    
           if (mysqli_num_rows($QryForDateResult) > 0) {
                // output data of each row
                while($row = mysqli_fetch_assoc($QryForDateResult)) {
                     $parsedMsg->transactionTime =$row["transactionTime"];
             }
         }
        
    } //else ends.
    
    $msg = array(
        'message' => json_encode($parsedMsg),
        'title' => '',
        'subtitle' => '',
        'tickerText' => '',
        'vibrate' => 1,
        'sound' => 1,
        'largeIcon' => 'large_icon',
        'smallIcon' => 'small_icon',
    );
    

    if(count($registrationIds) == 0){
    	return json_encode(array('success' => 0, 'failure'=>0));
    }


    $fields = array(
        'registration_ids' => $registrationIds,
        'data' => $msg
    );
    
    $headers = array(
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    );
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);
    
    return $result;
} //Android push function ends




//Code for Apple push...
function applePush($con, $parsedMsg)
{
    $registrationIds = array();
    
    //Test
    if ($parsedMsg->isTest == true) {
        $id  = $parsedMsg->personId;
        $sql = "select g.gcmId from gcm g where g.personId='$id' AND g.android=0";
        
        $result          = mysqli_query($con, $sql);
        $registrationIds = array();
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($registrationIds, $row['gcmId']);
        }
    }
    
    //Live
    else {
       // $memberType = $parsedMsg->memberType;
        $sql= "select g.gcmId from gcm g where g.gcmId!='' AND g.android=0";
        
        $result= mysqli_query($con, $sql);
        $registrationIds = array();
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($registrationIds, $row['gcmId']);
        }
        
    } //else ends.
    
    $msg = array(
        'message' => $parsedMsg,
        'title' => '',
        'subtitle' => '',
        'tickerText' => '',
        'vibrate' => 1,
        'sound' => 1,
        'largeIcon' => 'large_icon',
        'smallIcon' => 'small_icon'
    );
    


    if(count($registrationIds) == 0){
        return (object) array('success' => 0, 'failure'=>0);
    }


    $fields = array(
        'cert'=>"aps_sindhi.p12",
        'registration_ids' => $registrationIds,
        'data' => $msg
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://www.getfieldx.com/apn/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);    
    return (object) array('success' => $result, 'failure'=> (count($registrationIds)-$result));
   // return (object) array('success' => 0, 'failure'=>0);
} //push function ends







?>