<?php

require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$sql = mysqli_query($con, "SELECT * FROM events order by datetime DESC");
$rows_count = mysqli_num_rows($sql);
$events = array();
if ($rows_count != 0) {
    while ($rows_fetch = mysqli_fetch_assoc($sql)) {
        array_push($events, array('title' => $rows_fetch['title'], 'details' => $rows_fetch['details'], 'datetime' => $rows_fetch['datetime'], 'name' => $rows_fetch['name'], 'type' => $rows_fetch['type'], 'location' => $rows_fetch['location'], 'photos' => $rows_fetch['photos']));
    }
}

$connection->closeConnection();

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>image gallery</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<script src="../admin/js/utility/jQuery.loadScroll.js"></script>
</head>
 <style type="text/css">
   .image-class{
    width:100%;height:15vh !important;
   }
 </style>
<body>

<div class="container gallery-container">
 <div class="row" style="padding: 15px">
  
        <?php
          foreach($events as $item ) {
           echo  '<div class="col-xs-5 col-md-4">';
             echo '<div class="thumbnail">';
              echo '<a href="#" target="_blank">';

               echo '<img data-src="'.$item['photos'].'" src="http://placehold.it/600x350&text=loading" alt="" class="img-responsive image-class" >';
                echo  '<div class="caption hide"> <p>'.$item['title'].'</p></div></a></div></div>'; 
          }
         ?>
          
       
      
  </div>
</div>


<script>
$(function() {  
    // Custom fadeIn Duration
    $('img').loadScroll(4000);
    inview = images.filter(function() {
                
                var a = $window.scrollTop(),
                    b = $window.height(),
                    c = $(this).offset().top,
                    d = $(this).height();
                    
                return c + d >= a && c <= a + b;
                
            });
            
            loaded = inview.trigger('loadScroll');
            images = images.not(loaded);

});
</script>


</body>
</html>
