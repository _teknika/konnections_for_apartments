
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$sql = mysqli_query($con,"SELECT * FROM banners");
$rows_count = mysqli_num_rows($sql);
$banners=array();
if($rows_count!=0){
    while ($rows_fetch = mysqli_fetch_assoc($sql))
    {
        array_push($banners, array('bannerPath' => $rows_fetch['bannerPath'], 'description'=>$rows_fetch['description']));
    }
}
$response = json_encode($banners);
echo $response;
$connection->closeConnection();

?>
