
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

Class ContactInfo{

	public function getPersonalInfo($request, $con){
		$orgId = $request->request->orgId;
		$userId = $request->request->userId;


		$query = "SELECT * FROM person WHERE orgId='".$orgId."' && id='".$userId."' ";
		
		$sql = mysqli_query($con, $query);

		$rows_count = mysqli_num_rows($sql);

		$data=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql))
		    {
		    	//$contacts = $this->getAddressInfo($rows_fetch['id'], $con);	
		    	//$members = $this->getMembersInfo($rows_fetch['id'], $con, $orgId);	
		    	$info = $rows_fetch;	
		    	//$info['contacts'] = $contacts;
		    	//$info['members'] = $members;
		        array_push($data, $info);
		        $isSuccessful = true;
		        $code = 200;
		    }
		}else{
			$isSuccessful = true;
		    $code = 202;
		}
        $faltsArray=array();
        $personFlatsQuery = "SELECT flat_number FROM person_flats WHERE  personId='".$userId."' ";
		
		$sql_res = mysqli_query($con, $personFlatsQuery);

		$rows_cont = mysqli_num_rows($sql_res);

        if($rows_cont!=0){
            while($rows_fetch = mysqli_fetch_assoc($sql_res)){
               array_push($faltsArray, $rows_fetch);
            }
        }
		$response = array("resp"=>$data, "code"=>$code, "isSuccessful"=>$isSuccessful,"flatsArray"=>(object)$faltsArray);
		return $response;
	}




	public function getAddressInfo($personId, $con){
		$sql_address = mysqli_query($con,"SELECT * FROM contact WHERE personId='".$personId."' ");
		$rows_count = mysqli_num_rows($sql_address);
		$address=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql_address))
		    {	
		    	$cityId = $rows_fetch['cityId'];
		    	$sql_city = mysqli_query($con, "SELECT * FROM city WHERE id = '".$cityId."'");
		    	while ($rows_fetch1 = mysqli_fetch_assoc($sql_city))
			    {
			    	$cityName = $rows_fetch1['name'];
		    		$rows_fetch['cityName'] = $cityName;
			    }
			    array_push($address, $rows_fetch);
		    	
		    }
		    return $address;
		}
	}

	public function getMembersInfo($personId, $con, $orgId){
		$sql_members = mysqli_query($con,"SELECT * FROM person WHERE parentId='".$personId."' && orgId='".$orgId."' ");
		$rows_count = mysqli_num_rows($sql_members);
		$members=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql_members))
		    {
		    	array_push($members, $rows_fetch);
		    }
		    return $members;
		}
	}

}

$contactsObj = new ContactInfo();
$contacts = $contactsObj->getPersonalInfo($request, $con);

$response = json_encode($contacts);
echo $response;
$connection->closeConnection();

?>
