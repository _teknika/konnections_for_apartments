<?php

require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$sql = mysqli_query($con, "SELECT e.*,FROM_UNIXTIME(e.start_time/1000,'%h:%i %p') as startTime,FROM_UNIXTIME(e.end_time/1000,'%h:%i %p') as endTime FROM events e order by e.event_date");
$rows_count = mysqli_num_rows($sql);
$events = array();
if ($rows_count != 0) {
    while ($rows_fetch = mysqli_fetch_assoc($sql)) {
        array_push($events, array('id' => $rows_fetch['id'],'title' => $rows_fetch['title'], 'details' => $rows_fetch['details'], 'event_date' => $rows_fetch['event_date'], 'name' => $rows_fetch['name'], 'type' => $rows_fetch['type'], 'location' => $rows_fetch['location'], 'photos' => $rows_fetch['photos'],'startTime' => $rows_fetch['startTime'],'endTime' => $rows_fetch['endTime']));
    }
}
$response = json_encode($events);
$connection->closeConnection();
echo $response;
?>