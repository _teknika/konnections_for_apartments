<?php
session_start();
$GLOBALS['timeout'] = 15*60;
//$_SESSION['timeout'] = 2*10;
if (!isset($_SESSION['userId']) || (trim($_SESSION['userId']) == '')) {
	header("Location: logout.php");
    exit();
}
if (time() - $_SESSION['last_activity'] > $timeout) {
	header("Location: logout.php");
    exit();
} else {

    session_regenerate_id();
    $_SESSION['last_activity'] = time();
    session_write_close();
}

?>
