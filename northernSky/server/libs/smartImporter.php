<?php
function getRelativePath($filePath){
    $currentPath = getcwd();
    $currentPathSegment = explode('/', $currentPath);
    $filePathSegment = explode('_',$filePath);
    $projectName = $filePathSegment[0];
    $len = count($currentPathSegment);
    for($i=0;(strcmp($currentPathSegment[0],$projectName)!=0 )&& ($i<$len);$i++){
       array_shift($currentPathSegment);
    }
    if(count($filePathSegment)==0|| count($currentPathSegment)==0){
        return NULL;
    }
    $len = (count($filePathSegment)<count($currentPathSegment))?count($filePathSegment):count($currentPathSegment);
    for($i=0;(strcmp($filePathSegment[0],$currentPathSegment[0])==0) && ($i<$len);$i++){
       array_shift($filePathSegment);
       array_shift($currentPathSegment);
    }
    $relativePath = ".";
    for($i=0;$i<count($currentPathSegment);$i++){
        $relativePath=$relativePath.".";
    }
    
    for($i=0;$i<count($filePathSegment);$i++){
        $relativePath = $relativePath."/".$filePathSegment[$i];
    }
    return $relativePath;   
}


function importFile($filePath){
    require_once getRelativePath($filePath); 
}