<?php



class GcmNotify {
    private $apiKey;
    private $header;

    function __construct($apiKey) {
        $this->apiKey = $apiKey;
        $this->header = array("Content-Type:" . "application/json", "Authorization:" . "key=" . $this->apiKey);
    }

    function sendNotification($registrationIdsArray, $messageData) {
        $data = array(
            'data' => $messageData,
            'registration_ids' => $registrationIdsArray
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $response = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}




?>
