
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$request = $req;

Class UpdateContacts{

	public function updateBasicIfo($request, $con){

		// Basic Info
		$firstname = $request->basicInfo->firstnameR;
		$firstname = mysqli_real_escape_string($con, $firstname);//removed special charecters
		$lastname = $request->basicInfo->lastnameR;
		$lastname = mysqli_real_escape_string($con, $lastname);//removed special charecters
		$mobilenumber = $request->basicInfo->mobilenumberR;
		$altNumber = $request->basicInfo->altNumber;
		$dob = $request->basicInfo->dob;
	    $gender = $request->basicInfo->genderR;
		$emailId = $request->basicInfo->emailId;
		$altemailId = $request->basicInfo->altemailId;
		
		$dateofmarriage = $request->basicInfo->dateofmarriageR;
		
		$user_id = $request->basicInfo->userId;

		$query = "UPDATE person SET ";
		$query = $query." firstName='$firstname',lastName='$lastname',emailId='$emailId',alt_mail='$altemailId',gender='$gender',mobileNumber='$mobilenumber',alt_number='$altNumber',dob='$dob', dateOfMarriage='$dateofmarriage'";
		// if(!empty($dateofmarriage)){
		// 	$query = $query.",dateOfMarriage='$dateofmarriage'";
		// } else{
		// 	$query = $query.",dateOfMarriage=NULL";
		// }
		$query = $query." WHERE id=$user_id";

		$sql_basic = mysqli_query($con,$query);

		return $sql_basic;
	}

}

$user_id = $request->basicInfo->userId;

$contactsObj = new UpdateContacts();
$basicRes = $contactsObj->updateBasicIfo($request, $con);



if(($basicRes)){
	$isSuccessful = true;
}else{
	$isSuccessful = false;
}

$response = array('isSuccessful'=> $isSuccessful , 'userId'=> $user_id);
$response = json_encode($response);
echo $response;
$connection->closeConnection();
?>
