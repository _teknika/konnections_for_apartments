
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$request = $req->request;
$qry="";
Class UpdateContacts{

	public function updateBasicIfo($request, $con){

		// Basic Info
		$parentid = $request->basicInfo->parentid;
		$ishead = $request->basicInfo->ishead;
		$orgId = $request->basicInfo->orgId;
		$role = $request->basicInfo->role;
		$relationwithhead = $request->basicInfo->relationwithhead;
		$firstname = $request->basicInfo->firstname;
		$firstname = mysqli_real_escape_string($con, $firstname);//removed special charecters
		$lastname = $request->basicInfo->lastname;
		$lastname = mysqli_real_escape_string($con, $lastname);//removed special charecters

		$mobilenumber = $request->basicInfo->mobilenumber;
        $altnumber = $request->basicInfo->altnumber;
		$gender = $request->basicInfo->gender;
		$dateofmarriage = $request->basicInfo->dateofmarriage;
	    $emailId=$request->basicInfo->emailId ;
        $altMail = $request->basicInfo->altMail;
		$blockName=$request->basicInfo->blockName ;
		$flatNo=$request->basicInfo->flatNo;
		$flatHolder=$request->basicInfo->flatHolder ;
		$dob=$request->basicInfo->dob ;
		$user_id = $request->basicInfo->userId;

		$query = "UPDATE person SET ";
		$query = $query." orgId = '$orgId',parentId ='$parentid',firstName='$firstname',lastName='$lastname',isHead='$ishead',gender='$gender',mobileNumber='$mobilenumber',alt_number='$altnumber', dateOfMarriage='$dateofmarriage', role='$role', relationWithHead='$relationwithhead',emailId='$emailId',alt_mail='$altMail',blockName='$blockName',flatNo='$flatNo',dob='$dob',flat_holder='$flatHolder' ";
		// if(!empty($dateofmarriage)){
		// 	$query = $query.",dateOfMarriage='$dateofmarriage'";
		// } else{
		// 	$query = $query.",dateOfMarriage=NULL";
		// }
		$query = $query." WHERE id=$user_id";
        $GLOBALS['qry']=$query;
		$sql_basic = mysqli_query($con,$query);
		//adding flats starts
		try{
        $FlatsArray=$request->basicInfo->FlatArr;
        $addFlatsQry="";
        foreach ($FlatsArray as $value) {
			$BlockAndflatNo=$value;
		   $addFlatsQry.="INSERT INTO person_flats (personId,flat_number) VALUES ($user_id,'$BlockAndflatNo');";
		  
	     }
	      if (mysqli_multi_query($con, $addFlatsQry)) {
			   return $sql_basic;
			}else{
				if(empty($FlatsArray)) {
                    return $sql_basic;
                 }else{
                 	 return false;
                 }
			}
		}catch(Exception $e) {
			//echo 'Message: ' .$e->getMessage();
		  }
        //adding flats ends
		 
	}

	public function updateHomeAddInfo($request, $con){

		// Home Add Info
		$home_type = $request->homeAddInfo->type;
		$home_door = $request->homeAddInfo->door;
		$home_cross = $request->homeAddInfo->cross;
		$home_street = $request->homeAddInfo->street;
		$home_area = $request->homeAddInfo->area;
		$home_landmark = $request->homeAddInfo->landmark;
		$home_pincode = $request->homeAddInfo->pincode;
		$home_city = $request->homeAddInfo->city;
		$home_country = $request->homeAddInfo->country;
		$home_phone = $request->homeAddInfo->phone;
		$home_email = $request->homeAddInfo->email;

		$user_id = $request->basicInfo->userId;

		$query_home = "UPDATE contact SET  `door`='$home_door', `cross`='$home_cross', `street`='$home_street', `area`='$home_area', `landmark`='$home_landmark', `cityId`='$home_city', `country`='$home_country', `pincode`='$home_pincode', `phone`='$home_phone', `email`='$home_email' WHERE `personId`='$user_id' and `type`='HOME'";
		$sql_home = mysqli_query($con,$query_home);	
		return $sql_home;
	}

	public function updateNativeAddInfo($request, $con){

		// Native Add Info
		$native_type = $request->nativeAddInfo->type;
		$native_door = $request->nativeAddInfo->door;
		$native_cross = $request->nativeAddInfo->cross;
		$native_street = $request->nativeAddInfo->street;
		$native_area = $request->nativeAddInfo->area;
		$native_landmark = $request->nativeAddInfo->landmark;
		$native_pincode = $request->nativeAddInfo->pincode;
		$native_city = $request->nativeAddInfo->city;
		$native_country = $request->nativeAddInfo->country;
		$native_phone = $request->nativeAddInfo->phone;
		$native_email = $request->nativeAddInfo->email;

		$user_id = $request->basicInfo->userId;
		
		$query_native = "UPDATE contact SET  `door`='$native_door', `cross`='$native_cross', `street`='$native_street', `area`='$native_area', `landmark`='$native_landmark', `cityId`='$native_city', `country`='$native_country', `pincode`='$native_pincode', `phone`='$native_phone', `email`='$native_email' WHERE `personId`='$user_id' and `type`='NATIVE'";

		$sql_native = mysqli_query($con,$query_native);

		return $sql_native;
	}

	public function updateOfficeAddInfo($request, $con){

		// Office Add Info
		$office_type = $request->officeAddInfo->type;
		$office_door = $request->officeAddInfo->door;
		$office_cross = $request->officeAddInfo->cross;
		$office_street = $request->officeAddInfo->street;
		$office_area = $request->officeAddInfo->area;
		$office_landmark = $request->officeAddInfo->landmark;
		$office_pincode = $request->officeAddInfo->pincode;
		$office_city = $request->officeAddInfo->city;
		$office_country = $request->officeAddInfo->country;
		$office_phone = $request->officeAddInfo->phone;
		$office_email = $request->officeAddInfo->email;

		$user_id = $request->basicInfo->userId;

		$query_office = "UPDATE contact SET `door`='$office_door', `cross`='$office_cross', `street`='$office_street', `area`='$office_area', `landmark`='$office_landmark', `cityId`='$office_city', `country`='$office_country', `pincode`='$office_pincode', `phone`='$office_phone', `email`='$office_email' WHERE `personId`='$user_id' and `type`='OFFICE'";

		$sql_office = mysqli_query($con,$query_office);

		return $sql_office;
	}
}

$user_id = $request->basicInfo->userId;

$contactsObj = new UpdateContacts();
$basicRes = $contactsObj->updateBasicIfo($request, $con);



if($basicRes){
	$isSuccessful = true;
	$error="";
}else{
	$isSuccessful = false;
	$error=mysqli_error($con);
}

$response = array('isSuccessful'=> $isSuccessful , 'userId'=> $user_id,'error'=>$error,'qry'=>$qry);
$response = json_encode($response);
echo $response;
$connection->closeConnection();
?>
