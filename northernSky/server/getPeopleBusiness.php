<?php

require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$sql = mysqli_query($con, "SELECT * FROM people_business");
$rows_count = mysqli_num_rows($sql);
$events = array();
if ($rows_count != 0) {
    while ($rows_fetch = mysqli_fetch_assoc($sql)) {
        array_push($events, array('name' => $rows_fetch['name'], 'email' => $rows_fetch['email'], 'person_contact' => $rows_fetch['person_contact'], 'website_link' => $rows_fetch['website_link'], 'dealsIn' => $rows_fetch['dealsIn'], 'address' => $rows_fetch['address']));
    }
}
$response = json_encode($events);
$connection->closeConnection();
echo $response;
?>
