<?php
class Maintenance{
  var $con=null;
  var $userId,
      $amount,
      $updated_by,
      $perticular,
      $mobileNumber,
      $type,//type ca be "maintenanceBill","electricBill","facility"
      $flat;
  var $qryArr=[];
  var $paymentDetail=null;
  var $currentBal=0;//this gets initialized in method isRecordExistsInMaitenanceTB
  var $updatedMaintenaceBal;//this is initialized after updateMaitenceTB() mthod called
  function __construct($postdata){
    //
    require_once('libs/dbConnection.php');
    $connection = new dbconnection();
    $con = $connection->connectToDatabase();
    $this->con=$con;
   //
    $req = json_decode($postdata);
    $this->userId=$req->ownerId;
    $this->amount=$req->amount;
    $this->updated_by=$req->updated_by;
    $this->perticular=$req->perticular;
    $this->mobileNumber=$req->mobileNumber;
    $flatStr=mysqli_real_escape_string($con,$req->flat);//removed special charecters
    $this->flat=$flatStr;
    $this->paymentDetail=$postdata;
  }
  function connectDataBase(){
    
    $this->isRecordExistsInMaitenanceTB();
    //$res=$this->addToMaintainanceTB();
    //$connection->closeConnection();
   // return $res;
  }
  
  function isRecordExistsInMaitenanceTB(){
    $selectQry="select  balance from maintainance where userId='$this->userId' and flat='$this->flat'";
    array_push($this->qryArr, array('selectQry' =>$selectQry) );//del
    $output = mysqli_query($this->con, $selectQry);
    if(mysqli_num_rows($output) < 1){
      $maintenanceInsertQry="INSERT INTO maintainance (userId,flat,balance,created_by,update_by) VALUES ('$this->userId','$this->flat',0,'$this->updated_by',0)";
      array_push($this->qryArr, array('maintenanceInsertQry' =>$maintenanceInsertQry) );//del
      mysqli_query($this->con,$maintenanceInsertQry);
      $this->con->commit();
      }else{
        $rowRes= mysqli_fetch_row($output);
        $this->currentBal=$rowRes['0'];
      }
  }
  function addToMaintainanceTB(){
    $updateMaintenaceTBqry= "UPDATE maintainance SET balance=balance+'$this->amount',update_by='$this->updated_by',updated_at=now() WHERE userId='$this->userId' and flat='$this->flat'";
    return $this->updateMaintenaceTB("credit",$updateMaintenaceTBqry);
  }
  function deductFromMaintainanceTB(){
    $updateMaintenaceTBqry= "UPDATE maintainance SET balance=balance-('$this->amount'),update_by='$this->updated_by',updated_at=now() WHERE userId='$this->userId' and flat='$this->flat'";
    return $this->updateMaintenaceTB("debit",$updateMaintenaceTBqry);
  }
  function updateMaintenaceTB($transactionType,$updateMaintenaceTBqry){
    mysqli_autocommit($this->con,FALSE);		
    //$updateMaintenaceTBqry= "UPDATE maintainance SET balance=balance+'$this->amount',update_by='$this->updated_by',updated_at=now() WHERE userId='$this->userId' and flat='$this->flat'";
    array_push($this->qryArr, array('updateMaintenaceTBqry' =>$updateMaintenaceTBqry ));//del
    $updateMaintanceResult = mysqli_query($this->con,$updateMaintenaceTBqry);
    $updatedRecordQry="select id,userId,balance from maintainance where userId='$this->userId' and flat='$this->flat'";
    array_push($this->qryArr, array('updatedRecordQry' =>$updatedRecordQry ));//del
    $updatedRecordResult=mysqli_query($this->con,$updatedRecordQry);
    $rows_count = mysqli_num_rows($updatedRecordResult);
    $rowArr = array();
    if ($rows_count != 0) {
      while ($rows_fetch = mysqli_fetch_assoc($updatedRecordResult)) {
        $this->updatedMaintenaceBal=$rows_fetch['balance'];
       array_push($rowArr, array('id' => $rows_fetch['id'], 'userId' => $rows_fetch['userId'],'balance' => $rows_fetch['balance']));
      }
      }
      $updatedId=$rowArr[0]['id'];
      $remainingBalance=$rowArr[0]['balance'];
      if($transactionType == "credit"){
        return $this->creditMaintenanceHistoryTB($updatedId,$remainingBalance);
      }else{
        return $this->debitMaintenanceHistoryTB($updatedId,$remainingBalance);
      }
  }
  function creditMaintenanceHistoryTB($Id,$remainingBalance){
    $updatedId=$Id;
    $remainingBal=$remainingBalance;
    $transactionDetail = "";
    //$transactionDetail = mysqli_real_escape_string($this->con, $postdata);
    //$paymtDetail=json_decode($this->paymentDetail);
    $paymtDetail = mysqli_real_escape_string($this->con, $this->paymentDetail);//removed special charecters
    $paymentDetailObj=json_decode($this->paymentDetail);
    $insertInServiceHistoryQry="INSERT INTO maintenance_history(mId,credit,Balance,details,perticulars,created_by) VALUES ('$updatedId','$this->amount',$remainingBal,'$paymtDetail','$this->perticular','$this->updated_by')";
    // $id = mysqli_insert_id($con);
   
    if (mysqli_query($this->con,$insertInServiceHistoryQry)){
        $this->con->commit();
        return true;
    } else {
       // echo mysqli_error($con);
        array_push($this->qryArr, array('insertInMaintenanceHistoryQry' =>$insertInServiceHistoryQry ));//del
        return false;
    }
  }
  function debitMaintenanceHistoryTB($Id,$remainingBalance){
    $updatedId=$Id;
    $remainingBal=$remainingBalance;
    $transactionDetail = "";
    //$transactionDetail = mysqli_real_escape_string($this->con, $postdata);
    $milliseconds = round(microtime(true) * 1000);
    //$paymtDetail=json_decode($this->paymentDetail);
    $paymtDetail = mysqli_real_escape_string($this->con, $this->paymentDetail);//removed special charecters
    $paymentDetailObj=json_decode($this->paymentDetail);
    $insertInServiceHistoryQry="INSERT INTO maintenance_history(mId,debit,Balance,details,perticulars,created_by) VALUES ('$updatedId','$this->amount',$remainingBal,'$paymtDetail','$this->perticular','$this->updated_by')";
    // $id = mysqli_insert_id($con);
    if (mysqli_query($this->con,$insertInServiceHistoryQry)){
        $this->con->commit();
        return true;
    } else {
       // echo mysqli_error($con);
        array_push($this->qryArr, array('insertInMaintenanceHistoryQry' =>$insertInServiceHistoryQry ));//del
        return false;
    }
  }

  function sendSMS($typeDetail){
    //sending mesaage to owner
		 require_once('libs/smsGatewayConstants.php');
     require_once('libs/smsGateway.php');
     $messageDeatil =$this->SMSTemplateMain($this->perticular,$typeDetail);
		 $obj = new Sender(_HOST,_PORT,_USERNAME,_PASSWORD,_SOURCE,$messageDeatil->message,$messageDeatil->mobileNumber,_TYPE,_DLR);
     $result = $obj->Submit();
     $del=$result;
     $messageDeatils=$this->SMSTemplateFamilyHead($this->perticular,$typeDetail,$messageDeatil);
     if($messageDeatils){
      $objH = new Sender(_HOST,_PORT,_USERNAME,_PASSWORD,_SOURCE,$messageDeatils->message,$messageDeatils->mobileNumber,_TYPE,_DLR);
      $resultH = $objH->Submit();
      $delH=$result;
     }
  }
  function SMSTemplateMain($type,$typeDetail){
    //getting month and year
      $dateYMD=date("y-m-d");
      $dateDMY=date("d-m-y");
      $year = date('Y', strtotime($dateYMD));
      $month = date('F', strtotime($dateYMD));
    //
    $messageDeatil=new stdClass();
    $SMSDetails=(array)json_decode($this->paymentDetail);
    $selectQry="select * from person where id='$SMSDetails[personId]'";
    $qryResult = mysqli_query($this->con, $selectQry);
     if (mysqli_num_rows($qryResult) > 0) {
         // output data of each row
         while($row = mysqli_fetch_assoc($qryResult)) {
             $firstName=$row['firstName'];
             $lastName=$row['lastName'];
             $mobileNumber=$row['mobileNumber'];
         }
     } else {
         echo "";
     }
     switch ($type) {
      case "Maintenance":
        //$message= "Dear ".$firstName.",\nRs ".$this->amount." has been added to your maintenance amount successfully on ".$dateDMY; 
        $message="Dear ".$firstName.",\nAMOUNT CREDITED SUCCESSFULLY on  ".$dateDMY;
        break;
      case "Maintenance Bill":
        $message="Dear ".$firstName.",\nMAINTENANCE PAID on ".$dateDMY." for ".$month." ".$year;
         break;
      case "Electricity Bill":
        $message="Dear ".$firstName.",\nELECTRICITY BILL PAID on ".$dateDMY." for ".$month." ".$year;
            break;
      case "facility":
          if($SMSDetails['isCancel']){
            $message= "Dear ".$firstName.",\nBOOKING CANCELLED SUCCESSFULLY. Time-Slot: ".$SMSDetails['start']." for ".$SMSDetails['facilityName'];            
          }else{
            $message= "Dear ".$firstName.",\nBOOKING DONE SUCCESSFULLY. Time-Slot: ".$typeDetail->startTime." for ".$typeDetail->booked_facility;            
          }
          break;
      default:
      $message= "";
    }
     $message.=". FLAT NUMBER: ".$SMSDetails['flat']." OPN BAL: ".$this->currentBal." TRANS: ".$this->amount." AVL BAL: ".$this->updatedMaintenaceBal." Thank you!";
     $messageDeatil->message=$message;
     $messageDeatil->mobileNumber=$mobileNumber;
     $messageDeatil->firstName=$firstName;
     $messageDeatil->lastName=$lastName;
     return $messageDeatil;
   }
   function SMSTemplateFamilyHead($type,$typeDetail,$sentMsgDetail){
     //getting month and year
     $dateYMD=date("y-m-d");
     $dateDMY=date("d-m-y");
     $year = date('Y', strtotime($dateYMD));
     $month = date('F', strtotime($dateYMD));
    //
    $messageDeatil=new stdClass();
    $SMSDetails=(array)json_decode($this->paymentDetail);
    if($SMSDetails['parentId'] == "-1"){
      return false;
    }
    $selectQry="select * from person where id='$this->userId'";
    $qryResult = mysqli_query($this->con, $selectQry);
     if (mysqli_num_rows($qryResult) > 0) {
         // output data of each row
         while($row = mysqli_fetch_assoc($qryResult)) {
             $firstName=$row['firstName'];
             $lastName=$row['lastName'];
             $mobileNumber=$row['mobileNumber'];
         }
     } else {
         echo "";
     }
    switch ($type) {
      case "Maintenance":
        //$message= "Dear ".$firstName.",\nRs ".$this->amount." has been added to your maintenance amount successfully on ".$dateDMY; 
        $message="Dear ".$firstName.",\nAMOUNT CREDITED SUCCESSFULLY on  ".$dateDMY;
        if( $SMSDetails['from'] == "App"){
          $message.=" from ".$SMSDetails['name'];
        }
        break;
      case "Maintenance Bill":
        $message="Dear ".$firstName.",\nMAINTENANCE PAID on ".$dateDMY." for ".$month." ".$year;
         break;
      case "Electricity Bill":
        $message="Dear ".$firstName.",\nELECTRICITY BILL PAID on ".$dateDMY." for ".$month." ".$year;
        break;
      case "facility": 
        if($SMSDetails['isCancel']){
          $message= "Dear ".$firstName.",\nBOOKING CANCELLED  SUCCESSFULLY from ".$SMSDetails['name'].". Time-Slot: ".$SMSDetails['start']." for ".$SMSDetails['facilityName'];            
        }else{
          $message= "Dear ".$firstName.",\nBOOKING DONE SUCCESSFULLY from ".$SMSDetails['name'].". Time-Slot: ".$typeDetail->startTime." for ".$typeDetail->booked_facility;            
        }          
        break;
      default:
       $message= "";
     }
     $message.=". FLAT NUMBER: ".$SMSDetails['flat']." OPN BAL: ".$this->currentBal." TRANS: ".$this->amount." AVL BAL: ".$this->updatedMaintenaceBal." Thank you!";
     $messageDeatil->message=$message;
     $messageDeatil->mobileNumber=$mobileNumber; 
     return $messageDeatil;
   }

}
?>