

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>bookAppointment page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../admin/aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- Theme style -->
  <link rel="stylesheet" href="../admin/aLTE/dist/css/AdminLTE.min.css">
   
  <link rel="stylesheet" href="manageCalendar/fullcalendar.css">
  <link rel="stylesheet" href="manageCalendar/lomMenu.css">
  <link rel="stylesheet" href="manageCalendar/bookAppointment.css">
  <style >
     ::-webkit-scrollbar{
        width: 0px;
     }
     .form-control{
     	    font-size: 20px;
     }
     .fc-time-grid .fc-slats td {
      height: 0 !important;      
    }
    .fc .fc-axis {
       padding: 0px 0px !important;
      }
    #bookedSlotDetailModal input {
    background: transparent !important;
    color: #944848 !important;
    padding: 0px;
    /* outline: none !important; */
    border: none !important;
}
  </style>
     <?php
        require_once('libs/dbConnection.php');
        $connection = new dbconnection();
        $con = $connection->connectToDatabase();
        $userId=$_GET['userId'];
        $parentId=$_GET['parentId'];
        //$parentId == -1 ? $parentId=$userId:"";
        $sql = mysqli_query($con, "SELECT * FROM person where id='$userId'");
        $rows_count = mysqli_num_rows($sql);
        $userDataArry = array();
        if ($rows_count != 0) {
            while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                        $info = $rows_fetch;  
                    array_push($userDataArry, $info);
            }
        }
        $faltsArray=array();
        $personFlatsQuery = "SELECT flat_number FROM person_flats WHERE  personId='$parentId' ";
    
          $sql_res = mysqli_query($con, $personFlatsQuery);

          $rows_cont = mysqli_num_rows($sql_res);

              if($rows_cont!=0){
                  while($rows_fetch = mysqli_fetch_assoc($sql_res)){
                     array_push($faltsArray, $rows_fetch['flat_number']);
                  }
        }
        array_push($userDataArry, $faltsArray);
        echo "<script>var familyHeadObj = " . json_encode($userDataArry[0]) . ';'.
        "familyHeadObj.flatsArray = " . json_encode($userDataArry[1]) . ';</script>';
        $connection->closeConnection();
     ?> 
</head>
<body style="background-color: transparent;height: 100%">
<div class="container-fluid">
 <table class="table table-bordered" id="multiFacilityDetails">
    <thead>
      <tr>
        <th>Facility Type</th>
        <th>Current People</th>
      </tr>
    </thead>
    <tbody>
     
    </tbody>
  </table>
 </div>
<header id="header" class="header" style="padding:10px">
   <div class="row ">
    <div class="col-xs-offset-1 col-xs-10">
      <select id="facilityId" name="facilityId"  class="form-control">
        <!-- <option value="1">TT</option> -->
          
      </select>
    </div>
</div>
</header><!-- /header -->
<hr class="col-xs-offset-2 col-xs-8" style="margin-top: 0px;border-top-width: 2px;">
<div class="container fullCalenderConatiner" style="width:100%;position: fixed">
    <div id="fullCalendar"></div>
 </div>


             <!-- Modal -->
      <div class="modal" id="BookAppointmentModal" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm" style="margin: 0px ! important;padding: 0px !important;width: 100%">
          <div class="modal-content" style="height: 100%;width: 100%;background-color: lightgrey">
           <div class="modal-header" style="height: 50px;background: linear-gradient(to right , #25b9af, #30cdde);">
              <div class="row header-backbtnContainer" onclick="closeBookAppointmntPopup()">
                <span class=" backtologin-btn" >
                <span class="fa fa-arrow-left"></span><span class="textback">BACK</span></span>
              </div>
            </div>
           <div class="modal-body">
             <form id="registrationForm1" style="margin-top: 15px;">
               <div class="form container">
                  <div class="form-group labe-clor">
                    <label for="facility_Name"><i class="material-icons" style="color: rgba(175, 97, 97, 0.78);margin-right: 6px;font-size: 17px;">games</i>Facility Name</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="facility_Name" name="facility_Name" readonly> 
                  </div>
                  <div class="form-group labe-clor">
                    <label for="bookingDate" class="label-custom-css"><span class="glyphicon glyphicon-calendar" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Date</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="bookingDate" name="bookingDate" readonly> 
                  </div>
                  <div class="form-group labe-clor">
                    <label for="bookingTime" class="label-custom-css"><span class="glyphicon glyphicon-time" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Time</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="bookingTime" name="bookingTime" readonly style="display: none;"> 
                  </div>

                  <div class="form-group labe-clor col-xs-6" style="margin-top: -10px;padding-left: 0;">
                    <label for="bookingStartTime" class="label-custom-css" style="position: absolute;top: 10px;font-size: x-small;margin-left: 10px;">FROM:</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="bookingStartTime" name="bookingTime" readonly style="padding-left:50px;"> 
                  </div>
                  <div class="form-group labe-clor labe-clor col-xs-6" style="margin-top: -10px;padding-right: 0;">
                    <label for="bookingEndTime" class="label-custom-css" style="position: absolute;top: 10px;font-size: x-small;margin-left: 10px;">TO:</label>
                     <select  class="form-control inputHeight custm-padding" id="bookingEndTime" name="bookingTime" style="padding-left:35px;">
                     <!-- <option value="">6:00 pm</option>
                     <option value="">6:30 pm</option> -->
                     </select>
                  </div>
              </div>
              </form>
           </div>
            <div class="modal-footer">
              <button id="proceedToBookBtn" class="col-xs-offset-1 col-xs-10" style="background: linear-gradient(to right , #25b9af, #30cdde);color:white;height: 50px;">NEXT</button>
            
            </div>
          
        </div>
      </div>
    </div>

 
              <!-- Modal -->
      <div class="modal" id="bookedSlotDetailModal" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm" style="margin: 0px ! important;padding: 0px !important;width: 100%">
          <div class="modal-content" style="height: 100%;width: 100%;background-color: lightgrey">
           <div class="modal-header" style="height: 50px;background: linear-gradient(to right , #25b9af, #30cdde);">
              <div class="row header-backbtnContainer" onclick="closeAppointmntDeatilPopup()">
                <span class=" backtologin-btn" >
                <span class="fa fa-arrow-left"></span><span class="textback">BACK</span></span>
              </div>
            </div>
           <div class="modal-body">
             <form id="bookedSlotDetailForm" style="margin-top: 15px;">
               <div class="form container">
                <div class="row">
                  <div class="form-group labe-clor col-xs-6 cl-md-6">
                    <label for="owner_name"><i class="material-icons" style="color: rgba(175, 97, 97, 0.78);margin-right: 6px;font-size: 17px;">games</i>Flat holder Name</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="owner_name" name="owner_name" readonly> 
                  </div>
                   <div class="form-group labe-clor col-xs-6 cl-md-6">
                    <label for="mobile_no"><i class="material-icons" style="color: rgba(175, 97, 97, 0.78);margin-right: 6px;font-size: 17px;">games</i>Mobile</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="mobile_no" name="mobile_no" readonly> 
                  </div>
                </div>
                <div class="row">
                   <div class="form-group labe-clor col-xs-6 cl-md-6">
                    <label for="block" class="label-custom-css"><span class="glyphicon glyphicon-calendar" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Block</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="block" name="block" readonly> 
                  </div>
                   <div class="form-group labe-clor col-xs-6 cl-md-6">
                    <label for="flat" class="label-custom-css"><span class="glyphicon glyphicon-calendar" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Select Flat</label>
                    <select class="form-control inputHeight custm-padding" id="flat" name="flat" required>
                      
                       
                    </select> 
                  </div>
                </div>
                <div class="row">
                   <div class="form-group labe-clor col-xs-12 cl-md-12">
                    <label for="facility_name" class="label-custom-css"><span class="glyphicon glyphicon-calendar" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Facility Name</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="facility_name" name="facility_name" readonly> 
                  </div>
                </div> 
                <div class="row"> 
                  
                  <div class="form-group labe-clor col-xs-12 cl-md-12">
                    <label for="bookDate" class="label-custom-css"><span class="glyphicon glyphicon-calendar" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Date</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="bookDate" name="bookDate" readonly> 
                  </div>
                  
              </div>
              <div class="row">
                 <div class="form-group labe-clor col-xs-12 col-md-12">
                    <label for="bookTiming" class="label-custom-css"><span class="glyphicon glyphicon-time" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Time</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="bookTiming" name="bookTiming" readonly > 
                  </div>
                  <div class="form-group labe-clor col-xs-12 col-md-12">
                    <label for="slotCharge" class="label-custom-css"><span class="glyphicon glyphicon-time" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Amount</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="slotCharge" name="slotCharge" readonly > 
                  </div>
              </div>
              <div class="row" style="position: relative;top: 10px;">
                <button id="bookConfirm" class="col-xs-offset-3 col-xs-6" style="background: linear-gradient(to right , #25b9af, #30cdde);color:white;height: 50px;">BOOK</button>
              </div>
             </div> 
              </form>
           </div>
            <div class="modal-footer ">
             
            
            </div>
          
        </div>
      </div>
    </div>

         <!-- payment succes AS modal -->
       <div class="modal in" id="paymentSuccesModal" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog modal-sm">
             <div class="modal-content" style="background: linear-gradient(to right, #25b9af,#30cdde);;width:100%;">
               <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button> </div>
               <div class="modal-body" style="min-height: 100px;display: flex;justify-content: center;align-items: center;">
                 <div>
                   <h5 style="padding-right:15px;padding-left:15px; text-align: center;color:white">
                     Booking Successful.
                   </h5>
                   <div class="col-xs-12" style="margin-top: 10px;margin-bottom: 20px;">
                    <div class="center-block" style="height: 55px;width: 55px;background-color: green;border-radius: 50%;">
                      <span class="glyphicon glyphicon-ok" style="font-size: 25px;color: white;margin: auto;line-height: 2em;display: block;width: 25px;"></span>
                    </div>
                     
                   </div>
                 </div>
                </div>
               <div class="modal-footer ">
                <button  class="modal-footer-btns payLater-btn" style="width:100%;float: left;background-color:#FFFFFF;color:green">OK</button>
               </div>
              </div>
          </div>
        </div>

        <div class="modal in" id="paymentFailureModal" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog modal-sm">
             <div class="modal-content" style="background: linear-gradient(to right, #25b9af,#30cdde);width:100%;">
               <div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button> </div>
               <div class="modal-body" style="min-height: 100px;display: flex;justify-content: center;align-items: center;">
                 <div>
                   <h5  style="padding-right:15px;padding-left:15px; text-align: center;color:white">
                     Booking failed.Please load maitenance balance.
                   </h5>
                   <div class="col-xs-12" style="margin-top: 10px;margin-bottom: 20px;">
                    <div class="center-block" style="height: 55px;width: 55px;background-color: green;border-radius: 50%;">
                      <span class="glyphicon glyphicon-remove" style="font-size: 25px;color: red;line-height: 2em;margin:auto;display: block;width: 25px;"></span>
                    </div>
                     
                   </div> 
                 </div>
                </div>
             <!--  <div class="modal-footer ">
               <button  class="modal-footer-btns paymentFailure-retry-btn" style="width:100%;background-color:#119087;color:white">OK</button>
               
              </div> -->
            </div>
          </div>
        </div>  


<script src="../admin/aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../admin//aLTE/plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="../admin//aLTE/bootstrap/js/bootstrap.min.js"></script>

<script src="../admin//aLTE/plugins/fastclick/fastclick.js"></script>



<script src="manageCalendar/moment.min.js"></script>
<script src="manageCalendar/fullcalendar.js"></script>

<link rel="stylesheet" href="manageCalendar/lomPopUp.css">
<script src="manageCalendar/lomPopUp.js"></script>
<script src="manageCalendar/slotsRendering.js"></script>
<script src="manageCalendar/bookAppointment.js"></script>

<script src="manageCalendar/spinner/app.js"></script>
<link href="manageCalendar/spinner/app.css" rel="stylesheet">
  </body>
  </html>