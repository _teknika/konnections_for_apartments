<?php

require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$sql = mysqli_query($con, "SELECT * FROM managemembers");
$rows_count = mysqli_num_rows($sql);
$managemembers = array();
if ($rows_count != 0) {
    while ($rows_fetch = mysqli_fetch_assoc($sql)) {
        array_push($managemembers, array('name' => $rows_fetch['name'], 'post' => $rows_fetch['post'], 'mobile_number' => $rows_fetch['mobile_number']));
    }
}
$response = json_encode($managemembers);
$connection->closeConnection();
echo $response;
?>



