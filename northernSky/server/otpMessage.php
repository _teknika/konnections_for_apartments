
<?php
require_once('libs/dbConnection.php');

require_once('libs/smsGatewayConstants.php');

require_once('libs/smsGateway.php');

require_once("libs/mail_constants.php");

$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);


$mobileNumber = $request->request->mobileNumber;
$otp = rand(1000, 9999);

if(strlen($mobileNumber)!=10){
	echo json_encode(array('otpResult' =>  'failed', 'otp'=>0));
	return;
}

if(strcmp($mobileNumber, "9900489498") == 0){
	$otp = 1234;
	$result = '1234';
	$message = "Dear User, One Time Password(OTP) to access My Family App is 1234 Do not disclose OTP to anyone";
	//$obj = new Sender(_HOST,_PORT,_USERNAME,_PASSWORD,_SOURCE,$message,$mobileNumber,_TYPE,_DLR);
}else{
$message = "Dear User, One Time Password(OTP) to access My Family App is ".$otp.". Do not disclose OTP to anyone";
$obj = new Sender(_HOST,_PORT,_USERNAME,_PASSWORD,_SOURCE,$message,$mobileNumber,_TYPE,_DLR);
$result = $obj->Submit();

}

echo json_encode(array('otpResult' =>  $result, 'otp'=>$otp));

//below code is for sending OTP through mail
$qry="SELECT email FROM person WHERE mobileNumber='$mobileNumber'";
$sql = mysqli_query($con, $qry);
$rows_count = mysqli_num_rows($sql);
$mailId = "";
if ($rows_count != 0) {
    while ($rows_fetch = mysqli_fetch_assoc($sql)) {
		 $mailId = $rows_fetch['email'];	
    }
}

$connection->closeConnection();

$t ="OTP";
$subject = "Agog Church- $t";

$m = "Dear User,\n\n Your Agog Church One Time Password (OTP) is ".$otp.". Please enter the same to complete your registration process.";


$body = "$m \n\n\n";

$mailManager=new MailManager();
$from=$mailManager->getFromMailId();
$headers = array ('From' => $from,
                  'To' => $mailId,
                  'Subject' => $subject);                
$result=$mailManager->sendMail($mailId,$headers,$body);
$resp=array('result' => $result);
$response = json_encode($resp);

?>
