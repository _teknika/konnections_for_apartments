
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$req = json_decode($postdata);

$request = $req->request;


Class Search {

	public function searchUser($request, $con){

		// Basic Info
		$name=$request->name;
		$name = mysqli_real_escape_string($con, $name);//removed special charecters
		$mobilenumber = $request->mobilenumber;
		$blockName = $request->blockName;
		$blockName = mysqli_real_escape_string($con, $blockName);//removed special charecters
		$flatNo = $request->flatNo;

		$orgId = $request->orgId;
		//SELECT * FROM person p left JOIN `person_flats` pf on p.id=pf.`personId` WHERE p.flatNo=435 OR pf.flat_number=435

		$query = "select  DISTINCT p.id,p.firstName,p.lastName, p.mobileNumber,p.parentId from person p left JOIN person_flats pf on p.id=pf.`personId` where p.isHead!=2 and p.orgId='$orgId'";
        //Removed p.parentId=-1 inorder to display family members also
		if(!empty($flatNo)){
			$query = $query."  && (p.flatNo LIKE '%$flatNo%'  or pf.flat_number LIKE '%$flatNo%')";
		}
		if(!empty($name)){  
			$query = $query." && CONCAT(p.firstName,p.lastName) LIKE '%$name%' ";
		}
		if(!empty($mobilenumber)){
			$query = $query." && (p.mobileNumber like '%$mobilenumber%' || p.alt_number like '%$mobilenumber%') ";
		}
		if(!empty($blockName)){
			$query = $query." && p.blockName LIKE '%$blockName%'";
		}
		
		// if(!empty($city)){
		// 	$query = $query." && c.cityId LIKE '%$city%'";
		// }
	    
		$sql = mysqli_query($con, $query);

	

		$rows_count = mysqli_num_rows($sql);

		$data=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql))
		    {
		    	$info = $rows_fetch;	
		    	array_push($data, $info);
		        $isSuccessful = true;
		        $code = 200;
		        $qry=$query;
		    }
		}else{
			$isSuccessful = true;
		    $code = 202;
		}

		$response = array("resp"=>$data, "code"=>$code, "isSuccessful"=>$isSuccessful,"qry"=>$query);
		return $response;
	}

	
}




// Object

$contactsObj = new Search();
$result = $contactsObj->searchUser($request, $con);

$response = json_encode($result);
echo $response;
$connection->closeConnection();
?>
