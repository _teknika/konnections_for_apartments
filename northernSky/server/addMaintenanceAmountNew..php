<?php
class Maintenance{
  var $con=null;
  var $userId,
      $amount,
      $updated_by,
      $perticular,
      $mobileNumber,
      $type,//type can be  either "credit" or "debit"
      $flat;
  var $qryArr=[];
  var $paymentDetail=null;
  var $currentMaintenanceBalance;//this is initialized after updateMaitenceTB mthod called
  function __construct($postdata){
    $req = json_decode($postdata);
    $this->userId=$req->ownerId;
    $this->amount=$req->amount;
    $this->updated_by=$req->updated_by;
    $this->perticular=$req->perticular;
    $this->mobileNumber=$req->mobileNumber;
    
    $this->flat=$req->flat;
    $this->paymentDetail=$postdata;
  }
  function connectDataBase(){
    require_once('libs/dbConnection.php');
    $connection = new dbconnection();
    $con = $connection->connectToDatabase();
    $this->con=$con;
    $this->isRecordExistsInMaitenanceTB();
    //$res=$this->addToMaintainanceTB();
    //$connection->closeConnection();
   // return $res;
  }
  
  function isRecordExistsInMaitenanceTB(){
    $selectQry="select  * from maintainance where userId='$this->userId' and flat='$this->flat'";
    array_push($this->qryArr, array('selectQry' =>$selectQry) );//del
    $output = mysqli_query($this->con, $selectQry);
    if(mysqli_num_rows($output) < 1){
      $maintenanceInsertQry="INSERT INTO maintainance (userId,flat,balance,created_by,update_by) VALUES ('$this->userId','$this->flat',0,'$this->updated_by',0)";
      array_push($this->qryArr, array('maintenanceInsertQry' =>$maintenanceInsertQry) );//del
      mysqli_query($this->con,$maintenanceInsertQry);
      $this->con->commit();
      }
  }
  function addToMaintainanceTB(){
    $updateMaintenaceTBqry= "UPDATE maintainance SET balance=balance+'$this->amount',update_by='$this->updated_by',updated_at=now() WHERE userId='$this->userId' and flat='$this->flat'";
    return $this->updateMaintenaceTB("credit",$updateMaintenaceTBqry);
  }
  function deductFromMaintainanceTB(){
    $updateMaintenaceTBqry= "UPDATE maintainance SET balance=balance-('$this->amount'),update_by='$this->updated_by',updated_at=now() WHERE userId='$this->userId' and flat='$this->flat'";
    return $this->updateMaintenaceTB("debit",$updateMaintenaceTBqry);
  }
  function updateMaintenaceTB($transactionType,$updateMaintenaceTBqry){
    mysqli_autocommit($this->con,FALSE);		
    //$updateMaintenaceTBqry= "UPDATE maintainance SET balance=balance+'$this->amount',update_by='$this->updated_by',updated_at=now() WHERE userId='$this->userId' and flat='$this->flat'";
    array_push($this->qryArr, array('updateMaintenaceTBqry' =>$updateMaintenaceTBqry ));//del
    $updateMaintanceResult = mysqli_query($this->con,$updateMaintenaceTBqry);
    $updatedRecordQry="select id,userId,balance from maintainance where userId='$this->userId' and flat='$this->flat'";
    array_push($this->qryArr, array('updatedRecordQry' =>$updatedRecordQry ));//del
    $updatedRecordResult=mysqli_query($this->con,$updatedRecordQry);
    $rows_count = mysqli_num_rows($updatedRecordResult);
    $rowArr = array();
    if ($rows_count != 0) {
      while ($rows_fetch = mysqli_fetch_assoc($updatedRecordResult)) {
        $this->currentMaintenanceBalance=$rows_fetch['balance'];
       array_push($rowArr, array('id' => $rows_fetch['id'], 'userId' => $rows_fetch['userId'],'balance' => $rows_fetch['balance']));
      }
      }
      $updatedId=$rowArr[0]['id'];
      $remainingBalance=$rowArr[0]['balance'];
      if($transactionType == "credit"){
        return $this->creditMaintenanceHistoryTB($updatedId,$remainingBalance);
      }else{
        return $this->debitMaintenanceHistoryTB($updatedId,$remainingBalance);
      }
  }
  function creditMaintenanceHistoryTB($Id,$remainingBalance){
    $updatedId=$Id;
    $remainingBal=$remainingBalance;
    $transactionDetail = "";
    //$transactionDetail = mysqli_real_escape_string($this->con, $postdata);
    //$paymtDetail=json_decode($this->paymentDetail);
    $paymtDetail = mysqli_real_escape_string($this->con, $this->paymentDetail);//removed special charecters
    $paymentDetailObj=json_decode($this->paymentDetail);
    $insertInServiceHistoryQry="INSERT INTO maintenance_history(mId,credit,Balance,details,perticulars,created_by) VALUES ('$updatedId','$this->amount',$remainingBal,'$paymtDetail','$this->perticular','$this->updated_by')";
    // $id = mysqli_insert_id($con);
   
    if (mysqli_query($this->con,$insertInServiceHistoryQry)){
        $this->con->commit();
        return true;
    } else {
       // echo mysqli_error($con);
        array_push($this->qryArr, array('insertInMaintenanceHistoryQry' =>$insertInServiceHistoryQry ));//del
        return false;
    }
  }
  function debitMaintenanceHistoryTB($Id,$remainingBalance){
    $updatedId=$Id;
    $remainingBal=$remainingBalance;
    $transactionDetail = "";
    //$transactionDetail = mysqli_real_escape_string($this->con, $postdata);
    //$paymtDetail=json_decode($this->paymentDetail);
    $paymtDetail = mysqli_real_escape_string($this->con, $this->paymentDetail);//removed special charecters
    $paymentDetailObj=json_decode($this->paymentDetail);
    $insertInServiceHistoryQry="INSERT INTO maintenance_history(mId,debit,Balance,details,perticulars,created_by) VALUES ('$updatedId','$this->amount',$remainingBal,'$paymtDetail','$this->perticular','$this->updated_by')";
    // $id = mysqli_insert_id($con);
   
    if (mysqli_query($this->con,$insertInServiceHistoryQry)){
        $this->con->commit();
        return true;
    } else {
       // echo mysqli_error($con);
        array_push($this->qryArr, array('insertInMaintenanceHistoryQry' =>$insertInServiceHistoryQry ));//del
        return false;
    }
  }

  function sendSMS($typeDetail){
    //sending mesaage to owner
		 require_once('libs/smsGatewayConstants.php');
     require_once('libs/smsGateway.php');
     $messageDeatil =$this->SMSTemplateMain($this->perticular,$typeDetail);
		 $obj = new Sender(_HOST,_PORT,_USERNAME,_PASSWORD,_SOURCE,$messageDeatil->message,$messageDeatil->mobileNumber,_TYPE,_DLR);
     $result = $obj->Submit();
     $del=$result;
     $messageDeatils=$this->SMSTemplateFamilyHead($this->perticular,$typeDetail,$messageDeatil);
     if($messageDeatils){
     $objH = new Sender(_HOST,_PORT,_USERNAME,_PASSWORD,_SOURCE,$messageDeatils->message,$messageDeatils->mobileNumber,_TYPE,_DLR);
     $resultH = $objH->Submit();
     $delH=$result;
     }
  }
  function SMSTemplateMain($type,$typeDetail){
    
    $messageDeatil=new stdClass();
    $SMSDetails=(array)json_decode($this->paymentDetail);
    $selectQry="select * from person where id='$SMSDetails[personId]'";
    $qryResult = mysqli_query($this->con, $selectQry);
     if (mysqli_num_rows($qryResult) > 0) {
         // output data of each row
         while($row = mysqli_fetch_assoc($qryResult)) {
             $firstName=$row['firstName'];
             $lastName=$row['lastName'];
             $mobileNumber=$row['mobileNumber'];
         }
     } else {
         echo "";
     }
    switch ($type) {
       case "Maintenance":
         $message= "Dear ".$firstName." ".$lastName.",your maintenance amount of ".$this->amount."rs is added to your maintenance."; 
         break;
       case "maintenanceBill":
           $message= "Dear ".$firstName." ".$lastName.",your maintenance amount of ".$this->amount."rs is deducted from maintenance.";         
           break;
       case "electricBill":
          $message= "Dear ".$firstName." ".$lastName.",your electric bill of ".$this->amount."rs is deducted from maintenance.";         
          break;
       case "facility":
           $message= "Dear ".$firstName." ".$lastName.",your booking for ".$typeDetail->booked_facility." on ".$typeDetail->startTime." is successfully done.";            
           break;
       default:
       $message= "";
     }
     $message.="Remaining maitenance balalce is ".$this->currentMaintenanceBalance."rs";
     $messageDeatil->message=$message;
     $messageDeatil->mobileNumber=$mobileNumber;
     $messageDeatil->firstName=$firstName;
     $messageDeatil->lastName=$lastName;
     return $messageDeatil;
   }
   function SMSTemplateFamilyHead($type,$typeDetail,$sentMsgDetail){
    $messageDeatil=new stdClass();
    $SMSDetails=(array)json_decode($this->paymentDetail);
    if($SMSDetails['parentId'] == "-1"){
      return false;
    }
    $selectQry="select * from person where id='$this->userId'";
    $qryResult = mysqli_query($this->con, $selectQry);
     if (mysqli_num_rows($qryResult) > 0) {
         // output data of each row
         while($row = mysqli_fetch_assoc($qryResult)) {
             $firstName=$row['firstName'];
             $lastName=$row['lastName'];
             $mobileNumber=$row['mobileNumber'];
         }
     } else {
         echo "";
     }
   
    switch ($type) {
       case "Maintenance":
         $message= "Dear ".$firstName." ".$lastName.",your maintenance amount of ".$this->amount."rs is added to your maintenance by ".$sentMsgDetail->firstName; 
         break;
       case "maintenanceBill":
           $message= "Dear ".$firstName." ".$lastName.",your maintenance amount of ".$this->amount."rs is deducted from maintenance ".$sentMsgDetail->firstName;         
           break;
       case "electricBill":
          $message= "Dear ".$firstName." ".$lastName.",your electric bill of ".$this->amount."rs is deducted from maintenance ".$sentMsgDetail->firstName;          
          break;
       case "facility":
           $message= "Dear ".$firstName." ".$lastName.",your booking for ".$typeDetail->booked_facility." on ".$typeDetail->startTime." is successfully done by ".$sentMsgDetail->firstName;            
           break;
       default:
       $message= "";
     }
     $message.=".Remaining maitenance balalce is ".$this->currentMaintenanceBalance."rs";
     $messageDeatil->message=$message;
     $messageDeatil->mobileNumber=$mobileNumber; 
     return $messageDeatil;
   }
   function msgTextForFamilyHead(){
    //debit
    $messageTextForDebit=new stdClass();
    $messageTextForDebit->Maintenance= "Dear ".$firstName." ".$lastName.",your maintenance amount of ".$this->amount."rs is deducted from maintenance ".$sentMsgDetail->firstName; 
    $messageTextForDebit->facility= "Dear ".$firstName." ".$lastName.",your booking for ".$typeDetail->booked_facility." on ".$typeDetail->startTime." is successfully done by ".$sentMsgDetail->firstName;                  
     

    //credit
    $messageTextForCredit=new stdClass();
    $messageTextForDebit->Maintenance= "Dear ".$firstName." ".$lastName.",your maintenance amount of ".$this->amount."rs is deducted from maintenance ".$sentMsgDetail->firstName; 
    $messageTextForDebit->facility= "Dear ".$firstName." ".$lastName.",your booking for ".$typeDetail->booked_facility." on ".$typeDetail->startTime." is successfully done by ".$sentMsgDetail->firstName;                  
  }
}
?>