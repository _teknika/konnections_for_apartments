
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$minMaintenance = $req->minMaintenance;
$updatedBy = $req->updatedBy;

$updateQry="UPDATE maintenance_minimum SET min_maintenance_charge='$minMaintenance',updated_by=$updatedBy,updated_at=now() WHERE id=1";
if ($con->query($updateQry) === TRUE) {
    $isSuccessful=true;
    $error="";
} else {
    $isSuccessful=false;
    $error=$con->error;
}

$response = array('isSuccessful'=> $isSuccessful,'error'=>$error);
$response = json_encode($response);
echo $response;
$connection->closeConnection();
?>