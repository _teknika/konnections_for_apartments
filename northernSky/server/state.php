
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$sql = mysqli_query($con,"SELECT * FROM state");
$rows_count = mysqli_num_rows($sql);
$states=array();
if($rows_count!=0){
    while ($rows_fetch = mysqli_fetch_assoc($sql))
    {
        array_push($states, array('name' => $rows_fetch['name'], 'id'=>$rows_fetch['id'] ));
    }
}
$response = json_encode($states);
echo $response;
$connection->closeConnection();

?>
