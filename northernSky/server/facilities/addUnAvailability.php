<?php

require_once('../libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$facilityId=$req->facilityId;
$startTime=$req->starttime;
$endTime=$req->endtime;
$fromDate=$req->startDate;
$toDate=$req->endDate;
$sat=$req->sat;
$sun=$req->sun;
$qry="";

$date1 = new DateTime($fromDate);
$date2 = new DateTime($toDate);
while($date1 <= $date2){
	$startDate=date_format($date1,"Y-m-d");
	$startDateTime=$startDate."T".$startTime;
	$endDateTime=$startDate."T".$endTime;
	$dayofweek = date('w', strtotime($startDate));
  if($dayofweek==$sat || $dayofweek==$sun){

  }else{
	$qry.="INSERT INTO facility_unavailability(facility_id,start_time,end_time) VALUES ('$facilityId','$startDateTime','$endDateTime');";
	}
	
 date_add($date1,date_interval_create_from_date_string("1 days"));
}
if ($con->multi_query($qry) === TRUE) {
    $isSuccessful=true;
    $error="";
} else {
	$isSuccessful=false;
    $error=$con->error;
}

$response = array('isSuccessful' => $isSuccessful,'error'=>$error);
$response = json_encode($response);
$connection->closeConnection();
echo $response;

?>