<?php
  
require_once('../libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$facilityId=$req->facility;
$startTime=$req->startDate;
$endTime=$req->endDate;
$qry="SELECT FB.id,date(FB.start_time) as DateFormat,CONCAT(P.firstName,' ',P.lastName) as Name,CONCAT(P.mobileNumber,'/',alt_number) as mobileNum,CONCAT(time(FB.start_time),'-',time(FB.end_time)) as TimeFormat,FB.amount as Amount,FB.payment_status as Status,payment_detail,isCancelled FROM facility_booking FB INNER JOIN person P WHERE FB.personId=P.id and FB.facility_id='$facilityId' and FB.start_time>='$startTime' and FB.end_time<='$endTime' order by FB.start_time desc";
$sql = mysqli_query($con, $qry);
$rows_count = mysqli_num_rows($sql);
$resultData = array();
if ($rows_count != 0) {
    while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                $timeInfo = $rows_fetch;	
		        array_push($resultData, $timeInfo);
    }
}
$response = json_encode($resultData);
$connection->closeConnection();
echo $response;

?>