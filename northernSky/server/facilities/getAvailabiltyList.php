
<?php

require_once('../libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$facilityId=$req->facilityId;

$startDate=$req->start;
$endDate=$req->end;

//$sql = mysqli_query($con, "SELECT DATE_FORMAT(start_time, '%Y-%m-%dT%TZ') as start, DATE_FORMAT(end_time, '%Y-%m-%dT%TZ') as end,id, facility_id as title FROM availability");
$qry="SELECT  start_time as start, end_time as end,facility_id as title,id FROM facility_availability where facility_id='$facilityId' and start_time > '$startDate' and end_time < '$endDate'";
$sql = mysqli_query($con, $qry);
$rows_count = mysqli_num_rows($sql);
$resultData = array();
if ($rows_count != 0) {
    while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                $timeInfo = $rows_fetch;	
		        array_push($resultData, $timeInfo);
    }
}
$response = json_encode($resultData);
$connection->closeConnection();
echo $response;
?>