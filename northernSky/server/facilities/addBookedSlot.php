<?php
require_once('../addMaintenanceAmount.php');
require_once('../libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$req = json_decode($postdata);

class addBookedSlot
{
	public $personId;
	public $parentId;
	public $flat;
	public $firstName;
	public $lastName;
	public $mobileNumber;
	public $facilityId;
	public $weekId;
	public $startTime;
	public $endTime;
	public $amount;
	public $flatOwnerId;
    public $req=null;
    public $errorMsg="";
	function __construct($postdata)
	{  
		$req = json_decode($postdata);
		$this->personId=$req->personId;
		$this->parentId=$req->parentId;
		$this->flat=$req->flat;
		$this->firstName=$req->name;
		$this->lastName=$req->lastName;
		$this->mobileNumber=$req->mobileNumber;
		$this->facilityId=$req->facilityId;
		$this->weekId=$req->weekId;
		$this->startTime=$req->start;
		$this->endTime=$req->end;
		$this->amount=$req->chargePerHour;
		$this->flatOwnerId=$req->ownerId;
		$this->req=$req;
		//$this->parentId= -1 ? this->$flatOwnerId=$this->$parentId:$flatOwnerId=$this->$parentId;
	}
	function getErrorMsg(){
		return $this->errorMsg;
	}
	function checkMaintenaceBalance($con){
  //check mimimum maintenance
  $selectQry="SELECT * FROM maintainance mtnce join maintenance_minimum mtnce_min WHERE mtnce.balance > mtnce_min.min_maintenance_charge and mtnce.userId=".$this->flatOwnerId." and mtnce.flat='".$this->flat."'";
  $resultData = mysqli_query($con, $selectQry);

	if (mysqli_num_rows($resultData) > 0) {
		return  $this->DebuctMaintenanceBal($con);
	} else {
		
	    $this->errorMsg="you have low balance";
	    return false;
	}

  }
function DebuctMaintenanceBal($con){
	 $this->req->updated_by="101";
	 $this->req->amount=$this->amount;
	 $postdata=$this->req;
	 $maintenanceObj=new Maintenance(json_encode($postdata));
	 $maintenanceObj->connectDataBase();
	 $res=$maintenanceObj->deductFromMaintainanceTB();
	 $facilityName=$this->saveBookedSlotDetail($con,$postdata);
	 if($facilityName){
		 $typeDetail=new stdClass();
		 $typeDetail->booked_facility=$facilityName;
		 $typeDetail->startTime=$this->startTime;
		 $maintenanceObj->sendSMS($typeDetail);
	 }
     // $response = array('isSuccessful'=> $res);
}

//below function saves booked slot detail in facility_booking table
function saveBookedSlotDetail($con,$postdata){
	$reqDetail=json_encode($postdata);
	$reqDetail= mysqli_real_escape_string($con,$reqDetail);//removed special charecters
	$qry="INSERT INTO facility_booking(personId,facility_id,weekId,start_time,end_time,payment_status,payment_detail,amount,isCancelled) VALUES ($this->personId,$this->facilityId,$this->weekId,'$this->startTime','$this->endTime',0,'$reqDetail',$this->amount,0)";
	$sql_basic = mysqli_query($con,$qry);
	
	$id = mysqli_insert_id($con);
	if($id!=NULL){
		 $facilityNameByQry="select facility_name from facilities where id='$this->facilityId'";
		 $qry_res = mysqli_query($con,$facilityNameByQry);
		 $data_row = mysqli_fetch_assoc($qry_res);
	     $booked_facility= $data_row['facility_name'];		 
        return $booked_facility;
	}else{
		 $this->errorMsg=mysqli_error($con);
		 return false;
	}


}
}
$addBookedSlotObj=new addBookedSlot($postdata);

//ends hre
$txResult=$addBookedSlotObj->checkMaintenaceBalance($con);
$errorMsg=$addBookedSlotObj->getErrorMsg();
$response = array('isSuccessful' => $txResult,'error'=>$errorMsg);
$response = json_encode($response);
echo $response;
$connection->closeConnection();
?>