<?php
require_once('../libs/dbConnection.php');
$connection = new dbconnection();
$conn = $connection->connectToDatabase();
$id = $_GET['id'];
class DeleteFacilityManager{
    var $con=null;
    function __construct($con){
      $this->con=$con;
    }
    function reimburse($id,$slotDetail){
        require_once('../addMaintenanceAmount.php');
        $mainteanceObj=new Maintenance($slotDetail);
        $mainteanceObj->connectDataBase();
        $res=$mainteanceObj->addToMaintainanceTB();
        $isSMSent=$mainteanceObj->sendSMS(null);
        return $res;  
    }
    function cancellFacility($id){
        // Set autocommit to off
        mysqli_autocommit($this->con,FALSE);
        //update facility_booking
        $sql = "UPDATE facility_booking SET isCancelled='1' WHERE id='$id'";
        if ($this->con->query($sql) === TRUE) {
           $slotDetail=$this->bookedFacilitySlotDetail($id);
           $resp=$this->reimburse($id,$slotDetail);
            if($resp){
              // Commit transaction
              mysqli_commit($this->con);
            }
        } else {
            $isSuccessful = false;
        }
        $this->destory();
        return $resp;
    }
    function bookedFacilitySlotDetail($id){
        $sql = "SELECT payment_detail FROM facility_booking where id='$id'";
        $result = mysqli_query($this->con, $sql);
        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            $paymentDetail="";
            while($row = mysqli_fetch_assoc($result)) {
                $paymentDetail=$row['payment_detail'];
            }
            $paymentDetail=json_decode($paymentDetail);
            $paymentDetail->perticular="facility";
            $paymentDetail->isCancel=true;
            $paymentDetail=json_encode($paymentDetail);
            return $paymentDetail;
        } else {
            echo "no results";
        }

    }
    function destory(){
        $this->con=null;
    }
}
$facilityManager=new DeleteFacilityManager($conn);
$res=$facilityManager->cancellFacility($id);
$response = array('isSuccessful'=> $res);
$connection->closeConnection();
echo json_encode($response);  
?>