<?php 
 
 //$updatedBy=$_POST['updatedBy'];
$deduct_reason==$_POST['uploadingFor'];
 require_once('uploadExcel/uploadMaintenance.php');
 require_once('libs/dbConnection.php');
 $connection = new dbconnection();
 $con = $connection->connectToDatabase();
 
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
 
// Include Spout library 
require_once 'libs/exceluploadPlugin/spout/src/Spout/Autoloader/autoload.php';
 
// check file name is not empty
if (!empty($_FILES['file']['name'])) {
      
    // Get File extension eg. 'xlsx' to check file is excel sheet
    $pathinfo = pathinfo($_FILES["file"]["name"]);
     echo $_FILES['file']['tmp_name'];
    // check file has extension xlsx, xls and also check 
    // file is not empty
   if (($pathinfo['extension'] == 'xlsx' || $pathinfo['extension'] == 'xls') 
           && $_FILES['file']['size'] > 0 ) {
         
        // Temporary file name
        $inputFileName = $_FILES['file']['tmp_name']; 
    
        // Read excel file by using ReadFactory object.
        $reader = ReaderFactory::create(Type::XLSX);
        $writer = WriterFactory::create(Type::XLSX); // for XLSX files
        require_once("libs/fileUtil.php");
        $location=folderPath("Maintenance");
        $fileNameComingFromWeb=explode(".",$_FILES['file']['name']);
        $currentTimeInMilli= round(microtime(true) * 1000);
        $writer->openToFile($location.$fileNameComingFromWeb[0].$currentTimeInMilli.".".$fileNameComingFromWeb[1]);
       //$writer->openToBrowser("delete.xlsx"); 
        // Open file
        $reader->open($inputFileName);
        $count = 1;
        $failedRows=array();
        // Number of sheet in excel file
        foreach ($reader->getSheetIterator() as $sheet) {
             
            // Number of Rows in Excel sheet
            foreach ($sheet->getRowIterator() as $row) {
 
                // It reads data after header. In the my excel sheet, 
                // header is in the first row. 
                if ($count > 1) { 
 
                    // Data of excel sheet
                    $data['ownerName'] = $row[0];//flatId entered in excel sheet 
                    $data['flat'] = $row[1];//flatId entered in excel sheet 
                    $data['maintenanceCharge'] = $row[2];//amount entered in excel sheet
                    $processMaitenance=new uploadMaintenance();
                    $res=$processMaitenance->isUserID_flatExists($row[0],$row[1],$row[2]);
                   if($res===false){
                      $writer->addRow($data);
                       continue;
                    }
                }else{
                    $writer->addRow($row);//adding column names to excel
                }              
                 $count++;
         }
        }
        // Close excel file
        $writer->close();
        $reader->close();
        $processMaitenance->exportAction($location,$fileNameComingFromWeb[0].$currentTimeInMilli.".".$fileNameComingFromWeb[1]);
        
    } else {
 
        echo "Please Select Valid Excel File";
    }
 
} else {
 
    echo "Please Select Excel File";
     
}

?>
