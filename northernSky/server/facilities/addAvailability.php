<?php

require_once('../libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$facilityId=$req->facilityId;
$startTime=$req->start;
$endTime=$req->end;
$qry="INSERT INTO facility_availability(facility_id,start_time,end_time) VALUES ('$facilityId','$startTime','$endTime');";
if (mysqli_query($con, $qry)) {
    $isSuccessful=true;
    $error="";
    $id=mysqli_insert_id($con);
} else {
	$isSuccessful=false;
    $error=mysqli_error($con);
    $id="";
}

$response = array('isSuccessful' => $isSuccessful,'error'=>$error,'id'=>$id);
$response = json_encode($response);
$connection->closeConnection();
echo $response;

?>