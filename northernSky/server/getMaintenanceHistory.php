<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$req = json_decode($postdata);

$transactionDetailObj=new StdClass();
class maintenaceHistory{
    var $con=null,
        $ownerId,
        $flatNumber,
        $month;    
    var $transactionDetail=null;
    function __construct($con,$req,$transactionDetailObj){
        $this->con=$con;
        $this->ownerId=$req->ownerId;
        $this->month=$req->month;
        $this->flatNumber=$req->flat;
        $this->transactionDetail=$transactionDetailObj;

      }

    function isMaintenanceRecordExists(){
         //get owner Detail from maintenance main table
        $getUserDeatilQry="select id,balance from maintainance where userId='$this->ownerId' and flat='$this->flatNumber'";
        $resultQry= $this->con->query($getUserDeatilQry);
        if ($resultQry->num_rows > 0) {
            while($row = $resultQry->fetch_assoc()) {
                $maintenanceTableId=$row['id']; 
                $currentBal=$row['balance']; 
                $this->transactionDetail->currentBal=$currentBal;
                $this->getMaintenanceRecords($maintenanceTableId,$currentBal);
                return $this->transactionDetail;
            }
        } else {
            return $this->transactionDetail;
        }
        //ends hre
    }
    function getMaintenanceRecords($maintenanceTableId,$currentBal){
        $creditedBalQry="SELECT 'credit',sum(credit) as total from maintenance_history WHERE perticulars='Maintenance' and mId='$maintenanceTableId'";
        $facilityUsageQry="SELECT 'facilityUsage',sum(debit) as total  from maintenance_history WHERE perticulars='facility' and mId='$maintenanceTableId' and details like '%isCancel=false%'";
        $maintenanceUsageQry="SELECT 'maintenanceUsage',sum(debit) as total from maintenance_history WHERE perticulars='Maintenance Bill' and mId='$maintenanceTableId'";
        $ElectricityUsageQry="SELECT 'electricityUsage' ,sum(debit) as total from maintenance_history WHERE perticulars='Electricity Bill' and mId='$maintenanceTableId'";
        strlen($this->month) < 2 ? $this->month="0".$this->month : "";
        $datePart=date("Y")."-".$this->month;
        $additionalCondition=" and date_format( created_at, '%Y-%m' ) ='".$datePart."'";
        $creditedBalQry .= $additionalCondition;
        $facilityUsageQry .= $additionalCondition;
        $maintenanceUsageQry .= $additionalCondition;
        $ElectricityUsageQry .= $additionalCondition;
        //this below query checks for balance based on previous month last transaction date 
        //opening balance is equal to last month closing balance hence taking last month in query
        $lastMonth=($this->month)-1;
        $openingBalance="SELECT 'openingBal' as bal,balance FROM maintenance_history where date_format( created_at, '%Y-%m' ) = concat(YEAR(CURDATE()),'-',$lastMonth) and mId='$maintenanceTableId' order by created_at desc limit 1";

        $closingBalance="SELECT 'closingBalance' as bal,balance FROM maintenance_history where date_format( created_at, '%Y-%m' ) = concat(YEAR(CURDATE()),'-',$this->month) and mId='$maintenanceTableId' order by created_at desc limit 1";

        $qryArr = array($creditedBalQry,$facilityUsageQry,$maintenanceUsageQry,$ElectricityUsageQry,$openingBalance,$closingBalance);
        foreach ($qryArr as $qry) {
            //$qry.=" and mID='$ownerId'";
           // echo $qry;
            $result = $this->con->query($qry);
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $rowData = array_values($row);
                    $key=$rowData[0];
                    $val=$rowData[1];
                    $this->transactionDetail->$key=$val;   
                }
            } else {
               // echo "error";
               $this->transactionDetail->balance=null;  
            }
        }
    }
}

$maintenaceHistoryObj=new maintenaceHistory($con,$req,$transactionDetailObj);
$transactionDetail=$maintenaceHistoryObj->isMaintenanceRecordExists();
$response = json_encode($transactionDetail);
echo $response;
$connection->closeConnection();
?>