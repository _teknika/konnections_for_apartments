
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

Class SMSTracker{
	public function getSmsTracker($request, $con){
		$orgId = $request->request->orgId;
		$query = "SELECT p.id, p.firstName, p.middleName, p.lastName, sp.name, sp.noOfSms, sc.purchaseDate, sc.sentSms FROM person p, sms_counter sc, sms_pack sp WHERE p.orgId = '$orgId' && sc.packId = sp.id && sc.personId = p.id";
		$sql = mysqli_query($con,$query);

		$rows_count = mysqli_num_rows($sql);

		
		$data=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql))
		    {
		    	$info = $rows_fetch;	
		    	array_push($data, $info);
		        $isSuccessful = true;
		        $code = 200;
		    }
		}else{
			$isSuccessful = false;
		    $code = 202;
		}

		$response = array("resp"=>$data, "code"=>$code, "isSuccessful"=>$isSuccessful);
		return $response;

	}
}

$contactsObj = new SMSTracker();
$result = $contactsObj->getSmsTracker($request, $con);
$response = json_encode($result);
echo $response;
$connection->closeConnection();

?>
