
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

Class ContactsMinimalInfo{

	public function getPersonalInfo($request, $con){
		$orgId = $request->request->orgId;
		$limit = $request->request->count;
		$pageNo = $request->request->pageNo;
		$head = $request->request->head;
		$parentId = $request->request->parentId;


		
		if($head){
			$query = "SELECT id, parentId, firstName,lastName, mobileNumber FROM person WHERE orgId=$orgId AND parentId in (-1,-2)";
		}else{
			$query = "SELECT id, parentId, firstName,  lastName, mobileNumber FROM person WHERE orgId=$orgId AND parentId=$parentId";
		}


		if($limit != -1){
			$query = $query." LIMIT $limit";
		}
		
		$sql = mysqli_query($con, $query);



		$rows_count = mysqli_num_rows($sql);

		$data=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql))
		    {
		    	//var_dump($rows_fetch);
		    	$info = $rows_fetch;	
		    	array_push($data, $info);
		        $isSuccessful = true;
		        $code = 200;
		    }
		}else{
			$isSuccessful = true;
		    $code = 202;
		}

		$response = array("resp"=>$data, "code"=>$code, "isSuccessful"=>$isSuccessful);
		return $response;
	}

}

$contactsObj = new ContactsMinimalInfo();
$contacts = $contactsObj->getPersonalInfo($request, $con);



$response = json_encode($contacts);

echo $response;
$connection->closeConnection();

?>
