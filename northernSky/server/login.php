<?php
header('Content-Type: application/json; charset=utf-8');
header("Access-Control-Allow-Origin: *");
ob_start();
session_start();

require_once('libs/dbConnection.php');

$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$username = $request->request->username;
$password = $request->request->password;

$sql = mysqli_query($con,"SELECT pu.* , p.* FROM portal_user pu left outer join person p on pu.personId = p.id WHERE pu.username='".$username."' && pu.password='".$password."' && pu.orgId=p.orgId");
$rows_count = mysqli_num_rows($sql);

if($rows_count != 0){
	$isSuccessful = true;
	if($rows_count!=0){
	    while ($rows_fetch = mysqli_fetch_assoc($sql))
	    {
	    	$userId = $rows_fetch['personId'];
	    	$userName = $rows_fetch['username'];
	    	$userInfo = $rows_fetch;
	    	$name = "".$rows_fetch['firstName']." ".$rows_fetch['lastName'];
	    }
	}
	
}else{
	$isSuccessful = false;
	$userId = "";
	$userName = "";
	$userInfo = "";
}

$response = array("isSuccessful"=>$isSuccessful, "userId"=>$userId, "userName"=>$username, "userInfo"=> $userInfo);

$response = json_encode($response);

session_regenerate_id();
$_SESSION['userId'] = $userId;
$_SESSION['last_activity']= time();
session_write_close();

echo $response;
$connection->closeConnection();

?>
