
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

Class SMSHistory{
	public function getSmsTracker($request, $con){
		$orgId = $request->request->orgId;
		$senderId = $request->request->userId;
		$type = $request->request->type;

		$query = "select s.firstName as sFname, s.middleName as sMname, s.lastName as sLname, r.firstName as rFname, r.middleName as rMname, r.lastName as rLname, n.message as message, n.transactionTime, n.type, n.senderId FROM notifications n left outer join person s on n.senderId = s.id left outer join person r on r.id = n.receiverId where n.type = '$type'";
		if($type == 0){
			$query = $query." && n.receiverId = '$senderId'";
		}else{
			$query = $query." && n.senderId = '$senderId'";
		}

		$sql = mysqli_query($con,$query);
		$rows_count = mysqli_num_rows($sql);
		
		$data=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql))
		    {
		    	$info = $rows_fetch;	
		    	array_push($data, $info);
		    	$isSuccessful = true;
		    	$code = 200;
		    }
		}else{
			$isSuccessful = false;
		    $code = 202;
		}

		$response = array("resp"=>$data, "code"=>$code, "isSuccessful"=>$isSuccessful);
		return $response;

	}
}

$contactsObj = new SMSHistory();
$result = $contactsObj->getSmsTracker($request, $con);
$response = json_encode($result);
echo $response;
$connection->closeConnection();

?>
