
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$req = json_decode($postdata);

$request = $req->request;



Class ContactReg {

	public function regBasic($request, $con){

		// Basic Info
		$parentid = $request->basicInfo->parentid;
		$ishead = $request->basicInfo->ishead;
		$orgId = $request->basicInfo->orgId;
		$role = $request->basicInfo->role;
		$relationwithhead = $request->basicInfo->relationwithhead;
		$firstname = $request->basicInfo->firstname;
		$middlename = $request->basicInfo->middlename;
		$lastname = $request->basicInfo->lastname;
		$fathername = $request->basicInfo->fathername;
		$mothername = $request->basicInfo->mothername;
		$mobilenumber = $request->basicInfo->mobilenumber;
		$businesscategory = $request->basicInfo->businesscategory;
		$businessoccupation = $request->basicInfo->businessoccupation;
		$dob = $request->basicInfo->dob;
		$gender = $request->basicInfo->gender;
		$dateofmarriage = $request->basicInfo->dateofmarriage;
		$bloodgroup= $request->basicInfo->bloodgroup;
		$photo= $request->basicInfo->photo;
		


		$sql_basic = mysqli_query($con,"INSERT INTO person(orgId,parentId,firstName,middleName,lastName,fatherName, motherName, isHead,businessCategory, businessOccupation,dob,gender,mobileNumber,bloodGroup, photo, role, relationWithHead) VALUES ('$orgId','$parentid','$firstname', '$middlename', '$lastname', '$fathername', '$mothername', '$ishead', '$businesscategory', '$businessoccupation', '$dob', '$gender', '$mobilenumber','$bloodgroup', '$photo', '$role', '$relationwithhead')");

		$user_id = mysqli_insert_id($con);

		if(!empty($dateofmarriage)){
			$sql_update = mysqli_query($con, "UPDATE person SET dateOfMarriage= '$dateofmarriage' WHERE id='$user_id'");
		} else{
			$sql_update = mysqli_query($con, "UPDATE person SET dateOfMarriage= NULL WHERE id='$user_id'");
		}


		return $user_id;
	}

	function regNativeAdd($user_id, $request, $con){
		// Native Add Info
		$native_type = $request->nativeAddInfo->type;
		$native_door = $request->nativeAddInfo->door;
		$native_cross = $request->nativeAddInfo->cross;
		$native_street = $request->nativeAddInfo->street;
		$native_area = $request->nativeAddInfo->area;
		$native_landmark = $request->nativeAddInfo->landmark;
		$native_pincode = $request->nativeAddInfo->pincode;
		$native_city = $request->nativeAddInfo->city;
		$native_country = $request->nativeAddInfo->country;
		$native_phone = $request->nativeAddInfo->phone;
		$native_email = $request->nativeAddInfo->email;

		$sql_native = mysqli_query($con,"INSERT INTO contact(`personId`, `type`, `door`, `cross`, `street`, `area`, `landmark`, `cityId`, `country`, `pincode`, `phone`, `email`) VALUES ('$user_id','$native_type','$native_door', '$native_cross', '$native_street', '$native_area', '$native_landmark', '$native_city', '$native_country', '$native_pincode','$native_phone', '$native_email')");
		return $sql_native;
	}

	public function regHomeAdd($user_id, $request, $con){
		// Home Add Info
		$home_type = $request->homeAddInfo->type;
		$home_door = $request->homeAddInfo->door;
		$home_cross = $request->homeAddInfo->cross;
		$home_street = $request->homeAddInfo->street;
		$home_area = $request->homeAddInfo->area;
		$home_landmark = $request->homeAddInfo->landmark;
		$home_pincode = $request->homeAddInfo->pincode;
		$home_city = $request->homeAddInfo->city;
		$home_country = $request->homeAddInfo->country;
		$home_phone = $request->homeAddInfo->phone;
		$home_email = $request->homeAddInfo->email;

		$sql_home = mysqli_query($con,"INSERT INTO contact(`personId`, `type`, `door`, `cross`, `street`, `area`, `landmark`, `cityId`, `country`, `pincode`, `phone`, `email`) VALUES ('$user_id','$home_type','$home_door', '$home_cross', '$home_street', '$home_area', '$home_landmark', '$home_city', '$home_country', '$home_pincode','$home_phone', '$home_email')");

		return $sql_home;
	}

	public function regOfficeAdd($user_id, $request, $con){
		// Office Add Info
		$office_type = $request->officeAddInfo->type;
		$office_door = $request->officeAddInfo->door;
		$office_name = $request->officeAddInfo->name;
		$office_cross = $request->officeAddInfo->cross;
		$office_street = $request->officeAddInfo->street;
		$office_area = $request->officeAddInfo->area;
		$office_landmark = $request->officeAddInfo->landmark;
		$office_pincode = $request->officeAddInfo->pincode;
		$office_city = $request->officeAddInfo->city;
		$office_country = $request->officeAddInfo->country;
		$office_phone = $request->officeAddInfo->phone;
		$office_email = $request->officeAddInfo->email;

		$sql_office = mysqli_query($con,"INSERT INTO contact(`personId`, `type`, `door`, `cross`, `street`, `area`, `landmark`, `cityId`, `country`, `pincode`, `phone`, `email`, `name`) VALUES ('$user_id','$office_type','$office_door', '$office_cross', '$office_street', '$office_area', '$office_landmark', '$office_city', '$office_country', '$office_pincode','$office_phone', '$office_email', '$office_name')");
		$office_id = mysqli_insert_id($con);

		return $office_id;
	}
}




// Object

$contactsObj = new ContactReg();
$user_id = $contactsObj->regBasic($request, $con);

if($user_id != 0){
	$isSuccessful = true;
	$native = $contactsObj->regNativeAdd($user_id, $request, $con);
	$home = $contactsObj->regHomeAdd($user_id, $request, $con);
	$office_id = $contactsObj->regOfficeAdd($user_id, $request, $con);
	if($office_id != 0){
		$isSuccessful = TRUE;
		$msg = "SUCCESS";
	}else{
		$isSuccessful = false;
		$msg = "NATIVE_FAIL";
	}
}else{
	$isSuccessful = false;
	$msg = "BASIC_FAIL";
}

$response = array('isSuccessful'=> $isSuccessful , 'userId'=> $user_id, "msg"=>$msg);
$response = json_encode($response);
echo $response;
$connection->closeConnection();
?>
