<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$conn = $connection->connectToDatabase();
$id = $_GET['id'];
 class serviceRequestManager{
     var $con=null;
     var $reqId;
     function __construct($con,$id){
         $this->con=$con;
         $this->request=$id;
     }
     function closeService(){
        $sql = "UPDATE requests set service_status=1 ,closed_at=now() where id='$this->request'";
        if (mysqli_query($this->con, $sql)) {
           return $isSuccessful = true;
        } else {
            return $isSuccessful = false;
        }
     } 
     function getUserDetailOfService(){
         //beow query joins requests,services and person table 
        //joing request and services tables gives service name
        //joind request and person gives person detail
        $userDetailQry="SELECT service.service_name,prsn.firstName,prsn.lastName,prsn.mobileNumber from requests req inner JOIN services service on req.service=service.id  INNER JOIN person prsn on req.userId=prsn.id WHERE req.id=$this->request";
        $result = mysqli_query($this->con, $userDetailQry);
        if(mysqli_num_rows($result) > 0) {
            // output data of each row
            $messageInfo=new stdClass();
            while($row = mysqli_fetch_assoc($result)) {
                $messageInfo->serviceName=$row['service_name'];
                $messageInfo->firstName=$row['firstName'];
                $messageInfo->lastName=$row['lastName'];
                $messageInfo->mobileNumber=$row['mobileNumber'];
            }
        } else {
            echo "0 results";
        }
        return  $messageInfo; 
     }
     function sendSms($messageInfo){
        require_once('libs/smsGatewayConstants.php');
        require_once('libs/smsGateway.php');
        $dateDMY=date("d-m-y");
        $message="Dear ".$messageInfo->firstName.",\nSERVICE CLOSED: This is to inform that your request for ".$messageInfo->serviceName." service is closed successfully on ". $dateDMY; 
        $obj = new Sender(_HOST,_PORT,_USERNAME,_PASSWORD,_SOURCE,$message,$messageInfo->mobileNumber,_TYPE,_DLR);
        $result = $obj->Submit();
        $del=$result;
     }
 }
 $serviceManager=new serviceRequestManager($conn,$id);
 $isClosed=$serviceManager->closeService();
 $userDetail=$serviceManager->getUserDetailOfService();
 $serviceManager->sendSms($userDetail);
 $response = array('isSuccessful' => $isClosed);
 $response = json_encode($response);
 echo $response;
 $connection->closeConnection();
?>
