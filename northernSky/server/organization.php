
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$sql = mysqli_query($con,"SELECT * FROM organization");
$rows_count = mysqli_num_rows($sql);
$org=array();
if($rows_count!=0){
    while ($rows_fetch = mysqli_fetch_assoc($sql))
    {
        array_push($org, array('name' => $rows_fetch['name'],'id'=>$rows_fetch['id'], 'cityId'=>$rows_fetch['cityId'] , 'stateId'=>$rows_fetch['stateId'] ));
    }
}
$response = json_encode($org);
echo $response;
$connection->closeConnection();

?>
