
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

Class MemberStats{

	public function getAllMember($request, $con){
		$orgId = $request->request->orgId;
		$query = "SELECT * FROM person WHERE orgId='$orgId' and isHead!=2 ";
		$sql = mysqli_query($con, $query);
		$rowcount=mysqli_num_rows($sql);
		return $rowcount;
	}

	public function getParentMember($request, $con){
		$orgId = $request->request->orgId;
		$parentId = -1;
		$query = "SELECT * FROM person WHERE orgId='".$orgId."' && parentId='-1' ";
		$sql = mysqli_query($con, $query);
		$rowcount=mysqli_num_rows($sql);
		return $rowcount;
	}

	public function getNotificationCount($request, $con){
		$orgId = $request->request->orgId;
		$type = $request->request->type;
		$userId = $request->request->userId;	
		$query = "SELECT * FROM notifications WHERE receiverId='".$userId."' && type='".$type."' ";
		$sql = mysqli_query($con, $query);
		$rowcount=mysqli_num_rows($sql);
		return $rowcount;
	}

	public function getCitiesCount($request, $con){
		$orgId = $request->request->orgId;
		$type = $request->request->type;
		$userId = $request->request->userId;	
		$query = "SELECT * FROM city";
		$sql = mysqli_query($con, $query);
		$rowcount=mysqli_num_rows($sql);
		return $rowcount;
	}

}

$contactsObj = new MemberStats();
$allMember = $contactsObj->getAllMember($request, $con);

$parentMember = $contactsObj->getParentMember($request, $con);
$isSuccessful = true;

/*$notificationCount = $contactsObj->getNotificationCount($request, $con);
$cityCount = $contactsObj->getCitiesCount($request, $con);*/

$response = array("resp"=>array("memberCount"=>$allMember, "parentCount"=>$parentMember, "code"=>200, "isSuccessful"=>$isSuccessful));
$response = json_encode($response);
echo $response;
$connection->closeConnection();

?>
