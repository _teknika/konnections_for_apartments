
<?php
// Merchant key here as provided by Payu
$MERCHANT_KEY = "hDkYGPQe";
 
// Merchant Salt as provided by Payu
$SALT = "yIEkykqEH3";
// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://test.payu.in";
$northenSkyServerName="http://".$_SERVER['HTTP_HOST'];
$s_Url=$northenSkyServerName."/NSTEST/server/payu/success.php";
$f_url=$northenSkyServerName."/NSTEST/server/payu/failure.php";
$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
	
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
  <head>
   <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../admin/aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
    /*function getParameterByName(name) {
          url = window.location.href;
          name = name.replace(/[\[\]]/g, "\\$&");
          var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
              results = regex.exec(url);
          if (!results) return null;
          if (!results[2]) return '';
          return decodeURIComponent(results[2].replace(/\+/g, " "));
      }*/
     
  </script>
  <style >
      body{
        background: rgba(206, 203, 203, 0.3);;
      }
        #paymentDetailForm input {
            background: transparent !important;
            color: #944848 !important;
            padding: 0px;
            /* outline: none !important; */
            border: none !important;
        }
  </style>
  </head>
  <body onload="submitPayuForm()">
    <h4 class="text-center">Payment Details</h4>
    <br/>
    <?php if($formError) { ?>
	
      <span style="color:red">Please fill all mandatory fields.</span>
      <br/>
      <br/>
    <?php } ?>
    <form action="<?php echo $action; ?>" method="post" name="payuForm">
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="salt" value="<?php echo $SALT ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
      <input type="hidden" name="surl" value="<?php echo $s_Url ?>" size="64" />
      <input type="hidden" name="furl" value="<?php echo $f_url ?>" size="64" />
   <!--    <input id="udf1" name="udf1" value="" />
      <input  id="udf2" name="udf2" value="" />
       <input  id="udf3" name="udf3" value="" /> -->


       <!-- payment detail start-->

           
               <div class="form container" id="paymentDetailForm">
                <div class="row">
                  <div class="form-group labe-clor col-xs-6 cl-md-6">
                    <label for="firstname"><i class="material-icons" style="color: rgba(175, 97, 97, 0.78);margin-right: 6px;font-size: 17px;">games</i>Flat holder Name</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="firstname" name="firstname" readonly> 
                  </div>
                   <div class="form-group labe-clor col-xs-6 cl-md-6">
                    <label for="mobile_no"><i class="material-icons" style="color: rgba(175, 97, 97, 0.78);margin-right: 6px;font-size: 17px;">games</i>Mobile</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="phone" name="phone" readonly> 
                  </div>
                </div>

                  <div class="row">
                   <div class="form-group labe-clor col-xs-6 cl-md-6">
                    <label for="email" class="label-custom-css"><span class="glyphicon glyphicon-calendar" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Email</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="email" name="email" readonly> 
                  </div>
                   <div class="form-group labe-clor col-xs-6 cl-md-6">
                    <label for="amount" class="label-custom-css"><span class="glyphicon glyphicon-calendar" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Amount</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="amount" name="amount" readonly> 
                  </div>
                </div> 

                <div class="row">
                   <div class="form-group labe-clor col-xs-6 cl-md-6">
                    <label for="block" class="label-custom-css"><span class="glyphicon glyphicon-calendar" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Block</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="block" name="block" readonly> 
                  </div>
                   <div class="form-group labe-clor col-xs-6 cl-md-6">
                    <label for="flat" class="label-custom-css"><span class="glyphicon glyphicon-calendar" style="color: rgba(175, 97, 97, 0.78);
                    margin-right: 6px"></span>Flat</label>
                    <input type="text" class="form-control inputHeight custm-padding" id="flat" name="flat" readonly> 
                  </div>
                </div> 

                <div class="row">
                   <div class="form-group labe-clor col-xs-6 cl-md-6">
                   
                    <input type="hidden" class="form-control inputHeight custm-padding" id="productinfo" name="productinfo" value="Test" readonly> 
                  </div>
                   <div class="form-group labe-clor col-xs-6 cl-md-6">
              
                    <input class="form-control inputHeight custm-padding" type="hidden" name="service_provider" value="payu_paisa" size="64" > 
                  </div>
                </div> 
               
              <div class="row" style="position: relative;top: 10px;">
              <?php if(!$hash) { ?>
              <button type="submit" class="btn btn-default col-xs-offset-3 col-xs-6" style="background: linear-gradient(to right , #25b9af, #30cdde);
    color: white;">Proceed</button>
               <?php } ?>
              </div>
             </div> 
              
       <!-- payment detail ends-->


       <table style="visibility:hidden;"> 
 
        <tr>
          <td><b>Optional Parameters</b></td>
        </tr>
        <tr>
          <td>Last Name: </td>
          <td><input name="lastname" id="lastname" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" /></td>
          <td>Cancel URI: </td>
          <td><input name="curl" value="" /></td>
        </tr>
        <tr>
          <td>Address1: </td>
          <td><input name="address1" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" /></td>
          <td>Address2: </td>
          <td><input name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" /></td>
        </tr>
        <tr>
          <td>City: </td>
          <td><input name="city" value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" /></td>
          <td>State: </td>
          <td><input name="state" value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" /></td>
        </tr>
        <tr>
          <td>Country: </td>
          <td><input name="country" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" /></td>
          <td>Zipcode: </td>
          <td><input name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" /></td>
        </tr>
        <tr>
          <td>UDF1: </td>
          <td><input id="udf1" name="udf1" value="<?php echo ($_GET['facilityHeadId']) ?>" /></td>
          <td>UDF2: </td>
          <td><input id="udf2" name="udf2" value="<?php echo ($_GET['bookedBy']) ?>" /></td>
        </tr>
        <tr>
          <td>UDF3: </td>
          <td><input id="udf3" name="udf3" value="<?php echo ($_GET['facilityName']) ?>" /></td>
          <td>UDF4: </td>
          <td><input id="udf4" name="udf4" value="<?php echo ($_GET['BookedId']) ?>" /></td>
        </tr>
        <tr>
          <td>UDF5: </td>
          <td><input name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" /></td>
          <td>PG: </td>
          <td><input name="pg" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>" /></td>
        </tr>
      </table>
       <!--  <tr>
          <?php if(!$hash) { ?>
            <td colspan="4"><input type="submit" value="Submit" /></td>
          <?php } ?>
        </tr>  -->
      
    </form>


<script src="../../admin/aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../../admin//aLTE/plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="../../admin//aLTE/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
     <?php
        require_once('../libs/dbConnection.php');
        $connection = new dbconnection();
        $con = $connection->connectToDatabase();

        $parentId=$_GET['bookedBy'];
        $sql = mysqli_query($con, "SELECT * FROM person where id='$parentId'");
        $rows_count = mysqli_num_rows($sql);
        $userDataArry = array();
        if ($rows_count != 0) {
            while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                        $info = $rows_fetch;  
                    array_push($userDataArry, $info);
            }
        }
        echo "var payuDetail = " . json_encode($userDataArry[0]);
        $connection->closeConnection();
     ?> 
</script>
<script>
  $(function(){
     $("#firstname").val(payuDetail.firstName);
     $("#phone").val(payuDetail.mobileNumber);
     $("#block").val(payuDetail.blockName);
     $("#flat").val(payuDetail.flatNo);
     $("#amount").val(100);
     $("#email").val(payuDetail.emailId);
     
  });
</script>
  </body>
</html>
