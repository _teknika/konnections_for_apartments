 <?php


$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$FlatHeadId=$_POST["udf1"];
$BookedBy=$_POST["udf2"];
$facilityName=$_POST["udf3"];
$bookingId=$_POST["udf4"];
$salt="yIEkykqEH3";

//creating response json
  $payu_response = json_decode(file_get_contents('php://input'), true);
//

If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        
                  }
  else {    

        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

         }
     $hash = hash("sha512", $retHashSeq);
     
      
       
              $userId=$FlatHeadId;
              $BookedBy=$BookedBy;
              $isSuccessful=false;
                
              require_once('../libs/dbConnection.php');
              $connection = new dbconnection();
              $con = $connection->connectToDatabase();
              try{
                // Set autocommit to off
                    mysqli_autocommit($con,FALSE);    
              
                   
                  $transactionDetail = mysqli_real_escape_string($con, $payu_response);
                  $updateQry="UPDATE `facility_booking` SET `payment_status` = '1', `payment_detail` = '$transactionDetail' WHERE `facility_booking`.`id` = '$bookingId';";
                    mysqli_query($con,$updateQry);
                   
                        $con->commit();
                        $isSuccessful=true;
                     
                }
                catch(Exception $e){
                    $con->rollBack();
                    
                }
              
              

              $response = array('isSuccessful' => $isSuccessful, 'response' => $FlatHeadId);
              $response = json_encode($response);
             /* echo $response;*/
              $connection->closeConnection();
               
       
?>  


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>bookAppointment page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../admin/aLTE/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  
 <style >
      .payuSuccess{
            margin: 10px;
            padding:16px;
            background: white;
            border-radius: 8px;
            display: flex;
           flex-direction: column;
           align-items: center;
          text-align: center;
      }
      .paymentInwords,.pay-success-detail{
        display: inline;
      }
 </style>

   <script type="text/javascript">
      function closeWindow(){
         window.close();
      }
     
   </script>
</head>
<body style="background-color: #f1f1f1;height: 100%">
<div class="payuSuccess">
   <span class="glyphicon glyphicon-ok" style="color: #799e06;font-size: 30px;"></span>
  <h2 class="paymentInwords"><small>Payment Success</small></h2>
  <h5 class="pay-success-detail">We have received a payment of Rs. <?php echo $amount ?> for the <?php echo ($_POST["udf3"])  ?> booking. </h5>
  <button class="btn btn-success" onclick="closeWindow()" >GO BACK</button>
</div>

<script src="../../admin/aLTE/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../../admin//aLTE/plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="../../admin//aLTE/bootstrap/js/bootstrap.min.js"></script>



  </body>
  </html>