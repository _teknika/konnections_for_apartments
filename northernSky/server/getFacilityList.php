<?php

require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$sql = mysqli_query($con, "SELECT * FROM facilities f ORDER by f.facility_name");
$rows_count = mysqli_num_rows($sql);
$data = array();
if ($rows_count != 0) {
    while ($rows_fetch = mysqli_fetch_assoc($sql)) {
                $info = $rows_fetch;	
		        array_push($data, $info);
    }
}
$response = json_encode($data);
$connection->closeConnection();
echo $response;
?> 