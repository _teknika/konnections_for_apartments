
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

Class ContactInfo{

	public function getPersonalInfo($request, $con){
		$orgId = $request->request->orgId;
		$mobileNumber = $request->request->mobileNumber;

		$query = "SELECT * FROM person WHERE orgId='".$orgId."' && mobileNumber='".$mobileNumber."' ";
		
		$sql = mysqli_query($con, $query);

		$rows_count = mysqli_num_rows($sql);

		$data=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql))
		    {
		    	//$contacts = $this->getAddressInfo($rows_fetch['id'], $con);	
		    	$members = $this->getMembersInfo($rows_fetch['id'], $con, $orgId);	
				$rows_fetch['privacy_mask']=json_decode( $rows_fetch['privacy_mask']);
				$info = $rows_fetch;	
		    	//$info['photo'] = base64_encode($rows_fetch['photo']);
		    	//$info['contacts'] = $contacts;
		    	$info['members'] = $members;
		        array_push($data, $info);
		        $isSuccessful = true;
		        $code = 200;
		    }
		}else{
			$isSuccessful = true;
		    $code = 202;
		}

		$response = array("resp"=>$data, "code"=>$code, "isSuccessful"=>$isSuccessful);
		return $response;
	}




	public function getAddressInfo($personId, $con){
		$sql_address = mysqli_query($con,"SELECT * FROM contact WHERE personId='".$personId."' ");
		$rows_count = mysqli_num_rows($sql_address);
		$address=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql_address))
		    {
		    	array_push($address, $rows_fetch);
		    }
		    return $address;
		}
	}

	public function getMembersInfo($personId, $con, $orgId){
		$sql_members = mysqli_query($con,"SELECT * FROM person WHERE parentId='".$personId."' && orgId='".$orgId."' ");
		$rows_count = mysqli_num_rows($sql_members);
		$members=array();
		if($rows_count!=0){
		    while ($rows_fetch = mysqli_fetch_assoc($sql_members))
		    {
				//$rows_fetch['photo'] =base64_encode($rows_fetch['photo']);
				$rows_fetch['privacy_mask']=json_decode( $rows_fetch['privacy_mask']);
		    	array_push($members, $rows_fetch);
		    }
		    return $members;
		}
	}

}

$contactsObj = new ContactInfo();
$contacts = $contactsObj->getPersonalInfo($request, $con);

$response = json_encode($contacts);
echo $response;
$connection->closeConnection();

?>
