<?php

require_once('../libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$id=$req->id;
$dateChangedInDashboard=$req->dateChangedInDashboard;
$totalCharge=$req->totalCharge;
$qry="UPDATE facility_cards_detail SET changed_end_time='$dateChangedInDashboard',total_charge='$totalCharge',is_completed=1,updated_on=now() WHERE id='$id'";
if (mysqli_query($con, $qry)) {
    $isSuccessful=true;
    $error="";
} else {
	$isSuccessful=false;
    $error=mysqli_error($con);
}

$response = array('isSuccessful' => $isSuccessful,'error'=>$error);
$response = json_encode($response);
$connection->closeConnection();
echo $response;

?>