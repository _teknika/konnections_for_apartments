<?php
//this api returns family mambers names by flat id
require_once('../libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$sqlQry = "SELECT DISTINCT(f.facility_name),fm.facility_id FROM facilities f INNER JOIN facility_cards_main fm on f.id=fm.facility_id";
$catagoryList = array();
$result = mysqli_query($con, $sqlQry);
if (mysqli_num_rows($result) > 0)  {
    while ($rows_fetch = mysqli_fetch_assoc($result)) {
                $info = $rows_fetch;	
		        array_push($catagoryList, $info);
    }
}
$response=json_encode($catagoryList);
$connection->closeConnection();
echo $response;
?>