<?php
//this api returns family members names by flat id
require_once('../libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$facilityId=$_GET['facilityId'];

$sqlQry = "SELECT cardDetail.id,cardMain.card,fc.facility_name,cardDetail.issued_date,cardDetail.flat,cardDetail.name FROM facility_cards_detail cardDetail inner join facility_cards_main cardMain on cardDetail.card_id=cardMain.id inner join facilities fc on cardMain.facility_id=fc.id where cardDetail.is_completed=0";
$data = array();
$result = mysqli_query($con, $sqlQry);
if (mysqli_num_rows($result) > 0)  {
    while ($rows_fetch = mysqli_fetch_assoc($result)) {
                $info = $rows_fetch;	
		        array_push($data, $info);
    }
}
$response = json_encode($data);
$connection->closeConnection();
echo $response;
?>