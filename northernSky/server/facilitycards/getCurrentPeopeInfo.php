<?php
//this api returns family mambers names by flat id
require_once('../libs/dbConnection.php');
$connection = new dbconnection();
$conn = $connection->connectToDatabase();
class currentPeopleInfo{
    var $con=null;
    function __construct($conn){
        $this->con=$conn; 
    }
    function getCategories(){
        $sqlQry = "SELECT DISTINCT(f.facility_name),fm.facility_id FROM facilities f INNER JOIN facility_cards_main fm on f.id=fm.facility_id";
        $catagoryList = array();
        $result = mysqli_query($this->con, $sqlQry);
        if (mysqli_num_rows($result) > 0){
            while ($rows_fetch = mysqli_fetch_assoc($result)) {
                        $info = $rows_fetch;	
                        array_push($catagoryList, $info);
            }
        }
        return  $catagoryList;
    } 
    function getcurrentPeopleCountInfo($categoryList){
        $data = array();
        foreach ($categoryList as $detail) {        
            $detail=$this->getCountByFacility($detail);
            array_push($data, $detail);
        }
        $this->con=null;
        return $data;
    }
    function getCountByFacility($categoryInfo){
            $totalCards=0;
            $totalCards=$this->getTotalNumberOfcards($categoryInfo['facility_id']);
            $isCardIssuedQry="select * from facility_cards_main fcm INNER JOIN facility_cards_detail fcd on fcm.id=fcd.card_id where fcm.facility_id='$categoryInfo[facility_id]' and fcd.is_completed=0"; 
            $result = mysqli_query($this->con, $isCardIssuedQry);
            $numberOfPeopleInside=mysqli_num_rows($result);
            $facilityDetail=new stdClass();
            $facilityDetail->currentPeople=0;
            
            if ($numberOfPeopleInside > 0){
                $facilityDetail->currentPeople=$numberOfPeopleInside;    
            }
            $facilityDetail->totalCards=$totalCards;
            $facilityDetail->facilityName=$categoryInfo['facility_name'];
            return $facilityDetail;
    }
    function getTotalNumberOfcards($id){
        $sqlQry="select * from facility_cards_main where facility_id='$id'";
        $resultData = mysqli_query($this->con,$sqlQry);
        return mysqli_num_rows($resultData);
    }
}
$currentpeopleInfoObj=new currentPeopleInfo($conn);
$categoryList=$currentpeopleInfoObj->getCategories();
$peopleInfobj=$currentpeopleInfoObj->getcurrentPeopleCountInfo($categoryList);
$response = array('numberOfPeople'=>$peopleInfobj);
$response=json_encode($response);
$connection->closeConnection();
echo $response;
?>