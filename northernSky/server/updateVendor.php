
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$request = $req->request;

Class UpdateVendor{

	public function vendorUpdate($request, $con){

		// Basic Info
		$vendorId = $request->vendorId;
		$name = $request->nameR;
		$name = mysqli_real_escape_string($con, $name);//removed special charecters
		$mobilenumber = $request->mobilenumberR;
		$alt_number = $request->altNumber;
		$email = $request->emailR;
		$link = $request->link;

		$category = $request->category;
		$category = mysqli_real_escape_string($con, $category);//removed special charecters

		$address = $request->addressR;
		$address = mysqli_real_escape_string($con, $address);//removed special charecters
		$dealsin = $request->dealsin;
		$dealsin = mysqli_real_escape_string($con, $dealsin);//removed special charecters
		
		$query = "UPDATE vendor SET ";
		$query = $query." name='$name',email='$email',person_contact='$mobilenumber',alt_number='$alt_number',	website_link='$link', dealsIn='$dealsin', address='$address',category='$category'";
		// if(!empty($dateofmarriage)){
		// 	$query = $query.",dateOfMarriage='$dateofmarriage'";
		// } else{
		// 	$query = $query.",dateOfMarriage=NULL";
		// }
		$query = $query." WHERE id=$vendorId";
		$sql_basic = mysqli_query($con,$query);

		return $sql_basic;
	}

}



$vendorObj = new UpdateVendor();
$basicRes = $vendorObj->vendorUpdate($request, $con);



if(($basicRes)){
	$isSuccessful = true;
}else{
	$isSuccessful = false;
}

$response = array('isSuccessful'=> $isSuccessful , 'userId'=> $request->vendorId);
$response = json_encode($response);
echo $response;
$connection->closeConnection();
?>
