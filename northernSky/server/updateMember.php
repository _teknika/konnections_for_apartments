
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$req = json_decode($postdata);
$request = $req->request;

Class UpdateContacts{

	public function updateBasicIfo($request, $con){

		// Basic Info
		$parentid = $request->basicInfo->parentid;
		$ishead = $request->basicInfo->ishead;
		$orgId = $request->basicInfo->orgId;
		$role = $request->basicInfo->role;
		$relationwithhead = $request->basicInfo->relationwithhead;
		$firstname = $request->basicInfo->firstname;
		$firstname = mysqli_real_escape_string($con, $firstname);//removed special charecters

		$middlename = $request->basicInfo->middlename;
		$middlename = mysqli_real_escape_string($con, $middlename);//removed special charecters
		$lastname = $request->basicInfo->lastname;
		$lastname = mysqli_real_escape_string($con, $lastname);//removed special charecters
		$mobilenumber = $request->basicInfo->mobilenumber;
		$businesscategory = $request->basicInfo->businesscategory;
		$businessoccupation = $request->basicInfo->businessoccupation;
		$dob = $request->basicInfo->dob;
		$gender = $request->basicInfo->gender;
		$dateofmarriage = $request->basicInfo->dateofmarriage;
		$bloodgroup= $request->basicInfo->bloodgroup;
		$photo= $request->basicInfo->photo;
	
		//$imgData =	LOAD_FILE('C:\Users\Mohaideen\Desktop\role.png');
		$imgData =	base64_encode($photo);



		$user_id = $request->basicInfo->userId;

		$query = "UPDATE person SET ";
		$query = $query." orgId = '$orgId',parentId ='$parentid',firstName='$firstname',middleName='$middlename',lastName='$lastname',isHead='$ishead',businessCategory='$businesscategory', businessOccupation='$businessoccupation',gender='$gender',mobileNumber='$mobilenumber',bloodGroup='$bloodgroup', photo='$imgData', dob='$dob', dateOfMarriage='$dateofmarriage', role='$role', relationWithHead='$relationwithhead'";
		// if(!empty($dateofmarriage)){
		// 	$query = $query.",dateOfMarriage='$dateofmarriage'";
		// } else{
		// 	$query = $query.",dateOfMarriage=NULL";
		// }
		$query = $query." WHERE id=$user_id";

		$sql_basic = mysqli_query($con,$query);

		return $sql_basic;
	}

}

$user_id = $request->basicInfo->userId;

$contactsObj = new UpdateContacts();
$basicRes = $contactsObj->updateBasicIfo($request, $con);



if(($basicRes)){
	$isSuccessful = true;
}else{
	$isSuccessful = false;
}

$response = array('isSuccessful'=> $isSuccessful , 'userId'=> $user_id);
$response = json_encode($response);
echo $response;
$connection->closeConnection();
?>
