
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$req = json_decode($postdata);

$request = $req->request;


Class addMember {

	public function regBasic($request, $con){

		// Basic Info
		$parentid = $request->basicInfo->parentid;
		$ishead = $request->basicInfo->ishead;
		$orgId = $request->basicInfo->orgId;
		$role = $request->basicInfo->role;
		$relationwithhead = $request->basicInfo->relationwithhead;
		$firstname = $request->basicInfo->firstname;
		//$middlename = $request->basicInfo->middlename;
		$lastname = $request->basicInfo->lastname;
		$mobilenumber = $request->basicInfo->mobilenumber;
		//$businesscategory = $request->basicInfo->businesscategory;
		//$businessoccupation = $request->basicInfo->businessoccupation;
		$dob = $request->basicInfo->dob;
		$gender = $request->basicInfo->gender;
		$dateofmarriage = $request->basicInfo->dateofmarriage;
		//$bloodgroup= $request->basicInfo->bloodgroup;
		//$photo= $request->basicInfo->photo;

		
		

		$sql_basic = mysqli_query($con,"INSERT INTO person(orgId,parentId,firstName,lastName,isHead,dob,gender,mobileNumber, role, relationWithHead) VALUES ('$orgId','$parentid','$firstname', '$lastname', '$ishead', '$dob', '$gender', '$mobilenumber','$role', '$relationwithhead')");

		$user_id = mysqli_insert_id($con);

		if(!empty($dateofmarriage)){
			$sql_update = mysqli_query($con, "UPDATE person SET dateOfMarriage= '$dateofmarriage' WHERE id='$user_id'");
		} else{
			$sql_update = mysqli_query($con, "UPDATE person SET dateOfMarriage= NULL WHERE id='$user_id'");
		}
		
		return $user_id;
	}
   
	public function isMobileNumExist($request, $con){
		
		$mobilenumber = $request->basicInfo->mobilenumber;
		
		$sql = "SELECT *  FROM person where mobileNumber= '$mobilenumber'";
		$res=mysqli_query($con,$sql);
        

		$result = mysqli_num_rows($res);
		if($result < 1 ){
		  return false;//no duplicate phone number
		}else{
		  return true;
		}
	}
	
}




// Object

$contactsObj = new addMember();
$isMobileNumExists= $contactsObj->isMobileNumExist($request, $con);
if($isMobileNumExists){
	$response = array('isSuccessful'=> false,'msg'=> "Mobile number is alreay registered.");
	$response = json_encode($response);
	echo $response;
}else{
$user_id = $contactsObj->regBasic($request, $con);

if($user_id != 0){
	$isSuccessful = TRUE;
	$msg = "SUCCESS";
}else{
	$isSuccessful = false;
	$msg = "BASIC_FAIL";
}

$response = array('isSuccessful'=> $isSuccessful , 'userId'=> $user_id, "msg"=>$msg);
$response = json_encode($response);
echo $response;
}
$connection->closeConnection();
?>
