 $(function(){
 	$("#currBalance").text(familyHeadObj.balance | 0);
 	enableVendorSearch();
 	$("#payNow").click(function(event) {
 		/* Act on the event */
 		payMaintenance();
 	});
 })

 function  enableVendorSearch(){
      $("#SearchQry").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#payMaintenanceHistory tbody >tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
   } 
   function payMaintenance(){
   	var qry="userId="+familyHeadObj.userId+"&parentId="+familyHeadObj.id+"&email="+familyHeadObj.emailId+"&name="+familyHeadObj.firstName+"&mobNum="+familyHeadObj.mobileNumber;
    window.open("../payManintenance/maintenanceForm.php?"+qry);
   } 