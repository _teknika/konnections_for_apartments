<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$postdata = file_get_contents("php://input");
$req = json_decode($postdata);

$OwnerDetailObj=new StdClass();
class maintenaceHistory{
    var $con=null,
        $ownerId,
        $flatNumber,  
        $OwnerDetailObj=null;
    function __construct($con,$req,$OwnerDetailObj){
        $this->con=$con;
        $this->flatNumber=$req->fNumber;
        $this->OwnerDetailObj=$OwnerDetailObj;
      }

    //just modified parentID=-1 because it was display actual owener details instead of rent
    function getOwnerBasicDetail(){
        $flatNumber=$this->flatNumber;
        $getOwnerBasicDetailQry="SELECT prsn.*,prsnFlats.flat_number as otherflat FROM person prsn LEFT JOIN person_flats prsnFlats on prsn.id=prsnFlats.personId WHERE
        prsn.parentID='-1' AND prsn.flatNo='$flatNumber' || prsnFlats.flat_number='$flatNumber'";
        $resultQry= $this->con->query($getOwnerBasicDetailQry);
        if ($resultQry->num_rows > 0) {
            while($row = $resultQry->fetch_assoc()) {               
                $this->OwnerDetailObj->ownerInfo=$row;
                $this->getFamilySubmembers($row['id']);
                return $this->getMaintenanceDetail($row['id']);
            }
        } else {
            return $this->OwnerDetailObj;
        }
        //ends hre
    }
    function getMaintenanceDetail($userId){
        $getMaintenanceDetailQry="SELECT * from maintainance where userId='$userId' and flat='$this->flatNumber' ";
        $resultQry= $this->con->query($getMaintenanceDetailQry);
        $this->OwnerDetailObj->maintenanceInfo=new StdClass;
        if ($resultQry->num_rows > 0) {
            while($row = $resultQry->fetch_assoc()) {               
                $this->OwnerDetailObj->maintenanceInfo=$row;             
            }
        }
        return $this->OwnerDetailObj;
    }
    function getFamilySubmembers($parentId){
        $getFamilyMembersQry="SELECT id,firstName,lastName,mobileNumber from person where parentId='$parentId' ";
        $resultQry= $this->con->query($getFamilyMembersQry);
        $membersList = array();
        if ($resultQry->num_rows > 0) {
            while($row = $resultQry->fetch_assoc()) {
                array_push($membersList, $row);               
            }
        }
        $this->OwnerDetailObj->membersList=$membersList;
    }
}
$flatDetails=new maintenaceHistory($con,$req,$OwnerDetailObj);
$maintenanceDetail=$flatDetails->getOwnerBasicDetail();
$response = json_encode($maintenanceDetail);
echo $response;
$connection->closeConnection();
?>