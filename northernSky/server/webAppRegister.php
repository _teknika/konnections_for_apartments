
<?php
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();

$postdata = file_get_contents("php://input");
$req = json_decode($postdata);

$request = $req->request;


Class ContactReg {

	public function regBasic($request, $con){
	    $relationwithhead = $request->basicInfo->relationwithhead;
         	
         
		// Basic Info
		$parentid = $request->basicInfo->parentid;
		$ishead = $request->basicInfo->ishead;
		$orgId = $request->basicInfo->orgId;
		$role = $request->basicInfo->role;
		
		$firstname = $request->basicInfo->firstname;
		$firstname = mysqli_real_escape_string($con, $firstname);//removed special charecters

		$lastname = $request->basicInfo->lastname;
		$lastname = mysqli_real_escape_string($con, $lastname);//removed special charecters
		
		$mobilenumber = $request->basicInfo->mobilenumber;
		$altnumber = $request->basicInfo->altnumber;
		
		$gender = $request->basicInfo->gender;
		$dateofmarriage = $request->basicInfo->dateofmarriage;
		$dob = $request->basicInfo->dob==false? null:$request->basicInfo->dob;
		$emailId=$request->basicInfo->emailId ;
		$altmailId = $request->basicInfo->altmailId;
		$blockName=$request->basicInfo->blockName ;
		$flatNo=$request->basicInfo->flatNo ;
		$flatHolder=$request->basicInfo->flatHolder ;
		//default privacy mask
		$privasyMask=new stdClass();
		$privasyMask->email=true;
		$privasyMask->flat=true;
		$privasyMask->mobileNumber=true;
		 
		$encodedPrivacyMask=json_encode($privasyMask);
		//
		$blockAndFlat=$flatNo;
		
        if($dob){
             $qry="INSERT INTO person(orgId,parentId,firstName,lastName,isHead,gender,mobileNumber,alt_number,dob, role, relationWithHead,emailId,alt_mail,blockName,flatNo,flat_holder,privacy_mask) VALUES ('$orgId','$parentid','$firstname', '$lastname', '$ishead', '$gender', '$mobilenumber','$altnumber','$dob','$role', '$relationwithhead','$emailId','$altmailId','$blockName','$blockAndFlat','$flatHolder','$encodedPrivacyMask')";
        }else{
         $qry="INSERT INTO person(orgId,parentId,firstName,lastName,isHead,gender,mobileNumber,alt_number,role, relationWithHead,emailId,alt_mail,blockName,flatNo,flat_holder,privacy_mask) VALUES ('$orgId','$parentid','$firstname', '$lastname', '$ishead', '$gender', '$mobilenumber','$altnumber','$role', '$relationwithhead','$emailId','$altmailId','$blockName','$blockAndFlat','$flatHolder','$encodedPrivacyMask')";
        }
		
		$sql_basic = mysqli_query($con,$qry);

		$user_id = mysqli_insert_id($con);

		if(!empty($dateofmarriage)){
			$sql_update = mysqli_query($con, "UPDATE person SET dateOfMarriage= '$dateofmarriage' WHERE id='$user_id'");
		} else{
			$sql_update = mysqli_query($con, "UPDATE person SET dateOfMarriage= NULL WHERE id='$user_id'");
		}


		return $user_id;
	}
	

	public function isMobileNumExist($request, $con){
		
		$mobilenumber = $request->basicInfo->mobilenumber;
	//	$mobilenumber = $request->lesseeInfo->mobilenumber;
		$sql = "SELECT *  FROM person where mobileNumber= '$mobilenumber'";
		$res=mysqli_query($con,$sql);
        

		$result = mysqli_num_rows($res);
		if($result < 1 ){
		  return false;//no duplicate phone number
		}else{
		  return true;
		}
	}
	public function isFlatNumExistForParent($request, $con){
		$isExists=false;
		$parentId = $request->basicInfo->parentid;
		$flatholder=$request->basicInfo->flatHolder ;
        if($parentId == -1 && $flatholder=="owner"){
			$flatNumber=$request->basicInfo->flatNo ;
			
			$sqlqry = "SELECT * FROM person p left join person_flats pf on p.id=pf.personId where (p.flatNo='$flatNumber' or pf.flat_number='$flatNumber') && p.parentId=-1";
			$resultdata=mysqli_query($con,$sqlqry);
	        

			$totalRows = mysqli_num_rows($resultdata);
			if($totalRows > 0 ){
			  return true;//if duplicate numbers are there in db
			}
       }
       return $isExists;
	}
}

// Object

$contactsObj = new ContactReg();
$isMobileNumExists= $contactsObj->isMobileNumExist($request, $con);
$isFlatNumExists= $contactsObj->isFlatNumExistForParent($request, $con);
if($isMobileNumExists){
	$response = array('isSuccessful'=> false,'msg'=> "Mobile number is alreay registered.");
	$response = json_encode($response);
	echo $response;
}else if($isFlatNumExists){
	$response = array('isSuccessful'=> false,'msg'=> "Flat number is alreay registered.");
	$response = json_encode($response);
	echo $response;
}else{
	$user_id = $contactsObj->regBasic($request, $con);
				if($user_id != 0){
					$isSuccessful = true;
					$msg = "Successful";
                    
				}else{
					$isSuccessful = false;
					$msg = "BASIC_FAIL";
				}

	$response = array('isSuccessful'=> $isSuccessful , 'userId'=> $user_id, "msg"=>$msg);
	$response = json_encode($response);
	echo $response;
	
}
$connection->closeConnection();
?>
