
<?php
require_once('libs/smsGatewayConstants.php');
require_once('libs/smsGateway.php');
require_once('libs/dbConnection.php');
$connection = new dbconnection();
$con = $connection->connectToDatabase();
$id = $_GET['id'];
$messageR = $_GET['messageR'];
$messageInEscapeStr = mysqli_real_escape_string($con, $messageR);//removed special charecters
$sqlQry = "SELECT req.id,req.userId,req.flat,req.message,req.service,prsn.mobileNumber,servic.service_name FROM requests req INNER JOIN services servic on req.service=servic.id inner join person prsn on req.userId=prsn.id where req.id='$id'";
$result = mysqli_query($con, $sqlQry);
$rowData=new stdClass();
if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
        $rowData->Id=$row['id'];
        $rowData->userId=$row['userId'];
        $rowData->flat=$row['flat'];
        $rowData->message=$row['message'];
        $rowData->serviceName=$row['service_name'];
        $rowData->mobileNumber=$row['mobileNumber'];
    }
    $insertQry = "INSERT INTO response_to_requests(request_id, resp_message) VALUES ($rowData->Id,'$messageInEscapeStr')";

    if(mysqli_query($con, $insertQry)){
        $message = $messageR;
        $mobileNumber=$rowData->mobileNumber;
        $obj = new Sender(_HOST,_PORT,_USERNAME,_PASSWORD,_SOURCE,$message,$mobileNumber,_TYPE,_DLR);
        $result = $obj->Submit(); 
        $isSuccessful=true;
    }else{
        $isSuccessful=false;
    }
      
} else {
    $isSuccessful=false;
}

$response = array('isSuccessful' => $isSuccessful);
$response = json_encode($response);
echo $response;
$connection->closeConnection();
?>
