-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Jan 11, 2018 at 02:55 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `northernsky`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `bannerPath` varchar(1024) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `time` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `bannerPath`, `description`, `time`) VALUES
(30, 'http://www.kishorehousings.com/includes/builders/northernskyin-137/others/595dd9437ae65.jpg', 'Northern', NULL),
(38, 'https://www.northernsky.in/wp-content/uploads/2015/09/NorthernSky-Altair-1024x576.jpg', 'TEST', NULL),
(29, 'https://is1-2.housingcdn.com/012c1500/5e80fe5c2033856ec1abcb3b55aac1ff/v6/_m/northern_sky_alexandria_hampankatta-mangaluru-northern_sky_properties.jpg', 'TEST', NULL),
(32, 'https://image.slidesharecdn.com/northernskyproperties-161124112041/95/northernsky-properties-4-638.jpg?cb=1479986600', 'Mangalore', NULL),
(37, 'https://www.northernsky.in/wp-content/uploads/2015/09/NorthernSky-Alexandria-300x169.jpg', 'TEST', NULL),
(34, 'https://im.proptiger.com/1/664546/6/northern-sky-properties-courtyard-elevation-544976.jpeg', 'TEST', NULL),
(35, 'https://www.northernsky.in/wp-content/uploads/2015/08/ND4_9116-1024x682.jpg', 'TEST', NULL),
(36, 'https://image.slidesharecdn.com/northernskycitymangalore-140421064856-phpapp02/95/northernsky-city-mangalore-northernsky-city-pumpwell-properties-in-pumpwell-commonfloor-4-638.jpg?cb=1398063213', 'TEST', NULL),
(39, 'http://localhost/northernSky/server/uploads/Banners/apis.txt', 'sdfs', NULL),
(40, 'http://localhost/northernSky/server/uploads/Banners/leanonme.png', 'dsfsf', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `basic_services`
--

CREATE TABLE `basic_services` (
  `id` int(11) NOT NULL,
  `block_name` varchar(1000) NOT NULL,
  `flat_no` varchar(1000) NOT NULL,
  `amount` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic_services`
--

INSERT INTO `basic_services` (`id`, `block_name`, `flat_no`, `amount`, `from_date`, `to_date`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'sadsa', '12', 1000, '2017-11-03', '2017-11-30', '2017-11-03 09:54:36', 0, '2017-11-03 09:54:36', 0),
(2, 'sadsa', '12', 1000, '2017-11-24', '2017-11-30', '2017-11-03 09:55:42', 1125, '2017-11-03 09:55:42', 0),
(4, 'sadsa', '12', 1000, '2017-11-24', '2017-11-30', '2017-11-03 10:22:23', 1125, '2017-11-03 10:22:23', 0);

-- --------------------------------------------------------

--
-- Table structure for table `buildinginfo`
--

CREATE TABLE `buildinginfo` (
  `id` int(11) NOT NULL,
  `buildingName` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buildinginfo`
--

INSERT INTO `buildinginfo` (`id`, `buildingName`) VALUES
(22, 'Block A'),
(26, 'Block C');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `details` text,
  `datetime` date DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `photos` varchar(2500) DEFAULT NULL,
  `start_time` varchar(1000) DEFAULT NULL,
  `end_time` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` int(11) NOT NULL,
  `facility_name` varchar(200) NOT NULL,
  `charge_per_hour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `facility_name`, `charge_per_hour`) VALUES
(1, 'Table Tennis', 100),
(2, 'Football', 100),
(3, 'Tennis', 100),
(4, 'poker', 100);

-- --------------------------------------------------------

--
-- Table structure for table `facility_availability`
--

CREATE TABLE `facility_availability` (
  `id` int(11) NOT NULL,
  `facility_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility_availability`
--

INSERT INTO `facility_availability` (`id`, `facility_id`, `start_time`, `end_time`) VALUES
(347, 1, '2018-01-10 05:00:00', '2018-01-10 06:00:00'),
(355, 2, '2018-01-10 10:00:00', '2018-01-10 21:30:00'),
(356, 2, '2018-01-11 10:00:00', '2018-01-11 20:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `facility_booking`
--

CREATE TABLE `facility_booking` (
  `id` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `facility_id` int(11) NOT NULL,
  `weekId` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `amount` double NOT NULL,
  `payment_status` tinyint(4) NOT NULL COMMENT '0=>pending,1=>done',
  `payment_detail` text,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility_booking`
--

INSERT INTO `facility_booking` (`id`, `personId`, `facility_id`, `weekId`, `start_time`, `end_time`, `amount`, `payment_status`, `payment_detail`, `created_by`) VALUES
(13, 2838, 2, 3, '2018-01-10 16:30:00', '2018-01-10 17:00:00', 0, 0, '', NULL),
(14, 2838, 2, 3, '2018-01-10 17:00:00', '2018-01-10 17:30:00', 0, 0, '', NULL),
(15, 2838, 2, 3, '2018-01-10 17:30:00', '2018-01-10 18:00:00', 0, 0, '', NULL),
(16, 2838, 2, 3, '2018-01-10 18:00:00', '2018-01-10 18:30:00', 0, 0, NULL, NULL),
(17, 2838, 2, 3, '2018-01-10 18:30:00', '2018-01-10 19:00:00', 0, 1, '', NULL),
(18, 2838, 2, 3, '2018-01-10 19:00:00', '2018-01-10 19:30:00', 0, 1, '', NULL),
(19, 2838, 2, 4, '2018-01-11 10:00:00', '2018-01-11 10:30:00', 0, 0, NULL, NULL),
(20, 2838, 2, 4, '2018-01-11 14:00:00', '2018-01-11 14:30:00', 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `facility_unavailability`
--

CREATE TABLE `facility_unavailability` (
  `id` int(11) NOT NULL,
  `facility_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gcm`
--

CREATE TABLE `gcm` (
  `id` int(10) NOT NULL,
  `personId` int(10) NOT NULL,
  `gcmId` varchar(1024) NOT NULL,
  `updatedTime` datetime DEFAULT NULL,
  `android` int(11) DEFAULT '0',
  `deviceId` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gcm`
--

INSERT INTO `gcm` (`id`, `personId`, `gcmId`, `updatedTime`, `android`, `deviceId`) VALUES
(222, 2837, 'APA91bHevl0JTT5d8eep1T-KImp2gF_6qrUuSsH5m-pg3raEXRt9H5qgvB-gOOdUfGwcl9qhkT9qSf1w8tpyPhSdQObsL_iOvc-Jt2-0V2UDtSkwPNQwBQmk14noevYoTt3wsT5uuGnb', NULL, 1, 'aa3e7c2ac8e1be3a'),
(223, 2838, 'APA91bHcdCiTUs_aSSZeXrdd9fE-FLbQWWNojhGtDFj_MhzTrQW8w5OCFRgI7o81P5cpY37PP26AtS9kfrrMPviHoJkxvlyfMzaydqn4HyZbLSVzEAYu-gmF7qMW95NoCyGCq7BOBwMq', NULL, 1, '41a7eda3f4e026eb');

-- --------------------------------------------------------

--
-- Table structure for table `maintainance`
--

CREATE TABLE `maintainance` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `maintainance`
--

INSERT INTO `maintainance` (`id`, `userId`, `balance`, `created_at`, `created_by`, `update_by`, `updated_at`) VALUES
(15, 2861, 1934, '2018-01-11 10:28:02', 2861, 1125, '2018-01-11 15:58:02'),
(16, 15, 200, '2018-01-11 10:32:51', 15, 15, '2018-01-11 16:02:51'),
(17, 0, 0, '2018-01-11 10:55:45', 0, 0, '2018-01-11 16:25:45');

-- --------------------------------------------------------

--
-- Table structure for table `maintainance_track`
--

CREATE TABLE `maintainance_track` (
  `id` int(11) NOT NULL,
  `mId` int(11) NOT NULL,
  `accrued_or_utilised` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `modeOfPayment` varchar(1000) NOT NULL,
  `utilise_datail` varchar(1000) NOT NULL,
  `payment_detail` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `maintainance_track`
--

INSERT INTO `maintainance_track` (`id`, `mId`, `accrued_or_utilised`, `balance`, `modeOfPayment`, `utilise_datail`, `payment_detail`, `created_at`, `created_by`) VALUES
(26, 16, 100, 100, 'Net Banking', '', '', '2018-01-11 10:32:51', 15),
(27, 16, 100, 200, 'Net Banking', '', '', '2018-01-11 10:36:15', 15),
(28, 15, 100, 100, 'Net Banking', '', '', '2018-01-11 10:47:51', 2861),
(29, 15, 100, 200, 'Net Banking', '', '', '2018-01-11 11:05:14', 2861),
(30, 15, 300, 600, 'Cash', '', '{\"ownerId\":\"2861\",\"updatedBy\":\"1125\",\"paymentMode\":\"Cash\",\"name\":\"kirn\",\"amount\":\"300\",\"checkNum\":\"\",\"expireDate\":\"\"}', '2018-01-11 11:18:31', 1125),
(31, 15, 789, 1389, 'Cash', '', '', '2018-01-11 11:55:22', 1125),
(32, 15, 455, 1844, 'Cash', '', '', '2018-01-11 12:47:43', 1125),
(33, 15, 90, 1934, 'Cash', '', '', '2018-01-11 12:49:12', 1125);

-- --------------------------------------------------------

--
-- Table structure for table `managemembers`
--

CREATE TABLE `managemembers` (
  `id` int(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `post` varchar(100) NOT NULL,
  `mobile_number` varchar(70) NOT NULL,
  `time` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `managemembers`
--

INSERT INTO `managemembers` (`id`, `name`, `post`, `mobile_number`, `time`) VALUES
(27, 'Amit Surana1', 'Director', '9639636328', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `maps`
--

CREATE TABLE `maps` (
  `id` int(10) NOT NULL,
  `title` text NOT NULL,
  `details` text NOT NULL,
  `maps` varchar(20000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `maps`
--

INSERT INTO `maps` (`id`, `title`, `details`, `maps`) VALUES
(1, 'Map', 'Find the Venue in this map', 'https://www.google.com/maps/embed?');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) NOT NULL,
  `senderId` int(10) NOT NULL,
  `message` varchar(200) NOT NULL,
  `type` int(4) NOT NULL COMMENT '0=> GCM, 1=> SMS, 2=>GCM&&SMS, 3=>GCM || SMS',
  `transactionTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `senderId`, `message`, `type`, `transactionTime`, `title`) VALUES
(16, 1125, 'Test Send Notification in LIVE', 0, '2017-11-17 10:41:55', 'Test Notification'),
(18, 1125, 'message', 0, '2017-11-17 10:51:27', 'testing'),
(19, 1125, 'sadasd', 0, '2017-11-18 14:10:07', 'asda'),
(20, 1125, 'notifications', 0, '2017-12-27 17:15:23', 'testing'),
(21, 1125, 'notification Time', 0, '2017-12-28 11:35:48', 'testing'),
(22, 1125, 'notification Time', 0, '2017-12-28 11:53:09', 'testing'),
(23, 1125, 'notificationTestingTime', 0, '2017-12-28 11:56:21', 'testingGCm'),
(24, 1125, 'notificationTestingTime', 0, '2017-12-28 12:12:41', 'send Notification'),
(25, 1125, 'sdfsf', 0, '2017-12-29 15:10:34', 'sdfsdf'),
(26, 1125, 'sdfsf', 0, '2017-12-29 15:24:04', 'sdfsdf'),
(27, 1125, 'sdfsf', 0, '2017-12-29 15:25:06', 'sdfsdf'),
(28, 1125, 'sdfsf', 0, '2017-12-29 15:26:37', 'sdfsdf'),
(29, 1125, 'asdsa', 0, '2017-12-29 15:29:56', 'asdas'),
(30, 1125, 'asdsa', 0, '2017-12-29 15:56:19', 'asdas'),
(31, 1125, 'asdsa', 0, '2017-12-29 15:57:22', 'asdas'),
(32, 1125, 'asdsa', 0, '2017-12-29 15:58:24', 'asdas');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` int(10) NOT NULL,
  `parentId` int(10) DEFAULT NULL,
  `firstName` varchar(40) NOT NULL,
  `lastName` varchar(40) DEFAULT NULL,
  `isHead` tinyint(1) NOT NULL,
  `gender` tinyint(1) NOT NULL COMMENT '0=>Female, 1=>Male',
  `dob` date DEFAULT NULL,
  `dateOfMarriage` varchar(256) DEFAULT NULL,
  `mobileNumber` varchar(20) NOT NULL,
  `alt_number` varchar(20) NOT NULL,
  `orgId` int(10) DEFAULT NULL,
  `role` int(10) DEFAULT NULL,
  `relationWithHead` varchar(20) DEFAULT NULL,
  `emailId` varchar(1000) NOT NULL,
  `alt_mail` varchar(100) NOT NULL,
  `blockName` varchar(1000) NOT NULL,
  `flatNo` varchar(11) NOT NULL,
  `flat_holder` enum('owner','tenant') NOT NULL DEFAULT 'owner',
  `last_login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `parentId`, `firstName`, `lastName`, `isHead`, `gender`, `dob`, `dateOfMarriage`, `mobileNumber`, `alt_number`, `orgId`, `role`, `relationWithHead`, `emailId`, `alt_mail`, `blockName`, `flatNo`, `flat_holder`, `last_login_time`) VALUES
(1125, 1000000, 'northenSky', 'Admin', 2, 1, NULL, NULL, '9900489498', '0', 1, NULL, NULL, '', '', '', '0', 'owner', '2017-12-28 05:29:48'),
(2837, -1, 'Manuja', 'Gowda', 1, 0, '0000-00-00', '20/12/2017', '7411769681', '12323465653', 1, 0, '', 'manujatanuja@gmail.com', '', 'Block A', '1', 'owner', '2017-12-28 05:29:48'),
(2838, 2837, 'Tanuja', 'Gowda', 1, 0, NULL, '2017-11-20', '8095352709', '0', 1, -1, '', 'manuja@gmail.com', '', 'Block A', '1', 'owner', '2017-12-28 05:29:48'),
(2839, 2837, 'Nagesh', 'Gowda', 1, 1, NULL, '2017-11-20', '7760859685', '0', 1, -1, '', 'nagesh@gmail.com', '', 'Block A', '1', 'owner', '2017-12-28 05:29:48'),
(2840, -1, 'Palash', 'Roy', 1, 1, NULL, '22-11-2017', '9856326598', '0', 1, -1, '', 'palash@gmail.com', '', 'Block B', '2', 'owner', '2017-12-28 05:29:48'),
(2841, 2840, 'Anurag', 'Dhiman', 1, 1, NULL, '2017-11-22', '8989898989', '0', 1, -1, '', 'anurag@gmail.com', '', 'Block B', '2', 'owner', '2017-12-28 05:29:48'),
(2842, 2837, 'Santhosh', 'Gowda', 1, 1, NULL, '2017-11-23', '8863526352', '0', 1, -1, '', 'santhosh@gmail.com', '', 'Block A', '1', 'owner', '2017-12-28 05:29:48'),
(2843, 2840, 'Jayant', 'Gowda', 1, 1, NULL, '2017-11-29', '8956235698', '0', 1, -1, '', 'jayant@gmail.com', '', 'Block B', '2', 'owner', '2017-12-28 05:29:48'),
(2844, 2840, 'Kavitha', 'Teknika', 1, 0, NULL, '2017-11-28', '8529637412', '0', 1, -1, '', 'kavitha@gmail.com', '', 'Block B', '2', 'owner', '2017-12-28 05:29:48'),
(2845, -1, 'Yashwanth', 'Neptune', 1, 1, NULL, '30-11-2017', '9632587456', '0', 1, -1, '', 'yashwanth@gmail.com', '', 'Block C', '3', 'owner', '2017-12-28 05:29:48'),
(2846, 2845, 'Usha', 'Neptune', 1, 0, NULL, '2017-12-06', '8569685623', '0', 1, -1, '', 'usha@gmail.com', '', 'Block C', '3', 'owner', '2017-12-28 05:29:48'),
(2847, 2845, 'Chaya', 'Neptune', 1, 0, NULL, '2017-11-30', '9632587412', '0', 1, -1, '', 'chaya@gmail.com', '', 'Block C', '3', 'owner', '2017-12-28 05:29:48'),
(2848, -1, 'xczxc', 'zxczx', 1, 1, '2017-12-20', '12-12-2017', '4223424342', '0', 1, -1, '', 'sdfsd@gmail.com', '', '', '435', 'owner', '2017-12-29 11:05:25'),
(2849, -1, 'sadsadas', 'dsadsa', 1, 0, '2017-12-20', '26-12-2017', '1238675675', '0', 1, -1, '', 'gdf@gdfg', '', 'Block A', '465', 'owner', '2017-12-29 11:07:40'),
(2850, -1, 'asds', 'sadsad', 1, 1, '2017-12-19', '20-12-2017', '4564565467', '0', 1, -1, '', 'rtry@fdg', '', 'Block A', '767', '', '2017-12-29 11:11:43'),
(2852, -1, 'sdfs', 'sdfsf', 1, 1, '0000-00-00', '', '2323232325', '0', 1, 0, '', 'JGJGJ@gmail.com', '', '', '24', '', '2018-01-03 10:58:13'),
(2853, -1, 'fgfd', 'fdg', 1, 1, '0000-00-00', '', '123465464645', '0', 1, 0, '', 'dgsg@gmail.com', '', 'Block A', '567', 'owner', '2018-01-03 11:18:43'),
(2854, -1, 'test1', 'test', 1, 1, NULL, NULL, '1234534212', '0', 1, -1, '', 'agh@gmail.com', '', 'Block A', '453', 'owner', '2018-01-05 11:04:55'),
(2855, -1, 'test2', 'tst', 1, 1, NULL, NULL, '8939293939', '0', 1, -1, '', 'sdf@gmail.com', '', 'Block A', '999', 'owner', '2018-01-05 11:12:09'),
(2856, -1, 'test2', 'tst', 1, 1, NULL, NULL, '8939293934', '0', 1, -1, '', 'sdf@gmail.com', '', 'Block A', '999', 'owner', '2018-01-05 11:13:12'),
(2857, -1, 'test3', 'tst', 1, 1, '0000-00-00', '', '85858585858', '6546546546', 1, 0, '', 'sdfds@gmial.com', '', 'Block A', '878', 'owner', '2018-01-05 11:16:39'),
(2858, 2837, 'lolo', 'sdfsdf', 1, 1, NULL, NULL, '1232135565', '', 1, -1, '', 'werwe@gmail.com', '', 'Block A', '1', 'owner', '2018-01-05 12:15:45'),
(2859, 2837, 'dsfs', 'sdffs', 1, 1, NULL, NULL, '4545454545455', '', 1, -1, '', 'sdfsd@gmai', 'dsfs@sdfs', 'Block A', '1', 'owner', '2018-01-05 12:17:50'),
(2860, -1, 'duptest', 'tets', 1, 1, NULL, NULL, '8095352709', '2323454543', 1, -1, '', 'sdfsd@gmail.com', '', 'Block A', '20', 'owner', '2018-01-09 07:08:35'),
(2861, -1, 'duptest2', 'tst', 1, 1, NULL, NULL, '8095352708', '', 1, -1, '', 'sfdds@sfg', '', 'Block A', '2', '', '2018-01-09 07:43:35'),
(2862, -1, 'tesf', 'sdfs', 1, 1, '0000-00-00', '', '43545454545', '', 1, 0, '', 'dfgd@dfg', '', 'Block A', '5665', 'tenant', '2018-01-10 11:55:33');

-- --------------------------------------------------------

--
-- Table structure for table `person_flats`
--

CREATE TABLE `person_flats` (
  `id` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `flat_number` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person_flats`
--

INSERT INTO `person_flats` (`id`, `personId`, `flat_number`) VALUES
(1, 2837, '101'),
(2, 2837, '102'),
(3, 2837, '103');

-- --------------------------------------------------------

--
-- Table structure for table `portal_user`
--

CREATE TABLE `portal_user` (
  `id` int(10) NOT NULL,
  `personId` int(10) NOT NULL,
  `orgId` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portal_user`
--

INSERT INTO `portal_user` (`id`, `personId`, `orgId`, `username`, `password`) VALUES
(5, 1125, 1, 'admn', 'passw0rd');

-- --------------------------------------------------------

--
-- Table structure for table `queries`
--

CREATE TABLE `queries` (
  `id` int(11) NOT NULL,
  `message` varchar(5196) DEFAULT NULL,
  `user` varchar(5196) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `queries`
--

INSERT INTO `queries` (`id`, `message`, `user`) VALUES
(68, 'cbjfjd', 'Name: Sindhi Sadan Admin <<>> ID: 1125'),
(69, 'User entered message....', '1125'),
(70, 'User entered message....', '1125'),
(71, 'User entered message....', '1125'),
(72, 'Write your queries, complaints or information update details', '1365'),
(73, 'Write your queries, complaints or information update details', '1365'),
(74, 'there', '1365'),
(75, 'This is good app', '1365'),
(76, 'Amit sent this\n', '1125'),
(77, 'Hello', '1365'),
(78, 'Hello ', '1143'),
(79, 'sir I am not intrest in this app.pls.\n\n\n\n\n\n\n\n', 'Name: Kishore Bhai  Patel  <<>> ID: 2580'),
(80, '', ''),
(81, '', ''),
(82, '', ''),
(83, '', ''),
(84, '', ''),
(85, '', ''),
(86, '', ''),
(87, '', ''),
(88, '', ''),
(89, '', ''),
(90, '', ''),
(91, '', ''),
(92, '', ''),
(93, '', ''),
(94, '', ''),
(95, '', ''),
(96, '', ''),
(97, '', ''),
(98, '', ''),
(99, '', ''),
(100, '', ''),
(101, '', ''),
(102, '', ''),
(103, '', ''),
(104, '', ''),
(105, '', ''),
(106, '', ''),
(107, '', ''),
(108, '', ''),
(109, '', ''),
(110, '', ''),
(111, '', ''),
(112, '', ''),
(113, '', ''),
(114, '', ''),
(115, '', ''),
(116, '', ''),
(117, '', ''),
(118, '', ''),
(119, '', ''),
(120, '', ''),
(121, '', ''),
(122, '', ''),
(123, '', ''),
(124, '', ''),
(125, '', ''),
(126, '', ''),
(127, '', ''),
(128, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_bin NOT NULL,
  `message` varchar(2048) COLLATE utf8_bin NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approved` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `userId`, `name`, `message`, `datetime`, `approved`) VALUES
(256, 12, 'sdfdf', '\"sdfdf\'sdf!d\"', '2017-11-18 07:50:45', 1),
(254, 12, '\"kiran\"', '\"lorem-ipsum\"', '2017-11-18 07:41:36', 0),
(253, 12, '\"kiran\"', '\"lorem-ipsum\"', '2017-11-18 05:01:46', 1),
(251, 12, 'kiran', 'lorem ipsum', '2017-11-18 04:44:54', 1),
(252, 12, '\"kiran\"', '\"lorem-ipsum\"', '2017-11-18 04:45:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `email` varchar(2000) NOT NULL,
  `person_contact` varchar(2000) NOT NULL,
  `alt_number` int(15) DEFAULT NULL,
  `website_link` varchar(2000) NOT NULL,
  `dealsIn` text NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `name`, `email`, `person_contact`, `alt_number`, `website_link`, `dealsIn`, `address`) VALUES
(17, 'Neptune Controls Pvt Ltd', 'neptune@gmail.com', '9880060437', NULL, 'http://ncpl.co', 'Building Management Solution, Life Safety Systems, Security Solutions, Utility Monitoring, Industrial Automation, Structured Cabling, Lighting Solutions, Energy efficient systems, Solar Systems / LED lighting', 'No.9, 2nd cross, Ground Floor, 7th main, Nandidurga Extension, Bangalore -560046 Tel:080-23332160,40904685 +919845236967'),
(19, 'adsasd', '', '1234567908', 1234567907, '', 'rtgbdfgfghdhhdh', ''),
(21, 'erter', '', '3421234567', 0, '', 'rdfgf', ''),
(22, 'erter', '', '3421234567', 0, '', 'rdfgf', ''),
(23, 'asdas', '', '232131', 0, '', '123123', ''),
(24, 'sdf', '', '3242234234', 0, '', 'sdfsd', ''),
(25, 'sdfs', '', '1232132131', 0, '', 'sdfdfa', ''),
(26, 'vgjghvjg', '', '6767675656', 0, '', 'gfjfgu fghfg', ''),
(27, 'rwere', '', '1213543456', 0, '', 'dfg', ''),
(28, 'ertete', '', '1235467890', 0, '', 'ghghf', ''),
(29, 'werw', '', '8907895684', 0, '', 'sff', ''),
(30, 'sdfsd', '', '1234345434', 0, '', 'cxvxcv', ''),
(31, 'sdfsd2', '', '1234345434', NULL, '', 'cxvxcv', ''),
(32, 'zxczxcz', '', '1234567654', 0, '', 'sdczxcxzc', ''),
(33, 'asdas', '', '1234323456', 0, '', 'dsfsfsdf', ''),
(34, 'dfsf', '', '2343234323', 0, '', 'cvbcvb', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `basic_services`
--
ALTER TABLE `basic_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buildinginfo`
--
ALTER TABLE `buildinginfo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `facility_name` (`facility_name`);

--
-- Indexes for table `facility_availability`
--
ALTER TABLE `facility_availability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_booking`
--
ALTER TABLE `facility_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_unavailability`
--
ALTER TABLE `facility_unavailability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gcm`
--
ALTER TABLE `gcm`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `deviceId` (`deviceId`),
  ADD KEY `personId` (`personId`);

--
-- Indexes for table `maintainance`
--
ALTER TABLE `maintainance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maintainance_track`
--
ALTER TABLE `maintainance_track`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `managemembers`
--
ALTER TABLE `managemembers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maps`
--
ALTER TABLE `maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orgId` (`orgId`);

--
-- Indexes for table `person_flats`
--
ALTER TABLE `person_flats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `flat_number` (`flat_number`);

--
-- Indexes for table `portal_user`
--
ALTER TABLE `portal_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personId` (`personId`),
  ADD KEY `orgId` (`orgId`);

--
-- Indexes for table `queries`
--
ALTER TABLE `queries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `vendor` ADD FULLTEXT KEY `dealsIn` (`dealsIn`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `basic_services`
--
ALTER TABLE `basic_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `buildinginfo`
--
ALTER TABLE `buildinginfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `facility_availability`
--
ALTER TABLE `facility_availability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=357;
--
-- AUTO_INCREMENT for table `facility_booking`
--
ALTER TABLE `facility_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `facility_unavailability`
--
ALTER TABLE `facility_unavailability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gcm`
--
ALTER TABLE `gcm`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=224;
--
-- AUTO_INCREMENT for table `maintainance`
--
ALTER TABLE `maintainance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `maintainance_track`
--
ALTER TABLE `maintainance_track`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `managemembers`
--
ALTER TABLE `managemembers`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `maps`
--
ALTER TABLE `maps`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2863;
--
-- AUTO_INCREMENT for table `person_flats`
--
ALTER TABLE `person_flats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `portal_user`
--
ALTER TABLE `portal_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `queries`
--
ALTER TABLE `queries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `deletePastEvents` ON SCHEDULE EVERY 1 MINUTE STARTS '2017-11-30 11:14:34' ON COMPLETION NOT PRESERVE DISABLE DO delete FROM `events` WHERE `end_time` < now()$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
